#include "serpent-cuda.cuh"

#ifndef __SERPENT_TABLES_H
#define __SERPENT_TABLES_H

#define PHI 0x9e3779b9UL
#define ROL(x,r) ((x) = ((x) << (r)) | ((x) >> (32-(r))))
#define ROR(x,r) ((x) = ((x) >> (r)) | ((x) << (32-(r))))

#define ROL_bitwise(x, r) data_sh[ty][(tx + r) & 0x1f].d = x; x = data_sh[ty][tx].d;
#define ROR_bitwise(x, r) data_sh[ty][(tx - r) & 0x1f].d = x; x = data_sh[ty][tx].d;
#define sl_bitwise(x1, x2, r) data_sh[ty][(tx + r) & 0x1f].d = (tx + r - 0x1f <= 0) ? x1 : 0; x2 = data_sh[ty][tx].d;

#define keyiter(a,b,c,d,i,j) \
        b ^= d; b ^= c; b ^= a; b ^= PHI ^ i; ROL(b,11); k[j] = b;

#define loadkeys(x0,x1,x2,x3,i) \
	x0=k[i]; x1=k[i+1]; x2=k[i+2]; x3=k[i+3];

#define storekeys(x0,x1,x2,x3,i) \
	k[i]=x0; k[i+1]=x1; k[i+2]=x2; k[i+3]=x3;

#define K(x0, x1, x2, x3, i)				\
	x3 = ((k_constant[4*(i)+3] >> tx) & 1) ? ~x3 : x3;        x2 = ((k_constant[4*(i)+2] >> tx) & 1) ? ~x2 : x2;	\
	x1 = ((k_constant[4*(i)+1] >> tx) & 1) ? ~x1 : x1;        x0 = ((k_constant[4*(i)+0] >> tx) & 1) ? ~x0 : x0;

#define LK(x0, x1, x2, x3, x4, i)				\
	ROL_bitwise(x0, 13);\
	ROL_bitwise(x2,  3);\
	x1 ^= x0;\
	sl_bitwise(x0, x4, 3);\
	x3 ^= x2;	x1 ^= x2;			\
	ROL_bitwise(x1, 1);\
	x3 ^= x4;			\
	ROL_bitwise(x3, 7);\
	x4  = x1;			\
	x0 ^= x1;\
	sl_bitwise(x4, x4, 7);\
	x2 ^= x3;	\
	x0 ^= x3;	x2 ^= x4;\
	ROL_bitwise(x0, 5);\
	ROL_bitwise(x2, 22);\
	x0 = ((k_constant[4 * i + 0] >> tx) & 1) ? ~x0 : x0;\
	x1 = ((k_constant[4 * i + 1] >> tx) & 1) ? ~x1 : x1; \
	x2 = ((k_constant[4 * i + 2] >> tx) & 1) ? ~x2 : x2;\
	x3 = ((k_constant[4 * i + 3] >> tx) & 1) ? ~x3 : x3;

#define KL(x0, x1, x2, x3, x4, i)				\
	x0 = ((k_constant[4*i+0] >> tx) & 1) ? ~x0 : x0;\
	x1 = ((k_constant[4*i+1] >> tx) & 1) ? ~x1 : x1;\
	x2 = ((k_constant[4*i+2] >> tx) & 1) ? ~x2 : x2;	\
	x3 = ((k_constant[4*i+3] >> tx) & 1) ? ~x3 : x3;\
	ROR_bitwise(x0, 5);\
	ROR_bitwise(x2, 22);\
	x4 =  x1;	x2 ^= x3;	x0 ^= x3;	\
	sl_bitwise(x4, x4, 7);\
	x0 ^= x1;\
	ROR_bitwise(x1, 1);\
	x2 ^= x4;\
	ROR_bitwise(x3, 7);\
	sl_bitwise(x0, x4, 3);\
	x1 ^= x0;	x3 ^= x4;\
	ROR_bitwise(x0, 13);\
	x1 ^= x2;	x3 ^= x2;\
	ROR_bitwise(x2, 3);

#define S0(x0,x1,x2,x3,x4)				\
					x4  = x3;	\
	x3 |= x0;	x0 ^= x4;	x4 ^= x2;	\
	x4 =~ x4;	x3 ^= x1;	x1 &= x0;	\
	x1 ^= x4;	x2 ^= x0;	x0 ^= x3;	\
	x4 |= x0;	x0 ^= x2;	x2 &= x1;	\
	x3 ^= x2;	x1 =~ x1;	x2 ^= x4;	\
	x1 ^= x2;

#define S1(x0,x1,x2,x3,x4)				\
					x4  = x1;	\
	x1 ^= x0;	x0 ^= x3;	x3 =~ x3;	\
	x4 &= x1;	x0 |= x1;	x3 ^= x2;	\
	x0 ^= x3;	x1 ^= x3;	x3 ^= x4;	\
	x1 |= x4;	x4 ^= x2;	x2 &= x0;	\
	x2 ^= x1;	x1 |= x0;	x0 =~ x0;	\
	x0 ^= x2;	x4 ^= x1;

#define S2(x0,x1,x2,x3,x4)				\
					x3 =~ x3;	\
	x1 ^= x0;	x4  = x0;	x0 &= x2;	\
	x0 ^= x3;	x3 |= x4;	x2 ^= x1;	\
	x3 ^= x1;	x1 &= x0;	x0 ^= x2;	\
	x2 &= x3;	x3 |= x1;	x0 =~ x0;	\
	x3 ^= x0;	x4 ^= x0;	x0 ^= x2;	\
	x1 |= x2;

#define S3(x0,x1,x2,x3,x4)				\
					x4  = x1;	\
	x1 ^= x3;	x3 |= x0;	x4 &= x0;	\
	x0 ^= x2;	x2 ^= x1;	x1 &= x3;	\
	x2 ^= x3;	x0 |= x4;	x4 ^= x3;	\
	x1 ^= x0;	x0 &= x3;	x3 &= x4;	\
	x3 ^= x2;	x4 |= x1;	x2 &= x1;	\
	x4 ^= x3;	x0 ^= x3;	x3 ^= x2;

#define S4(x0,x1,x2,x3,x4)				\
					x4  = x3;	\
	x3 &= x0;	x0 ^= x4;			\
	x3 ^= x2;	x2 |= x4;	x0 ^= x1;	\
	x4 ^= x3;	x2 |= x0;			\
	x2 ^= x1;	x1 &= x0;			\
	x1 ^= x4;	x4 &= x2;	x2 ^= x3;	\
	x4 ^= x0;	x3 |= x1;	x1 =~ x1;	\
	x3 ^= x0;

#define S5(x0,x1,x2,x3,x4)				\
	x4  = x1;	x1 |= x0;			\
	x2 ^= x1;	x3 =~ x3;	x4 ^= x0;	\
	x0 ^= x2;	x1 &= x4;	x4 |= x3;	\
	x4 ^= x0;	x0 &= x3;	x1 ^= x3;	\
	x3 ^= x2;	x0 ^= x1;	x2 &= x4;	\
	x1 ^= x2;	x2 &= x0;			\
	x3 ^= x2;

#define S6(x0,x1,x2,x3,x4)				\
					x4  = x1;	\
	x3 ^= x0;	x1 ^= x2;	x2 ^= x0;	\
	x0 &= x3;	x1 |= x3;	x4 =~ x4;	\
	x0 ^= x1;	x1 ^= x2;			\
	x3 ^= x4;	x4 ^= x0;	x2 &= x0;	\
	x4 ^= x1;	x2 ^= x3;	x3 &= x1;	\
	x3 ^= x0;	x1 ^= x2;

#define S7(x0,x1,x2,x3,x4)				\
					x1 =~ x1;	\
	x4  = x1;	x0 =~ x0;	x1 &= x2;	\
	x1 ^= x3;	x3 |= x4;	x4 ^= x2;	\
	x2 ^= x3;	x3 ^= x0;	x0 |= x1;	\
	x2 &= x0;	x0 ^= x4;	x4 ^= x3;	\
	x3 &= x0;	x4 ^= x1;			\
	x2 ^= x4;	x3 ^= x1;	x4 |= x0;	\
	x4 ^= x1;

#define SI0(x0,x1,x2,x3,x4)				\
			x4  = x3;	x1 ^= x0;	\
	x3 |= x1;	x4 ^= x1;	x0 =~ x0;	\
	x2 ^= x3;	x3 ^= x0;	x0 &= x1;	\
	x0 ^= x2;	x2 &= x3;	x3 ^= x4;	\
	x2 ^= x3;	x1 ^= x3;	x3 &= x0;	\
	x1 ^= x0;	x0 ^= x2;	x4 ^= x3;

#define SI1(x0,x1,x2,x3,x4)				\
	x1 ^= x3;	x4  = x0;			\
	x0 ^= x2;	x2 =~ x2;	x4 |= x1;	\
	x4 ^= x3;	x3 &= x1;	x1 ^= x2;	\
	x2 &= x4;	x4 ^= x1;	x1 |= x3;	\
	x3 ^= x0;	x2 ^= x0;	x0 |= x4;	\
	x2 ^= x4;	x1 ^= x0;			\
	x4 ^= x1;

#define SI2(x0,x1,x2,x3,x4)				\
	x2 ^= x1;	x4  = x3;	x3 =~ x3;	\
	x3 |= x2;	x2 ^= x4;	x4 ^= x0;	\
	x3 ^= x1;	x1 |= x2;	x2 ^= x0;	\
	x1 ^= x4;	x4 |= x3;	x2 ^= x3;	\
	x4 ^= x2;	x2 &= x1;			\
	x2 ^= x3;	x3 ^= x4;	x4 ^= x0;

#define SI3(x0,x1,x2,x3,x4)				\
					x2 ^= x1;	\
	x4  = x1;	x1 &= x2;			\
	x1 ^= x0;	x0 |= x4;	x4 ^= x3;	\
	x0 ^= x3;	x3 |= x1;	x1 ^= x2;	\
	x1 ^= x3;	x0 ^= x2;	x2 ^= x3;	\
	x3 &= x1;	x1 ^= x0;	x0 &= x2;	\
	x4 ^= x3;	x3 ^= x0;	x0 ^= x1;

#define SI4(x0,x1,x2,x3,x4)				\
	x2 ^= x3;	x4  = x0;	x0 &= x1;	\
	x0 ^= x2;	x2 |= x3;	x4 =~ x4;	\
	x1 ^= x0;	x0 ^= x2;	x2 &= x4;	\
	x2 ^= x0;	x0 |= x4;			\
	x0 ^= x3;	x3 &= x2;			\
	x4 ^= x3;	x3 ^= x1;	x1 &= x0;	\
	x4 ^= x1;	x0 ^= x3;

#define SI5(x0,x1,x2,x3,x4)				\
			x4  = x1;	x1 |= x2;	\
	x2 ^= x4;	x1 ^= x3;	x3 &= x4;	\
	x2 ^= x3;	x3 |= x0;	x0 =~ x0;	\
	x3 ^= x2;	x2 |= x0;	x4 ^= x1;	\
	x2 ^= x4;	x4 &= x0;	x0 ^= x1;	\
	x1 ^= x3;	x0 &= x2;	x2 ^= x3;	\
	x0 ^= x2;	x2 ^= x4;	x4 ^= x3;

#define SI6(x0,x1,x2,x3,x4)				\
			x0 ^= x2;			\
	x4  = x0;	x0 &= x3;	x2 ^= x3;	\
	x0 ^= x2;	x3 ^= x1;	x2 |= x4;	\
	x2 ^= x3;	x3 &= x0;	x0 =~ x0;	\
	x3 ^= x1;	x1 &= x2;	x4 ^= x0;	\
	x3 ^= x4;	x4 ^= x2;	x0 ^= x1;	\
	x2 ^= x0;

#define SI7(x0,x1,x2,x3,x4)				\
	x4  = x3;	x3 &= x0;	x0 ^= x2;	\
	x2 |= x4;	x4 ^= x1;	x0 =~ x0;	\
	x1 |= x3;	x4 ^= x0;	x0 &= x2;	\
	x0 ^= x1;	x1 &= x2;	x3 ^= x2;	\
	x4 ^= x3;	x2 &= x3;	x3 |= x0;	\
	x1 ^= x4;	x3 ^= x4;	x4 &= x0;	\
	x4 ^= x2;


#endif 