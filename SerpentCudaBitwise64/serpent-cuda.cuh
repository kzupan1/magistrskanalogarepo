#ifndef __SERPENT_CUDA_H
#define __SERPENT_CUDA_H
#include "common-types.h"

typedef unsigned long long u64;

typedef struct{
	u64 d;
	u32 p;
} u64_p;


#define WORDS_PER_DWORD 2
#define BYTES_PER_DWORD (BYTES_PER_WORD * WORDS_PER_DWORD)
#define BITS_PER_DWORD (BYTES_PER_DWORD * BITS_PER_BYTE)

#define ROUNDS 32 /* # of rounds */
#define WORDS_PER_KEY 8 

/* derived lengths */
#define BYTES_PER_KEY (WORDS_PER_KEY*BYTES_PER_WORD)
#define BITS_PER_KEY (BITS_PER_WORD*WORDS_PER_KEY)
#define WORDS_PER_KEY_SCHEDULE ((ROUNDS+1)*WORDS_PER_BLOCK)

#define GRID_DIM_Y 8

#define CRYPTO_THREAD_BLOCK_SIZE_X 32
#define CRYPTO_THREAD_BLOCK_SIZE_Y 8

int serpentEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);
int serpentDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);



#endif /* __SERPENT_CUDA_H */