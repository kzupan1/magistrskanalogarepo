#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "mars-cuda.cuh"
#include "mars-tables.cuh"
#include <stdio.h>
#include <stdlib.h>
#include "measure-time.h"
#include <boost/chrono.hpp>

#define f_mix(a,b,c,d)                  \
		r = ROR(a, 8);                 \
		b ^= S[a & 0xff];            \
		b += S[(r & 0xff) + 256];    \
		r = ROR(a, 16);                \
		a = ROR(a, 24);               \
		c += S[r & 0xff];            \
		d ^= S[(a & 0xff) + 256]

#define b_mix(a,b,c,d)                  \
        r = ROL(a, 8);                 \
        b ^= s_sh[(a & 0xff) + 256];    \
        c -= s_sh[r & 0xff];            \
        r = ROL(a, 16);                \
        a  = ROL(a, 24);               \
        d -= s_sh[(r & 0xff) + 256];    \
        d ^= s_sh[a & 0xff]

#define f_ktr(a,b,c,d,i)    \
		m = a + rk_constant[i];       \
		a = ROL(a, 13);        \
		r = a * rk_constant[i + 1];   \
		l = s_sh[m & 0x1ff];     \
		r = ROL(r, 5);         \
		c += ROL(m, r);        \
		l ^= r;                 \
		r = ROL(r, 5);         \
		l ^= r;                 \
		d ^= r;                 \
		b += ROL(l, r)

#define r_ktr(a,b,c,d,i)    \
		r = a * rk_constant[i + 1];   \
		a = ROR(a, 13);        \
		m = a + rk_constant[i];       \
		l = s_sh[m & 0x1ff];     \
		r = ROL(r, 5);         \
		l ^= r;                 \
		c -= ROL(m, r);        \
		r = ROL(r, 5);         \
		l ^= r;                 \
		d ^= r;                 \
		b -= ROL(l, r)

int makeKey(const u32 cipherKey[], u32 rk[RK_SIZE], const unsigned int keyLen ){

	u32  i, j, m, t1, t2, *kp;
	u32 t_key[TMP_TABLE_SIZE];


	m = keyLen / BYTES_PER_WORD;

	for(i = 0; i < m; ++i){
	
		t_key[i] = cipherKey[i];
	
	}

	t_key[i++] = m;
	
	for(; i < TMP_TABLE_SIZE; i++){

		t_key[i] = 0;
	
	}

	kp = rk;

	#define tk1(j)  t1 = t_key[j] ^= ROL(t1 ^ t_key[(j + 8) % 15], 3) ^ (i + 4 * j)
	#define tk2(j)  t2 = t_key[j] ^= ROL(t2 ^ t_key[(j + 8) % 15], 3) ^ (i + 4 * j)
	#define tk3(j)  t_key[j] = t1 =  ROL(t_key[j] + S[t1 & 0x1ff], 9)

	for(i = 0; i < 4; ++i)
    {
        t1 = t_key[13]; t2 = t_key[14];

        tk1( 0); tk2( 1); tk1( 2); tk2( 3); tk1( 4); tk2( 5); tk1( 6); tk2( 7);
        tk1( 8); tk2( 9); tk1(10); tk2(11); tk1(12); tk2(13); tk1(14);

        tk3( 0); tk3( 1); tk3( 2); tk3( 3); tk3( 4); tk3( 5); tk3( 6); tk3( 7);
        tk3( 8); tk3( 9); tk3(10); tk3(11); tk3(12); tk3(13); tk3(14);

        tk3( 0); tk3( 1); tk3( 2); tk3( 3); tk3( 4); tk3( 5); tk3( 6); tk3( 7);
        tk3( 8); tk3( 9); tk3(10); tk3(11); tk3(12); tk3(13); tk3(14);

        tk3( 0); tk3( 1); tk3( 2); tk3( 3); tk3( 4); tk3( 5); tk3( 6); tk3( 7);
        tk3( 8); tk3( 9); tk3(10); tk3(11); tk3(12); tk3(13); tk3(14);

        tk3( 0); tk3( 1); tk3( 2); tk3( 3); tk3( 4); tk3( 5); tk3( 6); tk3( 7);
        tk3( 8); tk3( 9); tk3(10); tk3(11); tk3(12); tk3(13); tk3(14);

        *kp++ = t_key[ 0]; *kp++ = t_key[ 4]; *kp++ = t_key[ 8]; *kp++ = t_key[12];
        *kp++ = t_key[ 1]; *kp++ = t_key[ 5]; *kp++ = t_key[ 9]; *kp++ = t_key[13];
        *kp++ = t_key[ 2]; *kp++ = t_key[ 6];
    }

	for(i = 5; i < 37; i += 2)
    {
		j = rk[i] | 3;

		m = (~j ^ (j << 1)) & (~j ^ (j >> 1)) & 0x7ffffffe;

		m &= m >> 1; m &= m >> 2; m &= m >> 4;
		m |= m << 1; m |= m << 2; m |= m << 4;


		m &= 0x7ffffffc;

		j ^= (ROL(B[rk[i] & 3], rk[i - 1]) & m);


		rk[i] = j;
    }

	return 0;
}


__constant__ u32 rk_constant[RK_SIZE];

__global__ void encryptKernel(uint4 *pt, uint4 *ct, const u32 * S,   const unsigned int blocks)
{
	

	int t = threadIdx.x;
	int i = blockIdx.y * gridDim.x * blockDim.x + blockIdx.x * blockDim.x + t;
	

	__shared__ u32 s_sh[BIG_S_TABLE_SIZE];
	

	unsigned int j;

	for (int m = 0; m < (BIG_S_TABLE_SIZE - 1) / blockDim.x + 1; m++){
		j = m * blockDim.x + t;
		if (j < BIG_S_TABLE_SIZE)
			s_sh[j] = S[j];

	}

	__syncthreads();


	
	if(i < blocks){

		register u32  a, b, c, d, l, m, r;

		a = pt[i].x + rk_constant[0];
		b = pt[i].y + rk_constant[1];
		c = pt[i].z + rk_constant[2];
		d = pt[i].w + rk_constant[3];

		f_mix(a,b,c,d); a += d;
		f_mix(b,c,d,a); b += c;
		f_mix(c,d,a,b);
		f_mix(d,a,b,c);
		f_mix(a,b,c,d); a += d;
		f_mix(b,c,d,a); b += c;
		f_mix(c,d,a,b);
		f_mix(d,a,b,c);

		f_ktr(a,b,c,d, 4); f_ktr(b,c,d,a, 6); f_ktr(c,d,a,b, 8); f_ktr(d,a,b,c,10);
		f_ktr(a,b,c,d,12); f_ktr(b,c,d,a,14); f_ktr(c,d,a,b,16); f_ktr(d,a,b,c,18);
		f_ktr(a,d,c,b,20); f_ktr(b,a,d,c,22); f_ktr(c,b,a,d,24); f_ktr(d,c,b,a,26);
		f_ktr(a,d,c,b,28); f_ktr(b,a,d,c,30); f_ktr(c,b,a,d,32); f_ktr(d,c,b,a,34);

		b_mix(a,b,c,d);
		b_mix(b,c,d,a); c -= b;
		b_mix(c,d,a,b); d -= a;
		b_mix(d,a,b,c);
		b_mix(a,b,c,d);
		b_mix(b,c,d,a); c -= b;
		b_mix(c,d,a,b); d -= a;
		b_mix(d,a,b,c);


		ct[i] = { a - rk_constant[36], b - rk_constant[37], c - rk_constant[38], d - rk_constant[39] };
	}

}

__global__ void decryptKernel(uint4 *ct, uint4 *pt,  const u32 *  S,   const unsigned int blocks)
{
	

	int t = threadIdx.x;
	int i = blockIdx.y * gridDim.x * blockDim.x + blockIdx.x * blockDim.x + t;
	

	__shared__ u32 s_sh[BIG_S_TABLE_SIZE];
	
	unsigned int j;

	for (int m = 0; m < (BIG_S_TABLE_SIZE - 1) / blockDim.x + 1; m++){
		j = m * blockDim.x + t;
		if (j < BIG_S_TABLE_SIZE)
			s_sh[j] = S[j];

	}


	__syncthreads();


	if(i < blocks){


		register u32 a, b, c, d, l, m, r;

		d = ct[i].x + rk_constant[36];
		c = ct[i].y + rk_constant[37];
		b = ct[i].z + rk_constant[38];
		a = ct[i].w + rk_constant[39];
	
		f_mix(a,b,c,d); a += d;
		f_mix(b,c,d,a); b += c;
		f_mix(c,d,a,b);
		f_mix(d,a,b,c);
		f_mix(a,b,c,d); a += d;
		f_mix(b,c,d,a); b += c;
		f_mix(c,d,a,b);
		f_mix(d,a,b,c);

		r_ktr(a,b,c,d,34); r_ktr(b,c,d,a,32); r_ktr(c,d,a,b,30); r_ktr(d,a,b,c,28);
		r_ktr(a,b,c,d,26); r_ktr(b,c,d,a,24); r_ktr(c,d,a,b,22); r_ktr(d,a,b,c,20);
		r_ktr(a,d,c,b,18); r_ktr(b,a,d,c,16); r_ktr(c,b,a,d,14); r_ktr(d,c,b,a,12);
		r_ktr(a,d,c,b,10); r_ktr(b,a,d,c, 8); r_ktr(c,b,a,d, 6); r_ktr(d,c,b,a, 4);

		b_mix(a,b,c,d);
		b_mix(b,c,d,a); c -= b;
		b_mix(c,d,a,b); d -= a;
		b_mix(d,a,b,c);
		b_mix(a,b,c,d);
		b_mix(b,c,d,a); c -= b;
		b_mix(c,d,a,b); d -= a;
		b_mix(d,a,b,c);

		pt[i] = { d - rk_constant[0], c - rk_constant[1], b - rk_constant[2], a - rk_constant[3] };

	}
	
}

#define MAX_GRID_SIZE 65535

cudaError_t marsEncryptCuda(const u8 pt[], u8 ct[], const u32 rk[], const unsigned int plainTextLength, const unsigned int threadBlockSize)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

    uint4 *devPt;
    uint4 *devCt;

	u32 *devS;
	

    cudaError_t cudaStatus;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        return cudaStatus;
    }

    // cuda malloc
	start = startStopwatch();
	cudaHostGetDevicePointer((void **)&devPt, (void *)pt, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	cudaHostGetDevicePointer((void **)&devCt, (void *)ct, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	
	cudaStatus = cudaMalloc((void**)&devS, BIG_S_TABLE_SIZE * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        return cudaStatus;
    }

	
	endStopwatch("Malloc & GetDevicePointer (encryption)", start);
    
	start = startStopwatch();
	
	cudaStatus = cudaMemcpyToSymbol(rk_constant, rk, RK_SIZE * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }

	cudaStatus = cudaMemcpy(devS, S, BIG_S_TABLE_SIZE * sizeof(u32), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }

	
	totalTime = endStopwatch("Memory copy from host to device (encryption)", start);
    
	unsigned int threadBlocks = (blocks - 1) / threadBlockSize + 1;
	unsigned int gridDimY = (threadBlocks - 1) / MAX_GRID_SIZE + 1;
	unsigned int gridDimX = (threadBlocks - 1) / gridDimY + 1;

	dim3 dimGrid(gridDimX, gridDimY, 1);
	dim3 dimBlock(threadBlockSize, 1, 1);
	

    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	encryptKernel<<<dimGrid, dimBlock>>>(devPt, devCt, devS, blocks);
	cudaStatus = cudaGetLastError();
	// Check for any errors launching the kernel
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}
	
	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Encryption kernel", start);
   
	start = startStopwatch();
	cudaFree(devS);
	
	endStopwatch("Freeing memory (encryption)", start);

	printDuration("Encryption", totalTime);

    return cudaStatus;
}


cudaError_t marsDecryptCuda(const u8 ct[], u8 pt[], const u32 rk[], const unsigned int plainTextLength, const unsigned int threadBlockSize)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	uint4 *devCt;
    uint4 *devPt;

	u32 *devS;

    cudaError_t cudaStatus;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        return cudaStatus;
    }

    // cuda malloc
	start = startStopwatch();
	cudaHostGetDevicePointer((void **)&devCt, (void *)ct, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	cudaHostGetDevicePointer((void **)&devPt, (void *)pt, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	
	cudaStatus = cudaMalloc((void**)&devS, BIG_S_TABLE_SIZE * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        return cudaStatus;
    }

	
	endStopwatch("Malloc & GetDevicePointer (decryption)", start);
    
	start = startStopwatch();

	cudaStatus = cudaMemcpyToSymbol(rk_constant, rk, RK_SIZE * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }

	cudaStatus = cudaMemcpy(devS, S, BIG_S_TABLE_SIZE * sizeof(u32), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }

	
	totalTime = endStopwatch("Memory copy from host to device (decryption)", start);
    
	unsigned int threadBlocks = (blocks - 1) / threadBlockSize + 1;
	unsigned int gridDimY = (threadBlocks - 1) / MAX_GRID_SIZE + 1;
	unsigned int gridDimX = (threadBlocks - 1) / gridDimY + 1;

	dim3 dimGrid(gridDimX, gridDimY, 1);
	dim3 dimBlock(threadBlockSize, 1, 1);
	
    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	decryptKernel<<<dimGrid, dimBlock>>>(devCt, devPt, devS, blocks);
	
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}
	
	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Decryption kernel", start);
    
    
	start = startStopwatch();

	cudaFree(devS);

	endStopwatch("Freeing memory (decryption)", start);

	printDuration("Decryption", totalTime);

    return cudaStatus;
}


int marsEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength, const unsigned int threadBlockSize){

	boost::chrono::high_resolution_clock::time_point start;

	cudaSetDeviceFlags(cudaDeviceMapHost);
	cudaFree(NULL);
	u32 rk[RK_SIZE];;
	
	u8 *pinnedPt;
	u8 *pinnedCt;

	cudaHostAlloc((void **)&pinnedPt, plainTextLength, cudaHostAllocWriteCombined | cudaHostAllocMapped);
	cudaHostAlloc((void **)&pinnedCt, plainTextLength, cudaHostAllocDefault | cudaHostAllocMapped);

	memcpy(pinnedPt, pt, plainTextLength);

	start = startStopwatch();
	makeKey((u32*)cipherKey, rk, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);
	
	cudaError_t cudaStatus = marsEncryptCuda(pinnedPt, pinnedCt, rk, plainTextLength, threadBlockSize);
	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Encryption failed!");
        return 1;
    }

	memcpy(ct, pinnedCt, plainTextLength);
	cudaFreeHost(pinnedPt);
	cudaFreeHost(pinnedCt);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }

    return 0;




}
int marsDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength, const unsigned int threadBlockSize){
	
	boost::chrono::high_resolution_clock::time_point start;

	cudaSetDeviceFlags(cudaDeviceMapHost);
	cudaFree(NULL);
	u32 rk[RK_SIZE];;
	
	u8 *pinnedCt;
	u8 *pinnedPt;

	cudaHostAlloc((void **)&pinnedCt, plainTextLength, cudaHostAllocWriteCombined | cudaHostAllocMapped);
	cudaHostAlloc((void **)&pinnedPt, plainTextLength, cudaHostAllocDefault | cudaHostAllocMapped);

	memcpy(pinnedCt, ct, plainTextLength);

	start = startStopwatch();
	makeKey((u32*)cipherKey, rk, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);
	

	cudaError_t cudaStatus = marsDecryptCuda(pinnedCt, pinnedPt, rk, plainTextLength, threadBlockSize);

	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Encryption failed!");
        return 1;
    }

	memcpy(pt, pinnedPt, plainTextLength);
	cudaFreeHost(pinnedCt);
	cudaFreeHost(pinnedPt);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }

    return 0;

}


