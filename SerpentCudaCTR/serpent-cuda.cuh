#ifndef __SERPENT_CUDA_H
#define __SERPENT_CUDA_H
#include "common-types.h"
#include "cuda_runtime.h"


#define ROUNDS 32 /* # of rounds */
#define WORDS_PER_KEY 8 

/* derived lengths */
#define BYTES_PER_KEY (WORDS_PER_KEY*BYTES_PER_WORD)
#define BITS_PER_KEY (BITS_PER_WORD*WORDS_PER_KEY)
#define WORDS_PER_KEY_SCHEDULE ((ROUNDS+1)*WORDS_PER_BLOCK)

int serpentEncrypt(uint4 *pt, uint4 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);
int serpentDecrypt(uint4 *ct, uint4 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);



#endif /* __SERPENT_CUDA_H */