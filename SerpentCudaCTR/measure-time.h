#ifndef __MEASURE_TIME_H
#define __MEASURE_TIME_H
#include <boost/chrono.hpp>

using namespace boost::chrono;

high_resolution_clock::time_point startStopwatch();

duration<double, boost::milli> endStopwatch(const char *comment, high_resolution_clock::time_point start);

void printDuration(const char *comment, duration<double, boost::milli> time_span);

#endif /* __MEASURE_TIME_H */