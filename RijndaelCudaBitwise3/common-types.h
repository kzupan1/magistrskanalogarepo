#ifndef __COMMON_TYPES_H
#define __COMMON_TYPES_H


typedef unsigned char u8;
typedef unsigned int u32;

#define BLOCK_SIZE 128

#define BITS_PER_BYTE 8
#define BYTES_PER_WORD 4
#define BITS_PER_WORD (BYTES_PER_WORD * BITS_PER_BYTE)


#define BYTES_PER_BLOCK (BLOCK_SIZE / BITS_PER_BYTE)
#define WORDS_PER_BLOCK (BYTES_PER_BLOCK/BYTES_PER_WORD)


#endif /* __COMMON_TYPES_H */