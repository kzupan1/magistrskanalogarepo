#include "twofish-opencl.h"

/*	Macros for extracting bytes from dwords (correct for endianness) */
#define	_b(x,N)	((x >> (N & 3)) & 0xff) /* pick bytes out of a dword */

#define		b0(x)			_b(x,0)		/* extract LSB of DWORD */
#define		b1(x)			_b(x,1)
#define		b2(x)			_b(x,2)
#define		b3(x)			_b(x,3)		/* extract MSB of DWORD */

#define	Fe32(sBox, x,R) (sBox[        2*_b(x, R  )    ] ^ \
	sBox[2 * _b(x, R + 1) + 1] ^ \
	sBox[0x200 + 2 * _b(x, R + 2)] ^ \
	sBox[0x200 + 2 * _b(x, R + 3) + 1])

#define decrypt2Rounds()	t0 = Fe32(sBox_sh, x2, 0);\
	t1 = Fe32(sBox_sh, x3, 3); \
	x1 ^= t0 + 2 * t1 + sKey[k--]; \
	x1 = ROR(x1, 1); \
	x0 = ROL(x0, 1); \
	x0 ^= t0 + t1 + sKey[k--]; \
	t0 = Fe32(sBox_sh, x0, 0); \
	t1 = Fe32(sBox_sh, x1, 3); \
	x3 ^= t0 + 2 * t1 + sKey[k--]; \
	x3 = ROR(x3, 1); \
	x2 = ROL(x2, 1); \
	x2 ^= t0 + t1 + sKey[k--];

#define applyDecryptRounds() decrypt2Rounds();\
	decrypt2Rounds(); \
	decrypt2Rounds(); \
	decrypt2Rounds(); \
	decrypt2Rounds(); \
	decrypt2Rounds(); \
	decrypt2Rounds(); \
	decrypt2Rounds();


__kernel void decryptKernel(__global uint4 *ct, __global uint4 *pt, __constant u32 *sKey, __global u32 *sBox, const unsigned int blocks)
{																																		
																																																																
	int t = get_local_id(0);
	int i = get_global_id(0);

	__local u32 sBox_sh[S_BOX_SIZE];



	if (t * 4 < S_BOX_SIZE){

		sBox_sh[t + 0 * 256] = sBox[t + 0 * 256];
		sBox_sh[t + 1 * 256] = sBox[t + 1 * 256];
		sBox_sh[t + 2 * 256] = sBox[t + 2 * 256];
		sBox_sh[t + 3 * 256] = sBox[t + 3 * 256];
	}

	barrier(CLK_LOCAL_MEM_FENCE);


	if (i < blocks){


		register u32 x0, x1, x2, x3, t0, t1;
		register int k;

		uint4 block_in = ct[i];
		uint4 block_out;

		x2 = block_in.x;
		x3 = block_in.y;
		x0 = block_in.z;
		x1 = block_in.w;

		x2 ^= sKey[OUTPUT_WHITEN];
		x3 ^= sKey[OUTPUT_WHITEN + 1];
		x0 ^= sKey[OUTPUT_WHITEN + 2];
		x1 ^= sKey[OUTPUT_WHITEN + 3];

		k = ROUND_SUBKEYS + 2 * ROUNDS - 1;

		applyDecryptRounds();

		x0 ^= sKey[INPUT_WHITEN];
		x1 ^= sKey[INPUT_WHITEN + 1];
		x2 ^= sKey[INPUT_WHITEN + 2];
		x3 ^= sKey[INPUT_WHITEN + 3];

		block_out.x = x0;
		block_out.y = x1;
		block_out.z = x2;
		block_out.w = x3;

		pt[i] = block_out;

	}
																																																									
																																		
																																		
																																		
}																																		
