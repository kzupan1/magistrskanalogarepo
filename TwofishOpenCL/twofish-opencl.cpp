#include "twofish-opencl.h"
#include "twofish-tables.h"
#include "measure-time.h"
#include "opencl-helpers.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <CL/cl.h>
#include <boost/chrono.hpp>

#define BUILD_SOURCE 0

const char *options = "-I ./twofish-opencl.h";

const char *encryptKernelSource = "twofishEncryptKernel.cl";
const char *decryptKernelSource = "twofishDecryptKernel.cl";

const char *encryptKernelBin = "twofishEncryptKernel.clbin";
const char *decryptKernelBin = "twofishDecryptKernel.clbin";

cl_int twofishEncryptOpenCl(const u8 pt[], u8 ct[], const u32 subKeys[TOTAL_SUBKEYS], const u32 sBox[S_BOX_SIZE], const unsigned int plainTextLength, InitValues *initValues, cl_kernel krnl)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;


	cl_mem devSubKeys;
	cl_mem devSBox;

	cl_mem pinnedPt;
	cl_mem pinnedCt;

	u8* hostPinnedPt;
	u8* hostPinnedCt;

    cl_int clStatus;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	// Create a command queue
	cl_command_queue command_queue = clCreateCommandQueue(initValues->context, initValues->device_list[0], 0, &clStatus);
	start = startStopwatch();
	pinnedPt = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, plainTextLength *sizeof(u8), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedPt failed!");
		return clStatus;
	}

	pinnedCt = clCreateBuffer(initValues->context, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, plainTextLength *sizeof(u8), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedCt failed!");
		return clStatus;
	}

	hostPinnedPt = (u8 *)clEnqueueMapBuffer(command_queue, pinnedPt, CL_TRUE, CL_MAP_WRITE, 0, plainTextLength * sizeof(u8), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}

	memcpy(hostPinnedPt, pt, plainTextLength * sizeof(u8));


	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedPt, hostPinnedPt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);
    
	
	
	devSubKeys = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY, TOTAL_SUBKEYS * sizeof(u32), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clCreateBuffer failed!");
        return clStatus;
    }

	devSBox = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY, S_BOX_SIZE * sizeof(u32), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer failed!");
		return clStatus;
	}
	
	
	endStopwatch("clCreateBuffer (encryption) ", start);

	start = startStopwatch();

	clStatus = clEnqueueWriteBuffer(command_queue, devSubKeys, CL_TRUE, 0, TOTAL_SUBKEYS * sizeof(u32), subKeys, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clEnqueueWriteBuffer failed!");
        return clStatus;
    }
	
	clStatus = clEnqueueWriteBuffer(command_queue, devSBox, CL_TRUE, 0, S_BOX_SIZE * sizeof(u32), sBox, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueWriteBuffer failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);

	totalTime = endStopwatch("clEnqueueWriteBuffer (encryption)", start);


	// Set the arguments of the kernel
	clStatus = clSetKernelArg(krnl, 0, sizeof(cl_mem), (void *)&pinnedPt);
	clStatus = clSetKernelArg(krnl, 1, sizeof(cl_mem), (void *)&pinnedCt);
	clStatus = clSetKernelArg(krnl, 2, sizeof(cl_mem), (void *)&devSubKeys);
	clStatus = clSetKernelArg(krnl, 3, sizeof(cl_mem), (void *)&devSBox);
	clStatus = clSetKernelArg(krnl, 4, sizeof(unsigned int), (void *)&blocks);



	//!!!must be padded before!!!
	size_t global_size = ((plainTextLength/BYTES_PER_BLOCK - 1)/THREAD_BLOCK_SIZE + 1) * THREAD_BLOCK_SIZE ;
	size_t local_size = THREAD_BLOCK_SIZE;

    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	clStatus = clEnqueueNDRangeKernel(command_queue, krnl, 1, NULL, &global_size, &local_size, 0, NULL, NULL);
	
	// Check for any errors launching the kernel
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "encryption kernel launch failed!");
		printf("%d\n", clStatus);
		return clStatus;
	}

	clFlush(command_queue);
	clFinish(command_queue);
	totalTime += endStopwatch("Encryption kernel", start);


	hostPinnedCt = (u8 *)clEnqueueMapBuffer(command_queue, pinnedCt, CL_TRUE, CL_MAP_READ, 0, plainTextLength * sizeof(u8), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}
	memcpy(ct, hostPinnedCt, plainTextLength * sizeof(u8));
	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedCt, hostPinnedCt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);

		
	// Finally release all OpenCL allocated objePts and host buffers.
	start = startStopwatch();

	clStatus = clReleaseMemObject(devSubKeys);
	clStatus = clReleaseMemObject(devSBox);

	clStatus = clReleaseCommandQueue(command_queue);

	endStopwatch("clReleaseMemObject (encryption)", start);
	
	clStatus = clReleaseMemObject(pinnedPt);
	clStatus = clReleaseMemObject(pinnedCt);

	printDuration("Encryption", totalTime);
	
	return clStatus;
}

cl_int twofishDecryptOpenCl(const u8 ct[], u8 pt[], const u32 subKeys[TOTAL_SUBKEYS], const u32 sBox[S_BOX_SIZE], const unsigned int plainTextLength, InitValues *initValues, cl_kernel krnl)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	cl_mem devSubKeys;

	cl_mem devSBox;

	cl_mem pinnedCt;
	cl_mem pinnedPt;

	u8* hostPinnedPt;
	u8* hostPinnedCt;

    cl_int clStatus;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;
   
	// Create a command queue
	cl_command_queue command_queue = clCreateCommandQueue(initValues->context, initValues->device_list[0], 0, &clStatus);

	start = startStopwatch();
	pinnedCt = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, plainTextLength *sizeof(u8), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedPt failed!");
		return clStatus;
	}

	pinnedPt = clCreateBuffer(initValues->context, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, plainTextLength *sizeof(u8), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedCt failed!");
		return clStatus;
	}

	hostPinnedCt = (u8 *)clEnqueueMapBuffer(command_queue, pinnedCt, CL_TRUE, CL_MAP_WRITE, 0, plainTextLength * sizeof(u8), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}

	memcpy(hostPinnedCt, ct, plainTextLength * sizeof(u8));


	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedCt, hostPinnedCt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);
    
	

	devSubKeys = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY, TOTAL_SUBKEYS * sizeof(u32), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clCreateBuffer failed!");
        return clStatus;
    }

	devSBox = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY, S_BOX_SIZE * sizeof(u32), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer failed!");
		return clStatus;
	}
	
	endStopwatch("clCreateBuffer (decryption) ", start);

	start = startStopwatch();

	clStatus = clEnqueueWriteBuffer(command_queue, devSubKeys, CL_TRUE, 0, TOTAL_SUBKEYS * sizeof(u32), subKeys, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clEnqueueWriteBuffer failed!");
        return clStatus;
    }

	clStatus = clEnqueueWriteBuffer(command_queue, devSBox, CL_TRUE, 0, S_BOX_SIZE * sizeof(u32), sBox, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueWriteBuffer failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);

	totalTime = endStopwatch("clEnqueueWriteBuffer (decryption) ", start);


	// Set the arguments of the kernel
	clStatus = clSetKernelArg(krnl, 0, sizeof(cl_mem), (void *)&pinnedCt);
	clStatus = clSetKernelArg(krnl, 1, sizeof(cl_mem), (void *)&pinnedPt);
	clStatus = clSetKernelArg(krnl, 2, sizeof(cl_mem), (void *)&devSubKeys);
	clStatus = clSetKernelArg(krnl, 3, sizeof(cl_mem), (void *)&devSBox);
	clStatus = clSetKernelArg(krnl, 4, sizeof(unsigned int), (void *)&blocks);



	//!!!must be padded before!!!
	size_t global_size = ((plainTextLength/BYTES_PER_BLOCK - 1)/THREAD_BLOCK_SIZE + 1) * THREAD_BLOCK_SIZE ;
	size_t local_size = THREAD_BLOCK_SIZE; ;

	
    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	clStatus = clEnqueueNDRangeKernel(command_queue, krnl, 1, NULL, &global_size, &local_size, 0, NULL, NULL);
	
	// Check for any errors launching the kernel
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Decryption kernel launch failed!");
		return clStatus;
	}

	clFlush(command_queue);
	clFinish(command_queue);

	totalTime += endStopwatch("Decryption kernel", start);

    
	hostPinnedPt = (u8 *)clEnqueueMapBuffer(command_queue, pinnedPt, CL_TRUE, CL_MAP_READ, 0, plainTextLength * sizeof(u8), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}
	memcpy(pt, hostPinnedPt, plainTextLength * sizeof(u8));
	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedPt, hostPinnedPt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);
    
 
	// Finally release all OpenCL allocated objects and host buffers.
	start = startStopwatch();
	clStatus = clReleaseMemObject(devSubKeys);
	clStatus = clReleaseMemObject(devSBox);

	clStatus = clReleaseCommandQueue(command_queue);

	endStopwatch("clReleaseMemObject (decryption)", start);
	
	clStatus = clReleaseMemObject(pinnedCt);
	clStatus = clReleaseMemObject(pinnedPt);

	printDuration("Decryption", totalTime);

	return clStatus;
}

u32 RS_MDS_Encode(u32 k0, u32 k1)
{
	int i, j;
	u32 r;

	for (i = r = 0; i<2; i++)
	{
		r ^= (i) ? k0 : k1;			/* merge in 32 more key bits */
		for (j = 0; j<4; j++)			/* shift one u8 at a time */
			RS_rem(r);


	}
	return r;
}

int makeKey(const u32 cipherKey[], u32 subKeys[TOTAL_SUBKEYS], u32 sBox[S_BOX_SIZE], const unsigned int keyLen){

	/*	Macros for extracting bytes from dwords (correct for endianness) */
	#define	_b(x,N)	(((u8 *)&x)[((N) & 3)]) /* pick bytes out of a dword */

	#define		b0(x)			_b(x,0)		/* extract LSB of DWORD */
	#define		b1(x)			_b(x,1)
	#define		b2(x)			_b(x,2)
	#define		b3(x)			_b(x,3)		/* extract MSB of DWORD */


	#define	F32(res,x,k32)	\
		{ \
		u32 t = x;													\
			switch (k64Cnt & 3)											\
			{														\
				case 0:  /* same as 4 */								\
				b0(t) = p8(04)[b0(t)] ^ b0(k32[3]);		\
				b1(t) = p8(14)[b1(t)] ^ b1(k32[3]);		\
				b2(t) = p8(24)[b2(t)] ^ b2(k32[3]);		\
				b3(t) = p8(34)[b3(t)] ^ b3(k32[3]);		\
				/* fall thru, having pre-processed t */		\
				case 3:		b0(t) = p8(03)[b0(t)] ^ b0(k32[2]);		\
				b1(t) = p8(13)[b1(t)] ^ b1(k32[2]);		\
				b2(t) = p8(23)[b2(t)] ^ b2(k32[2]);		\
				b3(t) = p8(33)[b3(t)] ^ b3(k32[2]);		\
				/* fall thru, having pre-processed t */		\
				case 2:	 /* 128-bit keys (optimize for this case) */	\
				res = MDStab[0][p8(01)[p8(02)[b0(t)] ^ b0(k32[1])] ^ b0(k32[0])] ^ \
				MDStab[1][p8(11)[p8(12)[b1(t)] ^ b1(k32[1])] ^ b1(k32[0])] ^ \
				MDStab[2][p8(21)[p8(22)[b2(t)] ^ b2(k32[1])] ^ b2(k32[0])] ^ \
				MDStab[3][p8(31)[p8(32)[b3(t)] ^ b3(k32[1])] ^ b3(k32[0])];	\
			}														\
		}

	u32 k64Cnt = keyLen / 8;
	u32 subkeyCnt = TOTAL_SUBKEYS;
	u32 k32e[4]; // even 32-bit entities
	u32 k32o[4]; // odd 32-bit entities
	u32 sBoxKey[4];

	u32 i, j, offset = 0;

	for (i = 0, j = k64Cnt - 1; i < 4 && offset < keyLen / BYTES_PER_WORD; i++, j--) {
		k32e[i] = cipherKey[offset++];
		k32o[i] = cipherKey[offset++];
		sBoxKey[j] = RS_MDS_Encode(k32e[i], k32o[i]); // reverse order
	}


	// compute the round decryption subkeys for PHT. these same subkeys
	// will be used in encryption but will be applied in reverse order.
	u32 q, A, B;

	for (i = q = 0; i < subkeyCnt / 2; i++, q += SK_STEP) {
		F32(A, q, k32e); // A uses even key entities
		F32(B, q + SK_BUMP, k32o); // B uses odd  key entities
		B = B << 8 | ROR(B, 24);
		A += B;
		subKeys[2 * i] = A;               // combine with a PHT
		A += B;
		subKeys[2 * i + 1] = A << SK_ROTL | ROR(A, (32 - SK_ROTL));
	}

	// fully expand the table for speed
	u32 k0 = sBoxKey[0];
	u32 k1 = sBoxKey[1];
	u32 k2 = sBoxKey[2];
	u32 k3 = sBoxKey[3];
	u32 b0, b1, b2, b3;

	for (i = 0; i < 256; i++) {
		b0 = b1 = b2 = b3 = i;
		switch (k64Cnt & 3) {
		case 0: // same as 4
			b0 = (p8(04)[b0]) ^ b0(k3);
			b1 = (p8(14)[b1]) ^ b1(k3);
			b2 = (p8(24)[b2]) ^ b2(k3);
			b3 = (p8(34)[b3]) ^ b3(k3);
		case 3:
			b0 = (p8(03)[b0]) ^ b0(k2);
			b1 = (p8(13)[b1]) ^ b1(k2);
			b2 = (p8(23)[b2]) ^ b2(k2);
			b3 = (p8(33)[b3]) ^ b3(k2);
		case 2: // 128-bit keys
			sBox[2 * i] = MDStab[0][(p8(01)[(p8(02)[b0]) ^ b0(k1)]) ^ b0(k0)];
			sBox[2 * i + 1] = MDStab[1][(p8(11)[(p8(12)[b1]) ^ b1(k1)]) ^ b1(k0)];
			sBox[0x200 + 2 * i] = MDStab[2][(p8(21)[(p8(22)[b2]) ^ b2(k1)]) ^ b2(k0)];
			sBox[0x200 + 2 * i + 1] = MDStab[3][(p8(31)[(p8(32)[b3]) ^ b3(k1)]) ^ b3(k0)];
		}
	}



	return 0;
}




int twofishEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){


	boost::chrono::high_resolution_clock::time_point start;

	
	InitValues *initValues;
	cl_program program;
	cl_kernel krnl;
	cl_int clStatus;

	u32 subKeys[TOTAL_SUBKEYS];
	u32 sBox[S_BOX_SIZE];

	start = startStopwatch();
	initValues = openClInit();
	endStopwatch("OpenCl initialization (encryption)", start);


	start = startStopwatch();
	program = getProgram(initValues, encryptKernelSource, encryptKernelBin, options, BUILD_SOURCE);
	
	if (program == NULL){
		printf("Error while creating program. \n");
		return -1;

	}
	
	endStopwatch("Program building (encryption)", start);
	
	

	start = startStopwatch();
	// Create the OpenCL kernel
	krnl = clCreateKernel(program, "encryptKernel", &clStatus);
	
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Kernel creating failed!");
		return clStatus;
	}
	
	endStopwatch("Kernel creation (encryption) ", start);
	
	

	

	start = startStopwatch();
	makeKey((u32*)cipherKey, subKeys, sBox, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);

	
	clStatus = twofishEncryptOpenCl(pt, ct, subKeys, sBox, plainTextLength, initValues, krnl);
	


	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Encryption failed!");
		return 1;
	}

	start = startStopwatch();
	clStatus = clReleaseKernel(krnl);
	clStatus = clReleaseProgram(program);
	openClDeInit(initValues);
	endStopwatch("Freeing OpenCL resources (encryption)", start);


	return 0;




}

int twofishDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	
	InitValues *initValues;
	cl_program program;
	cl_kernel krnl;
	cl_int clStatus;

	u32 subKeys[TOTAL_SUBKEYS];
	u32 sBox[S_BOX_SIZE];
	
	start = startStopwatch();
	initValues = openClInit();
	endStopwatch("OpenCl initialization (decryption)", start);


	start = startStopwatch();
	program = getProgram(initValues, decryptKernelSource, decryptKernelBin, options, BUILD_SOURCE);
	if (program == NULL){
		printf("Error while creating program. \n");
		return -1;

	}
	
	endStopwatch("Program building (decryption)", start);

	start = startStopwatch();
	// Create the OpenCL kernel
	krnl = clCreateKernel(program, "decryptKernel", &clStatus);
	
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Kernel creating failed!\n ");
		return clStatus;
	}
	endStopwatch("Kernel creation (decryption)", start);
	


	start = startStopwatch();
	makeKey((u32*)cipherKey, subKeys, sBox, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);


	clStatus = twofishDecryptOpenCl(ct, pt, subKeys, sBox, plainTextLength, initValues, krnl);
	

	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Decryption failed!");
		return 1;
	}

	start = startStopwatch();
	clStatus = clReleaseKernel(krnl);
	clStatus = clReleaseProgram(program);
	openClDeInit(initValues);
	endStopwatch("Freeing OpenCL resources (decryption)", start);

	return 0;

}