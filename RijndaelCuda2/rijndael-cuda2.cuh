

#ifndef __RIJNDAEL_CUDA2_H
#define __RIJNDAEL_CUDA2_H
#include "common-types.h"

#define maxNr 14
#define rkLength 4 * (maxNr + 1)

#define T_TABLE_SIZE 256
#define BIG_T_TABLE_SIZE (5 * T_TABLE_SIZE)


int rijndaelEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);
int rijndaelDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);

#endif /* __RIJNDAEL_CUDA2_H */