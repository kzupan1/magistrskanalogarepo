
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "rijndael-cuda2.cuh"
#include "rijndael-tables2.cuh"
#include <stdio.h>
#include <stdlib.h>
#include "measure-time.h"
#include <boost/chrono.hpp>


__device__ inline u32 G4_mul(u32 x, u32 y) {
	u32 a, b, c, d, e, p, q;
	a = (x & 0x2) >> 1; b = (x & 0x1);
	c = (y & 0x2) >> 1; d = (y & 0x1);
	e = (a ^ b) & (c ^ d);
	p = (a & c) ^ e;
	q = (b & d) ^ e;
	return ((p << 1) | q);
}

__device__ inline u32 G4_scl_N(u32 x) {
	u32 a, b, p, q;
	a = (x & 0x2) >> 1; b = (x & 0x1);
	p = b;
	q = a ^ b;
	return ((p << 1) | q);
}

__device__ inline u32 G4_scl_N2(u32 x) {
	u32 a, b, p, q;
	a = (x & 0x2) >> 1; b = (x & 0x1);
	p = a ^ b;
	q = a;
	return ((p << 1) | q);
}


 __device__ inline u32 G4_sq(u32 x) {
	u32 a, b;
	a = (x & 0x2) >> 1; b = (x & 0x1);
	return ((b << 1) | a);
}

 __device__ inline u32 G16_mul(u32 x, u32 y) {
	u32 a, b, c, d, e, p, q;
	a = (x & 0xC) >> 2; b = (x & 0x3);
	c = (y & 0xC) >> 2; d = (y & 0x3);
	e = G4_mul(a ^ b, c ^ d);
	e = G4_scl_N(e);
	p = G4_mul(a, c) ^ e;
	q = G4_mul(b, d) ^ e;
	return ((p << 2) | q);
}

 __device__ inline u32 G16_sq_scl(u32 x) {
	u32 a, b, p, q;
	a = (x & 0xC) >> 2; b = (x & 0x3);
	p = G4_sq(a ^ b);
	q = G4_scl_N2(G4_sq(b));
	return ((p << 2) | q);
}

 __device__ inline u32 G16_inv(u32 x) {
	u32 a, b, c, d, e, p, q;
	a = (x & 0xC) >> 2; b = (x & 0x3);
	c = G4_scl_N(G4_sq(a ^ b));
	d = G4_mul(a, b);
	e = G4_sq(c ^ d); // really inverse, but same as square
	p = G4_mul(e, b);
	q = G4_mul(e, a);
	return ((p << 2) | q);
}

 __device__ u32 inline G256_inv(u32 x) {
	u32 a, b, c, d, e, p, q;
	a = (x & 0xF0) >> 4; b = (x & 0x0F);
	c = G16_sq_scl(a ^ b);
	d = G16_mul(a, b);
	e = G16_inv(c ^ d);
	p = G16_mul(e, b);
	q = G16_mul(e, a);
	return ((p << 4) | q);
}

__device__ u32 inline G256_newbasis(u32 x, u32 b[]) {
	int i;
	u32	y = 0;
	for (i = 7; i >= 0; i--) {
		if (x & 1) y ^= b[i];
		x >>= 1;
	}
	return (y);
}

 __device__ u32 inline  G256_newbasis_forward(u32 x){
	u32 y = 0;

	if (x & 0x01) y ^= 0xFF;		   
	if (x & 0x02) y ^= 0xA9;		   
	if (x & 0x04) y ^= 0x81;		   
	if (x & 0x08) y ^= 0x09;		   
	if (x & 0x10) y ^= 0x48;		   
	if (x & 0x20) y ^= 0xF2;		   
	if (x & 0x40) y ^= 0xF3;		   
	if (x & 0x80) y ^= 0x98;

	return y;
	
}

 __device__ u32 inline G256_newbasis_backward(u32 x){
	u32 y = 0;

	if (x & 0x01) y ^= 0x24;		  
	if (x & 0x02) y ^= 0x03;		  
	if (x & 0x04) y ^= 0x04;		  
	if (x & 0x08) y ^= 0xDC;		  
	if (x & 0x10) y ^= 0x0B;		  
	if (x & 0x20) y ^= 0x9E;		  
	if (x & 0x40) y ^= 0x2D;		  
	if (x & 0x80) y ^= 0x58;		  


	return y;
}


 __device__ u32 inline G256_newbasis_inv_forward(u32 x){
	u32 y = 0;

	if (x & 0x01) y ^= 0x53;		   
	if (x & 0x02) y ^= 0x51;		   
	if (x & 0x04) y ^= 0x04;		   
	if (x & 0x08) y ^= 0x12;		   
	if (x & 0x10) y ^= 0xEB;		   
	if (x & 0x20) y ^= 0x05;		   
	if (x & 0x40) y ^= 0x79;		   
	if (x & 0x80) y ^= 0x8C;		   

	return y;
}


 __device__ u32 inline G256_newbasis_inv_backward(u32 x){
	u32 y = 0;

	if (x & 0x01) y ^= 0x60;	   
	if (x & 0x02) y ^= 0xDE;	   
	if (x & 0x04) y ^= 0x29;	   
	if (x & 0x08) y ^= 0x68;	   
	if (x & 0x10) y ^= 0x8C;	   
	if (x & 0x20) y ^= 0x6E;	   
	if (x & 0x40) y ^= 0x78;	   
	if (x & 0x80) y ^= 0x64;	   


	return y;
}

 __device__ inline u32 Sbox(u32 n) {
	u32 t;
	t = G256_newbasis_forward(n);
	t = G256_inv(t);
	t = G256_newbasis_backward(t);
	return (t ^ 0x63);
}


 __device__ inline u32 iSbox(u32 n) {
	u32 t;
	t = G256_newbasis_inv_forward(n ^ 0x63);
	t = G256_inv(t);
	t = G256_newbasis_inv_backward(t);
	return (t);
}

 __device__ inline u32 xtime(u32 n){
	 return ((n << 1) ^ (((n >> 7) & 1) * 0x11b));
 }

__device__ inline u32 te0(u32 n){
	 u32 s = Sbox(n);
	 u32 s2 = xtime(s);

	 return s2 | (s << 8) | (s << 16) | ((s ^ s2) << 24);
 
 }

__device__ inline u32 te1(u32 n){
	u32 s = Sbox(n);
	u32 s2 = xtime(s);

	return (s ^ s2) |  (s2 << 8) | (s << 16) | (s << 24);

}

__device__ inline u32 te2(u32 n){
	u32 s = Sbox(n);
	u32 s2 = xtime(s);

	return s | ((s ^ s2) << 8) | (s2 << 16) | (s << 24);

}

__device__ inline u32 te3(u32 n){
	u32 s = Sbox(n);
	u32 s2 = xtime(s);

	return s | (s << 8) | ((s ^ s2) << 16) | (s2 << 24);

}

__device__ inline u32 te4(u32 n0, u32 n1, u32 n2, u32 n3){

	return (Sbox(n0) | ((Sbox(n1)) << 8) | ((Sbox(n2)) << 16) | ((Sbox(n3)) << 24));

}

__device__ inline u32 td0(u32 n){
	u32 is = iSbox(n);
	u32 gmul2 = xtime(is);
	u32 gmul4 = xtime(gmul2);
	u32 gmul8 = xtime(gmul4);
	u32 gmul9 = gmul8 ^ is;

	return (gmul8 ^ gmul4 ^ gmul2) | (gmul9 << 8) | ((gmul9 ^ gmul4) << 16) | ((gmul9 ^ gmul2) << 24);


}

__device__ inline u32 td1(u32 n){
	u32 is = iSbox(n);
	u32 gmul2 = xtime(is);
	u32 gmul4 = xtime(gmul2);
	u32 gmul8 = xtime(gmul4);
	u32 gmul9 = gmul8 ^ is;

	return ((gmul9 ^ gmul2)) | ((gmul8 ^ gmul4 ^ gmul2) << 8) | (gmul9 << 16) | ((gmul9 ^ gmul4) << 24);


}

__device__ inline u32 td2(u32 n){
	u32 is = iSbox(n);
	u32 gmul2 = xtime(is);
	u32 gmul4 = xtime(gmul2);
	u32 gmul8 = xtime(gmul4);
	u32 gmul9 = gmul8 ^ is;

	return ((gmul9 ^ gmul4)) | ((gmul9 ^ gmul2) << 8) | ((gmul8 ^ gmul4 ^ gmul2) << 16) | (gmul9 << 24);


}

__device__ inline u32 td3(u32 n){
	u32 is = iSbox(n);
	u32 gmul2 = xtime(is);
	u32 gmul4 = xtime(gmul2);
	u32 gmul8 = xtime(gmul4);
	u32 gmul9 = gmul8 ^ is;

	return (gmul9) | ((gmul9 ^ gmul4) << 8) | ((gmul9 ^ gmul2) << 16) | ((gmul8 ^ gmul4 ^ gmul2) << 24);

}

__device__ inline u32 td4(u32 n0, u32 n1, u32 n2, u32 n3){

	return iSbox(n0) | (iSbox(n1) << 8) | (iSbox(n2) << 16) | (iSbox(n3) << 24);

}


 __constant__ u32 rk_constant[rkLength];

 __global__ void encryptKernel(u32 *pt, u32 *ct, const unsigned int plainTextLengthInWords, const unsigned int Nr)
 {


	 int t = threadIdx.x;
	 int i = blockDim.x * blockIdx.x + t;

	 int offset = i * WORDS_PER_BLOCK;

	 if (offset < plainTextLengthInWords){

		 pt += offset;
		 ct += offset;

		 register u32 s0, s1, s2, s3, t0, t1, t2, t3;


		 s0 = pt[0] ^ rk_constant[0];
		 s1 = pt[1] ^ rk_constant[1];
		 s2 = pt[2] ^ rk_constant[2];
		 s3 = pt[3] ^ rk_constant[3];

		 /* round 1: */
		 t0 = te0((s0 & 0xff)) ^ te1(((s1 >> 8) & 0xff)) ^ te2(((s2 >> 16) & 0xff)) ^ te3((s3 >> 24)) ^ rk_constant[4];
		 t1 = te0((s1 & 0xff)) ^ te1(((s2 >> 8) & 0xff)) ^ te2(((s3 >> 16) & 0xff)) ^ te3((s0 >> 24)) ^ rk_constant[5];
		 t2 = te0((s2 & 0xff)) ^ te1(((s3 >> 8) & 0xff)) ^ te2(((s0 >> 16) & 0xff)) ^ te3((s1 >> 24)) ^ rk_constant[6];
		 t3 = te0((s3 & 0xff)) ^ te1(((s0 >> 8) & 0xff)) ^ te2(((s1 >> 16) & 0xff)) ^ te3((s2 >> 24)) ^ rk_constant[7];
		 /* round 2: */
		 s0 = te0((t0 & 0xff)) ^ te1(((t1 >> 8) & 0xff)) ^ te2(((t2 >> 16) & 0xff)) ^ te3((t3 >> 24)) ^ rk_constant[8];
		 s1 = te0((t1 & 0xff)) ^ te1(((t2 >> 8) & 0xff)) ^ te2(((t3 >> 16) & 0xff)) ^ te3((t0 >> 24)) ^ rk_constant[9];
		 s2 = te0((t2 & 0xff)) ^ te1(((t3 >> 8) & 0xff)) ^ te2(((t0 >> 16) & 0xff)) ^ te3((t1 >> 24)) ^ rk_constant[10];
		 s3 = te0((t3 & 0xff)) ^ te1(((t0 >> 8) & 0xff)) ^ te2(((t1 >> 16) & 0xff)) ^ te3((t2 >> 24)) ^ rk_constant[11];		 
		 /* round 3: */
		 t0 = te0((s0 & 0xff)) ^ te1(((s1 >> 8) & 0xff)) ^ te2(((s2 >> 16) & 0xff)) ^ te3((s3 >> 24)) ^ rk_constant[12];
		 t1 = te0((s1 & 0xff)) ^ te1(((s2 >> 8) & 0xff)) ^ te2(((s3 >> 16) & 0xff)) ^ te3((s0 >> 24)) ^ rk_constant[13];
		 t2 = te0((s2 & 0xff)) ^ te1(((s3 >> 8) & 0xff)) ^ te2(((s0 >> 16) & 0xff)) ^ te3((s1 >> 24)) ^ rk_constant[14];
		 t3 = te0((s3 & 0xff)) ^ te1(((s0 >> 8) & 0xff)) ^ te2(((s1 >> 16) & 0xff)) ^ te3((s2 >> 24)) ^ rk_constant[15];
		 /* round 4: */
		 s0 = te0((t0 & 0xff)) ^ te1(((t1 >> 8) & 0xff)) ^ te2(((t2 >> 16) & 0xff)) ^ te3((t3 >> 24)) ^ rk_constant[16];
		 s1 = te0((t1 & 0xff)) ^ te1(((t2 >> 8) & 0xff)) ^ te2(((t3 >> 16) & 0xff)) ^ te3((t0 >> 24)) ^ rk_constant[17];
		 s2 = te0((t2 & 0xff)) ^ te1(((t3 >> 8) & 0xff)) ^ te2(((t0 >> 16) & 0xff)) ^ te3((t1 >> 24)) ^ rk_constant[18];
		 s3 = te0((t3 & 0xff)) ^ te1(((t0 >> 8) & 0xff)) ^ te2(((t1 >> 16) & 0xff)) ^ te3((t2 >> 24)) ^ rk_constant[19];
		 /* round 5: */
		 t0 = te0((s0 & 0xff)) ^ te1(((s1 >> 8) & 0xff)) ^ te2(((s2 >> 16) & 0xff)) ^ te3((s3 >> 24)) ^ rk_constant[20];
		 t1 = te0((s1 & 0xff)) ^ te1(((s2 >> 8) & 0xff)) ^ te2(((s3 >> 16) & 0xff)) ^ te3((s0 >> 24)) ^ rk_constant[21];
		 t2 = te0((s2 & 0xff)) ^ te1(((s3 >> 8) & 0xff)) ^ te2(((s0 >> 16) & 0xff)) ^ te3((s1 >> 24)) ^ rk_constant[22];
		 t3 = te0((s3 & 0xff)) ^ te1(((s0 >> 8) & 0xff)) ^ te2(((s1 >> 16) & 0xff)) ^ te3((s2 >> 24)) ^ rk_constant[23];
		 /* round 6: */
		 s0 = te0((t0 & 0xff)) ^ te1(((t1 >> 8) & 0xff)) ^ te2(((t2 >> 16) & 0xff)) ^ te3((t3 >> 24)) ^ rk_constant[24];
		 s1 = te0((t1 & 0xff)) ^ te1(((t2 >> 8) & 0xff)) ^ te2(((t3 >> 16) & 0xff)) ^ te3((t0 >> 24)) ^ rk_constant[25];
		 s2 = te0((t2 & 0xff)) ^ te1(((t3 >> 8) & 0xff)) ^ te2(((t0 >> 16) & 0xff)) ^ te3((t1 >> 24)) ^ rk_constant[26];
		 s3 = te0((t3 & 0xff)) ^ te1(((t0 >> 8) & 0xff)) ^ te2(((t1 >> 16) & 0xff)) ^ te3((t2 >> 24)) ^ rk_constant[27];
		 /* round 7: */
		 t0 = te0((s0 & 0xff)) ^ te1(((s1 >> 8) & 0xff)) ^ te2(((s2 >> 16) & 0xff)) ^ te3((s3 >> 24)) ^ rk_constant[28];
		 t1 = te0((s1 & 0xff)) ^ te1(((s2 >> 8) & 0xff)) ^ te2(((s3 >> 16) & 0xff)) ^ te3((s0 >> 24)) ^ rk_constant[29];
		 t2 = te0((s2 & 0xff)) ^ te1(((s3 >> 8) & 0xff)) ^ te2(((s0 >> 16) & 0xff)) ^ te3((s1 >> 24)) ^ rk_constant[30];
		 t3 = te0((s3 & 0xff)) ^ te1(((s0 >> 8) & 0xff)) ^ te2(((s1 >> 16) & 0xff)) ^ te3((s2 >> 24)) ^ rk_constant[31];
		 /* round 8: */
		 s0 = te0((t0 & 0xff)) ^ te1(((t1 >> 8) & 0xff)) ^ te2(((t2 >> 16) & 0xff)) ^ te3((t3 >> 24)) ^ rk_constant[32];
		 s1 = te0((t1 & 0xff)) ^ te1(((t2 >> 8) & 0xff)) ^ te2(((t3 >> 16) & 0xff)) ^ te3((t0 >> 24)) ^ rk_constant[33];
		 s2 = te0((t2 & 0xff)) ^ te1(((t3 >> 8) & 0xff)) ^ te2(((t0 >> 16) & 0xff)) ^ te3((t1 >> 24)) ^ rk_constant[34];
		 s3 = te0((t3 & 0xff)) ^ te1(((t0 >> 8) & 0xff)) ^ te2(((t1 >> 16) & 0xff)) ^ te3((t2 >> 24)) ^ rk_constant[35];
		 /* round 9: */
		 t0 = te0((s0 & 0xff)) ^ te1(((s1 >> 8) & 0xff)) ^ te2(((s2 >> 16) & 0xff)) ^ te3((s3 >> 24)) ^ rk_constant[36];
		 t1 = te0((s1 & 0xff)) ^ te1(((s2 >> 8) & 0xff)) ^ te2(((s3 >> 16) & 0xff)) ^ te3((s0 >> 24)) ^ rk_constant[37];
		 t2 = te0((s2 & 0xff)) ^ te1(((s3 >> 8) & 0xff)) ^ te2(((s0 >> 16) & 0xff)) ^ te3((s1 >> 24)) ^ rk_constant[38];
		 t3 = te0((s3 & 0xff)) ^ te1(((s0 >> 8) & 0xff)) ^ te2(((s1 >> 16) & 0xff)) ^ te3((s2 >> 24)) ^ rk_constant[39];

		 if (Nr > 10) {
			 /* round 10: */
			 s0 = te0((t0 & 0xff)) ^ te1(((t1 >> 8) & 0xff)) ^ te2(((t2 >> 16) & 0xff)) ^ te3((t3 >> 24)) ^ rk_constant[40];
			 s1 = te0((t1 & 0xff)) ^ te1(((t2 >> 8) & 0xff)) ^ te2(((t3 >> 16) & 0xff)) ^ te3((t0 >> 24)) ^ rk_constant[41];
			 s2 = te0((t2 & 0xff)) ^ te1(((t3 >> 8) & 0xff)) ^ te2(((t0 >> 16) & 0xff)) ^ te3((t1 >> 24)) ^ rk_constant[42];
			 s3 = te0((t3 & 0xff)) ^ te1(((t0 >> 8) & 0xff)) ^ te2(((t1 >> 16) & 0xff)) ^ te3((t2 >> 24)) ^ rk_constant[43];
			 /* round 11: */
			 t0 = te0((s0 & 0xff)) ^ te1(((s1 >> 8) & 0xff)) ^ te2(((s2 >> 16) & 0xff)) ^ te3((s3 >> 24)) ^ rk_constant[44];
			 t1 = te0((s1 & 0xff)) ^ te1(((s2 >> 8) & 0xff)) ^ te2(((s3 >> 16) & 0xff)) ^ te3((s0 >> 24)) ^ rk_constant[45];
			 t2 = te0((s2 & 0xff)) ^ te1(((s3 >> 8) & 0xff)) ^ te2(((s0 >> 16) & 0xff)) ^ te3((s1 >> 24)) ^ rk_constant[46];
			 t3 = te0((s3 & 0xff)) ^ te1(((s0 >> 8) & 0xff)) ^ te2(((s1 >> 16) & 0xff)) ^ te3((s2 >> 24)) ^ rk_constant[47];
			 if (Nr > 12) {
				 /* round 12: */
				 s0 = te0((t0 & 0xff)) ^ te1(((t1 >> 8) & 0xff)) ^ te2(((t2 >> 16) & 0xff)) ^ te3((t3 >> 24)) ^ rk_constant[48];
				 s1 = te0((t1 & 0xff)) ^ te1(((t2 >> 8) & 0xff)) ^ te2(((t3 >> 16) & 0xff)) ^ te3((t0 >> 24)) ^ rk_constant[49];
				 s2 = te0((t2 & 0xff)) ^ te1(((t3 >> 8) & 0xff)) ^ te2(((t0 >> 16) & 0xff)) ^ te3((t1 >> 24)) ^ rk_constant[50];
				 s3 = te0((t3 & 0xff)) ^ te1(((t0 >> 8) & 0xff)) ^ te2(((t1 >> 16) & 0xff)) ^ te3((t2 >> 24)) ^ rk_constant[51];
				 /* round 13: */
				 t0 = te0((s0 & 0xff)) ^ te1(((s1 >> 8) & 0xff)) ^ te2(((s2 >> 16) & 0xff)) ^ te3((s3 >> 24)) ^ rk_constant[52];
				 t1 = te0((s1 & 0xff)) ^ te1(((s2 >> 8) & 0xff)) ^ te2(((s3 >> 16) & 0xff)) ^ te3((s0 >> 24)) ^ rk_constant[53];
				 t2 = te0((s2 & 0xff)) ^ te1(((s3 >> 8) & 0xff)) ^ te2(((s0 >> 16) & 0xff)) ^ te3((s1 >> 24)) ^ rk_constant[54];
				 t3 = te0((s3 & 0xff)) ^ te1(((s0 >> 8) & 0xff)) ^ te2(((s1 >> 16) & 0xff)) ^ te3((s2 >> 24)) ^ rk_constant[55];
			 }
		 }


		 u32 x = Nr << 2;


		 s0 = te4((t0 & 0xff), ((t1 >> 8) & 0xff), ((t2 >> 16) & 0xff), ((t3) >> 24)) ^ rk_constant[x++];
		 s1 = te4((t1 & 0xff), ((t2 >> 8) & 0xff), ((t3 >> 16) & 0xff), ((t0) >> 24)) ^ rk_constant[x++];
		 s2 = te4((t2 & 0xff), ((t3 >> 8) & 0xff), ((t0 >> 16) & 0xff), ((t1) >> 24)) ^ rk_constant[x++];
		 s3 = te4((t3 & 0xff), ((t0 >> 8) & 0xff), ((t1 >> 16) & 0xff), ((t2) >> 24)) ^ rk_constant[x];
		 
		


		 ct[0] = s0;
		 ct[1] = s1;
		 ct[2] = s2;
		 ct[3] = s3;


	 }




 }

 __global__ void decryptKernel(u32 *ct, u32 *pt, const unsigned int plainTextLengthInWords, const unsigned int Nr)
 {


	 int t = threadIdx.x;
	 int i = blockDim.x * blockIdx.x + t;


	 int offset = i * WORDS_PER_BLOCK;

	 if (offset < plainTextLengthInWords){


		 ct += offset;
		 pt += offset;

		 register u32 s0, s1, s2, s3, t0, t1, t2, t3;


		 s0 = ct[0] ^ rk_constant[0];
		 s1 = ct[1] ^ rk_constant[1];
		 s2 = ct[2] ^ rk_constant[2];
		 s3 = ct[3] ^ rk_constant[3];

		 /* round 1: */
		 t0 = td0((s0 & 0xff)) ^ td1(((s3 >> 8) & 0xff)) ^ td2(((s2 >> 16) & 0xff)) ^ td3((s1 >> 24)) ^ rk_constant[4];
		 t1 = td0((s1 & 0xff)) ^ td1(((s0 >> 8) & 0xff)) ^ td2(((s3 >> 16) & 0xff)) ^ td3((s2 >> 24)) ^ rk_constant[5];
		 t2 = td0((s2 & 0xff)) ^ td1(((s1 >> 8) & 0xff)) ^ td2(((s0 >> 16) & 0xff)) ^ td3((s3 >> 24)) ^ rk_constant[6];
		 t3 = td0((s3 & 0xff)) ^ td1(((s2 >> 8) & 0xff)) ^ td2(((s1 >> 16) & 0xff)) ^ td3((s0 >> 24)) ^ rk_constant[7];
		 /* round 2: */
		 s0 = td0((t0 & 0xff)) ^ td1(((t3 >> 8) & 0xff)) ^ td2(((t2 >> 16) & 0xff)) ^ td3((t1 >> 24)) ^ rk_constant[8];
		 s1 = td0((t1 & 0xff)) ^ td1(((t0 >> 8) & 0xff)) ^ td2(((t3 >> 16) & 0xff)) ^ td3((t2 >> 24)) ^ rk_constant[9];
		 s2 = td0((t2 & 0xff)) ^ td1(((t1 >> 8) & 0xff)) ^ td2(((t0 >> 16) & 0xff)) ^ td3((t3 >> 24)) ^ rk_constant[10];
		 s3 = td0((t3 & 0xff)) ^ td1(((t2 >> 8) & 0xff)) ^ td2(((t1 >> 16) & 0xff)) ^ td3((t0 >> 24)) ^ rk_constant[11];
		 /* round 3: */
		 t0 = td0((s0 & 0xff)) ^ td1(((s3 >> 8) & 0xff)) ^ td2(((s2 >> 16) & 0xff)) ^ td3((s1 >> 24)) ^ rk_constant[12];
		 t1 = td0((s1 & 0xff)) ^ td1(((s0 >> 8) & 0xff)) ^ td2(((s3 >> 16) & 0xff)) ^ td3((s2 >> 24)) ^ rk_constant[13];
		 t2 = td0((s2 & 0xff)) ^ td1(((s1 >> 8) & 0xff)) ^ td2(((s0 >> 16) & 0xff)) ^ td3((s3 >> 24)) ^ rk_constant[14];
		 t3 = td0((s3 & 0xff)) ^ td1(((s2 >> 8) & 0xff)) ^ td2(((s1 >> 16) & 0xff)) ^ td3((s0 >> 24)) ^ rk_constant[15];
		 /* round 4: */
		 s0 = td0((t0 & 0xff)) ^ td1(((t3 >> 8) & 0xff)) ^ td2(((t2 >> 16) & 0xff)) ^ td3((t1 >> 24)) ^ rk_constant[16];
		 s1 = td0((t1 & 0xff)) ^ td1(((t0 >> 8) & 0xff)) ^ td2(((t3 >> 16) & 0xff)) ^ td3((t2 >> 24)) ^ rk_constant[17];
		 s2 = td0((t2 & 0xff)) ^ td1(((t1 >> 8) & 0xff)) ^ td2(((t0 >> 16) & 0xff)) ^ td3((t3 >> 24)) ^ rk_constant[18];
		 s3 = td0((t3 & 0xff)) ^ td1(((t2 >> 8) & 0xff)) ^ td2(((t1 >> 16) & 0xff)) ^ td3((t0 >> 24)) ^ rk_constant[19];
		 /* round 5: */
		 t0 = td0((s0 & 0xff)) ^ td1(((s3 >> 8) & 0xff)) ^ td2(((s2 >> 16) & 0xff)) ^ td3((s1 >> 24)) ^ rk_constant[20];
		 t1 = td0((s1 & 0xff)) ^ td1(((s0 >> 8) & 0xff)) ^ td2(((s3 >> 16) & 0xff)) ^ td3((s2 >> 24)) ^ rk_constant[21];
		 t2 = td0((s2 & 0xff)) ^ td1(((s1 >> 8) & 0xff)) ^ td2(((s0 >> 16) & 0xff)) ^ td3((s3 >> 24)) ^ rk_constant[22];
		 t3 = td0((s3 & 0xff)) ^ td1(((s2 >> 8) & 0xff)) ^ td2(((s1 >> 16) & 0xff)) ^ td3((s0 >> 24)) ^ rk_constant[23];
		 /* round 6: */
		 s0 = td0((t0 & 0xff)) ^ td1(((t3 >> 8) & 0xff)) ^ td2(((t2 >> 16) & 0xff)) ^ td3((t1 >> 24)) ^ rk_constant[24];
		 s1 = td0((t1 & 0xff)) ^ td1(((t0 >> 8) & 0xff)) ^ td2(((t3 >> 16) & 0xff)) ^ td3((t2 >> 24)) ^ rk_constant[25];
		 s2 = td0((t2 & 0xff)) ^ td1(((t1 >> 8) & 0xff)) ^ td2(((t0 >> 16) & 0xff)) ^ td3((t3 >> 24)) ^ rk_constant[26];
		 s3 = td0((t3 & 0xff)) ^ td1(((t2 >> 8) & 0xff)) ^ td2(((t1 >> 16) & 0xff)) ^ td3((t0 >> 24)) ^ rk_constant[27];
		 /* round 7: */
		 t0 = td0((s0 & 0xff)) ^ td1(((s3 >> 8) & 0xff)) ^ td2(((s2 >> 16) & 0xff)) ^ td3((s1 >> 24)) ^ rk_constant[28];
		 t1 = td0((s1 & 0xff)) ^ td1(((s0 >> 8) & 0xff)) ^ td2(((s3 >> 16) & 0xff)) ^ td3((s2 >> 24)) ^ rk_constant[29];
		 t2 = td0((s2 & 0xff)) ^ td1(((s1 >> 8) & 0xff)) ^ td2(((s0 >> 16) & 0xff)) ^ td3((s3 >> 24)) ^ rk_constant[30];
		 t3 = td0((s3 & 0xff)) ^ td1(((s2 >> 8) & 0xff)) ^ td2(((s1 >> 16) & 0xff)) ^ td3((s0 >> 24)) ^ rk_constant[31];
		 /* round 8: */
		 s0 = td0((t0 & 0xff)) ^ td1(((t3 >> 8) & 0xff)) ^ td2(((t2 >> 16) & 0xff)) ^ td3((t1 >> 24)) ^ rk_constant[32];
		 s1 = td0((t1 & 0xff)) ^ td1(((t0 >> 8) & 0xff)) ^ td2(((t3 >> 16) & 0xff)) ^ td3((t2 >> 24)) ^ rk_constant[33];
		 s2 = td0((t2 & 0xff)) ^ td1(((t1 >> 8) & 0xff)) ^ td2(((t0 >> 16) & 0xff)) ^ td3((t3 >> 24)) ^ rk_constant[34];
		 s3 = td0((t3 & 0xff)) ^ td1(((t2 >> 8) & 0xff)) ^ td2(((t1 >> 16) & 0xff)) ^ td3((t0 >> 24)) ^ rk_constant[35];
		 /* round 9: */
		 t0 = td0((s0 & 0xff)) ^ td1(((s3 >> 8) & 0xff)) ^ td2(((s2 >> 16) & 0xff)) ^ td3((s1 >> 24)) ^ rk_constant[36];
		 t1 = td0((s1 & 0xff)) ^ td1(((s0 >> 8) & 0xff)) ^ td2(((s3 >> 16) & 0xff)) ^ td3((s2 >> 24)) ^ rk_constant[37];
		 t2 = td0((s2 & 0xff)) ^ td1(((s1 >> 8) & 0xff)) ^ td2(((s0 >> 16) & 0xff)) ^ td3((s3 >> 24)) ^ rk_constant[38];
		 t3 = td0((s3 & 0xff)) ^ td1(((s2 >> 8) & 0xff)) ^ td2(((s1 >> 16) & 0xff)) ^ td3((s0 >> 24)) ^ rk_constant[39];

		 if (Nr > 10) {
			 /* round 10: */
			 s0 = td0((t0 & 0xff)) ^ td1(((t3 >> 8) & 0xff)) ^ td2(((t2 >> 16) & 0xff)) ^ td3((t1 >> 24)) ^ rk_constant[40];
			 s1 = td0((t1 & 0xff)) ^ td1(((t0 >> 8) & 0xff)) ^ td2(((t3 >> 16) & 0xff)) ^ td3((t2 >> 24)) ^ rk_constant[41];
			 s2 = td0((t2 & 0xff)) ^ td1(((t1 >> 8) & 0xff)) ^ td2(((t0 >> 16) & 0xff)) ^ td3((t3 >> 24)) ^ rk_constant[42];
			 s3 = td0((t3 & 0xff)) ^ td1(((t2 >> 8) & 0xff)) ^ td2(((t1 >> 16) & 0xff)) ^ td3((t0 >> 24)) ^ rk_constant[43];
			 /* round 11: */
			 t0 = td0((s0 & 0xff)) ^ td1(((s3 >> 8) & 0xff)) ^ td2(((s2 >> 16) & 0xff)) ^ td3((s1 >> 24)) ^ rk_constant[44];
			 t1 = td0((s1 & 0xff)) ^ td1(((s0 >> 8) & 0xff)) ^ td2(((s3 >> 16) & 0xff)) ^ td3((s2 >> 24)) ^ rk_constant[45];
			 t2 = td0((s2 & 0xff)) ^ td1(((s1 >> 8) & 0xff)) ^ td2(((s0 >> 16) & 0xff)) ^ td3((s3 >> 24)) ^ rk_constant[46];
			 t3 = td0((s3 & 0xff)) ^ td1(((s2 >> 8) & 0xff)) ^ td2(((s1 >> 16) & 0xff)) ^ td3((s0 >> 24)) ^ rk_constant[47];
			 if (Nr > 12) {
				 /* round 12: */
				 s0 = td0((t0 & 0xff)) ^ td1(((t3 >> 8) & 0xff)) ^ td2(((t2 >> 16) & 0xff)) ^ td3((t1 >> 24)) ^ rk_constant[48];
				 s1 = td0((t1 & 0xff)) ^ td1(((t0 >> 8) & 0xff)) ^ td2(((t3 >> 16) & 0xff)) ^ td3((t2 >> 24)) ^ rk_constant[49];
				 s2 = td0((t2 & 0xff)) ^ td1(((t1 >> 8) & 0xff)) ^ td2(((t0 >> 16) & 0xff)) ^ td3((t3 >> 24)) ^ rk_constant[50];
				 s3 = td0((t3 & 0xff)) ^ td1(((t2 >> 8) & 0xff)) ^ td2(((t1 >> 16) & 0xff)) ^ td3((t0 >> 24)) ^ rk_constant[51];
				 /* round 13: */
				 t0 = td0((s0 & 0xff)) ^ td1(((s3 >> 8) & 0xff)) ^ td2(((s2 >> 16) & 0xff)) ^ td3((s1 >> 24)) ^ rk_constant[52];
				 t1 = td0((s1 & 0xff)) ^ td1(((s0 >> 8) & 0xff)) ^ td2(((s3 >> 16) & 0xff)) ^ td3((s2 >> 24)) ^ rk_constant[53];
				 t2 = td0((s2 & 0xff)) ^ td1(((s1 >> 8) & 0xff)) ^ td2(((s0 >> 16) & 0xff)) ^ td3((s3 >> 24)) ^ rk_constant[54];
				 t3 = td0((s3 & 0xff)) ^ td1(((s2 >> 8) & 0xff)) ^ td2(((s1 >> 16) & 0xff)) ^ td3((s0 >> 24)) ^ rk_constant[55];
			 }
		 }

		 u32 x = Nr << 2;

		 /* Apply last round */
		 s0 = td4((t0 & 0xff), ((t3 >> 8) & 0xff), ((t2 >> 16) & 0xff), ((t1) >> 24)) ^ rk_constant[x++];
		 s1 = td4((t1 & 0xff), ((t0 >> 8) & 0xff), ((t3 >> 16) & 0xff), ((t2) >> 24)) ^ rk_constant[x++];
		 s2 = td4((t2 & 0xff), ((t1 >> 8) & 0xff), ((t0 >> 16) & 0xff), ((t3) >> 24)) ^ rk_constant[x++];
		 s3 = td4((t3 & 0xff), ((t2 >> 8) & 0xff), ((t1 >> 16) & 0xff), ((t0) >> 24)) ^ rk_constant[x];
			 

		 pt[0] = s0;
		 pt[1] = s1;
		 pt[2] = s2;
		 pt[3] = s3;



	 }




 }



 cudaError_t rijndaelEncryptCuda(const u8 pt[], u8 ct[], const u32 rk[], const unsigned int plainTextLength, const unsigned int Nr)
 {

	 boost::chrono::high_resolution_clock::time_point start;
	 duration<double, boost::milli> totalTime;
	 u32 *devPt;
	 u32 *devCt;


	 cudaError_t cudaStatus;


	 // Choose which GPU to run on, change this on a multi-GPU system.
	 cudaStatus = cudaSetDevice(0);
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		 return cudaStatus;
	 }

	 // cuda malloc
	 start = startStopwatch();
	 cudaStatus = cudaMalloc((void**)&devPt, plainTextLength * sizeof(u8));
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaMalloc failed!");
		 return cudaStatus;
	 }

	 cudaStatus = cudaMalloc((void**)&devCt, plainTextLength * sizeof(u8));
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaMalloc failed!");
		 return cudaStatus;
	 }

	 totalTime += endStopwatch("Malloc (encryption)", start);

	 start = startStopwatch();
	 cudaStatus = cudaMemcpy(devPt, pt, plainTextLength * sizeof(u8), cudaMemcpyHostToDevice);
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaMemcpy failed!");
		 return cudaStatus;
	 }

	 cudaStatus = cudaMemcpyToSymbol(rk_constant, rk, rkLength * sizeof(u32));
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaMemcpy failed!");
		 return cudaStatus;
	 }

	 totalTime += endStopwatch("Memory copy from host to device (encryption)", start);

	 //!!!must be padded before!!!

	 dim3 dimGrid((plainTextLength / BYTES_PER_BLOCK - 1) / THREAD_BLOCK_SIZE + 1, 1, 1);
	 dim3 dimBlock(THREAD_BLOCK_SIZE, 1, 1);

	 // Launch a kernel on the GPU with one thread for each element.
	 start = startStopwatch();
	 encryptKernel << <dimGrid, dimBlock >> >(devPt, devCt, plainTextLength / BYTES_PER_WORD, Nr);


	 // Check for any errors launching the kernel
	 cudaStatus = cudaGetLastError();
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		 return cudaStatus;
	 }

	 // cudaDeviceSynchronize waits for the kernel to finish, and returns
	 // any errors encountered during the launch.
	 cudaStatus = cudaDeviceSynchronize();
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		 return cudaStatus;
	 }

	 totalTime += endStopwatch("Encryption kernel", start);

	 // Copy output vector from GPU buffer to host memory.
	 start = startStopwatch();
	 cudaStatus = cudaMemcpy(ct, devCt, plainTextLength * sizeof(u8), cudaMemcpyDeviceToHost);
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaMemcpy failed!");
		 return cudaStatus;
	 }
	 totalTime += endStopwatch("Copy from device to host (encryption)", start);

	 start = startStopwatch();
	 cudaFree(devPt);
	 cudaFree(devCt);

	 totalTime += endStopwatch("Freeing memory (encryption)", start);

	 printDuration("Encryption", totalTime);

	 return cudaStatus;
 }


 cudaError_t rijndaelDecryptCuda(const u8 ct[], u8 pt[], const u32 rk[], const unsigned int plainTextLength, const unsigned int Nr)
 {

	 boost::chrono::high_resolution_clock::time_point start;
	 duration<double, boost::milli> totalTime;

	 u32 *devCt;
	 u32 *devPt;

	 cudaError_t cudaStatus;


	 // Choose which GPU to run on, change this on a multi-GPU system.
	 cudaStatus = cudaSetDevice(0);
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		 return cudaStatus;
	 }


	 // cuda malloc
	 start = startStopwatch();
	 cudaStatus = cudaMalloc((void**)&devCt, plainTextLength * sizeof(u8));
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaMalloc failed!");
		 return cudaStatus;
	 }

	 cudaStatus = cudaMalloc((void**)&devPt, plainTextLength * sizeof(u8));
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaMalloc failed!");
		 return cudaStatus;
	 }

	 totalTime += endStopwatch("Malloc (decryption)", start);

	 start = startStopwatch();
	 cudaStatus = cudaMemcpy(devCt, ct, plainTextLength * sizeof(u8), cudaMemcpyHostToDevice);
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaMemcpy failed!");
		 return cudaStatus;
	 }

	 cudaStatus = cudaMemcpyToSymbol(rk_constant, rk, rkLength * sizeof(u32));
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaMemcpy failed!");
		 return cudaStatus;
	 }

	 totalTime += endStopwatch("Memory copy from host to device (decryption)", start);

	 //!!!must be padded before!!!

	 dim3 dimGrid((plainTextLength / BYTES_PER_BLOCK - 1) / THREAD_BLOCK_SIZE + 1, 1, 1);
	 dim3 dimBlock(THREAD_BLOCK_SIZE, 1, 1);

	 // Launch a kernel on the GPU with one thread for each element.
	 start = startStopwatch();
	 decryptKernel << <dimGrid, dimBlock >> >(devCt, devPt, plainTextLength / BYTES_PER_WORD, Nr);


	 // Check for any errors launching the kernel
	 cudaStatus = cudaGetLastError();
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		 return cudaStatus;
	 }

	 // cudaDeviceSynchronize waits for the kernel to finish, and returns
	 // any errors encountered during the launch.
	 cudaStatus = cudaDeviceSynchronize();
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		 return cudaStatus;
	 }
	 totalTime += endStopwatch("Decryption kernel", start);

	 // Copy output vector from GPU buffer to host memory.
	 start = startStopwatch();
	 cudaStatus = cudaMemcpy(pt, devPt, plainTextLength * sizeof(u8), cudaMemcpyDeviceToHost);
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaMemcpy failed!");
		 return cudaStatus;
	 }
	 totalTime += endStopwatch("Copy from device to host (decryption)", start);

	 start = startStopwatch();
	 cudaFree(devPt);
	 cudaFree(devCt);

	 totalTime += endStopwatch("Freeing memory (decryption)", start);
	 
	 printDuration("Decryption", totalTime);
	 
	 return cudaStatus;
 }

 int rijndaelKeySetupEnc(u32 rk[], const u32 cipherKey[], const unsigned int keyLen) {
	 int i = 0;
	 u32 temp;

	 rk[0] = cipherKey[0];
	 rk[1] = cipherKey[1];
	 rk[2] = cipherKey[2];
	 rk[3] = cipherKey[3];

	 if (keyLen == 16) {
		 for (;;) {
			 temp = rk[3];
			 rk[4] = rk[0] ^
				 (bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				 (bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				 (bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				 (bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				 rcon[i];

			 rk[5] = rk[1] ^ rk[4];
			 rk[6] = rk[2] ^ rk[5];
			 rk[7] = rk[3] ^ rk[6];
			 if (++i == 10) {
				 return 10;
			 }
			 rk += 4;
		 }
	 }
	 rk[4] = cipherKey[4];
	 rk[5] = cipherKey[5];

	 if (keyLen == 24) {
		 for (;;) {
			 temp = rk[5];
			 rk[6] = rk[0] ^
				 (bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				 (bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				 (bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				 (bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				 rcon[i];
			 rk[7] = rk[1] ^ rk[6];
			 rk[8] = rk[2] ^ rk[7];
			 rk[9] = rk[3] ^ rk[8];
			 if (++i == 8) {
				 return 12;
			 }
			 rk[10] = rk[4] ^ rk[9];
			 rk[11] = rk[5] ^ rk[10];
			 rk += 6;
		 }
	 }
	 rk[6] = cipherKey[6];
	 rk[7] = cipherKey[7];
	 if (keyLen == 32) {
		 for (;;) {
			 temp = rk[7];
			 rk[8] = rk[0] ^
				 (bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				 (bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				 (bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				 (bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				 rcon[i];
			 rk[9] = rk[1] ^ rk[8];
			 rk[10] = rk[2] ^ rk[9];
			 rk[11] = rk[3] ^ rk[10];
			 if (++i == 7) {
				 return 14;
			 }
			 temp = rk[11];
			 rk[12] = rk[4] ^
				 (bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0xff000000) ^
				 (bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x00ff0000) ^
				 (bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x0000ff00) ^
				 (bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0x000000ff);
			 rk[13] = rk[5] ^ rk[12];
			 rk[14] = rk[6] ^ rk[13];
			 rk[15] = rk[7] ^ rk[14];

			 rk += 8;
		 }
	 }
	 return 0;
 }



 int rijndaelKeySetupDec(u32 rk[], const u32 cipherKey[], const unsigned int keyLen) {
	 int Nr, i, j;
	 u32 temp;

	 /* expand the cipher key: */
	 Nr = rijndaelKeySetupEnc(rk, (u32 *)cipherKey, keyLen);
	 /* invert the order of the round keys: */
	 for (i = 0, j = 4 * Nr; i < j; i += 4, j -= 4) {
		 temp = rk[i]; rk[i] = rk[j]; rk[j] = temp;
		 temp = rk[i + 1]; rk[i + 1] = rk[j + 1]; rk[j + 1] = temp;
		 temp = rk[i + 2]; rk[i + 2] = rk[j + 2]; rk[j + 2] = temp;
		 temp = rk[i + 3]; rk[i + 3] = rk[j + 3]; rk[j + 3] = temp;
	 }
	 /* apply the inverse MixColumn transform to all round keys but the first and the last: */

	 for (i = 1; i < Nr; i++) {
		 rk += 4;


		 rk[0] =
			 bigTd[0 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[0]) & 0xff)] & 0xff)] ^
			 bigTd[1 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[0] >> 8) & 0xff)] & 0xff)] ^
			 bigTd[2 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[0] >> 16) & 0xff)] & 0xff)] ^
			 bigTd[3 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + (rk[0] >> 24)] & 0xff)];


		 rk[1] =
			 bigTd[0 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[1]) & 0xff)] & 0xff)] ^
			 bigTd[1 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[1] >> 8) & 0xff)] & 0xff)] ^
			 bigTd[2 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[1] >> 16) & 0xff)] & 0xff)] ^
			 bigTd[3 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + (rk[1] >> 24)] & 0xff)];


		 rk[2] =
			 bigTd[0 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[2]) & 0xff)] & 0xff)] ^
			 bigTd[1 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[2] >> 8) & 0xff)] & 0xff)] ^
			 bigTd[2 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[2] >> 16) & 0xff)] & 0xff)] ^
			 bigTd[3 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + (rk[2] >> 24)] & 0xff)];

		 rk[3] =
			 bigTd[0 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[3]) & 0xff)] & 0xff)] ^
			 bigTd[1 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[3] >> 8) & 0xff)] & 0xff)] ^
			 bigTd[2 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[3] >> 16) & 0xff)] & 0xff)] ^
			 bigTd[3 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + (rk[3] >> 24)] & 0xff)];



	 }
	 return Nr;
 }

 int rijndaelEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
 {
	 
	 boost::chrono::high_resolution_clock::time_point start;

	 cudaFree(NULL);
	 u32 rk[rkLength];

	 u8 *pinnedPt;
	 u8 *pinnedCt;

	 cudaMallocHost(&pinnedPt, plainTextLength);
	 cudaMallocHost(&pinnedCt, plainTextLength);

	 memcpy(pinnedPt, pt, plainTextLength);

	 start = startStopwatch();
	 unsigned int Nr = rijndaelKeySetupEnc(rk, (u32 *)cipherKey, cipherKeyLength);
	 endStopwatch("Encryption key calculation", start);


	 cudaError_t cudaStatus = rijndaelEncryptCuda(pinnedPt, pinnedCt, rk, plainTextLength, Nr);


	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "Encryption failed!");
		 return 1;
	 }

	 memcpy(ct, pinnedCt, plainTextLength);
	 cudaFreeHost(pinnedPt);
	 cudaFreeHost(pinnedCt);


	 // cudaDeviceReset must be called before exiting in order for profiling and
	 // tracing tools such as Nsight and Visual Profiler to show complete traces.
	 cudaStatus = cudaDeviceReset();
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaDeviceReset failed!");
		 return 1;
	 }




	 return 0;
 }

 int rijndaelDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
 {

	 boost::chrono::high_resolution_clock::time_point start;

	 cudaFree(NULL);
	 u32 rk[rkLength];
	 u8 *pinnedCt;
	 u8 *pinnedPt;

	 cudaMallocHost(&pinnedCt, plainTextLength);
	 cudaMallocHost(&pinnedPt, plainTextLength);

	 memcpy(pinnedCt, ct, plainTextLength);

	 start = startStopwatch();
	 unsigned int Nr = rijndaelKeySetupDec(rk, (u32 *)cipherKey, cipherKeyLength);
	 endStopwatch("Decryption key calculation", start);


	 cudaError_t cudaStatus = rijndaelDecryptCuda(pinnedCt, pinnedPt, rk, plainTextLength, Nr);


	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "Decription failed!");
		 return 1;
	 }

	 memcpy(pt, pinnedPt, plainTextLength);
	 cudaFreeHost(pinnedCt);
	 cudaFreeHost(pinnedPt);

	 // cudaDeviceReset must be called before exiting in order for profiling and
	 // tracing tools such as Nsight and Visual Profiler to show complete traces.
	 cudaStatus = cudaDeviceReset();
	 if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaDeviceReset failed!");
		 return 1;
	 }

	 return 0;
 }
