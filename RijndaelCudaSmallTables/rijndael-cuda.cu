
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "rijndael-cuda.cuh"
#include "rijndael-tables.cuh"
#include <stdio.h>
#include <stdlib.h>
#include "measure-time.h"
#include <boost/chrono.hpp>

int rijndaelKeySetupEnc(u32 rk[/*4*(Nr + 1)*/], const u32 cipherKey[], const unsigned int keyLen) {
	int i = 0;
	u32 temp;

	rk[0] = cipherKey[0];
	rk[1] = cipherKey[1];
	rk[2] = cipherKey[2];
	rk[3] = cipherKey[3];

	if (keyLen == 16) {
		for (;;) {
			temp = rk[3];
			rk[4] = rk[0] ^
				(Te4[((temp)& 0xff)] & 0xff000000) ^
				(Te4[(temp >> 24)] & 0x00ff0000) ^
				(Te4[((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(Te4[((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[5] = rk[1] ^ rk[4];
			rk[6] = rk[2] ^ rk[5];
			rk[7] = rk[3] ^ rk[6];
			if (++i == 10) {
				return 10;
			}
			rk += 4;
		}
	}
	rk[4] = cipherKey[4];
	rk[5] = cipherKey[5];

	if (keyLen == 24) {
		for (;;) {
			temp = rk[5];
			rk[6] = rk[0] ^
				(Te4[((temp)& 0xff)] & 0xff000000) ^
				(Te4[(temp >> 24)] & 0x00ff0000) ^
				(Te4[((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(Te4[((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[7] = rk[1] ^ rk[6];
			rk[8] = rk[2] ^ rk[7];
			rk[9] = rk[3] ^ rk[8];
			if (++i == 8) {
				return 12;
			}
			rk[10] = rk[4] ^ rk[9];
			rk[11] = rk[5] ^ rk[10];
			rk += 6;
		}
	}
	rk[6] = cipherKey[6];
	rk[7] = cipherKey[7];
	if (keyLen == 32) {
		for (;;) {
			temp = rk[7];
			rk[8] = rk[0] ^
				(Te4[((temp)& 0xff)] & 0xff000000) ^
				(Te4[(temp >> 24)] & 0x00ff0000) ^
				(Te4[((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(Te4[((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[9] = rk[1] ^ rk[8];
			rk[10] = rk[2] ^ rk[9];
			rk[11] = rk[3] ^ rk[10];
			if (++i == 7) {
				return 14;
			}
			temp = rk[11];
			rk[12] = rk[4] ^
				(Te4[(temp >> 24)] & 0xff000000) ^
				(Te4[(temp >> 16) & 0xff] & 0x00ff0000) ^
				(Te4[(temp >> 8) & 0xff] & 0x0000ff00) ^
				(Te4[(temp)& 0xff] & 0x000000ff);
			rk[13] = rk[5] ^ rk[12];
			rk[14] = rk[6] ^ rk[13];
			rk[15] = rk[7] ^ rk[14];

			rk += 8;
		}
	}
	return 0;
}

int rijndaelKeySetupDec(u32 rk[/*4*(Nr + 1)*/], const u32 cipherKey[], const unsigned int keyLen) {
	int Nr, i, j;
	u32 temp;

	/* expand the cipher key: */
	Nr = rijndaelKeySetupEnc(rk, cipherKey, keyLen);
	/* invert the order of the round keys: */
	for (i = 0, j = 4 * Nr; i < j; i += 4, j -= 4) {
		temp = rk[i]; rk[i] = rk[j]; rk[j] = temp;
		temp = rk[i + 1]; rk[i + 1] = rk[j + 1]; rk[j + 1] = temp;
		temp = rk[i + 2]; rk[i + 2] = rk[j + 2]; rk[j + 2] = temp;
		temp = rk[i + 3]; rk[i + 3] = rk[j + 3]; rk[j + 3] = temp;
	}
	/* apply the inverse MixColumn transform to all round keys but the first and the last: */
	for (i = 1; i < Nr; i++) {
		rk += 4;
		rk[0] =
			Td0[Te4[((rk[0]) & 0xff)] & 0xff] ^
			Td1[Te4[((rk[0] >> 8) & 0xff)] & 0xff] ^
			Td2[Te4[((rk[0] >> 16) & 0xff)] & 0xff] ^
			Td3[Te4[(rk[0] >> 24)] & 0xff];
		rk[1] =
			Td0[Te4[((rk[1]) & 0xff)] & 0xff] ^
			Td1[Te4[((rk[1] >> 8) & 0xff)] & 0xff] ^
			Td2[Te4[((rk[1] >> 16) & 0xff)] & 0xff] ^
			Td3[Te4[(rk[1] >> 24)] & 0xff];
		rk[2] =
			Td0[Te4[((rk[2]) & 0xff)] & 0xff] ^
			Td1[Te4[((rk[2] >> 8) & 0xff)] & 0xff] ^
			Td2[Te4[((rk[2] >> 16) & 0xff)] & 0xff] ^
			Td3[Te4[(rk[2] >> 24)] & 0xff];
		rk[3] =
			Td0[Te4[((rk[3]) & 0xff)] & 0xff] ^
			Td1[Te4[((rk[3] >> 8) & 0xff)] & 0xff] ^
			Td2[Te4[((rk[3] >> 16) & 0xff)] & 0xff] ^
			Td3[Te4[(rk[3] >> 24)] & 0xff];
	}
	return Nr;
}

__constant__ u32 rk_constant[rkLength];

__global__ void encryptKernel(uint4 *pt, uint4 *ct, const u32 * Te0, const u32 * Te1, const u32 * Te2, const u32 * Te3, const u32 * Te4, const unsigned int blocks, const unsigned int Nr)
{


	int t = threadIdx.x;
	int i = blockDim.x * blockIdx.x + t;


	__shared__ u32 Te0_sh[T_TABLE_SIZE];
	__shared__ u32 Te1_sh[T_TABLE_SIZE];
	__shared__ u32 Te2_sh[T_TABLE_SIZE];
	__shared__ u32 Te3_sh[T_TABLE_SIZE];
	__shared__ u32 Te4_sh[T_TABLE_SIZE];

	if (t < T_TABLE_SIZE){

		Te0_sh[t] = Te0[t];
		Te1_sh[t] = Te1[t];
		Te2_sh[t] = Te2[t];
		Te3_sh[t] = Te3[t];
		Te4_sh[t] = Te4[t];

	}


	__syncthreads();



	if (i < blocks){


		register u32 s0, s1, s2, s3, t0, t1, t2, t3;

		s0 = pt[i].x ^ rk_constant[0];
		s1 = pt[i].y ^ rk_constant[1];
		s2 = pt[i].z ^ rk_constant[2];
		s3 = pt[i].w ^ rk_constant[3];

		/* round 1: */
		t0 = Te0_sh[s0 & 0xff] ^ Te1_sh[(s1 >> 8) & 0xff] ^ Te2_sh[(s2 >> 16) & 0xff] ^ Te3_sh[s3 >> 24] ^ rk_constant[4];
		t1 = Te0_sh[s1 & 0xff] ^ Te1_sh[(s2 >> 8) & 0xff] ^ Te2_sh[(s3 >> 16) & 0xff] ^ Te3_sh[s0 >> 24] ^ rk_constant[5];
		t2 = Te0_sh[s2 & 0xff] ^ Te1_sh[(s3 >> 8) & 0xff] ^ Te2_sh[(s0 >> 16) & 0xff] ^ Te3_sh[s1 >> 24] ^ rk_constant[6];
		t3 = Te0_sh[s3 & 0xff] ^ Te1_sh[(s0 >> 8) & 0xff] ^ Te2_sh[(s1 >> 16) & 0xff] ^ Te3_sh[s2 >> 24] ^ rk_constant[7];
		/* round 2: */
		s0 = Te0_sh[t0 & 0xff] ^ Te1_sh[(t1 >> 8) & 0xff] ^ Te2_sh[(t2 >> 16) & 0xff] ^ Te3_sh[t3 >> 24] ^ rk_constant[8];
		s1 = Te0_sh[t1 & 0xff] ^ Te1_sh[(t2 >> 8) & 0xff] ^ Te2_sh[(t3 >> 16) & 0xff] ^ Te3_sh[t0 >> 24] ^ rk_constant[9];
		s2 = Te0_sh[t2 & 0xff] ^ Te1_sh[(t3 >> 8) & 0xff] ^ Te2_sh[(t0 >> 16) & 0xff] ^ Te3_sh[t1 >> 24] ^ rk_constant[10];
		s3 = Te0_sh[t3 & 0xff] ^ Te1_sh[(t0 >> 8) & 0xff] ^ Te2_sh[(t1 >> 16) & 0xff] ^ Te3_sh[t2 >> 24] ^ rk_constant[11];
		/* round 3: */
		t0 = Te0_sh[s0 & 0xff] ^ Te1_sh[(s1 >> 8) & 0xff] ^ Te2_sh[(s2 >> 16) & 0xff] ^ Te3_sh[s3 >> 24] ^ rk_constant[12];
		t1 = Te0_sh[s1 & 0xff] ^ Te1_sh[(s2 >> 8) & 0xff] ^ Te2_sh[(s3 >> 16) & 0xff] ^ Te3_sh[s0 >> 24] ^ rk_constant[13];
		t2 = Te0_sh[s2 & 0xff] ^ Te1_sh[(s3 >> 8) & 0xff] ^ Te2_sh[(s0 >> 16) & 0xff] ^ Te3_sh[s1 >> 24] ^ rk_constant[14];
		t3 = Te0_sh[s3 & 0xff] ^ Te1_sh[(s0 >> 8) & 0xff] ^ Te2_sh[(s1 >> 16) & 0xff] ^ Te3_sh[s2 >> 24] ^ rk_constant[15];
		/* round 4: */
		s0 = Te0_sh[t0 & 0xff] ^ Te1_sh[(t1 >> 8) & 0xff] ^ Te2_sh[(t2 >> 16) & 0xff] ^ Te3_sh[t3 >> 24] ^ rk_constant[16];
		s1 = Te0_sh[t1 & 0xff] ^ Te1_sh[(t2 >> 8) & 0xff] ^ Te2_sh[(t3 >> 16) & 0xff] ^ Te3_sh[t0 >> 24] ^ rk_constant[17];
		s2 = Te0_sh[t2 & 0xff] ^ Te1_sh[(t3 >> 8) & 0xff] ^ Te2_sh[(t0 >> 16) & 0xff] ^ Te3_sh[t1 >> 24] ^ rk_constant[18];
		s3 = Te0_sh[t3 & 0xff] ^ Te1_sh[(t0 >> 8) & 0xff] ^ Te2_sh[(t1 >> 16) & 0xff] ^ Te3_sh[t2 >> 24] ^ rk_constant[19];
		/* round 5: */
		t0 = Te0_sh[s0 & 0xff] ^ Te1_sh[(s1 >> 8) & 0xff] ^ Te2_sh[(s2 >> 16) & 0xff] ^ Te3_sh[s3 >> 24] ^ rk_constant[20];
		t1 = Te0_sh[s1 & 0xff] ^ Te1_sh[(s2 >> 8) & 0xff] ^ Te2_sh[(s3 >> 16) & 0xff] ^ Te3_sh[s0 >> 24] ^ rk_constant[21];
		t2 = Te0_sh[s2 & 0xff] ^ Te1_sh[(s3 >> 8) & 0xff] ^ Te2_sh[(s0 >> 16) & 0xff] ^ Te3_sh[s1 >> 24] ^ rk_constant[22];
		t3 = Te0_sh[s3 & 0xff] ^ Te1_sh[(s0 >> 8) & 0xff] ^ Te2_sh[(s1 >> 16) & 0xff] ^ Te3_sh[s2 >> 24] ^ rk_constant[23];
		/* round 6: */
		s0 = Te0_sh[t0 & 0xff] ^ Te1_sh[(t1 >> 8) & 0xff] ^ Te2_sh[(t2 >> 16) & 0xff] ^ Te3_sh[t3 >> 24] ^ rk_constant[24];
		s1 = Te0_sh[t1 & 0xff] ^ Te1_sh[(t2 >> 8) & 0xff] ^ Te2_sh[(t3 >> 16) & 0xff] ^ Te3_sh[t0 >> 24] ^ rk_constant[25];
		s2 = Te0_sh[t2 & 0xff] ^ Te1_sh[(t3 >> 8) & 0xff] ^ Te2_sh[(t0 >> 16) & 0xff] ^ Te3_sh[t1 >> 24] ^ rk_constant[26];
		s3 = Te0_sh[t3 & 0xff] ^ Te1_sh[(t0 >> 8) & 0xff] ^ Te2_sh[(t1 >> 16) & 0xff] ^ Te3_sh[t2 >> 24] ^ rk_constant[27];
		/* round 7: */
		t0 = Te0_sh[s0 & 0xff] ^ Te1_sh[(s1 >> 8) & 0xff] ^ Te2_sh[(s2 >> 16) & 0xff] ^ Te3_sh[s3 >> 24] ^ rk_constant[28];
		t1 = Te0_sh[s1 & 0xff] ^ Te1_sh[(s2 >> 8) & 0xff] ^ Te2_sh[(s3 >> 16) & 0xff] ^ Te3_sh[s0 >> 24] ^ rk_constant[29];
		t2 = Te0_sh[s2 & 0xff] ^ Te1_sh[(s3 >> 8) & 0xff] ^ Te2_sh[(s0 >> 16) & 0xff] ^ Te3_sh[s1 >> 24] ^ rk_constant[30];
		t3 = Te0_sh[s3 & 0xff] ^ Te1_sh[(s0 >> 8) & 0xff] ^ Te2_sh[(s1 >> 16) & 0xff] ^ Te3_sh[s2 >> 24] ^ rk_constant[31];
		/* round 8: */
		s0 = Te0_sh[t0 & 0xff] ^ Te1_sh[(t1 >> 8) & 0xff] ^ Te2_sh[(t2 >> 16) & 0xff] ^ Te3_sh[t3 >> 24] ^ rk_constant[32];
		s1 = Te0_sh[t1 & 0xff] ^ Te1_sh[(t2 >> 8) & 0xff] ^ Te2_sh[(t3 >> 16) & 0xff] ^ Te3_sh[t0 >> 24] ^ rk_constant[33];
		s2 = Te0_sh[t2 & 0xff] ^ Te1_sh[(t3 >> 8) & 0xff] ^ Te2_sh[(t0 >> 16) & 0xff] ^ Te3_sh[t1 >> 24] ^ rk_constant[34];
		s3 = Te0_sh[t3 & 0xff] ^ Te1_sh[(t0 >> 8) & 0xff] ^ Te2_sh[(t1 >> 16) & 0xff] ^ Te3_sh[t2 >> 24] ^ rk_constant[35];
		/* round 9: */
		t0 = Te0_sh[s0 & 0xff] ^ Te1_sh[(s1 >> 8) & 0xff] ^ Te2_sh[(s2 >> 16) & 0xff] ^ Te3_sh[s3 >> 24] ^ rk_constant[36];
		t1 = Te0_sh[s1 & 0xff] ^ Te1_sh[(s2 >> 8) & 0xff] ^ Te2_sh[(s3 >> 16) & 0xff] ^ Te3_sh[s0 >> 24] ^ rk_constant[37];
		t2 = Te0_sh[s2 & 0xff] ^ Te1_sh[(s3 >> 8) & 0xff] ^ Te2_sh[(s0 >> 16) & 0xff] ^ Te3_sh[s1 >> 24] ^ rk_constant[38];
		t3 = Te0_sh[s3 & 0xff] ^ Te1_sh[(s0 >> 8) & 0xff] ^ Te2_sh[(s1 >> 16) & 0xff] ^ Te3_sh[s2 >> 24] ^ rk_constant[39];

		if (Nr > 10) {
			/* round 10: */
			s0 = Te0_sh[t0 & 0xff] ^ Te1_sh[(t1 >> 8) & 0xff] ^ Te2_sh[(t2 >> 16) & 0xff] ^ Te3_sh[t3 >> 24] ^ rk_constant[40];
			s1 = Te0_sh[t1 & 0xff] ^ Te1_sh[(t2 >> 8) & 0xff] ^ Te2_sh[(t3 >> 16) & 0xff] ^ Te3_sh[t0 >> 24] ^ rk_constant[41];
			s2 = Te0_sh[t2 & 0xff] ^ Te1_sh[(t3 >> 8) & 0xff] ^ Te2_sh[(t0 >> 16) & 0xff] ^ Te3_sh[t1 >> 24] ^ rk_constant[42];
			s3 = Te0_sh[t3 & 0xff] ^ Te1_sh[(t0 >> 8) & 0xff] ^ Te2_sh[(t1 >> 16) & 0xff] ^ Te3_sh[t2 >> 24] ^ rk_constant[43];
			/* round 11: */
			t0 = Te0_sh[s0 & 0xff] ^ Te1_sh[(s1 >> 8) & 0xff] ^ Te2_sh[(s2 >> 16) & 0xff] ^ Te3_sh[s3 >> 24] ^ rk_constant[44];
			t1 = Te0_sh[s1 & 0xff] ^ Te1_sh[(s2 >> 8) & 0xff] ^ Te2_sh[(s3 >> 16) & 0xff] ^ Te3_sh[s0 >> 24] ^ rk_constant[45];
			t2 = Te0_sh[s2 & 0xff] ^ Te1_sh[(s3 >> 8) & 0xff] ^ Te2_sh[(s0 >> 16) & 0xff] ^ Te3_sh[s1 >> 24] ^ rk_constant[46];
			t3 = Te0_sh[s3 & 0xff] ^ Te1_sh[(s0 >> 8) & 0xff] ^ Te2_sh[(s1 >> 16) & 0xff] ^ Te3_sh[s2 >> 24] ^ rk_constant[47];
			if (Nr > 12) {
				/* round 12: */
				s0 = Te0_sh[t0 & 0xff] ^ Te1_sh[(t1 >> 8) & 0xff] ^ Te2_sh[(t2 >> 16) & 0xff] ^ Te3_sh[t3 >> 24] ^ rk_constant[48];
				s1 = Te0_sh[t1 & 0xff] ^ Te1_sh[(t2 >> 8) & 0xff] ^ Te2_sh[(t3 >> 16) & 0xff] ^ Te3_sh[t0 >> 24] ^ rk_constant[49];
				s2 = Te0_sh[t2 & 0xff] ^ Te1_sh[(t3 >> 8) & 0xff] ^ Te2_sh[(t0 >> 16) & 0xff] ^ Te3_sh[t1 >> 24] ^ rk_constant[50];
				s3 = Te0_sh[t3 & 0xff] ^ Te1_sh[(t0 >> 8) & 0xff] ^ Te2_sh[(t1 >> 16) & 0xff] ^ Te3_sh[t2 >> 24] ^ rk_constant[51];
				/* round 13: */
				t0 = Te0_sh[s0 & 0xff] ^ Te1_sh[(s1 >> 8) & 0xff] ^ Te2_sh[(s2 >> 16) & 0xff] ^ Te3_sh[s3 >> 24] ^ rk_constant[52];
				t1 = Te0_sh[s1 & 0xff] ^ Te1_sh[(s2 >> 8) & 0xff] ^ Te2_sh[(s3 >> 16) & 0xff] ^ Te3_sh[s0 >> 24] ^ rk_constant[53];
				t2 = Te0_sh[s2 & 0xff] ^ Te1_sh[(s3 >> 8) & 0xff] ^ Te2_sh[(s0 >> 16) & 0xff] ^ Te3_sh[s1 >> 24] ^ rk_constant[54];
				t3 = Te0_sh[s3 & 0xff] ^ Te1_sh[(s0 >> 8) & 0xff] ^ Te2_sh[(s1 >> 16) & 0xff] ^ Te3_sh[s2 >> 24] ^ rk_constant[55];
			}
		}

		u32 x = Nr << 2;

		s0 =
			(Te4_sh[(t0 & 0xff)] & 0x000000ff) ^
			(Te4_sh[((t1 >> 8) & 0xff)] & 0x0000ff00) ^
			(Te4_sh[((t2 >> 16) & 0xff)] & 0x00ff0000) ^
			(Te4_sh[((t3) >> 24)] & 0xff000000) ^
			rk_constant[x++];

		s1 =
			(Te4_sh[(t1 & 0xff)] & 0x000000ff) ^
			(Te4_sh[((t2 >> 8) & 0xff)] & 0x0000ff00) ^
			(Te4_sh[((t3 >> 16) & 0xff)] & 0x00ff0000) ^
			(Te4_sh[((t0) >> 24)] & 0xff000000) ^
			rk_constant[x++];

		s2 =
			(Te4_sh[(t2 & 0xff)] & 0x000000ff) ^
			(Te4_sh[((t3 >> 8) & 0xff)] & 0x0000ff00) ^
			(Te4_sh[((t0 >> 16) & 0xff)] & 0x00ff0000) ^
			(Te4_sh[((t1) >> 24)] & 0xff000000) ^
			rk_constant[x++];

		s3 =
			(Te4_sh[(t3 & 0xff)] & 0x000000ff) ^
			(Te4_sh[((t0 >> 8) & 0xff)] & 0x0000ff00) ^
			(Te4_sh[((t1 >> 16) & 0xff)] & 0x00ff0000) ^
			(Te4_sh[((t2) >> 24)] & 0xff000000) ^
			rk_constant[x];

		ct[i] = { s0, s1, s2, s3 };


	}




}

__global__ void decryptKernel(uint4 *ct, uint4 *pt, const u32 *  Td0, const u32 *  Td1, const u32 *  Td2, const u32 *  Td3, const u32 *  Td4, const unsigned int blocks, const unsigned int Nr)
{


	int t = threadIdx.x;
	int i = blockDim.x * blockIdx.x + t;


	__shared__ u32 Td0_sh[T_TABLE_SIZE];
	__shared__ u32 Td1_sh[T_TABLE_SIZE];
	__shared__ u32 Td2_sh[T_TABLE_SIZE];
	__shared__ u32 Td3_sh[T_TABLE_SIZE];
	__shared__ u32 Td4_sh[T_TABLE_SIZE];


	if (t < T_TABLE_SIZE){

		Td0_sh[t] = Td0[t];
		Td1_sh[t] = Td1[t];
		Td2_sh[t] = Td2[t];
		Td3_sh[t] = Td3[t];
		Td4_sh[t] = Td4[t];


	}

	__syncthreads();


	if (i < blocks){



		register u32 s0, s1, s2, s3, t0, t1, t2, t3;


		s0 = ct[i].x ^ rk_constant[0];
		s1 = ct[i].y ^ rk_constant[1];
		s2 = ct[i].z ^ rk_constant[2];
		s3 = ct[i].w ^ rk_constant[3];

		/* round 1: */
		t0 = Td0_sh[s0 & 0xff] ^ Td1_sh[(s3 >> 8) & 0xff] ^ Td2_sh[(s2 >> 16) & 0xff] ^ Td3_sh[s1 >> 24] ^ rk_constant[4];
		t1 = Td0_sh[s1 & 0xff] ^ Td1_sh[(s0 >> 8) & 0xff] ^ Td2_sh[(s3 >> 16) & 0xff] ^ Td3_sh[s2 >> 24] ^ rk_constant[5];
		t2 = Td0_sh[s2 & 0xff] ^ Td1_sh[(s1 >> 8) & 0xff] ^ Td2_sh[(s0 >> 16) & 0xff] ^ Td3_sh[s3 >> 24] ^ rk_constant[6];
		t3 = Td0_sh[s3 & 0xff] ^ Td1_sh[(s2 >> 8) & 0xff] ^ Td2_sh[(s1 >> 16) & 0xff] ^ Td3_sh[s0 >> 24] ^ rk_constant[7];
		/* round 2: */
		s0 = Td0_sh[t0 & 0xff] ^ Td1_sh[(t3 >> 8) & 0xff] ^ Td2_sh[(t2 >> 16) & 0xff] ^ Td3_sh[t1 >> 24] ^ rk_constant[8];
		s1 = Td0_sh[t1 & 0xff] ^ Td1_sh[(t0 >> 8) & 0xff] ^ Td2_sh[(t3 >> 16) & 0xff] ^ Td3_sh[t2 >> 24] ^ rk_constant[9];
		s2 = Td0_sh[t2 & 0xff] ^ Td1_sh[(t1 >> 8) & 0xff] ^ Td2_sh[(t0 >> 16) & 0xff] ^ Td3_sh[t3 >> 24] ^ rk_constant[10];
		s3 = Td0_sh[t3 & 0xff] ^ Td1_sh[(t2 >> 8) & 0xff] ^ Td2_sh[(t1 >> 16) & 0xff] ^ Td3_sh[t0 >> 24] ^ rk_constant[11];
		/* round 3: */
		t0 = Td0_sh[s0 & 0xff] ^ Td1_sh[(s3 >> 8) & 0xff] ^ Td2_sh[(s2 >> 16) & 0xff] ^ Td3_sh[s1 >> 24] ^ rk_constant[12];
		t1 = Td0_sh[s1 & 0xff] ^ Td1_sh[(s0 >> 8) & 0xff] ^ Td2_sh[(s3 >> 16) & 0xff] ^ Td3_sh[s2 >> 24] ^ rk_constant[13];
		t2 = Td0_sh[s2 & 0xff] ^ Td1_sh[(s1 >> 8) & 0xff] ^ Td2_sh[(s0 >> 16) & 0xff] ^ Td3_sh[s3 >> 24] ^ rk_constant[14];
		t3 = Td0_sh[s3 & 0xff] ^ Td1_sh[(s2 >> 8) & 0xff] ^ Td2_sh[(s1 >> 16) & 0xff] ^ Td3_sh[s0 >> 24] ^ rk_constant[15];
		/* round 4: */
		s0 = Td0_sh[t0 & 0xff] ^ Td1_sh[(t3 >> 8) & 0xff] ^ Td2_sh[(t2 >> 16) & 0xff] ^ Td3_sh[t1 >> 24] ^ rk_constant[16];
		s1 = Td0_sh[t1 & 0xff] ^ Td1_sh[(t0 >> 8) & 0xff] ^ Td2_sh[(t3 >> 16) & 0xff] ^ Td3_sh[t2 >> 24] ^ rk_constant[17];
		s2 = Td0_sh[t2 & 0xff] ^ Td1_sh[(t1 >> 8) & 0xff] ^ Td2_sh[(t0 >> 16) & 0xff] ^ Td3_sh[t3 >> 24] ^ rk_constant[18];
		s3 = Td0_sh[t3 & 0xff] ^ Td1_sh[(t2 >> 8) & 0xff] ^ Td2_sh[(t1 >> 16) & 0xff] ^ Td3_sh[t0 >> 24] ^ rk_constant[19];
		/* round 5: */
		t0 = Td0_sh[s0 & 0xff] ^ Td1_sh[(s3 >> 8) & 0xff] ^ Td2_sh[(s2 >> 16) & 0xff] ^ Td3_sh[s1 >> 24] ^ rk_constant[20];
		t1 = Td0_sh[s1 & 0xff] ^ Td1_sh[(s0 >> 8) & 0xff] ^ Td2_sh[(s3 >> 16) & 0xff] ^ Td3_sh[s2 >> 24] ^ rk_constant[21];
		t2 = Td0_sh[s2 & 0xff] ^ Td1_sh[(s1 >> 8) & 0xff] ^ Td2_sh[(s0 >> 16) & 0xff] ^ Td3_sh[s3 >> 24] ^ rk_constant[22];
		t3 = Td0_sh[s3 & 0xff] ^ Td1_sh[(s2 >> 8) & 0xff] ^ Td2_sh[(s1 >> 16) & 0xff] ^ Td3_sh[s0 >> 24] ^ rk_constant[23];
		/* round 6: */
		s0 = Td0_sh[t0 & 0xff] ^ Td1_sh[(t3 >> 8) & 0xff] ^ Td2_sh[(t2 >> 16) & 0xff] ^ Td3_sh[t1 >> 24] ^ rk_constant[24];
		s1 = Td0_sh[t1 & 0xff] ^ Td1_sh[(t0 >> 8) & 0xff] ^ Td2_sh[(t3 >> 16) & 0xff] ^ Td3_sh[t2 >> 24] ^ rk_constant[25];
		s2 = Td0_sh[t2 & 0xff] ^ Td1_sh[(t1 >> 8) & 0xff] ^ Td2_sh[(t0 >> 16) & 0xff] ^ Td3_sh[t3 >> 24] ^ rk_constant[26];
		s3 = Td0_sh[t3 & 0xff] ^ Td1_sh[(t2 >> 8) & 0xff] ^ Td2_sh[(t1 >> 16) & 0xff] ^ Td3_sh[t0 >> 24] ^ rk_constant[27];
		/* round 7: */
		t0 = Td0_sh[s0 & 0xff] ^ Td1_sh[(s3 >> 8) & 0xff] ^ Td2_sh[(s2 >> 16) & 0xff] ^ Td3_sh[s1 >> 24] ^ rk_constant[28];
		t1 = Td0_sh[s1 & 0xff] ^ Td1_sh[(s0 >> 8) & 0xff] ^ Td2_sh[(s3 >> 16) & 0xff] ^ Td3_sh[s2 >> 24] ^ rk_constant[29];
		t2 = Td0_sh[s2 & 0xff] ^ Td1_sh[(s1 >> 8) & 0xff] ^ Td2_sh[(s0 >> 16) & 0xff] ^ Td3_sh[s3 >> 24] ^ rk_constant[30];
		t3 = Td0_sh[s3 & 0xff] ^ Td1_sh[(s2 >> 8) & 0xff] ^ Td2_sh[(s1 >> 16) & 0xff] ^ Td3_sh[s0 >> 24] ^ rk_constant[31];
		/* round 8: */
		s0 = Td0_sh[t0 & 0xff] ^ Td1_sh[(t3 >> 8) & 0xff] ^ Td2_sh[(t2 >> 16) & 0xff] ^ Td3_sh[t1 >> 24] ^ rk_constant[32];
		s1 = Td0_sh[t1 & 0xff] ^ Td1_sh[(t0 >> 8) & 0xff] ^ Td2_sh[(t3 >> 16) & 0xff] ^ Td3_sh[t2 >> 24] ^ rk_constant[33];
		s2 = Td0_sh[t2 & 0xff] ^ Td1_sh[(t1 >> 8) & 0xff] ^ Td2_sh[(t0 >> 16) & 0xff] ^ Td3_sh[t3 >> 24] ^ rk_constant[34];
		s3 = Td0_sh[t3 & 0xff] ^ Td1_sh[(t2 >> 8) & 0xff] ^ Td2_sh[(t1 >> 16) & 0xff] ^ Td3_sh[t0 >> 24] ^ rk_constant[35];
		/* round 9: */
		t0 = Td0_sh[s0 & 0xff] ^ Td1_sh[(s3 >> 8) & 0xff] ^ Td2_sh[(s2 >> 16) & 0xff] ^ Td3_sh[s1 >> 24] ^ rk_constant[36];
		t1 = Td0_sh[s1 & 0xff] ^ Td1_sh[(s0 >> 8) & 0xff] ^ Td2_sh[(s3 >> 16) & 0xff] ^ Td3_sh[s2 >> 24] ^ rk_constant[37];
		t2 = Td0_sh[s2 & 0xff] ^ Td1_sh[(s1 >> 8) & 0xff] ^ Td2_sh[(s0 >> 16) & 0xff] ^ Td3_sh[s3 >> 24] ^ rk_constant[38];
		t3 = Td0_sh[s3 & 0xff] ^ Td1_sh[(s2 >> 8) & 0xff] ^ Td2_sh[(s1 >> 16) & 0xff] ^ Td3_sh[s0 >> 24] ^ rk_constant[39];

		if (Nr > 10) {
			/* round 10: */
			s0 = Td0_sh[t0 & 0xff] ^ Td1_sh[(t3 >> 8) & 0xff] ^ Td2_sh[(t2 >> 16) & 0xff] ^ Td3_sh[t1 >> 24] ^ rk_constant[40];
			s1 = Td0_sh[t1 & 0xff] ^ Td1_sh[(t0 >> 8) & 0xff] ^ Td2_sh[(t3 >> 16) & 0xff] ^ Td3_sh[t2 >> 24] ^ rk_constant[41];
			s2 = Td0_sh[t2 & 0xff] ^ Td1_sh[(t1 >> 8) & 0xff] ^ Td2_sh[(t0 >> 16) & 0xff] ^ Td3_sh[t3 >> 24] ^ rk_constant[42];
			s3 = Td0_sh[t3 & 0xff] ^ Td1_sh[(t2 >> 8) & 0xff] ^ Td2_sh[(t1 >> 16) & 0xff] ^ Td3_sh[t0 >> 24] ^ rk_constant[43];
			/* round 11: */
			t0 = Td0_sh[s0 & 0xff] ^ Td1_sh[(s3 >> 8) & 0xff] ^ Td2_sh[(s2 >> 16) & 0xff] ^ Td3_sh[s1 >> 24] ^ rk_constant[44];
			t1 = Td0_sh[s1 & 0xff] ^ Td1_sh[(s0 >> 8) & 0xff] ^ Td2_sh[(s3 >> 16) & 0xff] ^ Td3_sh[s2 >> 24] ^ rk_constant[45];
			t2 = Td0_sh[s2 & 0xff] ^ Td1_sh[(s1 >> 8) & 0xff] ^ Td2_sh[(s0 >> 16) & 0xff] ^ Td3_sh[s3 >> 24] ^ rk_constant[46];
			t3 = Td0_sh[s3 & 0xff] ^ Td1_sh[(s2 >> 8) & 0xff] ^ Td2_sh[(s1 >> 16) & 0xff] ^ Td3_sh[s0 >> 24] ^ rk_constant[47];
			if (Nr > 12) {
				/* round 12: */
				s0 = Td0_sh[t0 & 0xff] ^ Td1_sh[(t3 >> 8) & 0xff] ^ Td2_sh[(t2 >> 16) & 0xff] ^ Td3_sh[t1 >> 24] ^ rk_constant[48];
				s1 = Td0_sh[t1 & 0xff] ^ Td1_sh[(t0 >> 8) & 0xff] ^ Td2_sh[(t3 >> 16) & 0xff] ^ Td3_sh[t2 >> 24] ^ rk_constant[49];
				s2 = Td0_sh[t2 & 0xff] ^ Td1_sh[(t1 >> 8) & 0xff] ^ Td2_sh[(t0 >> 16) & 0xff] ^ Td3_sh[t3 >> 24] ^ rk_constant[50];
				s3 = Td0_sh[t3 & 0xff] ^ Td1_sh[(t2 >> 8) & 0xff] ^ Td2_sh[(t1 >> 16) & 0xff] ^ Td3_sh[t0 >> 24] ^ rk_constant[51];
				/* round 13: */
				t0 = Td0_sh[s0 & 0xff] ^ Td1_sh[(s3 >> 8) & 0xff] ^ Td2_sh[(s2 >> 16) & 0xff] ^ Td3_sh[s1 >> 24] ^ rk_constant[52];
				t1 = Td0_sh[s1 & 0xff] ^ Td1_sh[(s0 >> 8) & 0xff] ^ Td2_sh[(s3 >> 16) & 0xff] ^ Td3_sh[s2 >> 24] ^ rk_constant[53];
				t2 = Td0_sh[s2 & 0xff] ^ Td1_sh[(s1 >> 8) & 0xff] ^ Td2_sh[(s0 >> 16) & 0xff] ^ Td3_sh[s3 >> 24] ^ rk_constant[54];
				t3 = Td0_sh[s3 & 0xff] ^ Td1_sh[(s2 >> 8) & 0xff] ^ Td2_sh[(s1 >> 16) & 0xff] ^ Td3_sh[s0 >> 24] ^ rk_constant[55];
			}
		}

		u32 x = Nr << 2;

		/* Apply last round */
		s0 =
			(Td4_sh[(t0 & 0xff)] & 0x000000ff) ^
			(Td4_sh[(t3 >> 8) & 0xff] & 0x0000ff00) ^
			(Td4_sh[(t2 >> 16) & 0xff] & 0x00ff0000) ^
			(Td4_sh[(t1 >> 24)] & 0xff000000) ^
			rk_constant[x++];

		s1 =
			(Td4_sh[(t1 & 0xff)] & 0x000000ff) ^
			(Td4_sh[(t0 >> 8) & 0xff] & 0x0000ff00) ^
			(Td4_sh[(t3 >> 16) & 0xff] & 0x00ff0000) ^
			(Td4_sh[(t2 >> 24)] & 0xff000000) ^
			rk_constant[x++];

		s2 =
			(Td4_sh[(t2 & 0xff)] & 0x000000ff) ^
			(Td4_sh[(t1 >> 8) & 0xff] & 0x0000ff00) ^
			(Td4_sh[(t0 >> 16) & 0xff] & 0x00ff0000) ^
			(Td4_sh[(t3 >> 24)] & 0xff000000) ^
			rk_constant[x++];

		s3 =
			(Td4_sh[(t3 & 0xff)] & 0x000000ff) ^
			(Td4_sh[(t2 >> 8) & 0xff] & 0x0000ff00) ^
			(Td4_sh[(t1 >> 16) & 0xff] & 0x00ff0000) ^
			(Td4_sh[(t0 >> 24)] & 0xff000000) ^
			rk_constant[x];


		pt[i] = { s0, s1, s2, s3 };


	}




}



cudaError_t rijndaelEncryptCuda(const u8 pt[], u8 ct[], const u32 rk[], const unsigned int plainTextLength, const unsigned int Nr)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

    uint4 *devPt;
    uint4 *devCt;

	u32 *devTe0;
	u32 *devTe1;
	u32 *devTe2;
	u32 *devTe3;
	u32 *devTe4;

    cudaError_t cudaStatus;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        return cudaStatus;
    }

    // cuda malloc
	start = startStopwatch();
	cudaHostGetDevicePointer((void **)&devPt, (void *)pt, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	cudaHostGetDevicePointer((void **)&devCt, (void *)ct, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	

	cudaStatus = cudaMalloc((void**)&devTe0, T_TABLE_SIZE * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        return cudaStatus;
    }

	cudaStatus = cudaMalloc((void**)&devTe1, T_TABLE_SIZE * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMalloc((void**)&devTe2, T_TABLE_SIZE * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMalloc((void**)&devTe3, T_TABLE_SIZE * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMalloc((void**)&devTe4, T_TABLE_SIZE * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	
	endStopwatch("Malloc & GetDevicePointer (encryption)", start);
    
	start = startStopwatch();

	cudaStatus = cudaMemcpyToSymbol(rk_constant, rk, rkLength * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }

	cudaStatus = cudaMemcpy(devTe0, Te0, T_TABLE_SIZE * sizeof(u32), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }

	cudaStatus = cudaMemcpy(devTe1, Te1, T_TABLE_SIZE * sizeof(u32), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMemcpy(devTe2, Te2, T_TABLE_SIZE * sizeof(u32), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMemcpy(devTe3, Te3, T_TABLE_SIZE * sizeof(u32), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}
	cudaStatus = cudaMemcpy(devTe4, Te4, T_TABLE_SIZE * sizeof(u32), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}

	totalTime = endStopwatch("Memory copy from host to device (encryption)", start);
    
	//!!!must be padded before!!!

	dim3 dimGrid((plainTextLength/BYTES_PER_BLOCK - 1)/THREAD_BLOCK_SIZE + 1, 1, 1);
	dim3 dimBlock(THREAD_BLOCK_SIZE, 1, 1);
	
    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	encryptKernel << <dimGrid, dimBlock >> >(devPt, devCt, devTe0, devTe1, devTe2, devTe3, devTe4, blocks, Nr);
	
	
    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        return cudaStatus;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
        return cudaStatus;
    }

	totalTime += endStopwatch("Encryption kernel", start);
	

	start = startStopwatch();

	cudaFree(devTe0);
	cudaFree(devTe1);
	cudaFree(devTe2);
	cudaFree(devTe3);
	cudaFree(devTe4);

	endStopwatch("Freeing memory (encryption)", start);

	printDuration("Encryption", totalTime);

    return cudaStatus;
}


cudaError_t rijndaelDecryptCuda(const u8 ct[], u8 pt[], const u32 rk[], const unsigned int plainTextLength, const unsigned int Nr)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	uint4 *devCt;
    uint4 *devPt;

	u32 *devTd0;
	u32 *devTd1;
	u32 *devTd2;
	u32 *devTd3;
	u32 *devTd4;

    cudaError_t cudaStatus;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;
	
    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        return cudaStatus;
    }
	

    // cuda malloc
	start = startStopwatch();
	cudaHostGetDevicePointer((void **)&devCt, (void *)ct, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	cudaHostGetDevicePointer((void **)&devPt, (void *)pt, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	

	cudaStatus = cudaMalloc((void**)&devTd0, T_TABLE_SIZE * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMalloc((void**)&devTd1, T_TABLE_SIZE * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMalloc((void**)&devTd2, T_TABLE_SIZE * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMalloc((void**)&devTd3, T_TABLE_SIZE * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMalloc((void**)&devTd4, T_TABLE_SIZE * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}


	endStopwatch("Malloc & GetDevicePointer (decryption)", start);
    
	start = startStopwatch();

	cudaStatus = cudaMemcpyToSymbol(rk_constant, rk, rkLength * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }

	cudaStatus = cudaMemcpy(devTd0, Td0, T_TABLE_SIZE * sizeof(u32), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMemcpy(devTd1, Td1, T_TABLE_SIZE * sizeof(u32), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMemcpy(devTd2, Td2, T_TABLE_SIZE * sizeof(u32), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMemcpy(devTd3, Td3, T_TABLE_SIZE * sizeof(u32), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}
	cudaStatus = cudaMemcpy(devTd4, Td4, T_TABLE_SIZE * sizeof(u32), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}

	totalTime = endStopwatch("Memory copy from host to device (decryption)", start);
    
	//!!!must be padded before!!!

	dim3 dimGrid((plainTextLength/BYTES_PER_BLOCK - 1)/THREAD_BLOCK_SIZE + 1, 1, 1);
	dim3 dimBlock(THREAD_BLOCK_SIZE, 1, 1);
	
    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	decryptKernel << <dimGrid, dimBlock >> >(devCt, devPt, devTd0, devTd1, devTd2, devTd3, devTd4, blocks, Nr);
	
	
    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        return cudaStatus;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
        return cudaStatus;
    }
	totalTime += endStopwatch("Decryption kernel", start);
	

	start = startStopwatch();

	cudaFree(devTd0);
	cudaFree(devTd1);
	cudaFree(devTd2);
	cudaFree(devTd3);
	cudaFree(devTd4);

	endStopwatch("Freeing memory (decryption)", start);

	printDuration("Decryption", totalTime);

    return cudaStatus;
}


int rijndaelEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
{

	boost::chrono::high_resolution_clock::time_point start;

	cudaFree(NULL);
	u32 rk[rkLength];

	u8 *pinnedPt;
	u8 *pinnedCt;


	cudaHostAlloc((void **)&pinnedPt, plainTextLength, cudaHostAllocWriteCombined | cudaHostAllocMapped);
	cudaHostAlloc((void **)&pinnedCt, plainTextLength, cudaHostAllocDefault | cudaHostAllocMapped);

	memcpy(pinnedPt, pt, plainTextLength);

	start = startStopwatch();
	unsigned int Nr = rijndaelKeySetupEnc(rk, (u32 *)cipherKey, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);


	cudaError_t cudaStatus = rijndaelEncryptCuda(pinnedPt, pinnedCt, rk, plainTextLength, Nr);


	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Encryption failed!");
		return 1;
	}

	memcpy(ct, pinnedCt, plainTextLength);
	cudaFreeHost(pinnedPt);
	cudaFreeHost(pinnedCt);

	// cudaDeviceReset must be called before exiting in order for profiling and
	// tracing tools such as Nsight and Visual Profiler to show complete traces.
	cudaStatus = cudaDeviceReset();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceReset failed!");
		return 1;
	}



	return 0;
}

int rijndaelDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
{

	boost::chrono::high_resolution_clock::time_point start;

	cudaFree(NULL);
	u32 rk[rkLength];

	u8 *pinnedCt;
	u8 *pinnedPt;


	cudaHostAlloc((void **)&pinnedCt, plainTextLength, cudaHostAllocWriteCombined | cudaHostAllocMapped);
	cudaHostAlloc((void **)&pinnedPt, plainTextLength, cudaHostAllocDefault | cudaHostAllocMapped);
	memcpy(pinnedCt, ct, plainTextLength);


	start = startStopwatch();
	unsigned int Nr = rijndaelKeySetupDec(rk, (u32 *)cipherKey, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);


	cudaError_t cudaStatus = rijndaelDecryptCuda(pinnedCt, pinnedPt, rk, plainTextLength, Nr);


	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Decription failed!");
		return 1;
	}


	memcpy(pt, pinnedPt, plainTextLength);
	cudaFreeHost(pinnedCt);
	cudaFreeHost(pinnedPt);


	// cudaDeviceReset must be called before exiting in order for profiling and
	// tracing tools such as Nsight and Visual Profiler to show complete traces.
	cudaStatus = cudaDeviceReset();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceReset failed!");
		return 1;
	}


	return 0;
}


