#include "common-types.h"

#define INPUT_WHITEN 0
#define OUTPUT_WHITEN (INPUT_WHITEN +  WORDS_PER_BLOCK)
#define ROUND_SUBKEYS  (OUTPUT_WHITEN + WORDS_PER_BLOCK)
#define S_BOX_SIZE (4 * 256)

#define	ROL(x,n) (((x) << ((n) & 0x1F)) | ((x) >> (32-((n) & 0x1F))))
#define	ROR(x,n) (((x) >> ((n) & 0x1F)) | ((x) << (32-((n) & 0x1F))))

/*	Macros for extracting bytes from dwords (correct for endianness) */
#define	_b(x,N)	((x >> (N & 3)) & 0xff) /* pick bytes out of a dword */

#define		b0(x)			_b(x,0)		/* extract LSB of DWORD */
#define		b1(x)			_b(x,1)
#define		b2(x)			_b(x,2)
#define		b3(x)			_b(x,3)		/* extract MSB of DWORD */


#define	Fe32(sBox, x,R) (sBox[        2*_b(x, R  )    ] ^ \
	sBox[2 * _b(x, R + 1) + 1] ^ \
	sBox[0x200 + 2 * _b(x, R + 2)] ^ \
	sBox[0x200 + 2 * _b(x, R + 3) + 1])

#define encrypt2Rounds()	t0 = Fe32(sBox_sh, x0, 0);\
	t1 = Fe32(sBox_sh, x1, 3); \
	x2 ^= t0 + t1 + sKey[k++]; \
	x2 = ROR(x2, 1);		  \
	x3 = ROL(x3, 1);		  \
	x3 ^= t0 + 2 * t1 + sKey[k++]; \
	t0 = Fe32(sBox_sh, x2, 0); \
	t1 = Fe32(sBox_sh, x3, 3); \
	x0 ^= t0 + t1 + sKey[k++]; \
	x0 = ROR(x0, 1);		\
	x1 = ROL(x1, 1);			\
	x1 ^= t0 + 2 * t1 + sKey[k++];

#define applyEncryptRounds() encrypt2Rounds();\
	encrypt2Rounds(); \
	encrypt2Rounds(); \
	encrypt2Rounds(); \
	encrypt2Rounds(); \
	encrypt2Rounds(); \
	encrypt2Rounds(); \
	encrypt2Rounds();



__kernel void encryptKernel(__global uint4 *pt, __global uint4 *ct, __constant u32 *sKey, __global u32 *sBox, const int blocks, const u32 n0, const u32 n1) 
{																																	  
																																	  
	int t = get_local_id(0);
	u64 i = get_global_id(0);																										  
	
	__local u32 sBox_sh[S_BOX_SIZE];



	if(t * 4 < S_BOX_SIZE){

		sBox_sh[t + 0 * 256] = sBox[t + 0 * 256];
		sBox_sh[t + 1 * 256] = sBox[t + 1 * 256];
		sBox_sh[t + 2 * 256] = sBox[t + 2 * 256];
		sBox_sh[t + 3 * 256] = sBox[t + 3 * 256];
	}

	barrier(CLK_LOCAL_MEM_FENCE);
		

	if (i < blocks){

		register u32 x0, x1, x2, x3, t0, t1;
		register int k;


		x0 = n0 ^ sKey[INPUT_WHITEN];
		x1 = n1 ^ sKey[INPUT_WHITEN + 1];
		x2 = ((i & 0xffffffff00000000) >> 32) ^ sKey[INPUT_WHITEN + 2];
		x3 = (i & 0xffffffff) ^ sKey[INPUT_WHITEN + 3];

		k = ROUND_SUBKEYS;

		applyEncryptRounds();


		ct[i] = (uint4){ (x2 ^ sKey[OUTPUT_WHITEN]) ^ pt[i].x, (x3 ^ sKey[OUTPUT_WHITEN + 1]) ^ pt[i].y, (x0 ^ sKey[OUTPUT_WHITEN + 2]) ^ pt[i].z, (x1 ^ sKey[OUTPUT_WHITEN + 3]) ^ pt[i].w };
	}
																																	  
																							  
																																	 																																	  
																																	  
}																																	  

