#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdlib.h>
#include <stdio.h>
#include "twofish-cuda.cuh"
#include "twofish-tables.cuh"
#include "measure-time.h"
#include <boost/chrono.hpp>

#define Q0_T0 0x4ace95b023f6d718
#define Q0_T1 0xd9076a4f53218bce
#define Q0_T2 0x17423f8c09d6e5ab
#define Q0_T3 0xac5803b9e6214f7d

#define Q1_T0 0x5ca04913e67fdb82
#define Q1_T1 0x809f5ad673c4b2e1
#define Q1_T2 0xf3b28de0a96157c4
#define Q1_T3 0xa802f746ed3c159b

#define T(M, N, n) (Q##M_T##N >> (n << 2)) & 0xf

__device__ inline u32 q0_t0(u32 n){
	return  (Q0_T0 >> (n << 2)) & 0xf;
}

__device__ inline u32 q0_t1(u32 n){
	return  (Q0_T1 >> (n << 2)) & 0xf;
}

__device__ inline u32 q0_t2(u32 n){
	return  (Q0_T2 >> (n << 2)) & 0xf;
}

__device__ inline u32 q0_t3(u32 n){
	return  (Q0_T3 >> (n << 2)) & 0xf;
}

__device__ inline u32 q1_t0(u32 n){
	return  (Q1_T0 >> (n << 2)) & 0xf;
}

__device__ inline u32 q1_t1(u32 n){
	return  (Q1_T1 >> (n << 2)) & 0xf;
}

__device__ inline u32 q1_t2(u32 n){
	return  (Q1_T2 >> (n << 2)) & 0xf;
}

__device__ inline u32 q1_t3(u32 n){
	return  (Q1_T3 >> (n << 2)) & 0xf;
}

__device__ inline u32 q0(u32 x){
	u32 a0, a1;
	u32 b0, b1;

	a0 = x >> 4;
	b0 = x & 0xf;

	a1 = a0 ^ b0;
	b1 = (a0 ^ ROR4(b0, 1) ^ (a0 << 3)) & 0xf;

	a0 = q0_t0(a1);
	b0 = q0_t1(b1);

	a1 = a0 ^ b0;
	b1 = (a0 ^ ROR4(b0, 1) ^ (a0 << 3)) & 0xf;

	a0 = q0_t2(a1);
	b0 = q0_t3(b1);

	return (b0 << 4) | a0;


}

__device__ inline u32 q1(u32 x){
	u32 a0, a1;
	u32 b0, b1;

	a0 = x >> 4;
	b0 = x & 0xf;

	a1 = a0 ^ b0;
	b1 = (a0 ^ ROR4(b0, 1) ^ (a0 << 3)) & 0xf;

	a0 = q1_t0(a1);
	b0 = q1_t1(b1);

	a1 = a0 ^ b0;
	b1 = (a0 ^ ROR4(b0, 1) ^ (a0 << 3)) & 0xf;

	a0 = q1_t2(a1);
	b0 = q1_t3(b1);

	return (b0 << 4) | a0;


}

__device__ inline u32 f32(u32 x, const u8 *k8, u32 k64Cnt)
{
	u32 t0 = x & 0xff;
	u32 t1 = (x >> 8) & 0xff;
	u32 t2 = (x >> 16) & 0xff;
	u32 t3 = (x >> 24) & 0xff;

	switch (k64Cnt & 3)
	{
	case 0:		/* 256 bits of key */
		t0 = q1(t0) ^ k8[12];
		t1 = q0(t1) ^ k8[13];
		t2 = q0(t2) ^ k8[14];
		t3 = q1(t3) ^ k8[15];
		/* fall thru, having pre-processed b[0]..b[3] with k32[3] */
	case 3:		/* 192 bits of key */
		t0 = q1(t0) ^ k8[8];
		t1 = q1(t1) ^ k8[9];
		t2 = q0(t2) ^ k8[10];
		t3 = q0(t3) ^ k8[11];
		/* fall thru, having pre-processed b[0]..b[3] with k32[2] */
	case 2:		/* 128 bits of key */
		t0 = q1(q0(q0(t0) ^ k8[4]) ^ k8[0]);
		t1 = q0(q0(q1(t1) ^ k8[5]) ^ k8[1]);
		t2 = q1(q1(q0(t2) ^ k8[6]) ^ k8[2]);
		t3 = q0(q1(q1(t3) ^ k8[7]) ^ k8[3]);
	}

	/* Now perform the MDS matrix multiply inline. */
	return	((M00(t0) ^ M01(t1) ^ M02(t2) ^ M03(t3))) ^
		((M10(t0) ^ M11(t1) ^ M12(t2) ^ M13(t3)) << 8) ^
		((M20(t0) ^ M21(t1) ^ M22(t2) ^ M23(t3)) << 16) ^
		((M30(t0) ^ M31(t1) ^ M32(t2) ^ M33(t3)) << 24);
}




#define	Fe32(sBox, x,R) (sBox[        2*_b(x, R  )    ] ^ \
				sBox[        2*_b(x, R+1) + 1] ^ \
				sBox[0x200 + 2*_b(x, R+2)    ] ^ \
				sBox[0x200 + 2*_b(x, R+3) + 1])


__constant__ u32 sKey[TOTAL_SUBKEYS];
__constant__ u8  sBoxKey_c[16];


__global__ void encryptKernel(uint4 *pt, uint4 *ct,  const unsigned int blocks, const u32 k64Cnt){

	#define encrypt2Rounds()	t0 = f32(x0, sBoxKey_c, k64Cnt);\
								t1 = f32(ROL(x1, 8), sBoxKey_c, k64Cnt); \
								x2 ^= t0 + t1 + sKey[k++]; \
								x2 = ROR(x2, 1);		  \
								x3 = ROL(x3, 1);		  \
								x3 ^= t0 + 2 * t1 + sKey[k++]; \
								t0 = f32(x2, sBoxKey_c, k64Cnt); \
								t1 = f32(ROL(x3, 8), sBoxKey_c, k64Cnt); \
								x0 ^= t0 + t1 + sKey[k++]; \
								x0 = ROR(x0, 1);		\
								x1 = ROL(x1, 1);			\
								x1 ^= t0 + 2 * t1 + sKey[k++];

	#define applyEncryptRounds() encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();



	int t = threadIdx.x;
	int i = blockDim.x * blockIdx.x + t;

	
	if(i < blocks){
		
		
		register u32 x0, x1, x2, x3, t0, t1;
		register int k;

		x0 = pt[i].x ^ sKey[INPUT_WHITEN];
		x1 = pt[i].y ^ sKey[INPUT_WHITEN + 1];
		x2 = pt[i].z ^ sKey[INPUT_WHITEN + 2];
		x3 = pt[i].w ^ sKey[INPUT_WHITEN + 3];


		k = ROUND_SUBKEYS;
		
		applyEncryptRounds();


		ct[i] = { x2 ^ sKey[OUTPUT_WHITEN], x3 ^ sKey[OUTPUT_WHITEN + 1], x0 ^ sKey[OUTPUT_WHITEN + 2], x1 ^ sKey[OUTPUT_WHITEN + 3] };
	}

	

}

__global__ void decryptKernel(uint4 *ct, uint4 *pt, const unsigned int blocks, const u32 k64Cnt){

	#define decrypt2Rounds()	t0 = f32(x2, sBoxKey_c, k64Cnt);\
								t1 = f32(ROL(x3, 8), sBoxKey_c, k64Cnt); \
								x1 ^= t0 + 2 * t1 + sKey[k--]; \
								x1 = ROR(x1, 1); \
								x0 = ROL(x0, 1); \
								x0 ^= t0 + t1 + sKey[k--]; \
								t0 = f32(x0, sBoxKey_c, k64Cnt); \
								t1 = f32(ROL(x1, 8), sBoxKey_c, k64Cnt); \
								x3 ^= t0 + 2 * t1 + sKey[k--]; \
								x3 = ROR(x3, 1); \
								x2 = ROL(x2, 1); \
								x2 ^= t0 + t1 + sKey[k--];
	
	#define applyDecryptRounds() decrypt2Rounds();\
								 decrypt2Rounds();\
								 decrypt2Rounds();\
								 decrypt2Rounds();\
								 decrypt2Rounds();\
								 decrypt2Rounds();\
								 decrypt2Rounds();\
								 decrypt2Rounds();


	int t = threadIdx.x;
	int i = blockDim.x * blockIdx.x + t;
	
	
	if(i < blocks){
	
		
		register u32 x0, x1, x2, x3, t0, t1;
		register int k;


		x2 = ct[i].x ^ sKey[OUTPUT_WHITEN];
		x3 = ct[i].y ^ sKey[OUTPUT_WHITEN + 1];
		x0 = ct[i].z ^ sKey[OUTPUT_WHITEN + 2];
		x1 = ct[i].w ^ sKey[OUTPUT_WHITEN + 3];

		k = ROUND_SUBKEYS + 2*ROUNDS - 1;
		
		applyDecryptRounds();
     
		pt[i] = { x0 ^ sKey[INPUT_WHITEN], x1 ^ sKey[INPUT_WHITEN + 1], x2 ^ sKey[INPUT_WHITEN + 2], x3 ^ sKey[INPUT_WHITEN + 3] };
		
		
		
	}

	

}



cudaError_t twofishEncryptCuda(const u8 pt[], u8 ct[], const u32 subKeys[TOTAL_SUBKEYS], const u32 sBoxKey[4], const unsigned int plainTextLength, const u32 k64Cnt)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	uint4 *devPt;
	uint4 *devCt;

    cudaError_t cudaStatus;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        return cudaStatus;
    }

    // cuda malloc
	start = startStopwatch();
	cudaHostGetDevicePointer((void **)&devPt, (void *)pt, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	cudaHostGetDevicePointer((void **)&devCt, (void *)ct, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	
	endStopwatch("Malloc & GetDevicePointer(encryption)", start);

	start = startStopwatch();
	
	cudaStatus = cudaMemcpyToSymbol(sKey, subKeys, TOTAL_SUBKEYS * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMemcpyToSymbol(sBoxKey_c, sBoxKey, 4 * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}

	
	totalTime = endStopwatch("Memory copy from host to device (encryption)", start);

	//!!!must be padded before!!!

	dim3 dimGrid((plainTextLength/BYTES_PER_BLOCK - 1)/THREAD_BLOCK_SIZE + 1, 1, 1);
	dim3 dimBlock(THREAD_BLOCK_SIZE, 1, 1);
	
    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	encryptKernel<<<dimGrid, dimBlock>>>(devPt, devCt, blocks, k64Cnt);
	
	
    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        return cudaStatus;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
        return cudaStatus;
    }

	totalTime += endStopwatch("Encryption kernel", start);

	printDuration("Encryption", totalTime);

    return cudaStatus;
}

cudaError_t twofishDecryptCuda(const u8 ct[], u8 pt[], const u32 subKeys[TOTAL_SUBKEYS], const u32 sBoxKey[4], const unsigned int plainTextLength, const u32 k64Cnt)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	uint4 *devPt;
	uint4 *devCt;
	

    cudaError_t cudaStatus;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        return cudaStatus;
    }

    // cuda malloc
	start = startStopwatch();
	cudaHostGetDevicePointer((void **)&devCt, (void *)ct, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	cudaHostGetDevicePointer((void **)&devPt, (void *)pt, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
		
	endStopwatch("Malloc & GetDevicePointer (decryption)", start);

	start = startStopwatch();

	cudaStatus = cudaMemcpyToSymbol(sKey, subKeys, TOTAL_SUBKEYS * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }

	cudaStatus = cudaMemcpyToSymbol(sBoxKey_c, sBoxKey, 4 * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}
	
	totalTime = endStopwatch("Memory copy from host to device (decryption)", start);

	//!!!must be padded before!!!

	dim3 dimGrid((plainTextLength/BYTES_PER_BLOCK - 1)/THREAD_BLOCK_SIZE + 1, 1, 1);
	dim3 dimBlock(THREAD_BLOCK_SIZE, 1, 1);
	
    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	decryptKernel<<<dimGrid, dimBlock>>>(devCt, devPt, blocks, k64Cnt);
	
	
    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        return cudaStatus;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
        return cudaStatus;
    }
	
	totalTime += endStopwatch("Decryption kernel", start);


	printDuration("Decryption", totalTime);

    return cudaStatus;
}






u32 RS_MDS_Encode(u32 k0, u32 k1)
	{
	int i,j;
	u32 r;

	for (i=r=0;i<2;i++)
		{
		r ^= (i) ? k0 : k1;			/* merge in 32 more key bits */
		for (j=0;j<4;j++)			/* shift one u8 at a time */
			RS_rem(r);
					

		}
	return r;
	}


int makeKey(const u32 cipherKey[], u32 subKeys[TOTAL_SUBKEYS], u32 sBoxKey[4], const u32 k64Cnt){

#define	F32(res,x,k32)	\
	{ \
	u32 t = x;													\
	switch (k64Cnt & 3)											\
	{														\
	case 0:  /* same as 4 */								\
	b0(t) = p8(04)[b0(t)] ^ b0(k32[3]);		\
	b1(t) = p8(14)[b1(t)] ^ b1(k32[3]);		\
	b2(t) = p8(24)[b2(t)] ^ b2(k32[3]);		\
	b3(t) = p8(34)[b3(t)] ^ b3(k32[3]);		\
	/* fall thru, having pre-processed t */		\
	case 3:		b0(t) = p8(03)[b0(t)] ^ b0(k32[2]);		\
	b1(t) = p8(13)[b1(t)] ^ b1(k32[2]);		\
	b2(t) = p8(23)[b2(t)] ^ b2(k32[2]);		\
	b3(t) = p8(33)[b3(t)] ^ b3(k32[2]);		\
	/* fall thru, having pre-processed t */		\
	case 2:	 /* 128-bit keys (optimize for this case) */	\
	res = MDStab[0][p8(01)[p8(02)[b0(t)] ^ b0(k32[1])] ^ b0(k32[0])] ^ \
	MDStab[1][p8(11)[p8(12)[b1(t)] ^ b1(k32[1])] ^ b1(k32[0])] ^ \
	MDStab[2][p8(21)[p8(22)[b2(t)] ^ b2(k32[1])] ^ b2(k32[0])] ^ \
	MDStab[3][p8(31)[p8(32)[b3(t)] ^ b3(k32[1])] ^ b3(k32[0])];	\
}														\
}

	u32 subkeyCnt = TOTAL_SUBKEYS;
	u32 k32e[4]; // even 32-bit entities
	u32 k32o[4]; // odd 32-bit entities
	//u32 sBoxKey[4];

	u32 i, j, offset = 0;

	for (i = 0, j = k64Cnt - 1; i < 4 && offset < k64Cnt * 2; i++, j--) {
		k32e[i] = cipherKey[offset++];
		k32o[i] = cipherKey[offset++];
		sBoxKey[j] = RS_MDS_Encode(k32e[i], k32o[i]); // reverse order
	}


	// compute the round decryption subkeys for PHT. these same subkeys
	// will be used in encryption but will be applied in reverse order.
	u32 q, A, B;

	for (i = q = 0; i < subkeyCnt / 2; i++, q += SK_STEP) {
		F32(A, q, k32e); // A uses even key entities
		F32(B, q + SK_BUMP, k32o); // B uses odd  key entities
		B = B << 8 | ROR(B, 24);
		A += B;
		subKeys[2 * i] = A;               // combine with a PHT
		A += B;
		subKeys[2 * i + 1] = A << SK_ROTL | ROR(A, (32 - SK_ROTL));
	}

	// fully expand the table for speed
	/*
	u32 k0 = sBoxKey[0];
	u32 k1 = sBoxKey[1];
	u32 k2 = sBoxKey[2];
	u32 k3 = sBoxKey[3];
	u32 b0, b1, b2, b3;

	for (i = 0; i < 256; i++) {
	b0 = b1 = b2 = b3 = i;
	switch (k64Cnt & 3) {
	case 0: // same as 4
	b0 = (p8(04)[b0]) ^ b0(k3);
	b1 = (p8(14)[b1]) ^ b1(k3);
	b2 = (p8(24)[b2]) ^ b2(k3);
	b3 = (p8(34)[b3]) ^ b3(k3);
	case 3:
	b0 = (p8(03)[b0]) ^ b0(k2);
	b1 = (p8(13)[b1]) ^ b1(k2);
	b2 = (p8(23)[b2]) ^ b2(k2);
	b3 = (p8(33)[b3]) ^ b3(k2);
	case 2: // 128-bit keys
	sBox[      2*i  ] = MDStab[0][(p8(01)[(p8(02)[b0]) ^ b0(k1)]) ^ b0(k0)];
	sBox[      2*i+1] = MDStab[1][(p8(11)[(p8(12)[b1]) ^ b1(k1)]) ^ b1(k0)];
	sBox[0x200+2*i  ] = MDStab[2][(p8(21)[(p8(22)[b2]) ^ b2(k1)]) ^ b2(k0)];
	sBox[0x200+2*i+1] = MDStab[3][(p8(31)[(p8(32)[b3]) ^ b3(k1)]) ^ b3(k0)];
	}
	}

	*/

	return 0;
}




int twofishEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	cudaSetDeviceFlags(cudaDeviceMapHost);
	cudaFree(NULL);
	u32 subKeys[TOTAL_SUBKEYS];
	u32 sBoxKey[4];
	u32 k64Cnt = cipherKeyLength / 8;

	u8 *pinnedPt;
	u8 *pinnedCt;

	cudaHostAlloc((void **)&pinnedPt, plainTextLength, cudaHostAllocWriteCombined | cudaHostAllocMapped);
	cudaHostAlloc((void **)&pinnedCt, plainTextLength, cudaHostAllocDefault | cudaHostAllocMapped);

	memcpy(pinnedPt, pt, plainTextLength);

	start = startStopwatch();
	makeKey((u32*)cipherKey, subKeys, sBoxKey, k64Cnt);
	endStopwatch("Encryption key calculation", start);
	
	
	cudaError_t cudaStatus = twofishEncryptCuda(pinnedPt, pinnedCt, subKeys, sBoxKey, plainTextLength, k64Cnt);
	
	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Encryption failed!");
        return 1;
    }

	memcpy(ct, pinnedCt, plainTextLength);
	cudaFreeHost(pinnedPt);
	cudaFreeHost(pinnedCt);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }

    return 0;




}
int twofishDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){
	
	boost::chrono::high_resolution_clock::time_point start;

	cudaSetDeviceFlags(cudaDeviceMapHost);
	cudaFree(NULL);
	u32 subKeys[TOTAL_SUBKEYS];
	u32 sBoxKey[4];
	u32 k64Cnt = cipherKeyLength / 8;

	u8 *pinnedCt;
	u8 *pinnedPt;

	cudaHostAlloc((void **)&pinnedCt, plainTextLength, cudaHostAllocWriteCombined | cudaHostAllocMapped);
	cudaHostAlloc((void **)&pinnedPt, plainTextLength, cudaHostAllocDefault | cudaHostAllocMapped);

	memcpy(pinnedCt, ct, plainTextLength);

	start = startStopwatch();
	makeKey((u32*)cipherKey, subKeys, sBoxKey, k64Cnt);
	endStopwatch("Decryption key calculation", start);
	
	
	cudaError_t cudaStatus = twofishDecryptCuda(pinnedCt, pinnedPt, subKeys, sBoxKey, plainTextLength, k64Cnt);

	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Decryption failed!");
        return 1;
    }

	memcpy(pt, pinnedPt, plainTextLength);
	cudaFreeHost(pinnedCt);
	cudaFreeHost(pinnedPt);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }

    return 0;

}