#include "measure-time.h"
#include "rijndael-tables.h"
#include <boost/chrono.hpp>


int rijndaelKeySetupEnc(u32 rk[/*4*(Nr + 1)*/], const u32 cipherKey[], const unsigned int keyLen) {
   	int i = 0;
	u32 temp;

	rk[0] = cipherKey[0];
	rk[1] = cipherKey[1];
	rk[2] = cipherKey[2];
	rk[3] = cipherKey[3];

	if (keyLen == 16) {
		for (;;) {
			temp  = rk[3];
			rk[4] = rk[0] ^
				(Te4[((temp)& 0xff)] & 0xff000000) ^
				(Te4[(temp >> 24)] & 0x00ff0000) ^
				(Te4[((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(Te4[((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[5] = rk[1] ^ rk[4];
			rk[6] = rk[2] ^ rk[5];
			rk[7] = rk[3] ^ rk[6];
			if (++i == 10) {
				return 10;
			}
			rk += 4;
		}
	}
	rk[4] = cipherKey[4];
	rk[5] = cipherKey[5];

	if (keyLen == 24) {
		for (;;) {
			temp = rk[ 5];
			rk[ 6] = rk[ 0] ^
				(Te4[((temp)& 0xff)] & 0xff000000) ^
				(Te4[(temp >> 24)] & 0x00ff0000) ^
				(Te4[((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(Te4[((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[ 7] = rk[ 1] ^ rk[ 6];
			rk[ 8] = rk[ 2] ^ rk[ 7];
			rk[ 9] = rk[ 3] ^ rk[ 8];
			if (++i == 8) {
				return 12;
			}
			rk[10] = rk[ 4] ^ rk[ 9];
			rk[11] = rk[ 5] ^ rk[10];
			rk += 6;
		}
	}
	rk[6] = cipherKey[6];
	rk[7] = cipherKey[7];
	if (keyLen == 32) {
        for (;;) {
        	temp = rk[ 7];
        	rk[ 8] = rk[ 0] ^
				(Te4[((temp)& 0xff)] & 0xff000000) ^
				(Te4[(temp >> 24)] & 0x00ff0000) ^
				(Te4[((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(Te4[((temp >> 8) & 0xff)] & 0x000000ff) ^
        		rcon[i];
        	rk[ 9] = rk[ 1] ^ rk[ 8];
        	rk[10] = rk[ 2] ^ rk[ 9];
        	rk[11] = rk[ 3] ^ rk[10];
			if (++i == 7) {
				return 14;
			}
        	temp = rk[11];
        	rk[12] = rk[ 4] ^
        		(Te4[(temp >> 24)       ] & 0xff000000) ^
        		(Te4[(temp >> 16) & 0xff] & 0x00ff0000) ^
        		(Te4[(temp >>  8) & 0xff] & 0x0000ff00) ^
        		(Te4[(temp      ) & 0xff] & 0x000000ff);
        	rk[13] = rk[ 5] ^ rk[12];
        	rk[14] = rk[ 6] ^ rk[13];
        	rk[15] = rk[ 7] ^ rk[14];

			rk += 8;
        }
	}
	return 0;
}

int rijndaelKeySetupDec(u32 rk[/*4*(Nr + 1)*/], const u32 cipherKey[], const unsigned int keyLen) {
	int Nr, i, j;
	u32 temp;

	/* expand the cipher key: */
	Nr = rijndaelKeySetupEnc(rk, cipherKey, keyLen);
	/* invert the order of the round keys: */
	for (i = 0, j = 4*Nr; i < j; i += 4, j -= 4) {
		temp = rk[i    ]; rk[i    ] = rk[j    ]; rk[j    ] = temp;
		temp = rk[i + 1]; rk[i + 1] = rk[j + 1]; rk[j + 1] = temp;
		temp = rk[i + 2]; rk[i + 2] = rk[j + 2]; rk[j + 2] = temp;
		temp = rk[i + 3]; rk[i + 3] = rk[j + 3]; rk[j + 3] = temp;
	}
	/* apply the inverse MixColumn transform to all round keys but the first and the last: */
	for (i = 1; i < Nr; i++) {
		rk += 4;
		rk[0] =
			Td0[Te4[((rk[0]) & 0xff)] & 0xff] ^
			Td1[Te4[((rk[0] >> 8) & 0xff)] & 0xff] ^
			Td2[Te4[((rk[0] >> 16) & 0xff)] & 0xff] ^
			Td3[Te4[(rk[0] >> 24)] & 0xff];
		rk[1] =
			Td0[Te4[((rk[1]) & 0xff)] & 0xff] ^
			Td1[Te4[((rk[1] >> 8) & 0xff)] & 0xff] ^
			Td2[Te4[((rk[1] >> 16) & 0xff)] & 0xff] ^
			Td3[Te4[(rk[1] >> 24)] & 0xff];
		rk[2] =
			Td0[Te4[((rk[2]) & 0xff)] & 0xff] ^
			Td1[Te4[((rk[2] >> 8) & 0xff)] & 0xff] ^
			Td2[Te4[((rk[2] >> 16) & 0xff)] & 0xff] ^
			Td3[Te4[(rk[2] >> 24)] & 0xff];
		rk[3] =
			Td0[Te4[((rk[3]) & 0xff)] & 0xff] ^
			Td1[Te4[((rk[3] >> 8) & 0xff)] & 0xff] ^
			Td2[Te4[((rk[3] >> 16) & 0xff)] & 0xff] ^
			Td3[Te4[(rk[3] >> 24)] & 0xff];
	}
	return Nr;
}


int rijndaelEncryptCpu(u32 *pt, u32 *ct, const u32 * rk, const unsigned int Nr){

	register u32 s0, s1, s2, s3, t0, t1, t2, t3;

	

	s0 = pt[0] ^ rk[0];
	s1 = pt[1] ^ rk[1];
	s2 = pt[2] ^ rk[2];
	s3 = pt[3] ^ rk[3];



	/* round 1: */
	t0 = Te0[s0 & 0xff] ^ Te1[(s1 >> 8) & 0xff] ^ Te2[(s2 >> 16) & 0xff] ^ Te3[s3 >> 24] ^ rk[4];
	t1 = Te0[s1 & 0xff] ^ Te1[(s2 >> 8) & 0xff] ^ Te2[(s3 >> 16) & 0xff] ^ Te3[s0 >> 24] ^ rk[5];
	t2 = Te0[s2 & 0xff] ^ Te1[(s3 >> 8) & 0xff] ^ Te2[(s0 >> 16) & 0xff] ^ Te3[s1 >> 24] ^ rk[6];
	t3 = Te0[s3 & 0xff] ^ Te1[(s0 >> 8) & 0xff] ^ Te2[(s1 >> 16) & 0xff] ^ Te3[s2 >> 24] ^ rk[7];


	/* round 2: */
	s0 = Te0[t0 & 0xff] ^ Te1[(t1 >> 8) & 0xff] ^ Te2[(t2 >> 16) & 0xff] ^ Te3[t3 >> 24] ^ rk[8];
	s1 = Te0[t1 & 0xff] ^ Te1[(t2 >> 8) & 0xff] ^ Te2[(t3 >> 16) & 0xff] ^ Te3[t0 >> 24] ^ rk[9];
	s2 = Te0[t2 & 0xff] ^ Te1[(t3 >> 8) & 0xff] ^ Te2[(t0 >> 16) & 0xff] ^ Te3[t1 >> 24] ^ rk[10];
	s3 = Te0[t3 & 0xff] ^ Te1[(t0 >> 8) & 0xff] ^ Te2[(t1 >> 16) & 0xff] ^ Te3[t2 >> 24] ^ rk[11];

	/* round 3: */
	t0 = Te0[s0 & 0xff] ^ Te1[(s1 >> 8) & 0xff] ^ Te2[(s2 >> 16) & 0xff] ^ Te3[s3 >> 24] ^ rk[12];
	t1 = Te0[s1 & 0xff] ^ Te1[(s2 >> 8) & 0xff] ^ Te2[(s3 >> 16) & 0xff] ^ Te3[s0 >> 24] ^ rk[13];
	t2 = Te0[s2 & 0xff] ^ Te1[(s3 >> 8) & 0xff] ^ Te2[(s0 >> 16) & 0xff] ^ Te3[s1 >> 24] ^ rk[14];
	t3 = Te0[s3 & 0xff] ^ Te1[(s0 >> 8) & 0xff] ^ Te2[(s1 >> 16) & 0xff] ^ Te3[s2 >> 24] ^ rk[15];
	/* round 4: */
	s0 = Te0[t0 & 0xff] ^ Te1[(t1 >> 8) & 0xff] ^ Te2[(t2 >> 16) & 0xff] ^ Te3[t3 >> 24] ^ rk[16];
	s1 = Te0[t1 & 0xff] ^ Te1[(t2 >> 8) & 0xff] ^ Te2[(t3 >> 16) & 0xff] ^ Te3[t0 >> 24] ^ rk[17];
	s2 = Te0[t2 & 0xff] ^ Te1[(t3 >> 8) & 0xff] ^ Te2[(t0 >> 16) & 0xff] ^ Te3[t1 >> 24] ^ rk[18];
	s3 = Te0[t3 & 0xff] ^ Te1[(t0 >> 8) & 0xff] ^ Te2[(t1 >> 16) & 0xff] ^ Te3[t2 >> 24] ^ rk[19];


	/* round 5: */
	t0 = Te0[s0 & 0xff] ^ Te1[(s1 >> 8) & 0xff] ^ Te2[(s2 >> 16) & 0xff] ^ Te3[s3 >> 24] ^ rk[20];
	t1 = Te0[s1 & 0xff] ^ Te1[(s2 >> 8) & 0xff] ^ Te2[(s3 >> 16) & 0xff] ^ Te3[s0 >> 24] ^ rk[21];
	t2 = Te0[s2 & 0xff] ^ Te1[(s3 >> 8) & 0xff] ^ Te2[(s0 >> 16) & 0xff] ^ Te3[s1 >> 24] ^ rk[22];
	t3 = Te0[s3 & 0xff] ^ Te1[(s0 >> 8) & 0xff] ^ Te2[(s1 >> 16) & 0xff] ^ Te3[s2 >> 24] ^ rk[23];
	/* round 6: */
	s0 = Te0[t0 & 0xff] ^ Te1[(t1 >> 8) & 0xff] ^ Te2[(t2 >> 16) & 0xff] ^ Te3[t3 >> 24] ^ rk[24];
	s1 = Te0[t1 & 0xff] ^ Te1[(t2 >> 8) & 0xff] ^ Te2[(t3 >> 16) & 0xff] ^ Te3[t0 >> 24] ^ rk[25];
	s2 = Te0[t2 & 0xff] ^ Te1[(t3 >> 8) & 0xff] ^ Te2[(t0 >> 16) & 0xff] ^ Te3[t1 >> 24] ^ rk[26];
	s3 = Te0[t3 & 0xff] ^ Te1[(t0 >> 8) & 0xff] ^ Te2[(t1 >> 16) & 0xff] ^ Te3[t2 >> 24] ^ rk[27];
	
	/* round 7: */
	t0 = Te0[s0 & 0xff] ^ Te1[(s1 >> 8) & 0xff] ^ Te2[(s2 >> 16) & 0xff] ^ Te3[s3 >> 24] ^ rk[28];
	t1 = Te0[s1 & 0xff] ^ Te1[(s2 >> 8) & 0xff] ^ Te2[(s3 >> 16) & 0xff] ^ Te3[s0 >> 24] ^ rk[29];
	t2 = Te0[s2 & 0xff] ^ Te1[(s3 >> 8) & 0xff] ^ Te2[(s0 >> 16) & 0xff] ^ Te3[s1 >> 24] ^ rk[30];
	t3 = Te0[s3 & 0xff] ^ Te1[(s0 >> 8) & 0xff] ^ Te2[(s1 >> 16) & 0xff] ^ Te3[s2 >> 24] ^ rk[31];
	
	/* round 8: */
	s0 = Te0[t0 & 0xff] ^ Te1[(t1 >> 8) & 0xff] ^ Te2[(t2 >> 16) & 0xff] ^ Te3[t3 >> 24] ^ rk[32];
	s1 = Te0[t1 & 0xff] ^ Te1[(t2 >> 8) & 0xff] ^ Te2[(t3 >> 16) & 0xff] ^ Te3[t0 >> 24] ^ rk[33];
	s2 = Te0[t2 & 0xff] ^ Te1[(t3 >> 8) & 0xff] ^ Te2[(t0 >> 16) & 0xff] ^ Te3[t1 >> 24] ^ rk[34];
	s3 = Te0[t3 & 0xff] ^ Te1[(t0 >> 8) & 0xff] ^ Te2[(t1 >> 16) & 0xff] ^ Te3[t2 >> 24] ^ rk[35];


	/* round 9: */
	t0 = Te0[s0 & 0xff] ^ Te1[(s1 >> 8) & 0xff] ^ Te2[(s2 >> 16) & 0xff] ^ Te3[s3 >> 24] ^ rk[36];
	t1 = Te0[s1 & 0xff] ^ Te1[(s2 >> 8) & 0xff] ^ Te2[(s3 >> 16) & 0xff] ^ Te3[s0 >> 24] ^ rk[37];
	t2 = Te0[s2 & 0xff] ^ Te1[(s3 >> 8) & 0xff] ^ Te2[(s0 >> 16) & 0xff] ^ Te3[s1 >> 24] ^ rk[38];
	t3 = Te0[s3 & 0xff] ^ Te1[(s0 >> 8) & 0xff] ^ Te2[(s1 >> 16) & 0xff] ^ Te3[s2 >> 24] ^ rk[39];

	if (Nr > 10) {
		/* round 10: */
		s0 = Te0[t0 & 0xff] ^ Te1[(t1 >> 8) & 0xff] ^ Te2[(t2 >> 16) & 0xff] ^ Te3[t3 >> 24] ^ rk[40];
		s1 = Te0[t1 & 0xff] ^ Te1[(t2 >> 8) & 0xff] ^ Te2[(t3 >> 16) & 0xff] ^ Te3[t0 >> 24] ^ rk[41];
		s2 = Te0[t2 & 0xff] ^ Te1[(t3 >> 8) & 0xff] ^ Te2[(t0 >> 16) & 0xff] ^ Te3[t1 >> 24] ^ rk[42];
		s3 = Te0[t3 & 0xff] ^ Te1[(t0 >> 8) & 0xff] ^ Te2[(t1 >> 16) & 0xff] ^ Te3[t2 >> 24] ^ rk[43];
		/* round 11: */
		t0 = Te0[s0 & 0xff] ^ Te1[(s1 >> 8) & 0xff] ^ Te2[(s2 >> 16) & 0xff] ^ Te3[s3 >> 24] ^ rk[44];
		t1 = Te0[s1 & 0xff] ^ Te1[(s2 >> 8) & 0xff] ^ Te2[(s3 >> 16) & 0xff] ^ Te3[s0 >> 24] ^ rk[45];
		t2 = Te0[s2 & 0xff] ^ Te1[(s3 >> 8) & 0xff] ^ Te2[(s0 >> 16) & 0xff] ^ Te3[s1 >> 24] ^ rk[46];
		t3 = Te0[s3 & 0xff] ^ Te1[(s0 >> 8) & 0xff] ^ Te2[(s1 >> 16) & 0xff] ^ Te3[s2 >> 24] ^ rk[47];
		if (Nr > 12) {
			/* round 12: */
			s0 = Te0[t0 & 0xff] ^ Te1[(t1 >> 8) & 0xff] ^ Te2[(t2 >> 16) & 0xff] ^ Te3[t3 >> 24] ^ rk[48];
			s1 = Te0[t1 & 0xff] ^ Te1[(t2 >> 8) & 0xff] ^ Te2[(t3 >> 16) & 0xff] ^ Te3[t0 >> 24] ^ rk[49];
			s2 = Te0[t2 & 0xff] ^ Te1[(t3 >> 8) & 0xff] ^ Te2[(t0 >> 16) & 0xff] ^ Te3[t1 >> 24] ^ rk[50];
			s3 = Te0[t3 & 0xff] ^ Te1[(t0 >> 8) & 0xff] ^ Te2[(t1 >> 16) & 0xff] ^ Te3[t2 >> 24] ^ rk[51];
			/* round 13: */
			t0 = Te0[s0 & 0xff] ^ Te1[(s1 >> 8) & 0xff] ^ Te2[(s2 >> 16) & 0xff] ^ Te3[s3 >> 24] ^ rk[52];
			t1 = Te0[s1 & 0xff] ^ Te1[(s2 >> 8) & 0xff] ^ Te2[(s3 >> 16) & 0xff] ^ Te3[s0 >> 24] ^ rk[53];
			t2 = Te0[s2 & 0xff] ^ Te1[(s3 >> 8) & 0xff] ^ Te2[(s0 >> 16) & 0xff] ^ Te3[s1 >> 24] ^ rk[54];
			t3 = Te0[s3 & 0xff] ^ Te1[(s0 >> 8) & 0xff] ^ Te2[(s1 >> 16) & 0xff] ^ Te3[s2 >> 24] ^ rk[55];
		}
	}


	u32 x = Nr << 2;

	s0 =
		(Te4[(t0 & 0xff)] & 0x000000ff) ^
		(Te4[((t1 >> 8) & 0xff)] & 0x0000ff00) ^
		(Te4[((t2 >> 16) & 0xff)] & 0x00ff0000) ^
		(Te4[((t3) >> 24)] & 0xff000000) ^
		rk[x++];

	s1 =
		(Te4[(t1 & 0xff)] & 0x000000ff) ^
		(Te4[((t2 >> 8) & 0xff)] & 0x0000ff00) ^
		(Te4[((t3 >> 16) & 0xff)] & 0x00ff0000) ^
		(Te4[((t0) >> 24)] & 0xff000000) ^
		rk[x++];

	s2 =
		(Te4[(t2 & 0xff)] & 0x000000ff) ^
		(Te4[((t3 >> 8) & 0xff)] & 0x0000ff00) ^
		(Te4[((t0 >> 16) & 0xff)] & 0x00ff0000) ^
		(Te4[((t1) >> 24)] & 0xff000000) ^
		rk[x++];

	s3 =
		(Te4[(t3 & 0xff)] & 0x000000ff) ^
		(Te4[((t0 >> 8) & 0xff)] & 0x0000ff00) ^
		(Te4[((t1 >> 16) & 0xff)] & 0x00ff0000) ^
		(Te4[((t2) >> 24)] & 0xff000000) ^
		rk[x];

	


	ct[0] = s0;
	ct[1] = s1;
	ct[2] = s2;
	ct[3] = s3;



	return 0;
}

int rijndaelDecryptCpu(u32 *ct, u32 *pt, const u32  *rk, const unsigned int Nr){

	register u32 s0, s1, s2, s3, t0, t1, t2, t3;


	s0 = ct[0] ^ rk[0];
	s1 = ct[1] ^ rk[1];
	s2 = ct[2] ^ rk[2];
	s3 = ct[3] ^ rk[3];


	/* round 1: */
	t0 = Td0[s0 & 0xff] ^ Td1[(s3 >> 8) & 0xff] ^ Td2[(s2 >> 16) & 0xff] ^ Td3[s1 >> 24] ^ rk[4];
	t1 = Td0[s1 & 0xff] ^ Td1[(s0 >> 8) & 0xff] ^ Td2[(s3 >> 16) & 0xff] ^ Td3[s2 >> 24] ^ rk[5];
	t2 = Td0[s2 & 0xff] ^ Td1[(s1 >> 8) & 0xff] ^ Td2[(s0 >> 16) & 0xff] ^ Td3[s3 >> 24] ^ rk[6];
	t3 = Td0[s3 & 0xff] ^ Td1[(s2 >> 8) & 0xff] ^ Td2[(s1 >> 16) & 0xff] ^ Td3[s0 >> 24] ^ rk[7];
	/* round 2: */
	s0 = Td0[t0 & 0xff] ^ Td1[(t3 >> 8) & 0xff] ^ Td2[(t2 >> 16) & 0xff] ^ Td3[t1 >> 24] ^ rk[8];
	s1 = Td0[t1 & 0xff] ^ Td1[(t0 >> 8) & 0xff] ^ Td2[(t3 >> 16) & 0xff] ^ Td3[t2 >> 24] ^ rk[9];
	s2 = Td0[t2 & 0xff] ^ Td1[(t1 >> 8) & 0xff] ^ Td2[(t0 >> 16) & 0xff] ^ Td3[t3 >> 24] ^ rk[10];
	s3 = Td0[t3 & 0xff] ^ Td1[(t2 >> 8) & 0xff] ^ Td2[(t1 >> 16) & 0xff] ^ Td3[t0 >> 24] ^ rk[11];
	/* round 3: */
	t0 = Td0[s0 & 0xff] ^ Td1[(s3 >> 8) & 0xff] ^ Td2[(s2 >> 16) & 0xff] ^ Td3[s1 >> 24] ^ rk[12];
	t1 = Td0[s1 & 0xff] ^ Td1[(s0 >> 8) & 0xff] ^ Td2[(s3 >> 16) & 0xff] ^ Td3[s2 >> 24] ^ rk[13];
	t2 = Td0[s2 & 0xff] ^ Td1[(s1 >> 8) & 0xff] ^ Td2[(s0 >> 16) & 0xff] ^ Td3[s3 >> 24] ^ rk[14];
	t3 = Td0[s3 & 0xff] ^ Td1[(s2 >> 8) & 0xff] ^ Td2[(s1 >> 16) & 0xff] ^ Td3[s0 >> 24] ^ rk[15];
	/* round 4: */
	s0 = Td0[t0 & 0xff] ^ Td1[(t3 >> 8) & 0xff] ^ Td2[(t2 >> 16) & 0xff] ^ Td3[t1 >> 24] ^ rk[16];
	s1 = Td0[t1 & 0xff] ^ Td1[(t0 >> 8) & 0xff] ^ Td2[(t3 >> 16) & 0xff] ^ Td3[t2 >> 24] ^ rk[17];
	s2 = Td0[t2 & 0xff] ^ Td1[(t1 >> 8) & 0xff] ^ Td2[(t0 >> 16) & 0xff] ^ Td3[t3 >> 24] ^ rk[18];
	s3 = Td0[t3 & 0xff] ^ Td1[(t2 >> 8) & 0xff] ^ Td2[(t1 >> 16) & 0xff] ^ Td3[t0 >> 24] ^ rk[19];
	/* round 5: */
	t0 = Td0[s0 & 0xff] ^ Td1[(s3 >> 8) & 0xff] ^ Td2[(s2 >> 16) & 0xff] ^ Td3[s1 >> 24] ^ rk[20];
	t1 = Td0[s1 & 0xff] ^ Td1[(s0 >> 8) & 0xff] ^ Td2[(s3 >> 16) & 0xff] ^ Td3[s2 >> 24] ^ rk[21];
	t2 = Td0[s2 & 0xff] ^ Td1[(s1 >> 8) & 0xff] ^ Td2[(s0 >> 16) & 0xff] ^ Td3[s3 >> 24] ^ rk[22];
	t3 = Td0[s3 & 0xff] ^ Td1[(s2 >> 8) & 0xff] ^ Td2[(s1 >> 16) & 0xff] ^ Td3[s0 >> 24] ^ rk[23];
	/* round 6: */
	s0 = Td0[t0 & 0xff] ^ Td1[(t3 >> 8) & 0xff] ^ Td2[(t2 >> 16) & 0xff] ^ Td3[t1 >> 24] ^ rk[24];
	s1 = Td0[t1 & 0xff] ^ Td1[(t0 >> 8) & 0xff] ^ Td2[(t3 >> 16) & 0xff] ^ Td3[t2 >> 24] ^ rk[25];
	s2 = Td0[t2 & 0xff] ^ Td1[(t1 >> 8) & 0xff] ^ Td2[(t0 >> 16) & 0xff] ^ Td3[t3 >> 24] ^ rk[26];
	s3 = Td0[t3 & 0xff] ^ Td1[(t2 >> 8) & 0xff] ^ Td2[(t1 >> 16) & 0xff] ^ Td3[t0 >> 24] ^ rk[27];
	/* round 7: */
	t0 = Td0[s0 & 0xff] ^ Td1[(s3 >> 8) & 0xff] ^ Td2[(s2 >> 16) & 0xff] ^ Td3[s1 >> 24] ^ rk[28];
	t1 = Td0[s1 & 0xff] ^ Td1[(s0 >> 8) & 0xff] ^ Td2[(s3 >> 16) & 0xff] ^ Td3[s2 >> 24] ^ rk[29];
	t2 = Td0[s2 & 0xff] ^ Td1[(s1 >> 8) & 0xff] ^ Td2[(s0 >> 16) & 0xff] ^ Td3[s3 >> 24] ^ rk[30];
	t3 = Td0[s3 & 0xff] ^ Td1[(s2 >> 8) & 0xff] ^ Td2[(s1 >> 16) & 0xff] ^ Td3[s0 >> 24] ^ rk[31];
	/* round 8: */
	s0 = Td0[t0 & 0xff] ^ Td1[(t3 >> 8) & 0xff] ^ Td2[(t2 >> 16) & 0xff] ^ Td3[t1 >> 24] ^ rk[32];
	s1 = Td0[t1 & 0xff] ^ Td1[(t0 >> 8) & 0xff] ^ Td2[(t3 >> 16) & 0xff] ^ Td3[t2 >> 24] ^ rk[33];
	s2 = Td0[t2 & 0xff] ^ Td1[(t1 >> 8) & 0xff] ^ Td2[(t0 >> 16) & 0xff] ^ Td3[t3 >> 24] ^ rk[34];
	s3 = Td0[t3 & 0xff] ^ Td1[(t2 >> 8) & 0xff] ^ Td2[(t1 >> 16) & 0xff] ^ Td3[t0 >> 24] ^ rk[35];
	/* round 9: */
	t0 = Td0[s0 & 0xff] ^ Td1[(s3 >> 8) & 0xff] ^ Td2[(s2 >> 16) & 0xff] ^ Td3[s1 >> 24] ^ rk[36];
	t1 = Td0[s1 & 0xff] ^ Td1[(s0 >> 8) & 0xff] ^ Td2[(s3 >> 16) & 0xff] ^ Td3[s2 >> 24] ^ rk[37];
	t2 = Td0[s2 & 0xff] ^ Td1[(s1 >> 8) & 0xff] ^ Td2[(s0 >> 16) & 0xff] ^ Td3[s3 >> 24] ^ rk[38];
	t3 = Td0[s3 & 0xff] ^ Td1[(s2 >> 8) & 0xff] ^ Td2[(s1 >> 16) & 0xff] ^ Td3[s0 >> 24] ^ rk[39];

	if (Nr > 10) {
		/* round 10: */
		s0 = Td0[t0 & 0xff] ^ Td1[(t3 >> 8) & 0xff] ^ Td2[(t2 >> 16) & 0xff] ^ Td3[t1 >> 24] ^ rk[40];
		s1 = Td0[t1 & 0xff] ^ Td1[(t0 >> 8) & 0xff] ^ Td2[(t3 >> 16) & 0xff] ^ Td3[t2 >> 24] ^ rk[41];
		s2 = Td0[t2 & 0xff] ^ Td1[(t1 >> 8) & 0xff] ^ Td2[(t0 >> 16) & 0xff] ^ Td3[t3 >> 24] ^ rk[42];
		s3 = Td0[t3 & 0xff] ^ Td1[(t2 >> 8) & 0xff] ^ Td2[(t1 >> 16) & 0xff] ^ Td3[t0 >> 24] ^ rk[43];
		/* round 11: */
		t0 = Td0[s0 & 0xff] ^ Td1[(s3 >> 8) & 0xff] ^ Td2[(s2 >> 16) & 0xff] ^ Td3[s1 >> 24] ^ rk[44];
		t1 = Td0[s1 & 0xff] ^ Td1[(s0 >> 8) & 0xff] ^ Td2[(s3 >> 16) & 0xff] ^ Td3[s2 >> 24] ^ rk[45];
		t2 = Td0[s2 & 0xff] ^ Td1[(s1 >> 8) & 0xff] ^ Td2[(s0 >> 16) & 0xff] ^ Td3[s3 >> 24] ^ rk[46];
		t3 = Td0[s3 & 0xff] ^ Td1[(s2 >> 8) & 0xff] ^ Td2[(s1 >> 16) & 0xff] ^ Td3[s0 >> 24] ^ rk[47];
		if (Nr > 12) {
			/* round 12: */
			s0 = Td0[t0 & 0xff] ^ Td1[(t3 >> 8) & 0xff] ^ Td2[(t2 >> 16) & 0xff] ^ Td3[t1 >> 24] ^ rk[48];
			s1 = Td0[t1 & 0xff] ^ Td1[(t0 >> 8) & 0xff] ^ Td2[(t3 >> 16) & 0xff] ^ Td3[t2 >> 24] ^ rk[49];
			s2 = Td0[t2 & 0xff] ^ Td1[(t1 >> 8) & 0xff] ^ Td2[(t0 >> 16) & 0xff] ^ Td3[t3 >> 24] ^ rk[50];
			s3 = Td0[t3 & 0xff] ^ Td1[(t2 >> 8) & 0xff] ^ Td2[(t1 >> 16) & 0xff] ^ Td3[t0 >> 24] ^ rk[51];
			/* round 13: */
			t0 = Td0[s0 & 0xff] ^ Td1[(s3 >> 8) & 0xff] ^ Td2[(s2 >> 16) & 0xff] ^ Td3[s1 >> 24] ^ rk[52];
			t1 = Td0[s1 & 0xff] ^ Td1[(s0 >> 8) & 0xff] ^ Td2[(s3 >> 16) & 0xff] ^ Td3[s2 >> 24] ^ rk[53];
			t2 = Td0[s2 & 0xff] ^ Td1[(s1 >> 8) & 0xff] ^ Td2[(s0 >> 16) & 0xff] ^ Td3[s3 >> 24] ^ rk[54];
			t3 = Td0[s3 & 0xff] ^ Td1[(s2 >> 8) & 0xff] ^ Td2[(s1 >> 16) & 0xff] ^ Td3[s0 >> 24] ^ rk[55];
		}
	}

	u32 x = Nr << 2;

	/* Apply last round */
	s0 =
		(Td4[(t0 & 0xff)] & 0x000000ff) ^
		(Td4[(t3 >> 8) & 0xff] & 0x0000ff00) ^
		(Td4[(t2 >> 16) & 0xff] & 0x00ff0000) ^
		(Td4[(t1 >> 24)] & 0xff000000) ^
		rk[x++];

	s1 =
		(Td4[(t1 & 0xff)] & 0x000000ff) ^
		(Td4[(t0 >> 8) & 0xff] & 0x0000ff00) ^
		(Td4[(t3 >> 16) & 0xff] & 0x00ff0000) ^
		(Td4[(t2 >> 24)] & 0xff000000) ^
		rk[x++];

	s2 =
		(Td4[(t2 & 0xff)] & 0x000000ff) ^
		(Td4[(t1 >> 8) & 0xff] & 0x0000ff00) ^
		(Td4[(t0 >> 16) & 0xff] & 0x00ff0000) ^
		(Td4[(t3 >> 24)] & 0xff000000) ^
		rk[x++];

	s3 =
		(Td4[(t3 & 0xff)] & 0x000000ff) ^
		(Td4[(t2 >> 8) & 0xff] & 0x0000ff00) ^
		(Td4[(t1 >> 16) & 0xff] & 0x00ff0000) ^
		(Td4[(t0 >> 24)] & 0xff000000) ^
		rk[x];

	pt[0] = s0;
	pt[1] = s1;
	pt[2] = s2;
	pt[3] = s3;

	return 0;
}

int rijndaelEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	u32 rk[rkLength];
	

	start = startStopwatch();
	unsigned int Nr = rijndaelKeySetupEnc(rk, (u32 *)cipherKey, cipherKeyLength);
	endStopwatch("Encryption key calculation", start );
	
	start = startStopwatch();
	for (int numBlocks = plainTextLength >> 4; numBlocks > 0; numBlocks--, pt += BYTES_PER_BLOCK, ct += BYTES_PER_BLOCK){
		rijndaelEncryptCpu((u32 *)pt, (u32 *)ct, rk, Nr);
	}
	endStopwatch("Encryption", start);


    return 0;



}

int rijndaelDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	u32 rk[rkLength];
	
	start = startStopwatch();
	unsigned int Nr = rijndaelKeySetupDec(rk, (u32 *)cipherKey, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);

	start = startStopwatch();
	for (int numBlocks = plainTextLength >> 4; numBlocks > 0; numBlocks--, ct += BYTES_PER_BLOCK, pt += BYTES_PER_BLOCK){
		rijndaelDecryptCpu((u32 *)ct, (u32 *)pt, rk, Nr);
	}
	endStopwatch("Decryption", start);


    return 0;


}