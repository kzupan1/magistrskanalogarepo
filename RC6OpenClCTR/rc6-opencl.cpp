#include "rc6-opencl.h"
#include "measure-time.h"
#include "opencl-helpers.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <CL/cl.h>
#include <boost/chrono.hpp>

#define BUILD_SOURCE 0

const char *options = "-I ./common-types.h";

const char *encryptKernelSource = "rc6EncryptKernel.cl";
const char *encryptKernelBin = "rc6EncryptKernel.clbin";


cl_int rc6EncryptOpenCl(const cl_uint4 pt[], cl_uint4 ct[], const u32 rk[], const int blocks, InitValues *initValues, cl_kernel krnl)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;


	cl_mem devRk;

	cl_mem pinnedPt;
	cl_mem pinnedCt;

	cl_uint4* hostPinnedPt;
	cl_uint4* hostPinnedCt;

    cl_int clStatus;

	srand(0);
	u32 n0 = rand();
	u32 n1 = rand();
	ct[0].s[0] = n0;
	ct[0].s[1] = n1;
	ct[0].s[2] = 0;
	ct[0].s[3] = 0;

	ct++;

	// Create a command queue
	cl_command_queue command_queue = clCreateCommandQueue(initValues->context, initValues->device_list[0], 0, &clStatus);
	start = startStopwatch();
	pinnedPt = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, blocks * sizeof(cl_uint4), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedPt failed!");
		return clStatus;
	}

	pinnedCt = clCreateBuffer(initValues->context, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, blocks * sizeof(cl_uint4), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedCt failed!");
		return clStatus;
	}

	hostPinnedPt = (cl_uint4 *)clEnqueueMapBuffer(command_queue, pinnedPt, CL_TRUE, CL_MAP_WRITE, 0, blocks * sizeof(cl_uint4), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}

	memcpy(hostPinnedPt, pt, blocks * sizeof(cl_uint4));


	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedPt, hostPinnedPt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);

	
	

	devRk = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY, R24 * sizeof(u32), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clCreateBuffer failed!");
        return clStatus;
    }
	
	
	endStopwatch("clCreateBuffer (encryption) ", start);

	start = startStopwatch();
	

	clStatus = clEnqueueWriteBuffer(command_queue, devRk, CL_TRUE, 0, R24 * sizeof(u32), rk, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clEnqueueWriteBuffer failed!");
        return clStatus;
    }

	clFlush(command_queue);
	clFinish(command_queue);

	totalTime = endStopwatch("clEnqueueWriteBuffer (encryption)", start);


	// Set the arguments of the kernel
	clStatus = clSetKernelArg(krnl, 0, sizeof(cl_mem), (void *)&pinnedPt);
	clStatus = clSetKernelArg(krnl, 1, sizeof(cl_mem), (void *)&pinnedCt);
	clStatus = clSetKernelArg(krnl, 2, sizeof(cl_mem), (void *)&devRk);
	clStatus = clSetKernelArg(krnl, 3, sizeof(int), (void *)&blocks);
	clStatus = clSetKernelArg(krnl, 4, sizeof(u32), (void *)&n0);
	clStatus = clSetKernelArg(krnl, 5, sizeof(u32), (void *)&n1);



	//!!!must be padded before!!!
	size_t global_size = ((blocks - 1)/THREAD_BLOCK_SIZE + 1) * THREAD_BLOCK_SIZE ;
	size_t local_size = THREAD_BLOCK_SIZE;

    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	clStatus = clEnqueueNDRangeKernel(command_queue, krnl, 1, NULL, &global_size, &local_size, 0, NULL, NULL);
	
	// Check for any errors launching the kernel
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "encryption kernel launch failed!");
		printf("%d\n", clStatus);
		return clStatus;
	}

	clFlush(command_queue);
	clFinish(command_queue);

	totalTime += endStopwatch("Encryption kernel", start);

	hostPinnedCt = (cl_uint4 *)clEnqueueMapBuffer(command_queue, pinnedCt, CL_TRUE, CL_MAP_READ, 0, blocks * sizeof(cl_uint4), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}
	memcpy(ct, hostPinnedCt, blocks * sizeof(cl_uint4));
	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedCt, hostPinnedCt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);
    


	// Finally release all OpenCL allocated objePts and host buffers.
	start = startStopwatch();

	clStatus = clReleaseMemObject(devRk);

	clStatus = clReleaseCommandQueue(command_queue);

	endStopwatch("clReleaseMemObject (encryption)", start);

	clStatus = clReleaseMemObject(pinnedPt);
	clStatus = clReleaseMemObject(pinnedCt);

	printDuration("Encryption", totalTime);

	return clStatus;
}

cl_int rc6DecryptOpenCl(const cl_uint4 ct[], cl_uint4 pt[], const u32 rk[], const int blocks, InitValues *initValues, cl_kernel krnl)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;


	cl_mem devRk;

	cl_mem pinnedCt;
	cl_mem pinnedPt;

	u8* hostPinnedPt;
	u8* hostPinnedCt;

    cl_int clStatus;

	u32 n0 = ct[0].s[0];
	u32 n1 = ct[0].s[1];

	ct++;


	// Create a command queue
	cl_command_queue command_queue = clCreateCommandQueue(initValues->context, initValues->device_list[0], 0, &clStatus);

	start = startStopwatch();
	pinnedCt = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, blocks * sizeof(cl_uint4), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedPt failed!");
		return clStatus;
	}

	pinnedPt = clCreateBuffer(initValues->context, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, blocks * sizeof(cl_uint4), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedCt failed!");
		return clStatus;
	}

	hostPinnedCt = (u8 *)clEnqueueMapBuffer(command_queue, pinnedCt, CL_TRUE, CL_MAP_WRITE, 0, blocks * sizeof(cl_uint4), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}

	memcpy(hostPinnedCt, ct, blocks * sizeof(cl_uint4));


	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedCt, hostPinnedCt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);

	start = startStopwatch();
	

	devRk = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY, R24 * sizeof(u32), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clCreateBuffer failed!");
        return clStatus;
    }
	
	
	endStopwatch("clCreateBuffer (decryption) ", start);

	start = startStopwatch();
	

	clStatus = clEnqueueWriteBuffer(command_queue, devRk, CL_TRUE, 0, R24 * sizeof(u32), rk, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clEnqueueWriteBuffer failed!");
        return clStatus;
    }

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);

	totalTime = endStopwatch("clEnqueueWriteBuffer (decryption) ", start);


	// Set the arguments of the kernel
	clStatus = clSetKernelArg(krnl, 0, sizeof(cl_mem), (void *)&pinnedCt);
	clStatus = clSetKernelArg(krnl, 1, sizeof(cl_mem), (void *)&pinnedPt);
	clStatus = clSetKernelArg(krnl, 2, sizeof(cl_mem), (void *)&devRk);
	clStatus = clSetKernelArg(krnl, 3, sizeof(int), (void *)&blocks);
	clStatus = clSetKernelArg(krnl, 4, sizeof(u32), (void *)&n0);
	clStatus = clSetKernelArg(krnl, 5, sizeof(u32), (void *)&n1);


	//!!!must be padded before!!!
	size_t global_size = ((blocks - 1)/THREAD_BLOCK_SIZE + 1) * THREAD_BLOCK_SIZE ;
	size_t local_size = THREAD_BLOCK_SIZE; ;

	
    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	clStatus = clEnqueueNDRangeKernel(command_queue, krnl, 1, NULL, &global_size, &local_size, 0, NULL, NULL);
	
	// Check for any errors launching the kernel
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Decryption kernel launch failed!");
		return clStatus;
	}

	clFlush(command_queue);
	clFinish(command_queue);

	totalTime += endStopwatch("Decryption kernel", start);

	hostPinnedPt = (u8 *)clEnqueueMapBuffer(command_queue, pinnedPt, CL_TRUE, CL_MAP_READ, 0, blocks * sizeof(cl_uint4), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}
	memcpy(pt, hostPinnedPt, blocks * sizeof(cl_uint4));
	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedPt, hostPinnedPt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);

	
	// Finally release all OpenCL allocated objects and host buffers.
	start = startStopwatch();

	clStatus = clReleaseMemObject(devRk);

	clStatus = clReleaseCommandQueue(command_queue);
	
	endStopwatch("clReleaseMemObject (decryption)", start);

	clStatus = clReleaseMemObject(pinnedCt);
	clStatus = clReleaseMemObject(pinnedPt);

	printDuration("Decryption", totalTime);

	return clStatus;
}


int makeKey(const u32 cipherKey[], u32 rk[R24], const unsigned int keyLen){

	u32 l[8], i, j, a, k, b, t;

	rk[0] = P32;

	for (k = 1; k < R24; ++k)
		rk[k] = rk[k - 1] + Q32;

	for (k = 0; k < keyLen / BYTES_PER_WORD; ++k)
		l[k] = cipherKey[k];

	a = b = i = j = 0;

	t = (keyLen / BYTES_PER_WORD) - 1;

	for (k = 0; k < 132; ++k)
	{
		a = ROL(rk[i] + a + b, 3); b += a;
		b = ROL(l[j] + b, b);
		rk[i] = a; l[j] = b;
		i = (i == 43 ? 0 : i + 1);  // i = (i + 1) % 44;
		j = (j == t ? 0 : j + 1);   // j = (j + 1) % t;
	}


	return 0;
}


int rc6Encrypt(cl_uint4 *pt, cl_uint4 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
{
	boost::chrono::high_resolution_clock::time_point start;

	clReleaseMemObject(NULL);
	InitValues *initValues;
	cl_program program;
	cl_kernel krnl;
	cl_int clStatus;

	u32 rk[R24];

	start = startStopwatch();
	initValues = openClInit();
	endStopwatch("OpenCl initialization (encryption)", start);


	start = startStopwatch();
	program = getProgram(initValues, encryptKernelSource, encryptKernelBin, options, BUILD_SOURCE);
	endStopwatch("Program building (encryption)", start);

	if (program == NULL){
		printf("Error while creating program. \n");
		return -1;

	}
	

	start = startStopwatch();
	// Create the OpenCL kernel
	krnl = clCreateKernel(program, "encryptKernel", &clStatus);
	endStopwatch("Kernel creation (encryption) ", start);

	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Kernel creating failed!\n ");
		return clStatus;
	}

	

	start = startStopwatch();
	makeKey((u32*)cipherKey, rk, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	clStatus = rc6EncryptOpenCl(pt, ct, rk, blocks, initValues, krnl);
	

	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Encryption failed!");
		return 1;
	}

	start = startStopwatch();
	clStatus = clReleaseKernel(krnl);
	clStatus = clReleaseProgram(program);
	openClDeInit(initValues);

	endStopwatch("Freeing OpenCL resources (encryption)", start);


	return 0;




}
int rc6Decrypt(cl_uint4 *ct, cl_uint4 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	clReleaseMemObject(NULL);
	InitValues *initValues;
	cl_program program;
	cl_kernel krnl;
	cl_int clStatus;

	u32 rk[R24];;

	start = startStopwatch();
	initValues = openClInit();
	endStopwatch("OpenCl initialization (decryption)", start);


	start = startStopwatch();
	program = getProgram(initValues, encryptKernelSource, encryptKernelBin, options, BUILD_SOURCE);
	endStopwatch("Program building (decryption)", start);

	if (program == NULL){
		printf("Error while creating program. \n");
		return -1;

	}
	

	start = startStopwatch();
	// Create the OpenCL kernel
	krnl = clCreateKernel(program, "encryptKernel", &clStatus);
	endStopwatch("Kernel creation (decryption)", start);

	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Kernel creating failed!\n ");
		return clStatus;
	}

	


	start = startStopwatch();
	makeKey((u32*)cipherKey, rk, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	clStatus = rc6DecryptOpenCl(ct, pt, rk, blocks, initValues, krnl);
	

	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Decryption failed!");
		return 1;
	}

	start = startStopwatch();
	clStatus = clReleaseKernel(krnl);
	clStatus = clReleaseProgram(program);
	openClDeInit(initValues);
	endStopwatch("Freeing OpenCL resources (decryption)", start);


	return 0;

}