#include "common-types.h"

#define	ROL(x,n) (((x) << ((n) & 0x1F)) | ((x) >> (32-((n) & 0x1F))))


#define f_rnd(i,a,b,c,d)                        \
	u = ROL(d * (d + d + 1), 5);           \
	t = ROL(b * (b + b + 1), 5);           \
	a = ROL(a ^ t, u) + rk[i];     \
	c = ROL(c ^ u, t) + rk[i + 1]

__kernel void encryptKernel(__global uint4 *pt, __global uint4 *ct, __constant u32 *rk, const int blocks, const u32 n0, const u32 n1) 
{																																	  
	
	
																																	  
	u64 i = get_global_id(0);																										  																							  
																																	  
	if(i < blocks){																							  
																																	  																											  
																																	  
		register u32 a, b, c, d, t, u;


		a = n0;
		b = n1 + rk[0];
		c = ((i & 0xffffffff00000000) >> 32);
		d = (i & 0xffffffff) + rk[1];

		f_rnd(2, a, b, c, d); f_rnd(4, b, c, d, a);
		f_rnd(6, c, d, a, b); f_rnd(8, d, a, b, c);
		f_rnd(10, a, b, c, d); f_rnd(12, b, c, d, a);
		f_rnd(14, c, d, a, b); f_rnd(16, d, a, b, c);
		f_rnd(18, a, b, c, d); f_rnd(20, b, c, d, a);
		f_rnd(22, c, d, a, b); f_rnd(24, d, a, b, c);
		f_rnd(26, a, b, c, d); f_rnd(28, b, c, d, a);
		f_rnd(30, c, d, a, b); f_rnd(32, d, a, b, c);
		f_rnd(34, a, b, c, d); f_rnd(36, b, c, d, a);
		f_rnd(38, c, d, a, b); f_rnd(40, d, a, b, c);

				
		ct[i] = (uint4){ (a + rk[42]) ^ pt[i].x, b ^ pt[i].y, (c + rk[43]) ^ pt[i].z, d ^ pt[i].w };
	}																																  
																																	  
																							  
																																	 																																	  
																																	  
}																																	  

