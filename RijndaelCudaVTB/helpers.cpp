#include <string.h>
#include <stdlib.h>
#include <stdio.h>


#include "helpers.h"
#include "common-types.h"

int padBytes(DataMaterial *dataMaterial){

	int off, newLength, i;

	off = 16 - dataMaterial->bytesLength % 16;

	newLength = dataMaterial->bytesLength + off;

	dataMaterial->bytes = (u8 *)realloc(dataMaterial->bytes, newLength * sizeof(u8));
	
	for (i = dataMaterial->bytesLength; i < newLength; i++){
	
		dataMaterial->bytes[i] = off;
	
	}
	

	dataMaterial->bytesLength = newLength;

	return 0;


}

int unPadBytes(DataMaterial *dataMaterial){

	int off, newLength;

	off = dataMaterial->bytes[dataMaterial->bytesLength - 1];


	newLength = dataMaterial->bytesLength - off;


	dataMaterial->bytes = (u8 *)realloc(dataMaterial->bytes, newLength * sizeof(u8));
	dataMaterial->bytesLength = newLength;


	return 0;



}


int fromHexsToBytes(char *hexs, DataMaterial *dataMaterial){

	int count;
	u32 c;

	for (count = 0; count < dataMaterial->bytesLength; count++) {

		sscanf(hexs, "%2x", &c);
		dataMaterial->bytes[count] = c;

		hexs += 2;
	}



	return 0;

}

DataMaterial *fromHexPlaintextToByte(char *hexPlainText, DataMaterial *plainTextMaterial){

	int l;

	l = strlen(hexPlainText);

	if ((l & 1) != 0){
		printf("Hex number missing! in plaintext!\n");
		exit(1);
	}
	
	plainTextMaterial->bytesLength = l / 2;
	
	plainTextMaterial->hex = hexPlainText;
	plainTextMaterial->bytes = (u8 *)malloc(plainTextMaterial->bytesLength * sizeof(u8));
	
	fromHexsToBytes(hexPlainText, plainTextMaterial);
	padBytes(plainTextMaterial);

	
	
	
	return plainTextMaterial;


}


int fromHexKeyToByte(char *hexKey, DataMaterial *keyMaterial){

	int l;

	l = strlen(hexKey);

	if ((l & 1) != 0){
		printf("Hex number missing in key! \n");
		exit(1);
	}


	if (!(l == 32 || l == 48 || l == 64)){
		printf("Wrong key size! \n");
		exit(1);
	
	}
	
	keyMaterial->bytesLength = l / 2;
	keyMaterial->hex = hexKey;
	keyMaterial->bytes = (u8 *)malloc(keyMaterial->bytesLength * sizeof(u8));
	
	fromHexsToBytes(hexKey, keyMaterial);
	
	return 0;


}

void printHelp(){
	printf("Two options are allowed: \n");
	printf("./prog -k key -p plaintext\n");
	printf("or\n");
	printf("./prog -k key -f filename\n");
	printf("\n");
	exit(0);

}

void readPlainTextFromFile(char *fileName, DataMaterial *plainTextMaterial){
	
	FILE * fp;
	fp = fopen(fileName, "rb");
	if (fp != NULL) {

		fseek(fp, 0L, SEEK_END);
		size_t fileSize = ftell(fp);
		const unsigned int plainTextLength = fileSize;
		rewind(fp);

		u8  *pt = (u8*)malloc(sizeof(u8)*plainTextLength);

		size_t result = fread(pt, fileSize, 1, fp);

		if (result == fileSize)	
		{
			printf("Error: only %zd could be read\n", result);
			exit(-1);
		}
		fclose(fp);

		plainTextMaterial->bytes = pt;
		plainTextMaterial->bytesLength = plainTextLength;
		padBytes(plainTextMaterial);


	}
	else{
		printf("Cannot open file!\n");
		exit(-1);
	}
}


void GetArgs(int argc, char **argv, DataMaterial *keyMaterial, DataMaterial *plainTextMaterial, unsigned int *threadBlockSize){

	int idx;
	bool k, p, f, t;
	char *key;
	char *plainText;
	char *fileName;



	key = NULL;
	plainText = NULL;
	fileName = NULL;

	k = false;
	p = false;
	f = false;
	t = false;


	for (idx = 1; idx < argc; idx++) {
		if (strcmp(argv[idx], "-k") == 0) {
			if (k){
				printf("Error: Only one key is allowed!\n");
				exit(-1);
			}
			k = true;
			key = argv[++idx];
	
		}
		else if (strcmp(argv[idx], "-p") == 0) {
			if (p){
				printf("Error: Only one plaintext is allowed!\n");
				exit(-1);
			}

			p = true;
			plainText = argv[++idx];
		}
		else if (strcmp(argv[idx], "-f") == 0) {
			if (f){
				printf("Error: Only one filename is allowed!\n");
				exit(-1);
			}

			f = true;
			fileName = argv[++idx];
		}
		else if (strcmp(argv[idx], "-h") == 0) {
			printHelp();
		}
		else if (strcmp(argv[idx], "-t") == 0) {
			
			*threadBlockSize = atoi(argv[++idx]);
			if (*threadBlockSize == 0)
				t = false;
			else
				t = true;
		}
		else {
			printf("Error: unknown flag\n");
			exit(-1);
		}
	}

	if (!k){
		printf("Error: Key is missing!\n");
		exit(-1);
	}

	if (!p && !f){
		printf("Error: Plaintext or filename is missing!\n");
		exit(-1);

	}

	if (p && f){
		printf("Error: Both plaintext and filename can't be used together!\n");
		exit(-1);

	}
	if (!t)
		*threadBlockSize = 256;

	fromHexKeyToByte(key, keyMaterial);
	if (p)
		fromHexPlaintextToByte(plainText, plainTextMaterial);
	if (f)
		readPlainTextFromFile(fileName, plainTextMaterial);


}// end GetArgs