#ifndef __HELPERS_H
#define __HELPERS_H
#include "common-types.h"


typedef struct{
	char *hex;
	u8 *bytes;
	int bytesLength;
} DataMaterial;

int unPadBytes(DataMaterial *dataMaterial);
void GetArgs(int argc, char **argv, DataMaterial *keyMaterial, DataMaterial *plainTextMaterial, unsigned int *threadBlockSize);

#endif /* __HELPERS_H */