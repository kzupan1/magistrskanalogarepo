#ifndef __RC6_OPEN_CL_H
#define __RC6_OPEN_CL_H

#include "common-types.h"

#define P32 0xB7E15163
#define Q32 0x9E3779B9
#define lgw     5
#define _r 20
#define R24     (2 * _r + 4)

#define	ROL(x,n) (((x) << ((n) & 0x1F)) | ((x) >> (32-((n) & 0x1F))))
#define	ROR(x,n) (((x) >> ((n) & 0x1F)) | ((x) << (32-((n) & 0x1F))))


int rc6Encrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);
int rc6Decrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);

#endif /* __RC6_OPEN_CL_H */