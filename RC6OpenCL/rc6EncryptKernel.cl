#include "rc6-opencl.h"

#define f_rnd(i,a,b,c,d)                        \
	u = ROL(d * (d + d + 1), 5);           \
	t = ROL(b * (b + b + 1), 5);           \
	a = ROL(a ^ t, u) + rk[i];     \
	c = ROL(c ^ u, t) + rk[i + 1]

__kernel void encryptKernel(__global uint4 *pt, __global uint4 *ct, __constant u32 *rk, const unsigned int blocks) 
{																																	  
	
	
																																	  
	int i = get_global_id(0);																										  																							  
																																	  
	if(i < blocks){																							  
																																	  																											  
																																	  
		register u32 a, b, c, d, t, u;

		uint4 block_in = pt[i];
		uint4 block_out;

		a = block_in.x;
		b = block_in.y + rk[0];
		c = block_in.z;
		d = block_in.w + rk[1];

		f_rnd(2, a, b, c, d); f_rnd(4, b, c, d, a);
		f_rnd(6, c, d, a, b); f_rnd(8, d, a, b, c);
		f_rnd(10, a, b, c, d); f_rnd(12, b, c, d, a);
		f_rnd(14, c, d, a, b); f_rnd(16, d, a, b, c);
		f_rnd(18, a, b, c, d); f_rnd(20, b, c, d, a);
		f_rnd(22, c, d, a, b); f_rnd(24, d, a, b, c);
		f_rnd(26, a, b, c, d); f_rnd(28, b, c, d, a);
		f_rnd(30, c, d, a, b); f_rnd(32, d, a, b, c);
		f_rnd(34, a, b, c, d); f_rnd(36, b, c, d, a);
		f_rnd(38, c, d, a, b); f_rnd(40, d, a, b, c);

		block_out.x = a + rk[42];
		block_out.y = b;
		block_out.z = c + rk[43];
		block_out.w = d;
				
		ct[i] = block_out;
	}																																  
																																	  
																							  
																																	 																																	  
																																	  
}																																	  

