#include "rc6-opencl.h"

#define i_rnd(i,a,b,c,d)                        \
	u = ROL(d * (d + d + 1), 5);           \
	t = ROL(b * (b + b + 1), 5);           \
	c = ROR(c - rk[i + 1], t) ^ u; \
	a = ROR(a - rk[i], u) ^ t

__kernel void decryptKernel(__global uint4 *ct, __global uint4 *pt, __constant u32 *rk, const unsigned int blocks)	
{																																		
	
	


	int i = get_global_id(0);																											
																																		
																																																																																						
	if(i < blocks){																								
																																		

		register u32 a, b, c, d, t, u;

		uint4 block_in = ct[i];
		uint4 block_out;

		a = block_in.x - rk[42];
		b = block_in.y;
		c = block_in.z - rk[43];
		d = block_in.w;

		i_rnd(40, d, a, b, c); i_rnd(38, c, d, a, b);
		i_rnd(36, b, c, d, a); i_rnd(34, a, b, c, d);
		i_rnd(32, d, a, b, c); i_rnd(30, c, d, a, b);
		i_rnd(28, b, c, d, a); i_rnd(26, a, b, c, d);
		i_rnd(24, d, a, b, c); i_rnd(22, c, d, a, b);
		i_rnd(20, b, c, d, a); i_rnd(18, a, b, c, d);
		i_rnd(16, d, a, b, c); i_rnd(14, c, d, a, b);
		i_rnd(12, b, c, d, a); i_rnd(10, a, b, c, d);
		i_rnd(8, d, a, b, c); i_rnd(6, c, d, a, b);
		i_rnd(4, b, c, d, a); i_rnd(2, a, b, c, d);

		block_out.x = a;
		block_out.y = b - rk[0];
		block_out.z = c;
		block_out.w = d - rk[1];
									

		pt[i] = block_out;
	}																																	
																																																									
																																		
																																		
																																		
}																																		
