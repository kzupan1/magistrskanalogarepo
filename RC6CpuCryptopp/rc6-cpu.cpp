#include "rc6-cpu.h"
#include "measure-time.h"
#include <boost/chrono.hpp>
#include "rc6.h"
using CryptoPP::RC6;
#include <modes.h>
using CryptoPP::ECB_Mode;



int rc6Encrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	CryptoPP::ECB_Mode< RC6 >::Encryption e;

	start = startStopwatch();
	e.SetKey(cipherKey, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);

	start = startStopwatch();
	e.ProcessData(ct, pt, plainTextLength);
	endStopwatch("Encryption", start);
	


    return 0;




}
int rc6Decrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){
	
	boost::chrono::high_resolution_clock::time_point start;
	
	ECB_Mode< RC6 >::Decryption d;
	start = startStopwatch();
	d.SetKey(cipherKey, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);

	start = startStopwatch();
	d.ProcessData(pt, ct, plainTextLength);
	endStopwatch("Decryption", start);
	


    return 0;

}