#ifndef __RC6_CPU_H
#define __RC6_CPU_H
#include "common-types.h"

int rc6Encrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);
int rc6Decrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);


#endif /* __RC6_CPU_H */