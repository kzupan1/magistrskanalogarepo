#ifndef __TWOFISH_CUDA_H
#define __TWOFISH_CUDA_H
#include "common-types.h"

int twofishEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);
int twofishDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);



#endif /* __TWOFISH_CPU_H */