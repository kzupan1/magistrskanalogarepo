#include "twofish-cpu.h"
#include "measure-time.h"
#include <boost/chrono.hpp>
#include "twofish.h"
using CryptoPP::Twofish;
#include <modes.h>
using CryptoPP::ECB_Mode;


int twofishEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	CryptoPP::ECB_Mode< Twofish >::Encryption e;

	start = startStopwatch();
	e.SetKey(cipherKey, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);



	start = startStopwatch();
	e.ProcessData(ct, pt, plainTextLength);
	endStopwatch("Encryption", start);



    return 0;




}
int twofishDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){
	
	boost::chrono::high_resolution_clock::time_point start;

	ECB_Mode< Twofish >::Decryption d;

	start = startStopwatch();
	d.SetKey(cipherKey, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);


	start = startStopwatch();
	d.ProcessData(pt, ct, plainTextLength);
	endStopwatch("Decryption", start);



    return 0;

}