#include "rc6-cpu.h"
#include "measure-time.h"
#include <boost/chrono.hpp>


int makeKey(const u32 cipherKey[], u32 rk[R24], const unsigned int keyLen ){

	u32 l[8], i, j, a, k, b, t;

	rk[0] = P32;

	for(k = 1; k < R24; ++k)
		rk[k] = rk[k-1] + Q32;

	for(k = 0; k < keyLen/BYTES_PER_WORD; ++k)
		l[k] = cipherKey[k];

	a = b = i = j = 0;

	t = (keyLen / BYTES_PER_WORD) - 1;

	for(k = 0; k < 132; ++k)
		{   a = ROL(rk[i] + a + b, 3); b += a;
			b = ROL(l[j] + b, b);
			rk[i] = a; l[j] = b;
			i = (i == 43 ? 0 : i + 1);  // i = (i + 1) % 44;
			j = (j == t ? 0 : j + 1);   // j = (j + 1) % t;
		}


	return 0;
}


#define f_rnd(i,a,b,c,d)                        \
        u = ROL(d * (d + d + 1), 5);           \
        t = ROL(b * (b + b + 1), 5);           \
        a = ROL(a ^ t, u) + rk[i];     \
        c = ROL(c ^ u, t) + rk[i + 1]



uint4 rc6EncryptCpu(uint4 in, const u32 * rk){

	register u32 a, b, c, d, t, u;
	
	a = in.x;
	b = in.y + rk[0];
	c = in.z;
	d = in.w + rk[1];

	f_rnd( 2,a,b,c,d); f_rnd( 4,b,c,d,a);
    f_rnd( 6,c,d,a,b); f_rnd( 8,d,a,b,c);
    f_rnd(10,a,b,c,d); f_rnd(12,b,c,d,a);
    f_rnd(14,c,d,a,b); f_rnd(16,d,a,b,c);
    f_rnd(18,a,b,c,d); f_rnd(20,b,c,d,a);
    f_rnd(22,c,d,a,b); f_rnd(24,d,a,b,c);
    f_rnd(26,a,b,c,d); f_rnd(28,b,c,d,a);
    f_rnd(30,c,d,a,b); f_rnd(32,d,a,b,c);
    f_rnd(34,a,b,c,d); f_rnd(36,b,c,d,a);
    f_rnd(38,c,d,a,b); f_rnd(40,d,a,b,c);

	return {a + rk[42], b, c + rk[43], d};
}




int rc6Encrypt(uint4 *pt, uint4 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	u32 rk[R24];
	
	start = startStopwatch();
	makeKey((u32*)cipherKey, rk, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);

	srand(0);
	u32 n0 = rand();
	u32 n1 = rand();
	u64 ctr = 0;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	uint4 in;
	uint4 out;

	ct[0] = { n0, n1, 0, 0 };


	start = startStopwatch();
	for (int i = 0; i < blocks; i++){
		in = { n0, n1, ((ctr & 0xffffffff00000000) >> 32), (ctr & 0xffffffff) };

		out = rc6EncryptCpu(in, rk);


		ct[i + 1].x = pt[i].x ^ out.x;
		ct[i + 1].y = pt[i].y ^ out.y;
		ct[i + 1].z = pt[i].z ^ out.z;
		ct[i + 1].w = pt[i].w ^ out.w;


		ctr++;
	}

	endStopwatch("Encryption", start);
	


    return 0;




}
int rc6Decrypt(uint4 *ct, uint4 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){
	
	boost::chrono::high_resolution_clock::time_point start;
	
	u32 rk[R24];
	
	start = startStopwatch();
	makeKey((u32*)cipherKey, rk, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);
	
	u32 n0;
	u32 n1;
	u64 ctr = 0;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	uint4 in;
	uint4 out;

	n0 = ct[0].x;
	n1 = ct[0].y;

	start = startStopwatch();
	for (int i = 0; i < blocks; i++){
		in = { n0, n1, ((ctr & 0xffffffff00000000) >> 32), (ctr & 0xffffffff) };

		out = rc6EncryptCpu(in, rk);

		pt[i].x = ct[i + 1].x ^ out.x;
		pt[i].y = ct[i + 1].y ^ out.y;
		pt[i].z = ct[i + 1].z ^ out.z;
		pt[i].w = ct[i + 1].w ^ out.w;


		ctr += 1;
	}
	endStopwatch("Decryption", start);
	


    return 0;

}