#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdlib.h>
#include <stdio.h>
#include "twofish-cuda.cuh"
#include "twofish-tables.cuh"
#include "measure-time.h"
#include <boost/chrono.hpp>

#define	Fe32(sBox, x,R) (sBox[        2*_b(x, R  )    ] ^ \
				sBox[        2*_b(x, R+1) + 1] ^ \
				sBox[0x200 + 2*_b(x, R+2)    ] ^ \
				sBox[0x200 + 2*_b(x, R+3) + 1])


__constant__ u32 sKey[TOTAL_SUBKEYS];

__global__ void encryptKernel(uint4 *pt, uint4 *ct, const u32 * sBox,  const int blocks, const u32 n0, const u32 n1){

	#define encrypt2Rounds()	t0 = Fe32(sBox_sh, x0, 0);\
								t1 = Fe32(sBox_sh, x1, 3);\
								x2 ^= t0 + t1 + sKey[k++];\
								x2  = ROR(x2, 1);		  \
								x3  = ROL(x3, 1);		  \
								x3 ^= t0 + 2*t1 + sKey[k++];\
								t0 = Fe32(sBox_sh, x2, 0);\
								t1 = Fe32(sBox_sh, x3, 3);\
								x0 ^= t0 + t1 + sKey[k++];\
								x0  = ROR(x0, 1) ;		\
								x1  = ROL(x1, 1);			\
								x1 ^= t0 + 2*t1 + sKey[k++];

	#define applyEncryptRounds() encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();



	int t = threadIdx.x;
	u64 i = blockDim.x * blockIdx.x + t;
	

	__shared__ u32 sBox_sh[S_BOX_SIZE];

	if (t * 4 < S_BOX_SIZE){

		sBox_sh[t + 0 * 256] = sBox[t + 0 * 256];
		sBox_sh[t + 1 * 256] = sBox[t + 1 * 256];
		sBox_sh[t + 2 * 256] = sBox[t + 2 * 256];
		sBox_sh[t + 3 * 256] = sBox[t + 3 * 256];
	}



	__syncthreads();
	
	if(i < blocks){
		
		
		register u32 x0, x1, x2, x3, t0, t1;
		register int k;

		x0 = n0 ^ sKey[INPUT_WHITEN];
		x1 = n1 ^ sKey[INPUT_WHITEN + 1];
		x2 = ((i & 0xffffffff00000000) >> 32) ^ sKey[INPUT_WHITEN + 2];
		x3 = (i & 0xffffffff) ^ sKey[INPUT_WHITEN + 3];

		k = ROUND_SUBKEYS;
		
		applyEncryptRounds();

		ct[i] = { (x2 ^ sKey[OUTPUT_WHITEN]) ^ pt[i].x, (x3 ^ sKey[OUTPUT_WHITEN + 1]) ^ pt[i].y, (x0 ^ sKey[OUTPUT_WHITEN + 2]) ^ pt[i].z, (x1 ^ sKey[OUTPUT_WHITEN + 3]) ^ pt[i].w };
		
	}

	

}




cudaError_t twofishEncryptCuda(const uint4 pt[], uint4 ct[], const u32 subKeys[TOTAL_SUBKEYS], const u32 sBox[S_BOX_SIZE], const int blocks)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	uint4 *devPt;
	uint4 *devCt;
	u32 *devSBox;

	srand(0);
	u32 n0 = rand();
	u32 n1 = rand();
	ct[0] = { n0, n1, 0, 0 };

    cudaError_t cudaStatus;


    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        return cudaStatus;
    }

    // cuda malloc
	start = startStopwatch();
	cudaHostGetDevicePointer((void **)&devPt, (void *)pt, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	cudaHostGetDevicePointer((void **)&devCt, (void *)ct, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	

	cudaStatus = cudaMalloc((void**)&devSBox, S_BOX_SIZE * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!", 0);
        return cudaStatus;
    }

	
	endStopwatch("Malloc & GetDevicePointer(encryption)", start);

	start = startStopwatch();

	devCt++;

	cudaStatus = cudaMemcpyToSymbol(sKey, subKeys, TOTAL_SUBKEYS * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }

	cudaStatus = cudaMemcpy(devSBox, sBox, S_BOX_SIZE * sizeof(u32), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }

	
	totalTime = endStopwatch("Memory copy from host to device (encryption)", start);

	//!!!must be padded before!!!

	dim3 dimGrid((blocks - 1) / THREAD_BLOCK_SIZE + 1, 1, 1);
	dim3 dimBlock(THREAD_BLOCK_SIZE, 1, 1);
	
    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	encryptKernel<<<dimGrid, dimBlock>>>(devPt, devCt, devSBox, blocks, n0, n1);
	
	
    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        return cudaStatus;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
        return cudaStatus;
    }

	totalTime += endStopwatch("Encryption kernel", start);

	start = startStopwatch();

	cudaFree(devSBox);

	endStopwatch("Freeing memory (encryption)", start);

	printDuration("Encryption", totalTime);

    return cudaStatus;
}

cudaError_t twofishDecryptCuda(const uint4 ct[], uint4 pt[], const u32 subKeys[TOTAL_SUBKEYS], const u32 sBox[S_BOX_SIZE], const int blocks)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	uint4 *devPt;
	uint4 *devCt;
	u32 *devSBox;

    cudaError_t cudaStatus;

	u32 n0 = ct[0].x;
	u32 n1 = ct[0].y;

    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        return cudaStatus;
    }

    // cuda malloc
	start = startStopwatch();
	cudaHostGetDevicePointer((void **)&devCt, (void *)ct, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	cudaHostGetDevicePointer((void **)&devPt, (void *)pt, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	

	cudaStatus = cudaMalloc((void**)&devSBox, S_BOX_SIZE * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        return cudaStatus;
    }

	
	endStopwatch("Malloc & GetDevicePointer (decryption)", start);

	devCt++;

	start = startStopwatch();

	cudaStatus = cudaMemcpyToSymbol(sKey, subKeys, TOTAL_SUBKEYS * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }

	cudaStatus = cudaMemcpy(devSBox, sBox, S_BOX_SIZE * sizeof(u32), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }

	
	totalTime = endStopwatch("Memory copy from host to device (decryption)", start);

	//!!!must be padded before!!!

	dim3 dimGrid((blocks - 1)/THREAD_BLOCK_SIZE + 1, 1, 1);
	dim3 dimBlock(THREAD_BLOCK_SIZE, 1, 1);
	
    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	encryptKernel<<<dimGrid, dimBlock>>>(devCt, devPt, devSBox, blocks, n0, n1);
	
	
    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        return cudaStatus;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
        return cudaStatus;
    }
	
	totalTime += endStopwatch("Decryption kernel", start);

	start = startStopwatch();

	cudaFree(devSBox);

	endStopwatch("Freeing memory (decryption)", start);

	printDuration("Decryption", totalTime);

    return cudaStatus;
}






u32 RS_MDS_Encode(u32 k0, u32 k1)
	{
	int i,j;
	u32 r;

	for (i=r=0;i<2;i++)
		{
		r ^= (i) ? k0 : k1;			/* merge in 32 more key bits */
		for (j=0;j<4;j++)			/* shift one u8 at a time */
			RS_rem(r);
					

		}
	return r;
	}


int makeKey(const u32 cipherKey[], u32 subKeys[TOTAL_SUBKEYS], u32 sBox[S_BOX_SIZE], const unsigned int keyLen ){

	#define	F32(res,x,k32)	\
	{															\
	u32 t=x;													\
	switch (k64Cnt & 3)											\
			{														\
			case 0:  /* same as 4 */								\
						b0(t)   = p8(04)[b0(t)] ^ b0(k32[3]);		\
						b1(t)   = p8(14)[b1(t)] ^ b1(k32[3]);		\
						b2(t)   = p8(24)[b2(t)] ^ b2(k32[3]);		\
						b3(t)   = p8(34)[b3(t)] ^ b3(k32[3]);		\
					 /* fall thru, having pre-processed t */		\
			case 3:		b0(t)   = p8(03)[b0(t)] ^ b0(k32[2]);		\
						b1(t)   = p8(13)[b1(t)] ^ b1(k32[2]);		\
						b2(t)   = p8(23)[b2(t)] ^ b2(k32[2]);		\
						b3(t)   = p8(33)[b3(t)] ^ b3(k32[2]);		\
					 /* fall thru, having pre-processed t */		\
			case 2:	 /* 128-bit keys (optimize for this case) */	\
				res=	MDStab[0][p8(01)[p8(02)[b0(t)] ^ b0(k32[1])] ^ b0(k32[0])] ^	\
						MDStab[1][p8(11)[p8(12)[b1(t)] ^ b1(k32[1])] ^ b1(k32[0])] ^	\
						MDStab[2][p8(21)[p8(22)[b2(t)] ^ b2(k32[1])] ^ b2(k32[0])] ^	\
						MDStab[3][p8(31)[p8(32)[b3(t)] ^ b3(k32[1])] ^ b3(k32[0])] ;	\
			}														\
	}

	u32 k64Cnt = keyLen / 8;
    u32 subkeyCnt = TOTAL_SUBKEYS;
    u32 k32e[4]; // even 32-bit entities
    u32 k32o[4]; // odd 32-bit entities
    u32 sBoxKey[4];

	u32 i, j, offset = 0;
	
	for (i = 0, j = k64Cnt-1; i < 4 && offset < keyLen/BYTES_PER_WORD; i++, j--) {
		 k32e[i] = cipherKey[offset++];
         k32o[i] = cipherKey[offset++];
         sBoxKey[j] = RS_MDS_Encode(k32e[i], k32o[i]); // reverse order
	}


	// compute the round decryption subkeys for PHT. these same subkeys
    // will be used in encryption but will be applied in reverse order.
    u32 q, A, B;

    for (i = q = 0; i < subkeyCnt/2; i++, q += SK_STEP) {
        F32(A, q        , k32e); // A uses even key entities
        F32(B, q+SK_BUMP, k32o); // B uses odd  key entities
        B = B << 8 | ROR(B, 24);
        A += B;
        subKeys[2*i    ] = A;               // combine with a PHT
        A += B;
        subKeys[2*i + 1] = A << SK_ROTL | ROR(A, (32-SK_ROTL));
    }

	// fully expand the table for speed
    u32 k0 = sBoxKey[0];
    u32 k1 = sBoxKey[1];
    u32 k2 = sBoxKey[2];
    u32 k3 = sBoxKey[3];
    u32 b0, b1, b2, b3;
    
    for (i = 0; i < 256; i++) {
        b0 = b1 = b2 = b3 = i;
        switch (k64Cnt & 3) {
        case 0: // same as 4
			b0 = (p8(04)[b0]) ^ b0(k3);
			b1 = (p8(14)[b1]) ^ b1(k3);
			b2 = (p8(24)[b2]) ^ b2(k3);
			b3 = (p8(34)[b3]) ^ b3(k3);
        case 3:
			b0 = (p8(03)[b0]) ^ b0(k2);
			b1 = (p8(13)[b1]) ^ b1(k2);
			b2 = (p8(23)[b2]) ^ b2(k2);
			b3 = (p8(33)[b3]) ^ b3(k2);
        case 2: // 128-bit keys
			sBox[      2*i  ] = MDStab[0][(p8(01)[(p8(02)[b0]) ^ b0(k1)]) ^ b0(k0)];
			sBox[      2*i+1] = MDStab[1][(p8(11)[(p8(12)[b1]) ^ b1(k1)]) ^ b1(k0)];
			sBox[0x200+2*i  ] = MDStab[2][(p8(21)[(p8(22)[b2]) ^ b2(k1)]) ^ b2(k0)];
			sBox[0x200+2*i+1] = MDStab[3][(p8(31)[(p8(32)[b3]) ^ b3(k1)]) ^ b3(k0)];
        }
    }

	

	return 0;
}




int twofishEncrypt(uint4 *pt, uint4 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	cudaSetDeviceFlags(cudaDeviceMapHost);
	cudaFree(NULL);
	u32 subKeys[TOTAL_SUBKEYS];
	u32 sBox[S_BOX_SIZE];

	uint4 *pinnedPt;
	uint4 *pinnedCt;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	cudaHostAlloc((void **)&pinnedPt, blocks * sizeof(uint4), cudaHostAllocWriteCombined | cudaHostAllocMapped);
	cudaHostAlloc((void **)&pinnedCt, (blocks + 1) * sizeof(uint4), cudaHostAllocDefault | cudaHostAllocMapped);

	memcpy(pinnedPt, pt, blocks * sizeof(uint4));


	start = startStopwatch();
	makeKey((u32*)cipherKey, subKeys, sBox, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);
	
	
	cudaError_t cudaStatus = twofishEncryptCuda(pinnedPt, pinnedCt, subKeys, sBox, blocks);
	
	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Encryption failed!");
        return 1;
    }

	memcpy(ct, pinnedCt, (blocks + 1) * sizeof(uint4));
	cudaFreeHost(pinnedPt);
	cudaFreeHost(pinnedCt);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }

    return 0;




}
int twofishDecrypt(uint4 *ct, uint4 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){
	
	boost::chrono::high_resolution_clock::time_point start;
	
	cudaSetDeviceFlags(cudaDeviceMapHost);
	cudaFree(NULL);
	u32 subKeys[TOTAL_SUBKEYS];
	u32 sBox[S_BOX_SIZE];

	uint4 *pinnedCt;
	uint4 *pinnedPt;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	cudaHostAlloc((void **)&pinnedCt, (blocks + 1) * sizeof(uint4), cudaHostAllocWriteCombined | cudaHostAllocMapped);
	cudaHostAlloc((void **)&pinnedPt, blocks * sizeof(uint4), cudaHostAllocDefault | cudaHostAllocMapped);

	memcpy(pinnedCt, ct, (blocks + 1) * sizeof(uint4));

	start = startStopwatch();
	makeKey((u32*)cipherKey, subKeys, sBox, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);
	
	
	cudaError_t cudaStatus = twofishDecryptCuda(pinnedCt, pinnedPt, subKeys, sBox, blocks);

	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Decryption failed!");
        return 1;
    }

	memcpy(pt, pinnedPt, blocks * sizeof(uint4));
	cudaFreeHost(pinnedCt);
	cudaFreeHost(pinnedPt);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }

    return 0;

}