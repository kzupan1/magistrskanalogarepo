#include "mars-cpu.h"
#include "mars-tables.h"
#include "measure-time.h"
#include <boost/chrono.hpp>

#define f_mix(a,b,c,d)                  \
	r = ROR(a, 8);                 \
	b ^= S[a & 0xff];            \
	b += S[(r & 0xff) + 256];    \
	r = ROR(a, 16);                \
	a = ROR(a, 24);               \
	c += S[r & 0xff];            \
	d ^= S[(a & 0xff) + 256]

#define b_mix(a,b,c,d)                  \
	r = ROL(a, 8);                 \
	b ^= S[(a & 0xff) + 256];    \
	c -= S[r & 0xff];            \
	r = ROL(a, 16);                \
	a = ROL(a, 24);               \
	d -= S[(r & 0xff) + 256];    \
	d ^= S[a & 0xff]

#define f_ktr(a,b,c,d,i)    \
	m = a + rk[i];       \
	a = ROL(a, 13);        \
	r = a * rk[i + 1];   \
	l = S[m & 0x1ff];     \
	r = ROL(r, 5);         \
	c += ROL(m, r);        \
	l ^= r;                 \
	r = ROL(r, 5);         \
	l ^= r;                 \
	d ^= r;                 \
	b += ROL(l, r)

#define r_ktr(a,b,c,d,i)    \
	r = a * rk[i + 1];   \
	a = ROR(a, 13);        \
	m = a + rk[i];       \
	l = S[m & 0x1ff];     \
	r = ROL(r, 5);         \
	l ^= r;                 \
	c -= ROL(m, r);        \
	r = ROL(r, 5);         \
	l ^= r;                 \
	d ^= r;                 \
	b -= ROL(l, r)

int makeKey(const u32 cipherKey[], u32 rk[RK_SIZE], const unsigned int keyLen){

	u32  i, j, m, t1, t2, *kp;
	u32 t_key[TMP_TABLE_SIZE];


	m = keyLen / BYTES_PER_WORD;

	for (i = 0; i < m; ++i){

		t_key[i] = cipherKey[i];

	}

	t_key[i++] = m;

	for (; i < TMP_TABLE_SIZE; i++){

		t_key[i] = 0;

	}

	kp = rk;


//i and j are changed in this code
//-2 mod 15 = 13 and -1 mod 15 = 14
// j - 7 mod 15 = j + 8 mod 15
#define tk1(j)  t1 = t_key[j] ^= ROL(t1 ^ t_key[(j + 8) % 15], 3) ^ (i + 4 * j)
#define tk2(j)  t2 = t_key[j] ^= ROL(t2 ^ t_key[(j + 8) % 15], 3) ^ (i + 4 * j)
#define tk3(j)  t_key[j] = t1 =  ROL(t_key[j] + S[t1 & 0x1ff], 9)

	for (i = 0; i < 4; ++i)
	{
		t1 = t_key[13]; t2 = t_key[14];

		tk1(0); tk2(1); tk1(2); tk2(3); tk1(4); tk2(5); tk1(6); tk2(7);
		tk1(8); tk2(9); tk1(10); tk2(11); tk1(12); tk2(13); tk1(14);

		tk3(0); tk3(1); tk3(2); tk3(3); tk3(4); tk3(5); tk3(6); tk3(7);
		tk3(8); tk3(9); tk3(10); tk3(11); tk3(12); tk3(13); tk3(14);

		tk3(0); tk3(1); tk3(2); tk3(3); tk3(4); tk3(5); tk3(6); tk3(7);
		tk3(8); tk3(9); tk3(10); tk3(11); tk3(12); tk3(13); tk3(14);

		tk3(0); tk3(1); tk3(2); tk3(3); tk3(4); tk3(5); tk3(6); tk3(7);
		tk3(8); tk3(9); tk3(10); tk3(11); tk3(12); tk3(13); tk3(14);

		tk3(0); tk3(1); tk3(2); tk3(3); tk3(4); tk3(5); tk3(6); tk3(7);
		tk3(8); tk3(9); tk3(10); tk3(11); tk3(12); tk3(13); tk3(14);

		*kp++ = t_key[0]; *kp++ = t_key[4]; *kp++ = t_key[8]; *kp++ = t_key[12];
		*kp++ = t_key[1]; *kp++ = t_key[5]; *kp++ = t_key[9]; *kp++ = t_key[13];
		*kp++ = t_key[2]; *kp++ = t_key[6];
	}

	//j is actually w
	for (i = 5; i < 37; i += 2)
	{
		j = rk[i] | 3;

		m = (~j ^ (j << 1)) & (~j ^ (j >> 1)) & 0x7ffffffe;

		m &= m >> 1; m &= m >> 2; m &= m >> 4;
		m |= m << 1; m |= m << 2; m |= m << 4;

		
		m &= 0x7ffffffc;

		j ^= (ROL(B[rk[i] & 3], rk[i - 1]) & m);
		

		rk[i] = j;
	}

	return 0;
}



uint4 marsEncryptCpu(uint4 in,  const u32 * rk){

	register u32  a, b, c, d, l, m, r;

	a = in.x + rk[0];
	b = in.y + rk[1];
	c = in.z + rk[2];
	d = in.w + rk[3];

	f_mix(a,b,c,d); a += d;
    f_mix(b,c,d,a); b += c;
    f_mix(c,d,a,b);
    f_mix(d,a,b,c);
    f_mix(a,b,c,d); a += d;
    f_mix(b,c,d,a); b += c;
    f_mix(c,d,a,b);
    f_mix(d,a,b,c);

    f_ktr(a,b,c,d, 4); f_ktr(b,c,d,a, 6); f_ktr(c,d,a,b, 8); f_ktr(d,a,b,c,10);
    f_ktr(a,b,c,d,12); f_ktr(b,c,d,a,14); f_ktr(c,d,a,b,16); f_ktr(d,a,b,c,18);
    f_ktr(a,d,c,b,20); f_ktr(b,a,d,c,22); f_ktr(c,b,a,d,24); f_ktr(d,c,b,a,26);
    f_ktr(a,d,c,b,28); f_ktr(b,a,d,c,30); f_ktr(c,b,a,d,32); f_ktr(d,c,b,a,34);

    b_mix(a,b,c,d);
    b_mix(b,c,d,a); c -= b;
    b_mix(c,d,a,b); d -= a;
    b_mix(d,a,b,c);
    b_mix(a,b,c,d);
    b_mix(b,c,d,a); c -= b;
    b_mix(c,d,a,b); d -= a;
    b_mix(d,a,b,c);


	return{a - rk[36], b - rk[37], c - rk[38], d - rk[39]};
}


int marsEncrypt(uint4 *pt, uint4 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	u32 rk[RK_SIZE];;
	
	start = startStopwatch();
	makeKey((u32*)cipherKey, rk, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);

	srand(0);
	u32 n0 = rand();
	u32 n1 = rand();
	u64 ctr = 0;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	uint4 in;
	uint4 out;

	ct[0] = { n0, n1, 0, 0 };


	start = startStopwatch();
	for (int i = 0; i < blocks; i++){
		in = { n0, n1, ((ctr & 0xffffffff00000000) >> 32), (ctr & 0xffffffff) };

		out = marsEncryptCpu(in, rk);


		ct[i + 1].x = pt[i].x ^ out.x;
		ct[i + 1].y = pt[i].y ^ out.y;
		ct[i + 1].z = pt[i].z ^ out.z;
		ct[i + 1].w = pt[i].w ^ out.w;


		ctr++;
	}

	endStopwatch("Encryption", start);
	


    return 0;




}
int marsDecrypt(uint4 *ct, uint4 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){
	
	boost::chrono::high_resolution_clock::time_point start;

	u32 rk[RK_SIZE];;
	
	start = startStopwatch();
	makeKey((u32*)cipherKey, rk, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);
	
	u32 n0;
	u32 n1;
	u64 ctr = 0;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	uint4 in;
	uint4 out;

	n0 = ct[0].x;
	n1 = ct[0].y;

	start = startStopwatch();
	for (int i = 0; i < blocks; i++){
		in = { n0, n1, ((ctr & 0xffffffff00000000) >> 32), (ctr & 0xffffffff) };

		out = marsEncryptCpu(in, rk);

		pt[i].x = ct[i + 1].x ^ out.x;
		pt[i].y = ct[i + 1].y ^ out.y;
		pt[i].z = ct[i + 1].z ^ out.z;
		pt[i].w = ct[i + 1].w ^ out.w;


		ctr += 1;
	}
	endStopwatch("Decryption", start);
	


    return 0;

}