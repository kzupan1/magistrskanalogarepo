
#include <CL/cl.h>
#include <stdio.h>
#include <stdlib.h>
#include "opencl-helpers.h"

///
//  Create an OpenCL program from the kernel source file
//
cl_program createProgramWithSource(cl_context context, cl_device_id device, const char* fileName, const char *options)
{
    cl_int errNum;
    cl_program program;

	FILE* programHandle;
    size_t programSize;
	char *programBuffer;


	programHandle = fopen(fileName, "rb");


    
    if (programHandle == NULL)
    {
		printf("Failed to open file for reading: %d \n", fileName);
        return NULL;
    }

	fseek(programHandle, 0, SEEK_END);
    programSize = ftell(programHandle);
    rewind(programHandle);

	programBuffer = (char*) malloc(programSize + 1);
    programBuffer[programSize] = '\0';
    fread(programBuffer, sizeof(char), programSize, programHandle);
    fclose(programHandle);

	// create program from buffer

	program = clCreateProgramWithSource(context, 1, (const char**) &programBuffer, &programSize, &errNum);
    free(programBuffer);

    if (program == NULL)
    {
        printf("Failed to create CL program from source.\n");
        return NULL;
    }

    errNum = clBuildProgram(program, 0, NULL, options, NULL, NULL);

    if (errNum != CL_SUCCESS)
    {
        // Determine the reason for the error
        char buildLog[16384];
        clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG,
                              sizeof(buildLog), buildLog, NULL);

        printf("Error in kernel: \n");
        printf("%s\n", buildLog);

        clReleaseProgram(program);
        return NULL;
    }

    return program;

}

cl_program createProgramWithBinary(cl_context context, cl_device_id device, const char* fileName, const char* options)
{
    FILE *fp = fopen(fileName, "rb");
    if (fp == NULL)
    {
        return NULL;
    }

    // Determine the size of the binary
    size_t binarySize;
    fseek(fp, 0, SEEK_END);
    binarySize = ftell(fp);
    rewind(fp);

    unsigned char *programBinary = new unsigned char[binarySize];
    fread(programBinary, 1, binarySize, fp);
    fclose(fp);

    cl_int errNum = 0;
    cl_program program;
    cl_int binaryStatus;

    program = clCreateProgramWithBinary(context,
                                        1,
                                        &device,
                                        &binarySize,
                                        (const unsigned char**)&programBinary,
                                        &binaryStatus,
                                        &errNum);
    delete [] programBinary;
    if (errNum != CL_SUCCESS)
    {
        printf("Error loading program binary.\n");
        return NULL;
    }

    if (binaryStatus != CL_SUCCESS)
    {
        printf("Invalid binary for device\n");
        return NULL;
    }

    errNum = clBuildProgram(program, 0, NULL, options, NULL, NULL);
    if (errNum != CL_SUCCESS)
    {
        // Determine the reason for the error
        char buildLog[16384];
        clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG,
                              sizeof(buildLog), buildLog, NULL);

        printf("Error in program: \n");
        printf("%s\n", buildLog);
        clReleaseProgram(program);
        return NULL;
    }

    return program;
}

//
///
//  Retreive program binary for all of the devices attached to the
//  program an and store the one for the device passed in
//
bool saveProgramBinary(cl_program program, cl_device_id device, const char* fileName)
{
    cl_uint numDevices = 0;
    cl_int errNum;

    // 1 - Query for number of devices attached to program
    errNum = clGetProgramInfo(program, CL_PROGRAM_NUM_DEVICES, sizeof(cl_uint),
                              &numDevices, NULL);
    if (errNum != CL_SUCCESS)
    {
        printf("Error querying for number of devices.\n");
        return false;
    }

    // 2 - Get all of the Device IDs
    cl_device_id *devices = new cl_device_id[numDevices];

    errNum = clGetProgramInfo(program, CL_PROGRAM_DEVICES,
                              sizeof(cl_device_id) * numDevices,
                              devices, NULL);
    if (errNum != CL_SUCCESS)
    {
        printf("Error querying for devices.\n");
        delete [] devices;
        return false;
    }

    // 3 - Determine the size of each program binary
    size_t *programBinarySizes = new size_t [numDevices];
    
	errNum = clGetProgramInfo(program, CL_PROGRAM_BINARY_SIZES,
                              sizeof(size_t) * numDevices,
                              programBinarySizes, NULL);

    if (errNum != CL_SUCCESS)
    {
        printf("Error querying for program binary sizes.\n");
        delete [] devices;
        delete [] programBinarySizes;
        return false;
    }

    unsigned char **programBinaries = new unsigned char*[numDevices];
    for (cl_uint i = 0; i < numDevices; i++)
    {
        programBinaries[i] = new unsigned char[programBinarySizes[i]];
    }

    // 4 - Get all of the program binaries
    errNum = clGetProgramInfo(program, CL_PROGRAM_BINARIES, sizeof(unsigned char*) * numDevices,
                              programBinaries, NULL);
    if (errNum != CL_SUCCESS)
    {
        printf("Error querying for program binaries\n");

        delete [] devices;
        delete [] programBinarySizes;

        for (cl_uint i = 0; i < numDevices; i++)
        {
            delete [] programBinaries[i];
        }
        delete [] programBinaries;
        return false;
    }

    // 5 - Finally store the binaries for the device requested out to disk for future reading.
    for (cl_uint i = 0; i < numDevices; i++)
    {
        // Store the binary just for the device requested.  In a scenario where
        // multiple devices were being used you would save all of the binaries out here.
        if (devices[i] == device)
        {
            FILE *fp = fopen(fileName, "wb");
            fwrite(programBinaries[i], 1, programBinarySizes[i], fp);
            fclose(fp);
            break;
        }
    }

    // Cleanup
    delete [] devices;
    delete [] programBinarySizes;
    for (cl_uint i = 0; i < numDevices; i++)
    {
        delete [] programBinaries[i];
    }
    delete [] programBinaries;

    return true;
}

cl_program getProgram(InitValues *initValues, const char *kernelSource, const char *kernelBin, const char *options, bool buildSource){

	cl_program program;


	if (buildSource){

		program = createProgramWithSource(initValues->context, initValues->device_list[0], kernelSource, options);

		if (program == NULL){
			printf("Error while building program from source. \n");
			return NULL;

		}

		bool res = saveProgramBinary(program, initValues->device_list[0], kernelBin);

		if (res == false){
			printf("Error while saving program to file. \n");
			return NULL;

		}


	}
	else{

		program = createProgramWithBinary(initValues->context, initValues->device_list[0], kernelBin, options);
		
		if (program == NULL){
			printf("Error while creating program from binary. \n");
			return NULL;

		}

	}

	return program;

}


InitValues * openClInit(){

	cl_int clStatus;

	InitValues *initValues;

	// Get platform and device information
	cl_platform_id * platforms = NULL;
	cl_uint num_platforms;
	
	//Set up the Platform
	clStatus = clGetPlatformIDs(0, NULL, &num_platforms);
	platforms = (cl_platform_id *)malloc(sizeof(cl_platform_id)*num_platforms);
	clStatus = clGetPlatformIDs(num_platforms, platforms, NULL);
	
	//Get the devices list and choose the device you want to run on
	cl_device_id *device_list = NULL;
	cl_uint num_devices;

	clStatus = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_GPU, 0, NULL, &num_devices);
	device_list = (cl_device_id *)malloc(sizeof(cl_device_id)*num_devices);
	clStatus = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_GPU, num_devices, device_list, NULL);
	
	// Create one OpenCL context for each device in the platform
	cl_context context;
	context = clCreateContext(NULL, num_devices, device_list, NULL, NULL, &clStatus);
	
	initValues = (InitValues *) malloc(sizeof(InitValues));

	initValues->platforms = platforms;
	initValues->num_platforms = num_platforms;
	initValues->device_list = device_list;
	initValues->num_devices = num_devices;
	initValues->context = context;
	
	return initValues;


}

int openClDeInit(InitValues *initValues){

	cl_int clStatus;
	clStatus = clReleaseContext(initValues->context);


	free(initValues->platforms);
	free(initValues->device_list);

	free(initValues);

	return 0;
}
