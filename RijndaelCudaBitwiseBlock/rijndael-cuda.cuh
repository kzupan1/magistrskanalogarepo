

#ifndef __RIJNDAEL_CUDA_H
#define __RIJNDAEL_CUDA_H
#include "common-types.h"


typedef struct{
	u32 b0;
	u32 b1;
} G4;

typedef struct{
	G4 b0;
	G4 b1;
} G16;

typedef struct{
	G16 b0;
	G16 b1;
} G256;

typedef struct{
	G256 b0;
	G256 b1;
	G256 b2;
	G256 b3;
} G4G;

typedef struct{
	G256 g256;
	u32 padding1;
} G256_p;

#define T_TABLE_SIZE 256
#define BIG_T_TABLE_SIZE (5 * T_TABLE_SIZE)

#define CRYPTO_THREAD_BLOCK_SIZE 128


#define maxNr 14
#define rkLength 4 * (maxNr + 1)


#define GRID_DIM_Y 8

int rijndaelEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);
int rijndaelDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);

#endif /* __RIJNDAEL_CUDA_H */