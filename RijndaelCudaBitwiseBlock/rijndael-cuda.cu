
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "rijndael-cuda.cuh"
#include "rijndael-tables.cuh"
#include <stdio.h>
#include <stdlib.h>
#include "measure-time.h"
#include <boost/chrono.hpp>

__device__ inline G4 G4_mul(G4 x, G4 y){
	u32 e;
	e = (x.b1 ^  x.b0) & (y.b1 ^ y.b0);
	return{ (x.b0 & y.b0) ^ e, (x.b1 & y.b1) ^ e };
}

__device__ inline G4 G4_scl_N(G4 x){
	return{ x.b1 ^ x.b0, x.b0 };
}

__device__ inline G4 G4_scl_N2(G4 x){
	return{ x.b1, x.b1 ^ x.b0 };
}

__device__ inline G4 G4_sq(G4 x){
	return{ x.b1, x.b0 };
}

__device__ inline G4 G4_xor(G4 x, G4 y){
	return{ x.b0 ^ y.b0, x.b1 ^ y.b1 };
}

__device__ inline G16 G16_mul(G16 x, G16 y){
	G4 e;
	e = G4_scl_N(G4_mul(G4_xor(x.b1, x.b0), G4_xor(y.b1, y.b0)));
	
	return{ G4_xor(G4_mul(x.b0, y.b0), e), G4_xor(G4_mul(x.b1, y.b1), e) };
}

__device__ inline G16 G16_sq_scl(G16 x){
	return{ G4_scl_N2(G4_sq(x.b0)), G4_sq(G4_xor(x.b1, x.b0)) };
}

__device__ inline G16 G16_inv(G16 x){
	G4 e;
	e = G4_sq(G4_xor(G4_scl_N(G4_sq(G4_xor(x.b1, x.b0))), G4_mul(x.b1, x.b0)));
	return{ G4_mul(e, x.b1), G4_mul(e, x.b0) };

}

__device__ inline G16 G16_xor(G16 x, G16 y){
	return{ G4_xor(x.b0, y.b0), G4_xor(x.b1, y.b1) };
}

__device__ inline G256 G256_inv(G256 x){
	G16 e;
	e = G16_inv(G16_xor(G16_sq_scl(G16_xor(x.b1, x.b0)), G16_mul(x.b1, x.b0)));
	return{ G16_mul(e, x.b1), G16_mul(e, x.b0) };
}

__device__ inline G256 G256_xor(G256 x, G256 y){
	return{ G16_xor(x.b0, y.b0), G16_xor(x.b1, y.b1) };
}

__device__ inline G4G G4G_xor(G4G x, G4G y){
	return{ G256_xor(x.b0, y.b0), G256_xor(x.b1, y.b1), G256_xor(x.b2, y.b2), G256_xor(x.b3, y.b3) };
}

__device__ inline G256 G256_newbasis_forward(G256 n){

	return{
			{
				{ 
					n.b1.b1.b0 ^ n.b0.b1.b1 ^ n.b0.b1.b0 ^ n.b0.b0.b1 ^ n.b0.b0.b0,
					n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b0.b0
				}, 
				{
					n.b0.b0.b0,
					n.b1.b1.b1 ^ n.b1.b0.b0 ^ n.b0.b1.b1  ^ n.b0.b0.b1 ^ n.b0.b0.b0 
				} 
			},
			{ 
				{ 
					n.b1.b1.b1 ^ n.b1.b1.b0 ^ n.b1.b0.b1  ^ n.b0.b0.b0,
					n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b0.b1  ^ n.b0.b0.b0
				}, 
				{ 
					n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b1.b0.b0  ^ n.b0.b0.b0,
					n.b1.b1.b1 ^n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b1.b0 ^ n.b0.b0.b1 ^ n.b0.b0.b0 
				} 
			}

		};
}

__device__ inline G256 G256_newbasis_inv_forward(G256 n){
	
	return{
		{
			{
				n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b1.b0.b0 ^ n.b0.b0.b1 ^ n.b0.b0.b0,
				n.b1.b0.b0 ^ n.b0.b1.b1 ^ n.b0.b0.b0
			},
			{
				n.b1.b1.b1 ^ n.b1.b0.b1 ^ n.b0.b1.b0,
				n.b1.b1.b1 ^ n.b1.b1.b0 ^ n.b1.b0.b0
			}
		},
		{
			{
				n.b1.b1.b0 ^ n.b0.b1.b1 ^ n.b0.b0.b1  ^ n.b0.b0.b0,
				n.b1.b1.b0 ^ n.b1.b0.b0
			},
			{
				n.b1.b1.b0 ^ n.b1.b0.b0 ^ n.b0.b0.b1  ^ n.b0.b0.b0,
				n.b1.b1.b1 ^n.b1.b0.b0
			}
		}

	};
}

__device__ inline G256 G256_newbasis_backward(G256 n){
	
	return{
		{
			{
				n.b1.b1.b0 ^ n.b1.b0.b0 ^ n.b0.b0.b1,
				n.b1.b0.b1 ^ n.b1.b0.b0 ^ n.b0.b0.b1
			},
			{
				n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b1.b1  ^ n.b0.b1.b0 ^ n.b0.b0.b0,
				n.b1.b1.b1 ^ n.b1.b1.b0 ^ n.b1.b0.b1  ^ n.b1.b0.b0 ^ n.b0.b1.b1
			}
		},
		{
			{
				n.b1.b1.b1 ^ n.b1.b0.b1 ^ n.b0.b1.b1,
				n.b1.b1.b0 ^ n.b0.b0.b0
			},
			{
				n.b1.b1.b1 ^ n.b0.b1.b1,
				n.b1.b0.b1 ^ n.b0.b1.b1
			}
		}

	};
}

__device__ inline G256 G256_newbasis_inv_backward(G256 n){

	return{
		{
			{
				n.b0.b1.b0,
				n.b1.b0.b1 ^ n.b0.b0.b1
			},
			{
				n.b1.b1.b1 ^ n.b1.b0.b1 ^ n.b1.b0.b0  ^ n.b0.b0.b1,
				n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b1.b0.b0  ^ n.b0.b1.b1 ^ n.b0.b1.b0 ^ n.b0.b0.b1
			}
		},
		{
			{
				n.b1.b1.b0 ^ n.b0.b0.b1,
				n.b1.b1.b1 ^ n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b1.b1 ^ n.b0.b1.b0 ^ n.b0.b0.b0
			},
			{
				n.b1.b1.b1 ^ n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b1.b1 ^ n.b0.b0.b1 ^ n.b0.b0.b0,
				n.b1.b0.b0 ^ n.b0.b0.b1
			}
		}

	};


}

__device__ inline G256 Sbox(G256 n){
	G256 t;
	t = G256_newbasis_forward(n);
	t = G256_inv(t);
	t = G256_newbasis_backward(t);

	t.b1.b1.b0 = ~t.b1.b1.b0;
	t.b1.b0.b1 = ~t.b1.b0.b1;
	t.b0.b0.b1 = ~t.b0.b0.b1;
	t.b0.b0.b0 = ~t.b0.b0.b0;

	return t;
}

__device__ inline G256 iSbox(G256 n){
	G256 t;

	n.b1.b1.b0 = ~n.b1.b1.b0;
	n.b1.b0.b1 = ~n.b1.b0.b1;
	n.b0.b0.b1 = ~n.b0.b0.b1;
	n.b0.b0.b0 = ~n.b0.b0.b0;

	t = G256_newbasis_inv_forward(n);
	t = G256_inv(t);
	t = G256_newbasis_inv_backward(t);

	return t;
}

__device__ inline G256 xtime(G256 n){
	return{
		{
			{
				n.b1.b1.b1,
				n.b0.b0.b0 ^ n.b1.b1.b1
			},
			{
				n.b0.b0.b1,
				n.b0.b1.b0 ^ n.b1.b1.b1
			}
		},
		{
			{
				n.b0.b1.b1 ^ n.b1.b1.b1,
				n.b1.b0.b0
			},
			{
				n.b1.b0.b1,
				n.b1.b1.b0
			}
		}

	};
}

__device__ inline G256 gmul2(G256 n){
	return xtime(n);
}

__device__ inline G256 gmul3(G256 n){
	return G256_xor(xtime(n), n);
}

__device__ inline G256 gmul14(G256 n){
	G256 t1 = xtime(n);
	G256 t2 = xtime(t1);

	return G256_xor(G256_xor(xtime(t2), t2), t1);
}

__device__ inline G256 gmul11(G256 n){
	G256 t1 = xtime(n);
	
	return G256_xor(G256_xor(xtime(xtime(t1)), t1), n);
}

__device__ inline G256 gmul13(G256 n){
	G256 t1 = xtime(xtime(n));

	return G256_xor(G256_xor(xtime(t1), t1), n);
}

__device__ inline G256 gmul9(G256 n){

	return G256_xor(xtime(xtime(xtime(n))), n);
}

__device__ inline G256 keying(G256 n, u8 k){
	return{
		{
			{
				n.b0.b0.b0 ^ (-(k & 0x01)),
				n.b0.b0.b1 ^ (-((k >> 1) & 0x01))
			},
			{
				n.b0.b1.b0 ^ (-((k >> 2) & 0x01)),
				n.b0.b1.b1 ^ (-((k >> 3) & 0x01))
			}
		},
		{
			{
				n.b1.b0.b0 ^ (-((k >> 4) & 0x01)),
				n.b1.b0.b1 ^ (-((k >> 5) & 0x01))
			},
			{
				n.b1.b1.b0 ^ (-((k >> 6) & 0x01)),
				n.b1.b1.b1 ^ (-((k >> 7) & 0x01))
			}
		}

	};
}

__device__ inline G4G G4G_keying(G4G n, u32 k){
	return{ keying(n.b0, k & 0xff), keying(n.b1, (k >> 8) & 0xff), keying(n.b2, (k >> 16) & 0xff), keying(n.b3, k >> 24) };
}

__device__ inline G4G G4G_te0(G256 n){
	G256 s = Sbox(n);
	G256 s2 = gmul2(s);

	return{ s2, s, s, G256_xor(s, s2) };

}

__device__ inline G4G G4G_te1(G256 n){
	G256 s = Sbox(n);
	G256 s2 = gmul2(s);

	return{ G256_xor(s, s2), s2, s, s };

}

__device__ inline G4G G4G_te2(G256 n){
	G256 s = Sbox(n);
	G256 s2 = gmul2(s);

	return{ s, G256_xor(s, s2), s2, s, };

}

__device__ inline G4G G4G_te3(G256 n){
	G256 s = Sbox(n);
	G256 s2 = gmul2(s);

	return{ s, s, G256_xor(s, s2), s2 };

}

__device__ inline G4G G4G_te4(G256 n0, G256 n1, G256 n2, G256 n3){
	return{ Sbox(n0), Sbox(n1), Sbox(n2), Sbox(n3) };
}

__device__ inline G4G G4G_td0(G256 n){
	G256 is = iSbox(n);
	G256 gmul2 = xtime(is);
	G256 gmul4 = xtime(gmul2);
	G256 gmul8 = xtime(gmul4);

	G256 gmul9 = G256_xor(gmul8, is);

	return{ G256_xor(G256_xor(gmul8, gmul4), gmul2), gmul9, G256_xor(gmul9, gmul4), G256_xor(gmul9, gmul2) };
}

__device__ inline G4G G4G_td1(G256 n){
	G256 is = iSbox(n);
	G256 gmul2 = xtime(is);
	G256 gmul4 = xtime(gmul2);
	G256 gmul8 = xtime(gmul4);

	G256 gmul9 = G256_xor(gmul8, is);

	return{G256_xor(gmul9, gmul2), G256_xor(G256_xor(gmul8, gmul4), gmul2), gmul9, G256_xor(gmul9, gmul4) };
}

__device__ inline G4G G4G_td2(G256 n){
	G256 is = iSbox(n);
	G256 gmul2 = xtime(is);
	G256 gmul4 = xtime(gmul2);
	G256 gmul8 = xtime(gmul4);

	G256 gmul9 = G256_xor(gmul8, is);

	return{ G256_xor(gmul9, gmul4), G256_xor(gmul9, gmul2), G256_xor(G256_xor(gmul8, gmul4), gmul2), gmul9 };
}

__device__ inline G4G G4G_td3(G256 n){
	G256 is = iSbox(n);
	G256 gmul2 = xtime(is);
	G256 gmul4 = xtime(gmul2);
	G256 gmul8 = xtime(gmul4);

	G256 gmul9 = G256_xor(gmul8, is);

	return{ gmul9, G256_xor(gmul9, gmul4), G256_xor(gmul9, gmul2), G256_xor(G256_xor(gmul8, gmul4), gmul2) };
}

__device__ inline G4G G4G_td4(G256 n0, G256 n1, G256 n2, G256 n3){
	return{ iSbox(n0), iSbox(n1), iSbox(n2), iSbox(n3) };
}

__constant__ u32 rk_constant[rkLength];

__global__ void rijndaelEncryptBitwiseKernel(const G4G *in, G4G *out, const unsigned int g4gs ,const unsigned int Nr)
{


	int i = blockIdx.x* blockDim.x + threadIdx.x;

	G4G s0, s1, s2, s3, t0, t1, t2, t3;
	 
	int offset = i * WORDS_PER_BLOCK;
	
	if (offset < g4gs){

	
		in += offset;
		out += offset;

		s0 = G4G_keying(in[0], rk_constant[0]);
		s1 = G4G_keying(in[1], rk_constant[1]);
		s2 = G4G_keying(in[2], rk_constant[2]);
		s3 = G4G_keying(in[3], rk_constant[3]);

		/* round 1 */
		t0 = G4G_keying(G4G_xor(G4G_te0(s0.b0), G4G_xor(G4G_te1(s1.b1), G4G_xor(G4G_te2(s2.b2), G4G_te3(s3.b3)))), rk_constant[4]);
		t1 = G4G_keying(G4G_xor(G4G_te0(s1.b0), G4G_xor(G4G_te1(s2.b1), G4G_xor(G4G_te2(s3.b2), G4G_te3(s0.b3)))), rk_constant[5]);
		t2 = G4G_keying(G4G_xor(G4G_te0(s2.b0), G4G_xor(G4G_te1(s3.b1), G4G_xor(G4G_te2(s0.b2), G4G_te3(s1.b3)))), rk_constant[6]);
		t3 = G4G_keying(G4G_xor(G4G_te0(s3.b0), G4G_xor(G4G_te1(s0.b1), G4G_xor(G4G_te2(s1.b2), G4G_te3(s2.b3)))), rk_constant[7]);

		/* round 2 */
		s0 = G4G_keying(G4G_xor(G4G_te0(t0.b0), G4G_xor(G4G_te1(t1.b1), G4G_xor(G4G_te2(t2.b2), G4G_te3(t3.b3)))), rk_constant[8]);
		s1 = G4G_keying(G4G_xor(G4G_te0(t1.b0), G4G_xor(G4G_te1(t2.b1), G4G_xor(G4G_te2(t3.b2), G4G_te3(t0.b3)))), rk_constant[9]);
		s2 = G4G_keying(G4G_xor(G4G_te0(t2.b0), G4G_xor(G4G_te1(t3.b1), G4G_xor(G4G_te2(t0.b2), G4G_te3(t1.b3)))), rk_constant[10]);
		s3 = G4G_keying(G4G_xor(G4G_te0(t3.b0), G4G_xor(G4G_te1(t0.b1), G4G_xor(G4G_te2(t1.b2), G4G_te3(t2.b3)))), rk_constant[11]);

		/* round 3 */
		t0 = G4G_keying(G4G_xor(G4G_te0(s0.b0), G4G_xor(G4G_te1(s1.b1), G4G_xor(G4G_te2(s2.b2), G4G_te3(s3.b3)))), rk_constant[12]);
		t1 = G4G_keying(G4G_xor(G4G_te0(s1.b0), G4G_xor(G4G_te1(s2.b1), G4G_xor(G4G_te2(s3.b2), G4G_te3(s0.b3)))), rk_constant[13]);
		t2 = G4G_keying(G4G_xor(G4G_te0(s2.b0), G4G_xor(G4G_te1(s3.b1), G4G_xor(G4G_te2(s0.b2), G4G_te3(s1.b3)))), rk_constant[14]);
		t3 = G4G_keying(G4G_xor(G4G_te0(s3.b0), G4G_xor(G4G_te1(s0.b1), G4G_xor(G4G_te2(s1.b2), G4G_te3(s2.b3)))), rk_constant[15]);

		/* round 4 */
		s0 = G4G_keying(G4G_xor(G4G_te0(t0.b0), G4G_xor(G4G_te1(t1.b1), G4G_xor(G4G_te2(t2.b2), G4G_te3(t3.b3)))), rk_constant[16]);
		s1 = G4G_keying(G4G_xor(G4G_te0(t1.b0), G4G_xor(G4G_te1(t2.b1), G4G_xor(G4G_te2(t3.b2), G4G_te3(t0.b3)))), rk_constant[17]);
		s2 = G4G_keying(G4G_xor(G4G_te0(t2.b0), G4G_xor(G4G_te1(t3.b1), G4G_xor(G4G_te2(t0.b2), G4G_te3(t1.b3)))), rk_constant[18]);
		s3 = G4G_keying(G4G_xor(G4G_te0(t3.b0), G4G_xor(G4G_te1(t0.b1), G4G_xor(G4G_te2(t1.b2), G4G_te3(t2.b3)))), rk_constant[19]);

		/* round 5 */
		t0 = G4G_keying(G4G_xor(G4G_te0(s0.b0), G4G_xor(G4G_te1(s1.b1), G4G_xor(G4G_te2(s2.b2), G4G_te3(s3.b3)))), rk_constant[20]);
		t1 = G4G_keying(G4G_xor(G4G_te0(s1.b0), G4G_xor(G4G_te1(s2.b1), G4G_xor(G4G_te2(s3.b2), G4G_te3(s0.b3)))), rk_constant[21]);
		t2 = G4G_keying(G4G_xor(G4G_te0(s2.b0), G4G_xor(G4G_te1(s3.b1), G4G_xor(G4G_te2(s0.b2), G4G_te3(s1.b3)))), rk_constant[22]);
		t3 = G4G_keying(G4G_xor(G4G_te0(s3.b0), G4G_xor(G4G_te1(s0.b1), G4G_xor(G4G_te2(s1.b2), G4G_te3(s2.b3)))), rk_constant[23]);

		/* round 6 */
		s0 = G4G_keying(G4G_xor(G4G_te0(t0.b0), G4G_xor(G4G_te1(t1.b1), G4G_xor(G4G_te2(t2.b2), G4G_te3(t3.b3)))), rk_constant[24]);
		s1 = G4G_keying(G4G_xor(G4G_te0(t1.b0), G4G_xor(G4G_te1(t2.b1), G4G_xor(G4G_te2(t3.b2), G4G_te3(t0.b3)))), rk_constant[25]);
		s2 = G4G_keying(G4G_xor(G4G_te0(t2.b0), G4G_xor(G4G_te1(t3.b1), G4G_xor(G4G_te2(t0.b2), G4G_te3(t1.b3)))), rk_constant[26]);
		s3 = G4G_keying(G4G_xor(G4G_te0(t3.b0), G4G_xor(G4G_te1(t0.b1), G4G_xor(G4G_te2(t1.b2), G4G_te3(t2.b3)))), rk_constant[27]);

		/* round 7 */
		t0 = G4G_keying(G4G_xor(G4G_te0(s0.b0), G4G_xor(G4G_te1(s1.b1), G4G_xor(G4G_te2(s2.b2), G4G_te3(s3.b3)))), rk_constant[28]);
		t1 = G4G_keying(G4G_xor(G4G_te0(s1.b0), G4G_xor(G4G_te1(s2.b1), G4G_xor(G4G_te2(s3.b2), G4G_te3(s0.b3)))), rk_constant[29]);
		t2 = G4G_keying(G4G_xor(G4G_te0(s2.b0), G4G_xor(G4G_te1(s3.b1), G4G_xor(G4G_te2(s0.b2), G4G_te3(s1.b3)))), rk_constant[30]);
		t3 = G4G_keying(G4G_xor(G4G_te0(s3.b0), G4G_xor(G4G_te1(s0.b1), G4G_xor(G4G_te2(s1.b2), G4G_te3(s2.b3)))), rk_constant[31]);

		/* round 8 */
		s0 = G4G_keying(G4G_xor(G4G_te0(t0.b0), G4G_xor(G4G_te1(t1.b1), G4G_xor(G4G_te2(t2.b2), G4G_te3(t3.b3)))), rk_constant[32]);
		s1 = G4G_keying(G4G_xor(G4G_te0(t1.b0), G4G_xor(G4G_te1(t2.b1), G4G_xor(G4G_te2(t3.b2), G4G_te3(t0.b3)))), rk_constant[33]);
		s2 = G4G_keying(G4G_xor(G4G_te0(t2.b0), G4G_xor(G4G_te1(t3.b1), G4G_xor(G4G_te2(t0.b2), G4G_te3(t1.b3)))), rk_constant[34]);
		s3 = G4G_keying(G4G_xor(G4G_te0(t3.b0), G4G_xor(G4G_te1(t0.b1), G4G_xor(G4G_te2(t1.b2), G4G_te3(t2.b3)))), rk_constant[35]);

		/* round 9 */
		t0 = G4G_keying(G4G_xor(G4G_te0(s0.b0), G4G_xor(G4G_te1(s1.b1), G4G_xor(G4G_te2(s2.b2), G4G_te3(s3.b3)))), rk_constant[36]);
		t1 = G4G_keying(G4G_xor(G4G_te0(s1.b0), G4G_xor(G4G_te1(s2.b1), G4G_xor(G4G_te2(s3.b2), G4G_te3(s0.b3)))), rk_constant[37]);
		t2 = G4G_keying(G4G_xor(G4G_te0(s2.b0), G4G_xor(G4G_te1(s3.b1), G4G_xor(G4G_te2(s0.b2), G4G_te3(s1.b3)))), rk_constant[38]);
		t3 = G4G_keying(G4G_xor(G4G_te0(s3.b0), G4G_xor(G4G_te1(s0.b1), G4G_xor(G4G_te2(s1.b2), G4G_te3(s2.b3)))), rk_constant[39]);

		if (Nr > 10){

			/* round 10 */
			s0 = G4G_keying(G4G_xor(G4G_te0(t0.b0), G4G_xor(G4G_te1(t1.b1), G4G_xor(G4G_te2(t2.b2), G4G_te3(t3.b3)))), rk_constant[40]);
			s1 = G4G_keying(G4G_xor(G4G_te0(t1.b0), G4G_xor(G4G_te1(t2.b1), G4G_xor(G4G_te2(t3.b2), G4G_te3(t0.b3)))), rk_constant[41]);
			s2 = G4G_keying(G4G_xor(G4G_te0(t2.b0), G4G_xor(G4G_te1(t3.b1), G4G_xor(G4G_te2(t0.b2), G4G_te3(t1.b3)))), rk_constant[42]);
			s3 = G4G_keying(G4G_xor(G4G_te0(t3.b0), G4G_xor(G4G_te1(t0.b1), G4G_xor(G4G_te2(t1.b2), G4G_te3(t2.b3)))), rk_constant[43]);

			/* round 11 */
			t0 = G4G_keying(G4G_xor(G4G_te0(s0.b0), G4G_xor(G4G_te1(s1.b1), G4G_xor(G4G_te2(s2.b2), G4G_te3(s3.b3)))), rk_constant[44]);
			t1 = G4G_keying(G4G_xor(G4G_te0(s1.b0), G4G_xor(G4G_te1(s2.b1), G4G_xor(G4G_te2(s3.b2), G4G_te3(s0.b3)))), rk_constant[45]);
			t2 = G4G_keying(G4G_xor(G4G_te0(s2.b0), G4G_xor(G4G_te1(s3.b1), G4G_xor(G4G_te2(s0.b2), G4G_te3(s1.b3)))), rk_constant[46]);
			t3 = G4G_keying(G4G_xor(G4G_te0(s3.b0), G4G_xor(G4G_te1(s0.b1), G4G_xor(G4G_te2(s1.b2), G4G_te3(s2.b3)))), rk_constant[47]);

			if (Nr > 12){
				/* round 12 */
				s0 = G4G_keying(G4G_xor(G4G_te0(t0.b0), G4G_xor(G4G_te1(t1.b1), G4G_xor(G4G_te2(t2.b2), G4G_te3(t3.b3)))), rk_constant[48]);
				s1 = G4G_keying(G4G_xor(G4G_te0(t1.b0), G4G_xor(G4G_te1(t2.b1), G4G_xor(G4G_te2(t3.b2), G4G_te3(t0.b3)))), rk_constant[49]);
				s2 = G4G_keying(G4G_xor(G4G_te0(t2.b0), G4G_xor(G4G_te1(t3.b1), G4G_xor(G4G_te2(t0.b2), G4G_te3(t1.b3)))), rk_constant[50]);
				s3 = G4G_keying(G4G_xor(G4G_te0(t3.b0), G4G_xor(G4G_te1(t0.b1), G4G_xor(G4G_te2(t1.b2), G4G_te3(t2.b3)))), rk_constant[51]);

				/* round 13 */
				t0 = G4G_keying(G4G_xor(G4G_te0(s0.b0), G4G_xor(G4G_te1(s1.b1), G4G_xor(G4G_te2(s2.b2), G4G_te3(s3.b3)))), rk_constant[52]);
				t1 = G4G_keying(G4G_xor(G4G_te0(s1.b0), G4G_xor(G4G_te1(s2.b1), G4G_xor(G4G_te2(s3.b2), G4G_te3(s0.b3)))), rk_constant[53]);
				t2 = G4G_keying(G4G_xor(G4G_te0(s2.b0), G4G_xor(G4G_te1(s3.b1), G4G_xor(G4G_te2(s0.b2), G4G_te3(s1.b3)))), rk_constant[54]);
				t3 = G4G_keying(G4G_xor(G4G_te0(s3.b0), G4G_xor(G4G_te1(s0.b1), G4G_xor(G4G_te2(s1.b2), G4G_te3(s2.b3)))), rk_constant[55]);
			}
		}

		u32 x = Nr << 2;

		s0 = G4G_keying(G4G_te4(t0.b0, t1.b1, t2.b2, t3.b3), rk_constant[x++]);
		s1 = G4G_keying(G4G_te4(t1.b0, t2.b1, t3.b2, t0.b3), rk_constant[x++]);
		s2 = G4G_keying(G4G_te4(t2.b0, t3.b1, t0.b2, t1.b3), rk_constant[x++]);
		s3 = G4G_keying(G4G_te4(t3.b0, t0.b1, t1.b2, t2.b3), rk_constant[x]);

		out[0] = s0;
		out[1] = s1;
		out[2] = s2;
		out[3] = s3;
	}

}

__global__ void rijndaelDecryptBitwiseKernel(const G4G *in, G4G *out, const unsigned int g4gs, const unsigned int Nr)
{


	int i = blockIdx.x* blockDim.x + threadIdx.x;

	G4G s0, s1, s2, s3, t0, t1, t2, t3;

	int offset = i * WORDS_PER_BLOCK;

	if (offset < g4gs){


		in += offset;
		out += offset;

		s0 = G4G_keying(in[0], rk_constant[0]);
		s1 = G4G_keying(in[1], rk_constant[1]);
		s2 = G4G_keying(in[2], rk_constant[2]);
		s3 = G4G_keying(in[3], rk_constant[3]);

		/* round 1 */
		t0 = G4G_keying(G4G_xor(G4G_td0(s0.b0), G4G_xor(G4G_td1(s3.b1), G4G_xor(G4G_td2(s2.b2), G4G_td3(s1.b3)))), rk_constant[4]);
		t1 = G4G_keying(G4G_xor(G4G_td0(s1.b0), G4G_xor(G4G_td1(s0.b1), G4G_xor(G4G_td2(s3.b2), G4G_td3(s2.b3)))), rk_constant[5]);
		t2 = G4G_keying(G4G_xor(G4G_td0(s2.b0), G4G_xor(G4G_td1(s1.b1), G4G_xor(G4G_td2(s0.b2), G4G_td3(s3.b3)))), rk_constant[6]);
		t3 = G4G_keying(G4G_xor(G4G_td0(s3.b0), G4G_xor(G4G_td1(s2.b1), G4G_xor(G4G_td2(s1.b2), G4G_td3(s0.b3)))), rk_constant[7]);

		/* round 2 */
		s0 = G4G_keying(G4G_xor(G4G_td0(t0.b0), G4G_xor(G4G_td1(t3.b1), G4G_xor(G4G_td2(t2.b2), G4G_td3(t1.b3)))), rk_constant[8]);
		s1 = G4G_keying(G4G_xor(G4G_td0(t1.b0), G4G_xor(G4G_td1(t0.b1), G4G_xor(G4G_td2(t3.b2), G4G_td3(t2.b3)))), rk_constant[9]);
		s2 = G4G_keying(G4G_xor(G4G_td0(t2.b0), G4G_xor(G4G_td1(t1.b1), G4G_xor(G4G_td2(t0.b2), G4G_td3(t3.b3)))), rk_constant[10]);
		s3 = G4G_keying(G4G_xor(G4G_td0(t3.b0), G4G_xor(G4G_td1(t2.b1), G4G_xor(G4G_td2(t1.b2), G4G_td3(t0.b3)))), rk_constant[11]);

		/* round 3 */
		t0 = G4G_keying(G4G_xor(G4G_td0(s0.b0), G4G_xor(G4G_td1(s3.b1), G4G_xor(G4G_td2(s2.b2), G4G_td3(s1.b3)))), rk_constant[12]);
		t1 = G4G_keying(G4G_xor(G4G_td0(s1.b0), G4G_xor(G4G_td1(s0.b1), G4G_xor(G4G_td2(s3.b2), G4G_td3(s2.b3)))), rk_constant[13]);
		t2 = G4G_keying(G4G_xor(G4G_td0(s2.b0), G4G_xor(G4G_td1(s1.b1), G4G_xor(G4G_td2(s0.b2), G4G_td3(s3.b3)))), rk_constant[14]);
		t3 = G4G_keying(G4G_xor(G4G_td0(s3.b0), G4G_xor(G4G_td1(s2.b1), G4G_xor(G4G_td2(s1.b2), G4G_td3(s0.b3)))), rk_constant[15]);

		/* round 4 */
		s0 = G4G_keying(G4G_xor(G4G_td0(t0.b0), G4G_xor(G4G_td1(t3.b1), G4G_xor(G4G_td2(t2.b2), G4G_td3(t1.b3)))), rk_constant[16]);
		s1 = G4G_keying(G4G_xor(G4G_td0(t1.b0), G4G_xor(G4G_td1(t0.b1), G4G_xor(G4G_td2(t3.b2), G4G_td3(t2.b3)))), rk_constant[17]);
		s2 = G4G_keying(G4G_xor(G4G_td0(t2.b0), G4G_xor(G4G_td1(t1.b1), G4G_xor(G4G_td2(t0.b2), G4G_td3(t3.b3)))), rk_constant[18]);
		s3 = G4G_keying(G4G_xor(G4G_td0(t3.b0), G4G_xor(G4G_td1(t2.b1), G4G_xor(G4G_td2(t1.b2), G4G_td3(t0.b3)))), rk_constant[19]);

		/* round 5 */
		t0 = G4G_keying(G4G_xor(G4G_td0(s0.b0), G4G_xor(G4G_td1(s3.b1), G4G_xor(G4G_td2(s2.b2), G4G_td3(s1.b3)))), rk_constant[20]);
		t1 = G4G_keying(G4G_xor(G4G_td0(s1.b0), G4G_xor(G4G_td1(s0.b1), G4G_xor(G4G_td2(s3.b2), G4G_td3(s2.b3)))), rk_constant[21]);
		t2 = G4G_keying(G4G_xor(G4G_td0(s2.b0), G4G_xor(G4G_td1(s1.b1), G4G_xor(G4G_td2(s0.b2), G4G_td3(s3.b3)))), rk_constant[22]);
		t3 = G4G_keying(G4G_xor(G4G_td0(s3.b0), G4G_xor(G4G_td1(s2.b1), G4G_xor(G4G_td2(s1.b2), G4G_td3(s0.b3)))), rk_constant[23]);

		/* round 6 */
		s0 = G4G_keying(G4G_xor(G4G_td0(t0.b0), G4G_xor(G4G_td1(t3.b1), G4G_xor(G4G_td2(t2.b2), G4G_td3(t1.b3)))), rk_constant[24]);
		s1 = G4G_keying(G4G_xor(G4G_td0(t1.b0), G4G_xor(G4G_td1(t0.b1), G4G_xor(G4G_td2(t3.b2), G4G_td3(t2.b3)))), rk_constant[25]);
		s2 = G4G_keying(G4G_xor(G4G_td0(t2.b0), G4G_xor(G4G_td1(t1.b1), G4G_xor(G4G_td2(t0.b2), G4G_td3(t3.b3)))), rk_constant[26]);
		s3 = G4G_keying(G4G_xor(G4G_td0(t3.b0), G4G_xor(G4G_td1(t2.b1), G4G_xor(G4G_td2(t1.b2), G4G_td3(t0.b3)))), rk_constant[27]);

		/* round 7 */
		t0 = G4G_keying(G4G_xor(G4G_td0(s0.b0), G4G_xor(G4G_td1(s3.b1), G4G_xor(G4G_td2(s2.b2), G4G_td3(s1.b3)))), rk_constant[28]);
		t1 = G4G_keying(G4G_xor(G4G_td0(s1.b0), G4G_xor(G4G_td1(s0.b1), G4G_xor(G4G_td2(s3.b2), G4G_td3(s2.b3)))), rk_constant[29]);
		t2 = G4G_keying(G4G_xor(G4G_td0(s2.b0), G4G_xor(G4G_td1(s1.b1), G4G_xor(G4G_td2(s0.b2), G4G_td3(s3.b3)))), rk_constant[30]);
		t3 = G4G_keying(G4G_xor(G4G_td0(s3.b0), G4G_xor(G4G_td1(s2.b1), G4G_xor(G4G_td2(s1.b2), G4G_td3(s0.b3)))), rk_constant[31]);

		/* round 8 */
		s0 = G4G_keying(G4G_xor(G4G_td0(t0.b0), G4G_xor(G4G_td1(t3.b1), G4G_xor(G4G_td2(t2.b2), G4G_td3(t1.b3)))), rk_constant[32]);
		s1 = G4G_keying(G4G_xor(G4G_td0(t1.b0), G4G_xor(G4G_td1(t0.b1), G4G_xor(G4G_td2(t3.b2), G4G_td3(t2.b3)))), rk_constant[33]);
		s2 = G4G_keying(G4G_xor(G4G_td0(t2.b0), G4G_xor(G4G_td1(t1.b1), G4G_xor(G4G_td2(t0.b2), G4G_td3(t3.b3)))), rk_constant[34]);
		s3 = G4G_keying(G4G_xor(G4G_td0(t3.b0), G4G_xor(G4G_td1(t2.b1), G4G_xor(G4G_td2(t1.b2), G4G_td3(t0.b3)))), rk_constant[35]);

		/* round 9 */
		t0 = G4G_keying(G4G_xor(G4G_td0(s0.b0), G4G_xor(G4G_td1(s3.b1), G4G_xor(G4G_td2(s2.b2), G4G_td3(s1.b3)))), rk_constant[36]);
		t1 = G4G_keying(G4G_xor(G4G_td0(s1.b0), G4G_xor(G4G_td1(s0.b1), G4G_xor(G4G_td2(s3.b2), G4G_td3(s2.b3)))), rk_constant[37]);
		t2 = G4G_keying(G4G_xor(G4G_td0(s2.b0), G4G_xor(G4G_td1(s1.b1), G4G_xor(G4G_td2(s0.b2), G4G_td3(s3.b3)))), rk_constant[38]);
		t3 = G4G_keying(G4G_xor(G4G_td0(s3.b0), G4G_xor(G4G_td1(s2.b1), G4G_xor(G4G_td2(s1.b2), G4G_td3(s0.b3)))), rk_constant[39]);

		if (Nr > 10){

			/* round 10 */
			s0 = G4G_keying(G4G_xor(G4G_td0(t0.b0), G4G_xor(G4G_td1(t3.b1), G4G_xor(G4G_td2(t2.b2), G4G_td3(t1.b3)))), rk_constant[40]);
			s1 = G4G_keying(G4G_xor(G4G_td0(t1.b0), G4G_xor(G4G_td1(t0.b1), G4G_xor(G4G_td2(t3.b2), G4G_td3(t2.b3)))), rk_constant[41]);
			s2 = G4G_keying(G4G_xor(G4G_td0(t2.b0), G4G_xor(G4G_td1(t1.b1), G4G_xor(G4G_td2(t0.b2), G4G_td3(t3.b3)))), rk_constant[42]);
			s3 = G4G_keying(G4G_xor(G4G_td0(t3.b0), G4G_xor(G4G_td1(t2.b1), G4G_xor(G4G_td2(t1.b2), G4G_td3(t0.b3)))), rk_constant[43]);

			/* round 11 */
			t0 = G4G_keying(G4G_xor(G4G_td0(s0.b0), G4G_xor(G4G_td1(s3.b1), G4G_xor(G4G_td2(s2.b2), G4G_td3(s1.b3)))), rk_constant[44]);
			t1 = G4G_keying(G4G_xor(G4G_td0(s1.b0), G4G_xor(G4G_td1(s0.b1), G4G_xor(G4G_td2(s3.b2), G4G_td3(s2.b3)))), rk_constant[45]);
			t2 = G4G_keying(G4G_xor(G4G_td0(s2.b0), G4G_xor(G4G_td1(s1.b1), G4G_xor(G4G_td2(s0.b2), G4G_td3(s3.b3)))), rk_constant[46]);
			t3 = G4G_keying(G4G_xor(G4G_td0(s3.b0), G4G_xor(G4G_td1(s2.b1), G4G_xor(G4G_td2(s1.b2), G4G_td3(s0.b3)))), rk_constant[47]);

			if (Nr > 12){
				/* round 12 */
				s0 = G4G_keying(G4G_xor(G4G_td0(t0.b0), G4G_xor(G4G_td1(t3.b1), G4G_xor(G4G_td2(t2.b2), G4G_td3(t1.b3)))), rk_constant[48]);
				s1 = G4G_keying(G4G_xor(G4G_td0(t1.b0), G4G_xor(G4G_td1(t0.b1), G4G_xor(G4G_td2(t3.b2), G4G_td3(t2.b3)))), rk_constant[49]);
				s2 = G4G_keying(G4G_xor(G4G_td0(t2.b0), G4G_xor(G4G_td1(t1.b1), G4G_xor(G4G_td2(t0.b2), G4G_td3(t3.b3)))), rk_constant[50]);
				s3 = G4G_keying(G4G_xor(G4G_td0(t3.b0), G4G_xor(G4G_td1(t2.b1), G4G_xor(G4G_td2(t1.b2), G4G_td3(t0.b3)))), rk_constant[51]);

				/* round 13 */
				t0 = G4G_keying(G4G_xor(G4G_td0(s0.b0), G4G_xor(G4G_td1(s3.b1), G4G_xor(G4G_td2(s2.b2), G4G_td3(s1.b3)))), rk_constant[52]);
				t1 = G4G_keying(G4G_xor(G4G_td0(s1.b0), G4G_xor(G4G_td1(s0.b1), G4G_xor(G4G_td2(s3.b2), G4G_td3(s2.b3)))), rk_constant[53]);
				t2 = G4G_keying(G4G_xor(G4G_td0(s2.b0), G4G_xor(G4G_td1(s1.b1), G4G_xor(G4G_td2(s0.b2), G4G_td3(s3.b3)))), rk_constant[54]);
				t3 = G4G_keying(G4G_xor(G4G_td0(s3.b0), G4G_xor(G4G_td1(s2.b1), G4G_xor(G4G_td2(s1.b2), G4G_td3(s0.b3)))), rk_constant[55]);
			}
		}

		u32 x = Nr << 2;

		s0 = G4G_keying(G4G_td4(t0.b0, t3.b1, t2.b2, t1.b3), rk_constant[x++]);
		s1 = G4G_keying(G4G_td4(t1.b0, t0.b1, t3.b2, t2.b3), rk_constant[x++]);
		s2 = G4G_keying(G4G_td4(t2.b0, t1.b1, t0.b2, t3.b3), rk_constant[x++]);
		s3 = G4G_keying(G4G_td4(t3.b0, t2.b1, t1.b2, t0.b3), rk_constant[x]);

		out[0] = s0;
		out[1] = s1;
		out[2] = s2;
		out[3] = s3;
	}

}



__global__ void transposeKernel(u8 *in, u8 *out, const unsigned int blocks)
{


	int tx = threadIdx.x;
	int ty = threadIdx.y;

	int by = blockIdx.y;
	int tz = threadIdx.z;

	int block = (by * blockDim.z + tz) * gridDim.z + blockIdx.z;

	if (block < blocks){
		register u32 i0, i1, i2, i3, i4, i5, i6, i7;
		register u32 o0, o1, o2, o3, o4, o5, o6, o7;

		in += block * (BLOCK_SIZE * BYTES_PER_WORD);
		out += block * (BLOCK_SIZE * BYTES_PER_WORD);

		int offset1, offset2;
		offset1 = tx * BLOCK_SIZE + ty;
		offset2 = ty * BITS_PER_WORD + tx;

		i0 = in[offset1 + 0 * BYTES_PER_BLOCK];
		i1 = in[offset1 + 1 * BYTES_PER_BLOCK];
		i2 = in[offset1 + 2 * BYTES_PER_BLOCK];
		i3 = in[offset1 + 3 * BYTES_PER_BLOCK];
		i4 = in[offset1 + 4 * BYTES_PER_BLOCK];
		i5 = in[offset1 + 5 * BYTES_PER_BLOCK];
		i6 = in[offset1 + 6 * BYTES_PER_BLOCK];
		i7 = in[offset1 + 7 * BYTES_PER_BLOCK];

		o0 = ((i0 & 0x01) << 0) | ((i1 & 0x01) << 1) | ((i2 & 0x01) << 2) | ((i3 & 0x01) << 3) | ((i4 & 0x01) << 4) | ((i5 & 0x01) << 5) | ((i6 & 0x01) << 6) | ((i7 & 0x01) << 7);
		o1 = ((i0 & 0x02) >> 1) | ((i1 & 0x02) << 0) | ((i2 & 0x02) << 1) | ((i3 & 0x02) << 2) | ((i4 & 0x02) << 3) | ((i5 & 0x02) << 4) | ((i6 & 0x02) << 5) | ((i7 & 0x02) << 6);
		o2 = ((i0 & 0x04) >> 2) | ((i1 & 0x04) >> 1) | ((i2 & 0x04) << 0) | ((i3 & 0x04) << 1) | ((i4 & 0x04) << 2) | ((i5 & 0x04) << 3) | ((i6 & 0x04) << 4) | ((i7 & 0x04) << 5);
		o3 = ((i0 & 0x08) >> 3) | ((i1 & 0x08) >> 2) | ((i2 & 0x08) >> 1) | ((i3 & 0x08) << 0) | ((i4 & 0x08) << 1) | ((i5 & 0x08) << 2) | ((i6 & 0x08) << 3) | ((i7 & 0x08) << 4);
		o4 = ((i0 & 0x10) >> 4) | ((i1 & 0x10) >> 3) | ((i2 & 0x10) >> 2) | ((i3 & 0x10) >> 1) | ((i4 & 0x10) << 0) | ((i5 & 0x10) << 1) | ((i6 & 0x10) << 2) | ((i7 & 0x10) << 3);
		o5 = ((i0 & 0x20) >> 5) | ((i1 & 0x20) >> 4) | ((i2 & 0x20) >> 3) | ((i3 & 0x20) >> 2) | ((i4 & 0x20) >> 1) | ((i5 & 0x20) << 0) | ((i6 & 0x20) << 1) | ((i7 & 0x20) << 2);
		o6 = ((i0 & 0x40) >> 6) | ((i1 & 0x40) >> 5) | ((i2 & 0x40) >> 4) | ((i3 & 0x40) >> 3) | ((i4 & 0x40) >> 2) | ((i5 & 0x40) >> 1) | ((i6 & 0x40) << 0) | ((i7 & 0x40) << 1);
		o7 = ((i0 & 0x80) >> 7) | ((i1 & 0x80) >> 6) | ((i2 & 0x80) >> 5) | ((i3 & 0x80) >> 4) | ((i4 & 0x80) >> 3) | ((i5 & 0x80) >> 2) | ((i6 & 0x80) >> 1) | ((i7 & 0x80) << 0);

		out[offset2 + 0 * BYTES_PER_WORD] = o0;
		out[offset2 + 1 * BYTES_PER_WORD] = o1;
		out[offset2 + 2 * BYTES_PER_WORD] = o2;
		out[offset2 + 3 * BYTES_PER_WORD] = o3;
		out[offset2 + 4 * BYTES_PER_WORD] = o4;
		out[offset2 + 5 * BYTES_PER_WORD] = o5;
		out[offset2 + 6 * BYTES_PER_WORD] = o6;
		out[offset2 + 7 * BYTES_PER_WORD] = o7;
	}

}

__global__ void transposeInvKernel(u8 *in, u8 *out, const unsigned int blocks)
{


	int tx = threadIdx.x;
	int ty = threadIdx.y;

	int bx = blockIdx.x;
	int tz = threadIdx.z;

	int block = (bx * blockDim.z + tz) * gridDim.z + blockIdx.z;

	if (block < blocks){

		register u32 i0, i1, i2, i3, i4, i5, i6, i7;
		register u32 o0, o1, o2, o3, o4, o5, o6, o7;

		in += block * (BLOCK_SIZE * BYTES_PER_WORD);
		out += block * (BLOCK_SIZE * BYTES_PER_WORD);

		int offset1, offset2;
		offset1 = tx * BITS_PER_WORD + ty;
		offset2 = ty * BLOCK_SIZE + tx;


		i0 = in[offset1 + 0 * BYTES_PER_WORD];
		i1 = in[offset1 + 1 * BYTES_PER_WORD];
		i2 = in[offset1 + 2 * BYTES_PER_WORD];
		i3 = in[offset1 + 3 * BYTES_PER_WORD];
		i4 = in[offset1 + 4 * BYTES_PER_WORD];
		i5 = in[offset1 + 5 * BYTES_PER_WORD];
		i6 = in[offset1 + 6 * BYTES_PER_WORD];
		i7 = in[offset1 + 7 * BYTES_PER_WORD];


		o0 = ((i0 & 0x01) << 0) | ((i1 & 0x01) << 1) | ((i2 & 0x01) << 2) | ((i3 & 0x01) << 3) | ((i4 & 0x01) << 4) | ((i5 & 0x01) << 5) | ((i6 & 0x01) << 6) | ((i7 & 0x01) << 7);
		o1 = ((i0 & 0x02) >> 1) | ((i1 & 0x02) << 0) | ((i2 & 0x02) << 1) | ((i3 & 0x02) << 2) | ((i4 & 0x02) << 3) | ((i5 & 0x02) << 4) | ((i6 & 0x02) << 5) | ((i7 & 0x02) << 6);
		o2 = ((i0 & 0x04) >> 2) | ((i1 & 0x04) >> 1) | ((i2 & 0x04) << 0) | ((i3 & 0x04) << 1) | ((i4 & 0x04) << 2) | ((i5 & 0x04) << 3) | ((i6 & 0x04) << 4) | ((i7 & 0x04) << 5);
		o3 = ((i0 & 0x08) >> 3) | ((i1 & 0x08) >> 2) | ((i2 & 0x08) >> 1) | ((i3 & 0x08) << 0) | ((i4 & 0x08) << 1) | ((i5 & 0x08) << 2) | ((i6 & 0x08) << 3) | ((i7 & 0x08) << 4);
		o4 = ((i0 & 0x10) >> 4) | ((i1 & 0x10) >> 3) | ((i2 & 0x10) >> 2) | ((i3 & 0x10) >> 1) | ((i4 & 0x10) << 0) | ((i5 & 0x10) << 1) | ((i6 & 0x10) << 2) | ((i7 & 0x10) << 3);
		o5 = ((i0 & 0x20) >> 5) | ((i1 & 0x20) >> 4) | ((i2 & 0x20) >> 3) | ((i3 & 0x20) >> 2) | ((i4 & 0x20) >> 1) | ((i5 & 0x20) << 0) | ((i6 & 0x20) << 1) | ((i7 & 0x20) << 2);
		o6 = ((i0 & 0x40) >> 6) | ((i1 & 0x40) >> 5) | ((i2 & 0x40) >> 4) | ((i3 & 0x40) >> 3) | ((i4 & 0x40) >> 2) | ((i5 & 0x40) >> 1) | ((i6 & 0x40) << 0) | ((i7 & 0x40) << 1);
		o7 = ((i0 & 0x80) >> 7) | ((i1 & 0x80) >> 6) | ((i2 & 0x80) >> 5) | ((i3 & 0x80) >> 4) | ((i4 & 0x80) >> 3) | ((i5 & 0x80) >> 2) | ((i6 & 0x80) >> 1) | ((i7 & 0x80) << 0);


		out[offset2 + 0 * BYTES_PER_BLOCK] = o0;
		out[offset2 + 1 * BYTES_PER_BLOCK] = o1;
		out[offset2 + 2 * BYTES_PER_BLOCK] = o2;
		out[offset2 + 3 * BYTES_PER_BLOCK] = o3;
		out[offset2 + 4 * BYTES_PER_BLOCK] = o4;
		out[offset2 + 5 * BYTES_PER_BLOCK] = o5;
		out[offset2 + 6 * BYTES_PER_BLOCK] = o6;
		out[offset2 + 7 * BYTES_PER_BLOCK] = o7;
	}

}




cudaError_t rijndaelEncryptBitwiseCuda(const u8 pt[], u8 ct[], const u32 rk[], const unsigned int plainTextLength, const unsigned int Nr){


	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	u32 words, blocks;
	words = ((plainTextLength - 1) / (BLOCK_SIZE * BYTES_PER_WORD) + 1) * BLOCK_SIZE;
	blocks = (words - 1) / BLOCK_SIZE + 1;



	u32 *devIn;
	u32 *devOut;

	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		return cudaStatus;
	}

	// cuda malloc
	start = startStopwatch();
	cudaStatus = cudaMalloc((void**)&devIn, words * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMalloc((void**)&devOut, words * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}
	
	endStopwatch("Malloc (bitwise encryption)", start);

	start = startStopwatch();
	cudaStatus = cudaMemcpy(devIn, pt, plainTextLength * sizeof(u8), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMemcpyToSymbol(rk_constant, rk, rkLength * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}


	totalTime = endStopwatch("Memory copy from host to device (bitwise encryption)", start);

	//!!!must be padded before!!!
	dim3 dimGridTransposing(1, (blocks - 1) / 8 + 1, 4);
	dim3 dimBlockTransposing(BYTES_PER_WORD, BYTES_PER_BLOCK, 2);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	transposeKernel << <dimGridTransposing, dimBlockTransposing >> >((u8 *)devIn, (u8 *)devOut, blocks);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Transposing data kernel (encryption)", start);



	//!!!must be padded before!!!
	dim3 dimGridCrypto((blocks - 1 ) / CRYPTO_THREAD_BLOCK_SIZE + 1, 1, 1);
	dim3 dimBlockCrypto(CRYPTO_THREAD_BLOCK_SIZE, 1, 1);

	int g4gs = words / BITS_PER_WORD;

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	rijndaelEncryptBitwiseKernel << <dimGridCrypto, dimBlockCrypto >> >((G4G *)devOut, (G4G *)devIn, g4gs, Nr);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Encryption kernel", start);

	dim3 dimGridInverseTransposing((blocks - 1) / 8 + 1, 1, 4);
	dim3 dimBlockInverseTransposing(BYTES_PER_BLOCK, BYTES_PER_WORD, 2);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	transposeInvKernel << <dimGridInverseTransposing, dimBlockInverseTransposing >> >((u8 *)devIn, (u8 *)devOut, blocks);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Inverse transposing data kernel (encryption)", start);


	// Copy output vector from GPU buffer to host memory.
	start = startStopwatch();
	cudaStatus = cudaMemcpy(ct, devOut, plainTextLength * sizeof(u8), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}
	totalTime += endStopwatch("Copy from device to host (bitwise encryption)", start);

	start = startStopwatch();
	cudaFree(devIn);
	cudaFree(devOut);

	endStopwatch("Freeing memory (bitwise encryption)", start);

	printDuration("Encryption", totalTime);

	return cudaStatus;
}

cudaError_t rijndaelDecryptBitwiseCuda(const u8 ct[], u8 pt[], const u32 rk[], const unsigned int plainTextLength, const unsigned int Nr){


	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	u32 words, blocks;
	words = ((plainTextLength - 1) / (BLOCK_SIZE * BYTES_PER_WORD) + 1) * BLOCK_SIZE;
	blocks = (words - 1) / BLOCK_SIZE + 1;


	u32 *devIn;
	u32 *devOut;


	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		return cudaStatus;
	}

	// cuda malloc
	start = startStopwatch();
	cudaStatus = cudaMalloc((void**)&devIn, words * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMalloc((void**)&devOut, words * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	endStopwatch("Malloc (bitwise decryption)", start);

	start = startStopwatch();
	cudaStatus = cudaMemcpy(devIn, ct, plainTextLength * sizeof(u8), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMemcpyToSymbol(rk_constant, rk, rkLength * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}


	totalTime = endStopwatch("Memory copy from host to device (bitwise decryption)", start);

	//!!!must be padded before!!!
	dim3 dimGridTransposing(1, (blocks - 1) / 8 + 1, 4);
	dim3 dimBlockTransposing(BYTES_PER_WORD, BYTES_PER_BLOCK, 2);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	transposeKernel << <dimGridTransposing, dimBlockTransposing >> >((u8 *)devIn, (u8 *)devOut, blocks);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Transposing data kernel (decryption)", start);



	//!!!must be padded before!!!
	dim3 dimGridCrypto((blocks - 1) / CRYPTO_THREAD_BLOCK_SIZE + 1, 1, 1);
	dim3 dimBlockCrypto(CRYPTO_THREAD_BLOCK_SIZE, 1, 1);

	int g4gs = words / BITS_PER_WORD;

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	rijndaelDecryptBitwiseKernel << <dimGridCrypto, dimBlockCrypto >> >((G4G *)devOut, (G4G *)devIn, g4gs, Nr);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Decryption kernel", start);

	dim3 dimGridInverseTransposing((blocks - 1) / 8 + 1, 1, 4);
	dim3 dimBlockInverseTransposing(BYTES_PER_BLOCK, BYTES_PER_WORD, 2);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	transposeInvKernel << <dimGridInverseTransposing, dimBlockInverseTransposing >> >((u8 *)devIn, (u8 *)devOut, blocks);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Inverse transposing data kernel (decryption)", start);


	// Copy output vector from GPU buffer to host memory.
	start = startStopwatch();
	cudaStatus = cudaMemcpy(pt, devOut, plainTextLength * sizeof(u8), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}
	totalTime += endStopwatch("Copy from device to host (bitwise decryption)", start);

	start = startStopwatch();
	cudaFree(devIn);
	cudaFree(devOut);

	endStopwatch("Freeing memory (bitwise decryption)", start);

	printDuration("Decryption", totalTime);

	return cudaStatus;
}

int rijndaelKeySetupEnc(u32 rk[], const u32 cipherKey[], const unsigned int keyLen) {
	int i = 0;
	u32 temp;

	rk[0] = cipherKey[0];
	rk[1] = cipherKey[1];
	rk[2] = cipherKey[2];
	rk[3] = cipherKey[3];

	if (keyLen == 16) {
		for (;;) {
			temp = rk[3];
			rk[4] = rk[0] ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];

			rk[5] = rk[1] ^ rk[4];
			rk[6] = rk[2] ^ rk[5];
			rk[7] = rk[3] ^ rk[6];
			if (++i == 10) {
				return 10;
			}
			rk += 4;
		}
	}
	rk[4] = cipherKey[4];
	rk[5] = cipherKey[5];

	if (keyLen == 24) {
		for (;;) {
			temp = rk[5];
			rk[6] = rk[0] ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[7] = rk[1] ^ rk[6];
			rk[8] = rk[2] ^ rk[7];
			rk[9] = rk[3] ^ rk[8];
			if (++i == 8) {
				return 12;
			}
			rk[10] = rk[4] ^ rk[9];
			rk[11] = rk[5] ^ rk[10];
			rk += 6;
		}
	}
	rk[6] = cipherKey[6];
	rk[7] = cipherKey[7];
	if (keyLen == 32) {
		for (;;) {
			temp = rk[7];
			rk[8] = rk[0] ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[9] = rk[1] ^ rk[8];
			rk[10] = rk[2] ^ rk[9];
			rk[11] = rk[3] ^ rk[10];
			if (++i == 7) {
				return 14;
			}
			temp = rk[11];
			rk[12] = rk[4] ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0x000000ff);
			rk[13] = rk[5] ^ rk[12];
			rk[14] = rk[6] ^ rk[13];
			rk[15] = rk[7] ^ rk[14];

			rk += 8;
		}
	}
	return 0;
}



int rijndaelKeySetupDec(u32 rk[], const u32 cipherKey[], const unsigned int keyLen) {
	int Nr, i, j;
	u32 temp;

	/* expand the cipher key: */
	Nr = rijndaelKeySetupEnc(rk, (u32 *)cipherKey, keyLen);
	/* invert the order of the round keys: */
	for (i = 0, j = 4 * Nr; i < j; i += 4, j -= 4) {
		temp = rk[i]; rk[i] = rk[j]; rk[j] = temp;
		temp = rk[i + 1]; rk[i + 1] = rk[j + 1]; rk[j + 1] = temp;
		temp = rk[i + 2]; rk[i + 2] = rk[j + 2]; rk[j + 2] = temp;
		temp = rk[i + 3]; rk[i + 3] = rk[j + 3]; rk[j + 3] = temp;
	}
	/* apply the inverse MixColumn transform to all round keys but the first and the last: */

	for (i = 1; i < Nr; i++) {
		rk += 4;


		rk[0] =
			bigTd[0 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[0]) & 0xff)] & 0xff)] ^
			bigTd[1 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[0] >> 8) & 0xff)] & 0xff)] ^
			bigTd[2 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[0] >> 16) & 0xff)] & 0xff)] ^
			bigTd[3 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + (rk[0] >> 24)] & 0xff)];


		rk[1] =
			bigTd[0 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[1]) & 0xff)] & 0xff)] ^
			bigTd[1 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[1] >> 8) & 0xff)] & 0xff)] ^
			bigTd[2 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[1] >> 16) & 0xff)] & 0xff)] ^
			bigTd[3 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + (rk[1] >> 24)] & 0xff)];


		rk[2] =
			bigTd[0 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[2]) & 0xff)] & 0xff)] ^
			bigTd[1 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[2] >> 8) & 0xff)] & 0xff)] ^
			bigTd[2 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[2] >> 16) & 0xff)] & 0xff)] ^
			bigTd[3 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + (rk[2] >> 24)] & 0xff)];

		rk[3] =
			bigTd[0 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[3]) & 0xff)] & 0xff)] ^
			bigTd[1 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[3] >> 8) & 0xff)] & 0xff)] ^
			bigTd[2 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[3] >> 16) & 0xff)] & 0xff)] ^
			bigTd[3 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + (rk[3] >> 24)] & 0xff)];



	}
	return Nr;
}

int rijndaelEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
{

	boost::chrono::high_resolution_clock::time_point start;

	cudaFree(NULL);
	u32 rk[rkLength];
	
	u8 *pinnedPt;
	u8 *pinnedCt;

	cudaMallocHost(&pinnedPt, plainTextLength);
	cudaMallocHost(&pinnedCt, plainTextLength);

	memcpy(pinnedPt, pt, plainTextLength);

	start = startStopwatch();
	unsigned int Nr = rijndaelKeySetupEnc(rk, (u32 *)cipherKey, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);
	
	
    cudaError_t cudaStatus = rijndaelEncryptBitwiseCuda(pinnedPt, pinnedCt, rk, plainTextLength, Nr);
	
	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Encryption failed!");
        return 1;
    }

	memcpy(ct, pinnedCt, plainTextLength);
	cudaFreeHost(pinnedPt);
	cudaFreeHost(pinnedCt);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }
	
    return 0;
}

int rijndaelDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
{

	boost::chrono::high_resolution_clock::time_point start;

	
	cudaFree(NULL);
	u32 rk[rkLength];
	
	u8 *pinnedCt;
	u8 *pinnedPt;

	cudaMallocHost(&pinnedCt, plainTextLength);
	cudaMallocHost(&pinnedPt, plainTextLength);

	memcpy(pinnedCt, ct, plainTextLength);

	start = startStopwatch();
	unsigned int Nr = rijndaelKeySetupDec(rk, (u32 *)cipherKey, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);
		
    cudaError_t cudaStatus = rijndaelDecryptBitwiseCuda(pinnedCt, pinnedPt, rk, plainTextLength, Nr);
	
	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Decription failed!");
        return 1;
    }

	memcpy(pt, pinnedPt, plainTextLength);
	cudaFreeHost(pinnedCt);
	cudaFreeHost(pinnedPt);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }
	
    return 0;
}


