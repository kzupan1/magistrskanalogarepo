#include "common-types.h"
#include "serpent-tables.h"

__kernel void encryptKernel(__global uint4 *pt, __global uint4 *ct, __constant u32 *k, const int blocks, const u32 n0, const u32 n1) 
{																																	  
																																	  
																																	  
	u64 i = get_global_id(0);																										  

																																	  
	if(i < blocks){																							  
																																	  
																																																						  
		register unsigned int r0, r1, r2, r3, r4;																						  
					
		r0 = n0;
		r1 = n1;
		r2 = ((i & 0xffffffff00000000) >> 32);
		r3 = (i & 0xffffffff);
																																	  
		K(r0,r1,r2,r3,0);																											  
		S0(r0,r1,r2,r3,r4);	LK(r2,r1,r3,r0,r4,1);																					  
		S1(r2,r1,r3,r0,r4);	LK(r4,r3,r0,r2,r1,2);																					  
		S2(r4,r3,r0,r2,r1);	LK(r1,r3,r4,r2,r0,3);																					  
		S3(r1,r3,r4,r2,r0);	LK(r2,r0,r3,r1,r4,4);																					  
		S4(r2,r0,r3,r1,r4);	LK(r0,r3,r1,r4,r2,5);																					  
		S5(r0,r3,r1,r4,r2);	LK(r2,r0,r3,r4,r1,6);																					  
		S6(r2,r0,r3,r4,r1);	LK(r3,r1,r0,r4,r2,7);																					  
		S7(r3,r1,r0,r4,r2);	LK(r2,r0,r4,r3,r1,8);																					  
		S0(r2,r0,r4,r3,r1);	LK(r4,r0,r3,r2,r1,9);																					  
		S1(r4,r0,r3,r2,r1);	LK(r1,r3,r2,r4,r0,10);																					  
		S2(r1,r3,r2,r4,r0);	LK(r0,r3,r1,r4,r2,11);																					  
		S3(r0,r3,r1,r4,r2);	LK(r4,r2,r3,r0,r1,12);																					  
		S4(r4,r2,r3,r0,r1);	LK(r2,r3,r0,r1,r4,13);																					  
		S5(r2,r3,r0,r1,r4);	LK(r4,r2,r3,r1,r0,14);																					  
		S6(r4,r2,r3,r1,r0);	LK(r3,r0,r2,r1,r4,15);																					  
		S7(r3,r0,r2,r1,r4);	LK(r4,r2,r1,r3,r0,16);																					  
		S0(r4,r2,r1,r3,r0);	LK(r1,r2,r3,r4,r0,17);																					  
		S1(r1,r2,r3,r4,r0);	LK(r0,r3,r4,r1,r2,18);																					  
		S2(r0,r3,r4,r1,r2);	LK(r2,r3,r0,r1,r4,19);																					  
		S3(r2,r3,r0,r1,r4);	LK(r1,r4,r3,r2,r0,20);																					  
		S4(r1,r4,r3,r2,r0);	LK(r4,r3,r2,r0,r1,21);																					  
		S5(r4,r3,r2,r0,r1);	LK(r1,r4,r3,r0,r2,22);																					  
		S6(r1,r4,r3,r0,r2);	LK(r3,r2,r4,r0,r1,23);																					  
		S7(r3,r2,r4,r0,r1);	LK(r1,r4,r0,r3,r2,24);																					  
		S0(r1,r4,r0,r3,r2);	LK(r0,r4,r3,r1,r2,25);																					  
		S1(r0,r4,r3,r1,r2);	LK(r2,r3,r1,r0,r4,26);																					  
		S2(r2,r3,r1,r0,r4);	LK(r4,r3,r2,r0,r1,27);																					  
		S3(r4,r3,r2,r0,r1);	LK(r0,r1,r3,r4,r2,28);																					  
		S4(r0,r1,r3,r4,r2);	LK(r1,r3,r4,r2,r0,29);																					  
		S5(r1,r3,r4,r2,r0);	LK(r0,r1,r3,r2,r4,30);																					  
		S6(r0,r1,r3,r2,r4);	LK(r3,r4,r1,r2,r0,31);																					  
		S7(r3,r4,r1,r2,r0);	 K(r0,r1,r2,r3,32);																						  
																																	  

		ct[i] = (uint4){ r0 ^ pt[i].x, r1 ^ pt[i].y, r2 ^ pt[i].z, r3 ^ pt[i].w };

	}																																  
																																	  
																							  
																																	 																																	  
																																	  
}																																	  

