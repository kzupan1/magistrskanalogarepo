#include "mars-opencl.h"
#include "mars-tables.h"
#include "measure-time.h"
#include "opencl-helpers.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <CL/cl.h>
#include <boost/chrono.hpp>

#define BUILD_SOURCE 0

const char *options = "-I ./common-types.h";

const char *encryptKernelSource = "marsEncryptKernel.cl";
const char *encryptKernelBin = "marsEncryptKernel.clbin";

cl_int marsEncryptOpenCl(const cl_uint4 pt[], cl_uint4 ct[], const u32 rk[], const int blocks, InitValues *initValues, cl_kernel krnl)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	cl_mem devRk;
	cl_mem devS;

	cl_mem pinnedPt;
	cl_mem pinnedCt;

	cl_uint4* hostPinnedPt;
	cl_uint4* hostPinnedCt;

    cl_int clStatus;

	srand(0);
	u32 n0 = rand();
	u32 n1 = rand();
	ct[0].s[0] = n0;
	ct[0].s[1] = n1;
	ct[0].s[2] = 0;
	ct[0].s[3] = 0;

	ct++;

	// Create a command queue
	cl_command_queue command_queue = clCreateCommandQueue(initValues->context, initValues->device_list[0], 0, &clStatus);
	
	start = startStopwatch();

	pinnedPt = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, blocks * sizeof(cl_uint4), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedPt failed!");
		return clStatus;
	}

	pinnedCt = clCreateBuffer(initValues->context, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, blocks * sizeof(cl_uint4), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedCt failed!");
		return clStatus;
	}

	hostPinnedPt = (cl_uint4 *)clEnqueueMapBuffer(command_queue, pinnedPt, CL_TRUE, CL_MAP_WRITE, 0, blocks * sizeof(cl_uint4), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}

	memcpy(hostPinnedPt, pt, blocks * sizeof(cl_uint4));

	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedPt, hostPinnedPt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);
    
	
	devRk = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY, RK_SIZE * sizeof(u32), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clCreateBuffer failed!");
        return clStatus;
    }

	devS = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY, BIG_S_TABLE_SIZE * sizeof(u32), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer failed!");
		return clStatus;
	}
	
	
	endStopwatch("clCreateBuffer (encryption) ", start);

	start = startStopwatch();

	clStatus = clEnqueueWriteBuffer(command_queue, devRk, CL_TRUE, 0, RK_SIZE * sizeof(u32), rk, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clEnqueueWriteBuffer failed!");
        return clStatus;
    }
	
	clStatus = clEnqueueWriteBuffer(command_queue, devS, CL_TRUE, 0, BIG_S_TABLE_SIZE * sizeof(u32), S, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueWriteBuffer failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);

	totalTime = endStopwatch("clEnqueueWriteBuffer (encryption)", start);


	// Set the arguments of the kernel
	clStatus = clSetKernelArg(krnl, 0, sizeof(cl_mem), (void *)&pinnedPt);
	clStatus = clSetKernelArg(krnl, 1, sizeof(cl_mem), (void *)&pinnedCt);
	clStatus = clSetKernelArg(krnl, 2, sizeof(cl_mem), (void *)&devRk);
	clStatus = clSetKernelArg(krnl, 3, sizeof(cl_mem), (void *)&devS);
	clStatus = clSetKernelArg(krnl, 4, sizeof(int), (void *)&blocks);
	clStatus = clSetKernelArg(krnl, 5, sizeof(u32), (void *)&n0);
	clStatus = clSetKernelArg(krnl, 6, sizeof(u32), (void *)&n1);


	//!!!must be padded before!!!
	size_t global_size = ((blocks - 1)/THREAD_BLOCK_SIZE + 1) * THREAD_BLOCK_SIZE ;
	size_t local_size = THREAD_BLOCK_SIZE;

    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	clStatus = clEnqueueNDRangeKernel(command_queue, krnl, 1, NULL, &global_size, &local_size, 0, NULL, NULL);
	
	// Check for any errors launching the kernel
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "encryption kernel launch failed!");
		printf("%d\n", clStatus);
		return clStatus;
	}

	clFlush(command_queue);
	clFinish(command_queue);
	totalTime += endStopwatch("Encryption kernel", start);

    
	hostPinnedCt = (cl_uint4 *)clEnqueueMapBuffer(command_queue, pinnedCt, CL_TRUE, CL_MAP_READ, 0, blocks * sizeof(cl_uint4), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}
	memcpy(ct, hostPinnedCt, blocks * sizeof(cl_uint4));
	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedCt, hostPinnedCt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);

    

			
	// Finally release all OpenCL allocated objePts and host buffers.7
	start = startStopwatch();

	clStatus = clReleaseMemObject(devRk);
	clStatus = clReleaseMemObject(devS);

	clStatus = clReleaseCommandQueue(command_queue);
	
	endStopwatch("clReleaseMemObject (encryption)", start);

	clStatus = clReleaseMemObject(pinnedPt);
	clStatus = clReleaseMemObject(pinnedCt);

	printDuration("Encryption", totalTime);

	return clStatus;
}

cl_int marsDecryptOpenCl(const cl_uint4 ct[], cl_uint4 pt[], const u32 rk[], const int blocks, InitValues *initValues, cl_kernel krnl)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	cl_mem devRk;

	cl_mem pinnedCt;
	cl_mem pinnedPt;

	cl_uint4* hostPinnedPt;
	cl_uint4* hostPinnedCt;

	cl_mem devS;

    cl_int clStatus;

	u32 n0 = ct[0].s[0];
	u32 n1 = ct[0].s[1];

	ct++;

	// Create a command queue
	cl_command_queue command_queue = clCreateCommandQueue(initValues->context, initValues->device_list[0], 0, &clStatus);

	start = startStopwatch();
	pinnedCt = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, blocks * sizeof(cl_uint4), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedPt failed!");
		return clStatus;
	}

	pinnedPt = clCreateBuffer(initValues->context, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, blocks * sizeof(cl_uint4), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedCt failed!");
		return clStatus;
	}

	hostPinnedCt = (cl_uint4 *)clEnqueueMapBuffer(command_queue, pinnedCt, CL_TRUE, CL_MAP_WRITE, 0, blocks * sizeof(cl_uint4), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}

	memcpy(hostPinnedCt, ct, blocks * sizeof(cl_uint4));


	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedCt, hostPinnedCt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);
   
	
	devRk = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY, RK_SIZE * sizeof(u32), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clCreateBuffer failed!");
        return clStatus;
    }

	devS = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY, BIG_S_TABLE_SIZE * sizeof(u32), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer failed!");
		return clStatus;
	}
	
	
	endStopwatch("clCreateBuffer (decryption) ", start);

	start = startStopwatch();

	clStatus = clEnqueueWriteBuffer(command_queue, devRk, CL_TRUE, 0, RK_SIZE * sizeof(u32), rk, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clEnqueueWriteBuffer failed!");
        return clStatus;
    }

	clStatus = clEnqueueWriteBuffer(command_queue, devS, CL_TRUE, 0, BIG_S_TABLE_SIZE * sizeof(u32), S, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueWriteBuffer failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);

	totalTime = endStopwatch("clEnqueueWriteBuffer (decryption) ", start);


	// Set the arguments of the kernel
	clStatus = clSetKernelArg(krnl, 0, sizeof(cl_mem), (void *)&pinnedCt);
	clStatus = clSetKernelArg(krnl, 1, sizeof(cl_mem), (void *)&pinnedPt);
	clStatus = clSetKernelArg(krnl, 2, sizeof(cl_mem), (void *)&devRk);
	clStatus = clSetKernelArg(krnl, 3, sizeof(cl_mem), (void *)&devS);
	clStatus = clSetKernelArg(krnl, 4, sizeof(int), (void *)&blocks);
	clStatus = clSetKernelArg(krnl, 5, sizeof(u32), (void *)&n0);
	clStatus = clSetKernelArg(krnl, 6, sizeof(u32), (void *)&n1);


	//!!!must be padded before!!!
	size_t global_size = ((blocks - 1)/THREAD_BLOCK_SIZE + 1) * THREAD_BLOCK_SIZE ;
	size_t local_size = THREAD_BLOCK_SIZE; ;

	
    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	clStatus = clEnqueueNDRangeKernel(command_queue, krnl, 1, NULL, &global_size, &local_size, 0, NULL, NULL);
	
	// Check for any errors launching the kernel
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Decryption kernel launch failed!");
		return clStatus;
	}

	clFlush(command_queue);
	clFinish(command_queue);
	totalTime += endStopwatch("Decryption kernel", start);

	hostPinnedPt = (cl_uint4 *)clEnqueueMapBuffer(command_queue, pinnedPt, CL_TRUE, CL_MAP_READ, 0, blocks * sizeof(cl_uint4), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}
	memcpy(pt, hostPinnedPt, blocks * sizeof(cl_uint4));
	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedPt, hostPinnedPt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);

    

	// Finally release all OpenCL allocated objects and host buffers.
	start = startStopwatch();
	clStatus = clReleaseMemObject(devRk);
	clStatus = clReleaseMemObject(devS);

	clStatus = clReleaseCommandQueue(command_queue);

	endStopwatch("clReleaseMemObject (decryption)", start);

	clStatus = clReleaseMemObject(pinnedCt);
	clStatus = clReleaseMemObject(pinnedPt);

	printDuration("Decryption", totalTime);

	return clStatus;
}


int makeKey(const u32 cipherKey[], u32 rk[RK_SIZE], const unsigned int keyLen){

	u32  i, j, m, t1, t2, *kp;
	u32 t_key[TMP_TABLE_SIZE];


	m = keyLen / BYTES_PER_WORD;

	for (i = 0; i < m; ++i){

		t_key[i] = cipherKey[i];

	}

	t_key[i++] = m;

	for (; i < TMP_TABLE_SIZE; i++){

		t_key[i] = 0;

	}

	kp = rk;

	#define tk1(j)  t1 = t_key[j] ^= ROL(t1 ^ t_key[(j + 8) % 15], 3) ^ (i + 4 * j)
	#define tk2(j)  t2 = t_key[j] ^= ROL(t2 ^ t_key[(j + 8) % 15], 3) ^ (i + 4 * j)
	#define tk3(j)  t_key[j] = t1 =  ROL(t_key[j] + S[t1 & 511], 9)

	for (i = 0; i < 4; ++i)
	{
		t1 = t_key[13]; t2 = t_key[14];

		tk1(0); tk2(1); tk1(2); tk2(3); tk1(4); tk2(5); tk1(6); tk2(7);
		tk1(8); tk2(9); tk1(10); tk2(11); tk1(12); tk2(13); tk1(14);

		tk3(0); tk3(1); tk3(2); tk3(3); tk3(4); tk3(5); tk3(6); tk3(7);
		tk3(8); tk3(9); tk3(10); tk3(11); tk3(12); tk3(13); tk3(14);

		tk3(0); tk3(1); tk3(2); tk3(3); tk3(4); tk3(5); tk3(6); tk3(7);
		tk3(8); tk3(9); tk3(10); tk3(11); tk3(12); tk3(13); tk3(14);

		tk3(0); tk3(1); tk3(2); tk3(3); tk3(4); tk3(5); tk3(6); tk3(7);
		tk3(8); tk3(9); tk3(10); tk3(11); tk3(12); tk3(13); tk3(14);

		tk3(0); tk3(1); tk3(2); tk3(3); tk3(4); tk3(5); tk3(6); tk3(7);
		tk3(8); tk3(9); tk3(10); tk3(11); tk3(12); tk3(13); tk3(14);

		*kp++ = t_key[0]; *kp++ = t_key[4]; *kp++ = t_key[8]; *kp++ = t_key[12];
		*kp++ = t_key[1]; *kp++ = t_key[5]; *kp++ = t_key[9]; *kp++ = t_key[13];
		*kp++ = t_key[2]; *kp++ = t_key[6];
	}

	for (i = 5; i < 37; i += 2)
	{
		j = rk[i] | 3;

		m = (~j ^ (j << 1)) & (~j ^ (j >> 1)) & 0x7ffffffe;

		m &= m >> 1; m &= m >> 2; m &= m >> 4;
		m |= m << 1; m |= m << 2; m |= m << 4;


		m &= 0x7ffffffc;

		j ^= (ROL(B[rk[i] & 3], rk[i - 1]) & m);


		rk[i] = j;
	}

	return 0;
}


int marsEncrypt(cl_uint4 *pt, cl_uint4 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
{

	boost::chrono::high_resolution_clock::time_point start;

	
	InitValues *initValues;
	cl_program program;
	cl_kernel krnl;
	cl_int clStatus;

	u32 rk[RK_SIZE];;

	start = startStopwatch();
	initValues = openClInit();
	endStopwatch("OpenCl initialization (encryption)", start);


	start = startStopwatch();
	program = getProgram(initValues, encryptKernelSource, encryptKernelBin, options, BUILD_SOURCE);

	if (program == NULL){
		printf("Error while creating program. \n");
		return -1;

	}
	endStopwatch("Program building (encryption)", start);

	start = startStopwatch();
	// Create the OpenCL kernel
	krnl = clCreateKernel(program, "encryptKernel", &clStatus);

	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Kernel creating failed!");
		return clStatus;
	}

	endStopwatch("Kernel creation (encryption) ", start);

	start = startStopwatch();
	makeKey((u32*)cipherKey, rk, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	clStatus = marsEncryptOpenCl(pt, ct, rk, blocks, initValues, krnl);

	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Encryption failed!");
		return 1;
	}

	start = startStopwatch();
	clStatus = clReleaseKernel(krnl);
	clStatus = clReleaseProgram(program);
	openClDeInit(initValues);

	endStopwatch("Freeing OpenCL resources (encryption)", start);

	return 0;




}
int marsDecrypt(cl_uint4 *ct, cl_uint4 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
{

	boost::chrono::high_resolution_clock::time_point start;

	
	InitValues *initValues;
	cl_program program;
	cl_kernel krnl;
	cl_int clStatus;

	u32 rk[RK_SIZE];

	start = startStopwatch();
	initValues = openClInit();
	endStopwatch("OpenCl initialization (decryption)", start);


	start = startStopwatch();
	program = getProgram(initValues, encryptKernelSource, encryptKernelBin, options, BUILD_SOURCE);

	if (program == NULL){
		printf("Error while creating program. \n");
		return -1;

	}
	endStopwatch("Program building (decryption)", start);

	start = startStopwatch();
	// Create the OpenCL kernel
	krnl = clCreateKernel(program, "encryptKernel", &clStatus);

	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Kernel creating failed!\n ");
		return clStatus;
	}

	endStopwatch("Kernel creation (decryption) ", start);

	start = startStopwatch();
	makeKey((u32*)cipherKey, rk, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	clStatus = marsDecryptOpenCl(ct, pt, rk, blocks, initValues, krnl);
	


	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Decryption failed!");
		return 1;
	}

	start = startStopwatch();
	clStatus = clReleaseKernel(krnl);
	clStatus = clReleaseProgram(program);
	openClDeInit(initValues);

	endStopwatch("Freeing OpenCL resources (decryption)", start);

	return 0;

}