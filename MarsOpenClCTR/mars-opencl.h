#ifndef __MARS_OPEN_CL_H
#define __MARS_OPEN_CL_H
#include "common-types.h"
#include <CL/cl.h>

#define	ROL(x,n) (((x) << ((n) & 0x1F)) | ((x) >> (32-((n) & 0x1F))))
#define	ROR(x,n) (((x) >> ((n) & 0x1F)) | ((x) << (32-((n) & 0x1F))))


#define RK_SIZE 40

#define S_TABLE_SIZE 256
#define BIG_S_TABLE_SIZE (2* S_TABLE_SIZE)
#define TMP_TABLE_SIZE 15

#define THREAD_BLOCK_SIZE 256

int marsEncrypt(cl_uint4 *pt, cl_uint4 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);
int marsDecrypt(cl_uint4 *ct, cl_uint4 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);


#endif /* __MARS_CUDA_H */