#include "common-types.h"

#define	ROL(x,n) (((x) << ((n) & 0x1F)) | ((x) >> (32-((n) & 0x1F))))
#define	ROR(x,n) (((x) >> ((n) & 0x1F)) | ((x) << (32-((n) & 0x1F))))

#define f_mix(a,b,c,d)                  \
	r = ROR(a, 8);                 \
	b ^= s_sh[a & 0xff];            \
	b += s_sh[(r & 0xff) + 256];    \
	r = ROR(a, 16);                \
	a = ROR(a, 24);               \
	c += s_sh[r & 0xff];            \
	d ^= s_sh[(a & 0xff) + 256]

#define b_mix(a,b,c,d)                  \
	r = ROL(a, 8);                 \
	b ^= s_sh[(a & 0xff) + 256];    \
	c -= s_sh[r & 0xff];            \
	r = ROL(a, 16);                \
	a = ROL(a, 24);               \
	d -= s_sh[(r & 0xff) + 256];    \
	d ^= s_sh[a & 0xff]

#define f_ktr(a,b,c,d,i)    \
	m = a + rk[i];       \
	a = ROL(a, 13);        \
	r = a * rk[i + 1];   \
	l = s_sh[m & 0x1ff];     \
	r = ROL(r, 5);         \
	c += ROL(m, r);        \
	l ^= r;                 \
	r = ROL(r, 5);         \
	l ^= r;                 \
	d ^= r;                 \
	b += ROL(l, r)

#define S_TABLE_SIZE 256
#define BIG_S_TABLE_SIZE (2* S_TABLE_SIZE)

__kernel void encryptKernel(__global uint4 *pt, __global uint4 *ct, __constant u32 *rk, __global u32 *S, const int blocks, const u32 n0, const u32 n1) 
{																																	  
																																	  
	int t = get_local_id(0);
	u64 i = get_global_id(0);																										  
	
	__local u32 s_sh[BIG_S_TABLE_SIZE];



	if (t < S_TABLE_SIZE){

		s_sh[t] = S[t];
		s_sh[t + S_TABLE_SIZE] = S[t + S_TABLE_SIZE];

	}

	barrier(CLK_LOCAL_MEM_FENCE);
	
																							  
																																	  
	if (i < blocks){



		register u32  a, b, c, d, l, m, r;

		a = n0 + rk[0];
		b = n1 + rk[1];
		c = ((i & 0xffffffff00000000) >> 32) + rk[2];
		d = (i & 0xffffffff) + rk[3];

		f_mix(a, b, c, d); a += d;
		f_mix(b, c, d, a); b += c;
		f_mix(c, d, a, b);
		f_mix(d, a, b, c);
		f_mix(a, b, c, d); a += d;
		f_mix(b, c, d, a); b += c;
		f_mix(c, d, a, b);
		f_mix(d, a, b, c);

		f_ktr(a, b, c, d, 4); f_ktr(b, c, d, a, 6); f_ktr(c, d, a, b, 8); f_ktr(d, a, b, c, 10);
		f_ktr(a, b, c, d, 12); f_ktr(b, c, d, a, 14); f_ktr(c, d, a, b, 16); f_ktr(d, a, b, c, 18);
		f_ktr(a, d, c, b, 20); f_ktr(b, a, d, c, 22); f_ktr(c, b, a, d, 24); f_ktr(d, c, b, a, 26);
		f_ktr(a, d, c, b, 28); f_ktr(b, a, d, c, 30); f_ktr(c, b, a, d, 32); f_ktr(d, c, b, a, 34);

		b_mix(a, b, c, d);
		b_mix(b, c, d, a); c -= b;
		b_mix(c, d, a, b); d -= a;
		b_mix(d, a, b, c);
		b_mix(a, b, c, d);
		b_mix(b, c, d, a); c -= b;
		b_mix(c, d, a, b); d -= a;
		b_mix(d, a, b, c);


		ct[i] =(uint4) { (a - rk[36]) ^ pt[i].x, (b - rk[37]) ^ pt[i].y, (c - rk[38]) ^ pt[i].z, (d - rk[39]) ^ pt[i].w };
	}
																																	  
																							  
																																	 																																	  
																																	  
}																																	  

