#ifndef __MARS_CPU_H
#define __MARS_CPU_H
#include "common-types.h"


int marsEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);
int marsDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);


#endif /* __MARS_CPU_H */