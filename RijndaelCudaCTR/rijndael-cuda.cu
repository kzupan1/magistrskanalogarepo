
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "rijndael-cuda.cuh"
#include "rijndael-tables.cuh"
#include <stdio.h>
#include <stdlib.h>
#include "measure-time.h"
#include <boost/chrono.hpp>

int rijndaelKeySetupEnc(u32 rk[], const u32 cipherKey[], const unsigned int keyLen) {
	int i = 0;
	u32 temp;

	rk[0] = cipherKey[0];
	rk[1] = cipherKey[1];
	rk[2] = cipherKey[2];
	rk[3] = cipherKey[3];

	if (keyLen == 16) {
		for (;;) {
			temp = rk[3];
			rk[4] = rk[0] ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];

			rk[5] = rk[1] ^ rk[4];
			rk[6] = rk[2] ^ rk[5];
			rk[7] = rk[3] ^ rk[6];
			if (++i == 10) {
				return 10;
			}
			rk += 4;
		}
	}
	rk[4] = cipherKey[4];
	rk[5] = cipherKey[5];

	if (keyLen == 24) {
		for (;;) {
			temp = rk[5];
			rk[6] = rk[0] ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[7] = rk[1] ^ rk[6];
			rk[8] = rk[2] ^ rk[7];
			rk[9] = rk[3] ^ rk[8];
			if (++i == 8) {
				return 12;
			}
			rk[10] = rk[4] ^ rk[9];
			rk[11] = rk[5] ^ rk[10];
			rk += 6;
		}
	}
	rk[6] = cipherKey[6];
	rk[7] = cipherKey[7];
	if (keyLen == 32) {
		for (;;) {
			temp = rk[7];
			rk[8] = rk[0] ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[9] = rk[1] ^ rk[8];
			rk[10] = rk[2] ^ rk[9];
			rk[11] = rk[3] ^ rk[10];
			if (++i == 7) {
				return 14;
			}
			temp = rk[11];
			rk[12] = rk[4] ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0x000000ff);
			rk[13] = rk[5] ^ rk[12];
			rk[14] = rk[6] ^ rk[13];
			rk[15] = rk[7] ^ rk[14];

			rk += 8;
		}
	}
	return 0;
}




__constant__ u32 rk_constant[rkLength];


__global__ void encryptKernel(uint4 *pt, uint4 *ct, const u32 * bigTe, const unsigned int blocks, const u32 n0, const u32 n1 ,const unsigned int Nr)
{


	int t = threadIdx.x;
	u64 i = blockDim.x * blockIdx.x + t;


	__shared__ u32 bigTe_sh[BIG_T_TABLE_SIZE];
	
	if (t < T_TABLE_SIZE){

		bigTe_sh[0 * T_TABLE_SIZE + t] = bigTe[0 * T_TABLE_SIZE + t];
		bigTe_sh[1 * T_TABLE_SIZE + t] = bigTe[1 * T_TABLE_SIZE + t];
		bigTe_sh[2 * T_TABLE_SIZE + t] = bigTe[2 * T_TABLE_SIZE + t];
		bigTe_sh[3 * T_TABLE_SIZE + t] = bigTe[3 * T_TABLE_SIZE + t];
		bigTe_sh[4 * T_TABLE_SIZE + t] = bigTe[4 * T_TABLE_SIZE + t];

	}

	
	__syncthreads();



	if (i < blocks){

		register u32 s0, s1, s2, s3, t0, t1, t2, t3;


		s0 = n0 ^ rk_constant[0];
		s1 = n1 ^ rk_constant[1];
		s2 = ((i & 0xffffffff00000000) >> 32) ^ rk_constant[2];
		s3 = (i & 0xffffffff) ^ rk_constant[3];

		
		/* round 1: */
		t0 = bigTe_sh[0 * T_TABLE_SIZE + (s0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s3 >> 24)] ^ rk_constant[4];
		t1 = bigTe_sh[0 * T_TABLE_SIZE + (s1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s0 >> 24)] ^ rk_constant[5];
		t2 = bigTe_sh[0 * T_TABLE_SIZE + (s2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s1 >> 24)] ^ rk_constant[6];
		t3 = bigTe_sh[0 * T_TABLE_SIZE + (s3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s2 >> 24)] ^ rk_constant[7];
		/* round 2: */
		s0 = bigTe_sh[0 * T_TABLE_SIZE + (t0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t3 >> 24)] ^ rk_constant[8];
		s1 = bigTe_sh[0 * T_TABLE_SIZE + (t1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t0 >> 24)] ^ rk_constant[9];
		s2 = bigTe_sh[0 * T_TABLE_SIZE + (t2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t1 >> 24)] ^ rk_constant[10];
		s3 = bigTe_sh[0 * T_TABLE_SIZE + (t3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t2 >> 24)] ^ rk_constant[11];
		/* round 3: */
		t0 = bigTe_sh[0 * T_TABLE_SIZE + (s0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s3 >> 24)] ^ rk_constant[12];
		t1 = bigTe_sh[0 * T_TABLE_SIZE + (s1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s0 >> 24)] ^ rk_constant[13];
		t2 = bigTe_sh[0 * T_TABLE_SIZE + (s2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s1 >> 24)] ^ rk_constant[14];
		t3 = bigTe_sh[0 * T_TABLE_SIZE + (s3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s2 >> 24)] ^ rk_constant[15];
		/* round 4: */
		s0 = bigTe_sh[0 * T_TABLE_SIZE + (t0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t3 >> 24)] ^ rk_constant[16];
		s1 = bigTe_sh[0 * T_TABLE_SIZE + (t1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t0 >> 24)] ^ rk_constant[17];
		s2 = bigTe_sh[0 * T_TABLE_SIZE + (t2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t1 >> 24)] ^ rk_constant[18];
		s3 = bigTe_sh[0 * T_TABLE_SIZE + (t3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t2 >> 24)] ^ rk_constant[19];
		/* round 5: */
		t0 = bigTe_sh[0 * T_TABLE_SIZE + (s0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s3 >> 24)] ^ rk_constant[20];
		t1 = bigTe_sh[0 * T_TABLE_SIZE + (s1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s0 >> 24)] ^ rk_constant[21];
		t2 = bigTe_sh[0 * T_TABLE_SIZE + (s2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s1 >> 24)] ^ rk_constant[22];
		t3 = bigTe_sh[0 * T_TABLE_SIZE + (s3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s2 >> 24)] ^ rk_constant[23];
		/* round 6: */
		s0 = bigTe_sh[0 * T_TABLE_SIZE + (t0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t3 >> 24)] ^ rk_constant[24];
		s1 = bigTe_sh[0 * T_TABLE_SIZE + (t1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t0 >> 24)] ^ rk_constant[25];
		s2 = bigTe_sh[0 * T_TABLE_SIZE + (t2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t1 >> 24)] ^ rk_constant[26];
		s3 = bigTe_sh[0 * T_TABLE_SIZE + (t3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t2 >> 24)] ^ rk_constant[27];
		/* round 7: */
		t0 = bigTe_sh[0 * T_TABLE_SIZE + (s0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s3 >> 24)] ^ rk_constant[28];
		t1 = bigTe_sh[0 * T_TABLE_SIZE + (s1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s0 >> 24)] ^ rk_constant[29];
		t2 = bigTe_sh[0 * T_TABLE_SIZE + (s2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s1 >> 24)] ^ rk_constant[30];
		t3 = bigTe_sh[0 * T_TABLE_SIZE + (s3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s2 >> 24)] ^ rk_constant[31];
		/* round 8: */
		s0 = bigTe_sh[0 * T_TABLE_SIZE + (t0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t3 >> 24)] ^ rk_constant[32];
		s1 = bigTe_sh[0 * T_TABLE_SIZE + (t1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t0 >> 24)] ^ rk_constant[33];
		s2 = bigTe_sh[0 * T_TABLE_SIZE + (t2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t1 >> 24)] ^ rk_constant[34];
		s3 = bigTe_sh[0 * T_TABLE_SIZE + (t3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t2 >> 24)] ^ rk_constant[35];
		/* round 9: */
		t0 = bigTe_sh[0 * T_TABLE_SIZE + (s0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s3 >> 24)] ^ rk_constant[36];
		t1 = bigTe_sh[0 * T_TABLE_SIZE + (s1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s0 >> 24)] ^ rk_constant[37];
		t2 = bigTe_sh[0 * T_TABLE_SIZE + (s2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s1 >> 24)] ^ rk_constant[38];
		t3 = bigTe_sh[0 * T_TABLE_SIZE + (s3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s2 >> 24)] ^ rk_constant[39];

		if (Nr > 10) {
			/* round 10: */
			s0 = bigTe_sh[0 * T_TABLE_SIZE + (t0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t3 >> 24)] ^ rk_constant[40];
			s1 = bigTe_sh[0 * T_TABLE_SIZE + (t1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t0 >> 24)] ^ rk_constant[41];
			s2 = bigTe_sh[0 * T_TABLE_SIZE + (t2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t1 >> 24)] ^ rk_constant[42];
			s3 = bigTe_sh[0 * T_TABLE_SIZE + (t3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t2 >> 24)] ^ rk_constant[43];
			/* round 11: */
			t0 = bigTe_sh[0 * T_TABLE_SIZE + (s0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s3 >> 24)] ^ rk_constant[44];
			t1 = bigTe_sh[0 * T_TABLE_SIZE + (s1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s0 >> 24)] ^ rk_constant[45];
			t2 = bigTe_sh[0 * T_TABLE_SIZE + (s2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s1 >> 24)] ^ rk_constant[46];
			t3 = bigTe_sh[0 * T_TABLE_SIZE + (s3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s2 >> 24)] ^ rk_constant[47];
			if (Nr > 12) {
				/* round 12: */
				s0 = bigTe_sh[0 * T_TABLE_SIZE + (t0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t3 >> 24)] ^ rk_constant[48];
				s1 = bigTe_sh[0 * T_TABLE_SIZE + (t1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t0 >> 24)] ^ rk_constant[49];
				s2 = bigTe_sh[0 * T_TABLE_SIZE + (t2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t1 >> 24)] ^ rk_constant[50];
				s3 = bigTe_sh[0 * T_TABLE_SIZE + (t3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t2 >> 24)] ^ rk_constant[51];
				/* round 13: */
				t0 = bigTe_sh[0 * T_TABLE_SIZE + (s0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s3 >> 24)] ^ rk_constant[52];
				t1 = bigTe_sh[0 * T_TABLE_SIZE + (s1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s0 >> 24)] ^ rk_constant[53];
				t2 = bigTe_sh[0 * T_TABLE_SIZE + (s2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s1 >> 24)] ^ rk_constant[54];
				t3 = bigTe_sh[0 * T_TABLE_SIZE + (s3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s2 >> 24)] ^ rk_constant[55];
			}
		}

		u32 x = Nr << 2;

		s0 =
			(bigTe_sh[4 * T_TABLE_SIZE + (t0 & 0xff)] & 0x000000ff) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t1 >> 8) & 0xff)] & 0x0000ff00) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t2 >> 16) & 0xff)] & 0x00ff0000) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t3) >> 24)] & 0xff000000) ^
			rk_constant[x++];

		s1 =
			(bigTe_sh[4 * T_TABLE_SIZE + (t1 & 0xff)] & 0x000000ff) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t2 >> 8) & 0xff)] & 0x0000ff00) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t3 >> 16) & 0xff)] & 0x00ff0000) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t0) >> 24)] & 0xff000000) ^
			rk_constant[x++];

		s2 =
			(bigTe_sh[4 * T_TABLE_SIZE + (t2 & 0xff)] & 0x000000ff) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t3 >> 8) & 0xff)] & 0x0000ff00) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t0 >> 16) & 0xff)] & 0x00ff0000) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t1) >> 24)] & 0xff000000) ^
			rk_constant[x++];

		s3 =
			(bigTe_sh[4 * T_TABLE_SIZE + (t3  & 0xff)] & 0x000000ff) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t0 >> 8) & 0xff)] & 0x0000ff00) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t1 >> 16) & 0xff)] & 0x00ff0000) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t2) >> 24)] & 0xff000000) ^
			rk_constant[x];

		


		ct[i] = { pt[i].x ^ s0, pt[i].y ^ s1, pt[i].z ^ s2, pt[i].w ^ s3 };
		


			
	}




}


cudaError_t rijndaelEncryptCuda(const uint4  pt[], uint4 ct[], const u32 rk[], const int blocks, const unsigned int Nr)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

    uint4 *devPt;
    uint4 *devCt;

	u32 *devBigTe;

	srand(0);
	u32 n0 = rand();
	u32 n1 = rand();
	ct[0] = { n0, n1, 0, 0 };


    cudaError_t cudaStatus;

    // Choose which GPU to run on, change this on a multi-GPU system.
	
	cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        return cudaStatus;
    }

	

    // cuda malloc
	start = startStopwatch();

	cudaHostGetDevicePointer((void **)&devPt, (void *)pt, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	cudaHostGetDevicePointer((void **)&devCt, (void *)ct, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}

	


	cudaStatus = cudaMalloc((void**)&devBigTe, BIG_T_TABLE_SIZE * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        return cudaStatus;
    }

	
	endStopwatch("Malloc & GetDevicePointer (encryption)", start);
    
	devCt++;

	start = startStopwatch();

	cudaStatus = cudaMemcpyToSymbol(rk_constant, rk, rkLength * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }
	
	cudaStatus = cudaMemcpy(devBigTe, bigTe, BIG_T_TABLE_SIZE * sizeof(u32), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }

	totalTime = endStopwatch("Memory copy from host to device (encryption)", start);
    
	//!!!must be padded before!!!

	dim3 dimGrid((blocks - 1 )/THREAD_BLOCK_SIZE + 1, 1, 1);
	dim3 dimBlock(THREAD_BLOCK_SIZE, 1, 1);
	

    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	encryptKernel <<<dimGrid, dimBlock >>>(devPt, devCt, devBigTe, blocks, n0, n1, Nr);
	
	
    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        return cudaStatus;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
        return cudaStatus;
    }

	totalTime += endStopwatch("Encryption kernel", start);
	


	start = startStopwatch();

	cudaFree(devBigTe);
	endStopwatch("Freeing memory (encryption)", start);

	printDuration("Encryption", totalTime);

    return cudaStatus;
}


cudaError_t rijndaelDecryptCuda(const uint4 ct[], uint4 pt[], const u32 rk[], const int blocks, const unsigned int Nr)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	uint4 *devCt;
    uint4 *devPt;

	u32 *devBigTe;

    cudaError_t cudaStatus;

	

	u32 n0 = ct[0].x;
	u32 n1 = ct[0].y;


    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        return cudaStatus;
    }
	

    // cuda malloc
	start = startStopwatch();
	cudaHostGetDevicePointer((void **)&devCt, (void *)ct, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	cudaHostGetDevicePointer((void **)&devPt, (void *)pt, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	
	


	cudaStatus = cudaMalloc((void**)&devBigTe, BIG_T_TABLE_SIZE * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        return cudaStatus;
    }

	endStopwatch("Malloc & GetDevicePointer (decryption)", start);
    
	devCt++;

	start = startStopwatch();
	
	cudaStatus = cudaMemcpyToSymbol(rk_constant, rk, rkLength * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }

	cudaStatus = cudaMemcpy(devBigTe, bigTe, BIG_T_TABLE_SIZE * sizeof(u32), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }

	totalTime = endStopwatch("Memory copy from host to device (decryption)", start);
    
	//!!!must be padded before!!!

	dim3 dimGrid((blocks - 1)/THREAD_BLOCK_SIZE + 1, 1, 1);
	dim3 dimBlock(THREAD_BLOCK_SIZE, 1, 1);
	
    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	encryptKernel <<<dimGrid, dimBlock >> >(devCt, devPt, devBigTe, blocks, n0, n1, Nr);
	
	
    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        return cudaStatus;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
        return cudaStatus;
    }
	totalTime += endStopwatch("Decryption kernel", start);
	

	start = startStopwatch();


	cudaFree(devBigTe);

	endStopwatch("Freeing memory (decryption)", start);
	
	printDuration("Decryption", totalTime);

    return cudaStatus;
}


int rijndaelEncrypt(uint4 *pt, uint4 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
{

	boost::chrono::high_resolution_clock::time_point start;
	u32 rk[rkLength];
	
	cudaSetDeviceFlags(cudaDeviceMapHost);
	cudaFree(NULL);


	uint4 *pinnedPt;
	uint4 *pinnedCt;
	
	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	cudaHostAlloc((void **)&pinnedPt, blocks * sizeof(uint4), cudaHostAllocWriteCombined | cudaHostAllocMapped);
	cudaHostAlloc((void **)&pinnedCt, (blocks + 1) * sizeof(uint4), cudaHostAllocDefault | cudaHostAllocMapped);

	memcpy(pinnedPt, pt, blocks * sizeof(uint4));

	start = startStopwatch();
	unsigned int Nr = rijndaelKeySetupEnc(rk, (u32 *)cipherKey, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);
	
	
    cudaError_t cudaStatus = rijndaelEncryptCuda(pinnedPt, pinnedCt, rk, blocks, Nr);
	
	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Encryption failed!");
        return 1;
    }

	memcpy(ct, pinnedCt, (blocks + 1) * sizeof(uint4));
	cudaFreeHost(pinnedPt);
	cudaFreeHost(pinnedCt);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }



    return 0;
}

int rijndaelDecrypt(uint4 *ct, uint4 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
{

	boost::chrono::high_resolution_clock::time_point start;

	u32 rk[rkLength];
	cudaSetDeviceFlags(cudaDeviceMapHost);
	cudaFree(NULL);

	uint4 *pinnedCt;
	uint4 *pinnedPt;
	
	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	cudaHostAlloc((void **)&pinnedCt, (blocks + 1) * sizeof(uint4), cudaHostAllocWriteCombined | cudaHostAllocMapped);
	cudaHostAlloc((void **)&pinnedPt, blocks * sizeof(uint4), cudaHostAllocDefault | cudaHostAllocMapped);
	
	memcpy(pinnedCt, ct, (blocks + 1) * sizeof(uint4));

	start = startStopwatch();
	unsigned int Nr = rijndaelKeySetupEnc(rk, (u32 *)cipherKey, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);
	

    cudaError_t cudaStatus = rijndaelDecryptCuda(pinnedCt, pinnedPt, rk, blocks, Nr);
	
	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Decription failed!");
        return 1;
    }


	memcpy(pt, pinnedPt, blocks * sizeof(uint4));
	cudaFreeHost(pinnedCt);
	cudaFreeHost(pinnedPt);
	
	
    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }


    return 0;
}


