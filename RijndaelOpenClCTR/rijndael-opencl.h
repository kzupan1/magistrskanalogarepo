#ifndef __RIJNDAEL_OPEN_CL_H
#define __RIJNDAEL_OPEN_CL_H
#include "common-types.h"
#include <CL\cl.h>

#define T_TABLE_SIZE 256
#define BIG_T_TABLE_SIZE (5 * T_TABLE_SIZE)

#define maxNr 14
#define rkLength 4 * (maxNr + 1)



int rijndaelEncrypt(cl_uint4 *pt, cl_uint4 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);
int rijndaelDecrypt(cl_uint4 *ct, cl_uint4 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);

#endif /* __RIJNDAEL_OPEN_CL_H */