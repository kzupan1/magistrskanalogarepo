#include "rijndael-opencl.h"
#include "rijndael-tables.h"
#include "measure-time.h"
#include "opencl-helpers.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <CL/cl.h>
#include <boost/chrono.hpp>

#define BUILD_SOURCE 0

const char *options = "-I ./common-types.h";

const char *encryptKernelSource = "rijndaelEncryptKernel.cl";
const char *encryptKernelBin = "rijndaelEncryptKernel.clbin";


cl_int rijndaelEncryptOpenCl(const cl_uint4 pt[], cl_uint4 ct[], const u32 rk[], const int blocks, const unsigned int Nr, InitValues *initValues, cl_kernel krnl)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;


	cl_mem devRk;

	cl_mem pinnedPt;
	cl_mem pinnedCt;

	cl_uint4* hostPinnedPt;
	cl_uint4* hostPinnedCt;

	cl_mem devBigTe;
	
    cl_int clStatus;

	srand(0);
	u32 n0 = rand();
	u32 n1 = rand();
	ct[0].s[0] = n0;
	ct[0].s[1] = n1;
	ct[0].s[2] = 0;
	ct[0].s[3] = 0;

	ct++;

	// Create a command queue
	cl_command_queue command_queue = clCreateCommandQueue(initValues->context, initValues->device_list[0], 0, &clStatus);
	start = startStopwatch();
	pinnedPt = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, blocks * sizeof(cl_uint4), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedPt failed!");
		return clStatus;
	}

	pinnedCt = clCreateBuffer(initValues->context, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, blocks *sizeof(cl_uint4), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedCt failed!");
		return clStatus;
	}

	hostPinnedPt = (cl_uint4 *)clEnqueueMapBuffer(command_queue, pinnedPt, CL_TRUE, CL_MAP_WRITE, 0, blocks * sizeof(cl_uint4), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}
	
	memcpy(hostPinnedPt, pt, blocks * sizeof(cl_uint4));


	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedPt, hostPinnedPt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);


	devRk = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY, rkLength * sizeof(u32), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clCreateBuffer rk failed!");
        return clStatus;
    }

	devBigTe= clCreateBuffer(initValues->context, CL_MEM_READ_ONLY, BIG_T_TABLE_SIZE * sizeof(u32), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer Te0 failed!");
		return clStatus;
	}

	endStopwatch("clCreateBuffer (encryption) ", start);
    
	start = startStopwatch();

	clStatus = clEnqueueWriteBuffer(command_queue, devRk, CL_TRUE, 0, rkLength * sizeof(u32), rk, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clEnqueueWriteBuffer failed!");
        return clStatus;
    }
	
	clStatus = clEnqueueWriteBuffer(command_queue, devBigTe, CL_TRUE, 0, BIG_T_TABLE_SIZE * sizeof(u32), bigTe, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueWriteBuffer failed!");
		return clStatus;
	}

	
	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);
	
	totalTime = endStopwatch("clEnqueueWriteBuffer (encryption)", start);
    
	
	// Set the arguments of the kernel
	clStatus = clSetKernelArg(krnl, 0, sizeof(cl_mem), (void *)&pinnedPt);
	clStatus = clSetKernelArg(krnl, 1, sizeof(cl_mem), (void *)&pinnedCt);
	clStatus = clSetKernelArg(krnl, 2, sizeof(cl_mem), (void *)&devRk);
	clStatus = clSetKernelArg(krnl, 3, sizeof(cl_mem), (void *)&devBigTe);
	clStatus = clSetKernelArg(krnl, 4, sizeof(int), (void *)&blocks);
	clStatus = clSetKernelArg(krnl, 5, sizeof(u32), (void *)&n0);
	clStatus = clSetKernelArg(krnl, 6, sizeof(u32), (void *)&n1);
	clStatus = clSetKernelArg(krnl, 7, sizeof(unsigned int), (void *)&Nr);



	//!!!must be padded before!!!
	size_t global_size = ((blocks - 1)/THREAD_BLOCK_SIZE + 1) * THREAD_BLOCK_SIZE ;
	size_t local_size = THREAD_BLOCK_SIZE;
    
	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	clStatus = clEnqueueNDRangeKernel(command_queue, krnl, 1, NULL, &global_size, &local_size, 0, NULL, NULL);
	
	// Check for any errors launching the kernel
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "encryption kernel launch failed!");
		printf("%d\n", clStatus);
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);
	totalTime += endStopwatch("Encryption kernel", start);

        
	hostPinnedCt = (cl_uint4 *)clEnqueueMapBuffer(command_queue, pinnedCt, CL_TRUE, CL_MAP_READ, 0, blocks * sizeof(cl_uint4), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}
	memcpy(ct, hostPinnedCt, blocks * sizeof(cl_uint4));
	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedCt, hostPinnedCt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}
	
	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);

	// Finally release all OpenCL allocated objePts and host buffers.
	start = startStopwatch();
	
	clStatus = clReleaseMemObject(devBigTe);

	clStatus = clReleaseMemObject(devRk);
	
	clStatus = clReleaseCommandQueue(command_queue);
	
	endStopwatch("clReleaseMemObject (encryption)", start);

	clStatus = clReleaseMemObject(pinnedPt);
	clStatus = clReleaseMemObject(pinnedCt);

	printDuration("Encryption", totalTime);

	return clStatus;
}

cl_int rijndaelDecryptOpenCl(const cl_uint4 ct[], cl_uint4 pt[], const u32 rk[], const int blocks, const unsigned int Nr, InitValues *initValues, cl_kernel krnl)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	cl_mem devRk;

	cl_mem pinnedCt;
	cl_mem pinnedPt;
	
	cl_uint4* hostPinnedPt;
	cl_uint4* hostPinnedCt;

	cl_mem devBigTe;
    cl_int clStatus;
	
	u32 n0 = ct[0].s[0];
	u32 n1 = ct[0].s[1];

	ct++;


	// Create a command queue
	cl_command_queue command_queue = clCreateCommandQueue(initValues->context, initValues->device_list[0], 0, &clStatus);
	start = startStopwatch();
	pinnedCt = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, blocks * sizeof(cl_uint4), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedPt failed!");
		return clStatus;
	}

	pinnedPt = clCreateBuffer(initValues->context, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, blocks * sizeof(cl_uint4), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedCt failed!");
		return clStatus;
	}

	hostPinnedCt = (cl_uint4 *)clEnqueueMapBuffer(command_queue, pinnedCt, CL_TRUE, CL_MAP_WRITE, 0, blocks * sizeof(cl_uint4), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}

	memcpy(hostPinnedCt, ct, blocks * sizeof(cl_uint4));


	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedCt, hostPinnedCt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}
    
	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);


	devRk = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY, rkLength * sizeof(u32), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer failed!");
		return clStatus;
	}

	devBigTe= clCreateBuffer(initValues->context, CL_MEM_READ_ONLY, BIG_T_TABLE_SIZE * sizeof(u32), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer failed!");
		return clStatus;
	}


	endStopwatch("clCreateBuffer (decryption) ", start);
    
	start = startStopwatch();
	
	clStatus = clEnqueueWriteBuffer(command_queue, devRk, CL_TRUE, 0, rkLength * sizeof(u32), rk, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clEnqueueWriteBuffer failed!");
        return clStatus;
    }

	clStatus = clEnqueueWriteBuffer(command_queue, devBigTe, CL_TRUE, 0, BIG_T_TABLE_SIZE * sizeof(u32), bigTe, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueWriteBuffer failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);

	totalTime = endStopwatch("clEnqueueWriteBuffer (decryption) ", start);
    
	// Set the arguments of the kernel
	clStatus = clSetKernelArg(krnl, 0, sizeof(cl_mem), (void *)&pinnedCt);
	clStatus = clSetKernelArg(krnl, 1, sizeof(cl_mem), (void *)&pinnedPt);
	clStatus = clSetKernelArg(krnl, 2, sizeof(cl_mem), (void *)&devRk);
	clStatus = clSetKernelArg(krnl, 3, sizeof(cl_mem), (void *)&devBigTe);
	clStatus = clSetKernelArg(krnl, 4, sizeof(int), (void *)&blocks);
	clStatus = clSetKernelArg(krnl, 5, sizeof(u32), (void *)&n0);
	clStatus = clSetKernelArg(krnl, 6, sizeof(u32), (void *)&n1);
	clStatus = clSetKernelArg(krnl, 7, sizeof(unsigned int), (void *)&Nr);



	//!!!must be padded before!!!
	size_t global_size = ((blocks - 1)/THREAD_BLOCK_SIZE + 1) * THREAD_BLOCK_SIZE ;
	size_t local_size = THREAD_BLOCK_SIZE;
	
    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	clStatus = clEnqueueNDRangeKernel(command_queue, krnl, 1, NULL, &global_size, &local_size, 0, NULL, NULL);
	
	// Check for any errors launching the kernel
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Decryption kernel launch failed!");
		return clStatus;
	}

	clFlush(command_queue);
	clFinish(command_queue);

	totalTime +=endStopwatch("Decryption kernel", start);
    
	
    

	hostPinnedPt = (cl_uint4 *)clEnqueueMapBuffer(command_queue, pinnedPt, CL_TRUE, CL_MAP_READ, 0, blocks * sizeof(cl_uint4), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}
	memcpy(pt, hostPinnedPt, blocks * sizeof(cl_uint4));
	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedPt, hostPinnedPt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);


	// Finally release all OpenCL allocated objects and host buffers.
	start = startStopwatch();
	clStatus = clReleaseMemObject(devBigTe);

	clStatus = clReleaseMemObject(devRk);

	clStatus = clReleaseCommandQueue(command_queue);
	
	endStopwatch("clReleaseMemObject (decryption)", start);
	
	clStatus = clReleaseMemObject(pinnedCt);

	clStatus = clReleaseMemObject(pinnedPt);

	printDuration("Decryption", totalTime);

	return clStatus;
}


int rijndaelKeySetupEnc(u32 rk[], const u32 cipherKey[], const unsigned int keyLen) {
	int i = 0;
	u32 temp;

	rk[0] = cipherKey[0];
	rk[1] = cipherKey[1];
	rk[2] = cipherKey[2];
	rk[3] = cipherKey[3];

	if (keyLen == 16) {
		for (;;) {
			temp = rk[3];
			rk[4] = rk[0] ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];

			rk[5] = rk[1] ^ rk[4];
			rk[6] = rk[2] ^ rk[5];
			rk[7] = rk[3] ^ rk[6];
			if (++i == 10) {
				return 10;
			}
			rk += 4;
		}
	}
	rk[4] = cipherKey[4];
	rk[5] = cipherKey[5];

	if (keyLen == 24) {
		for (;;) {
			temp = rk[5];
			rk[6] = rk[0] ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[7] = rk[1] ^ rk[6];
			rk[8] = rk[2] ^ rk[7];
			rk[9] = rk[3] ^ rk[8];
			if (++i == 8) {
				return 12;
			}
			rk[10] = rk[4] ^ rk[9];
			rk[11] = rk[5] ^ rk[10];
			rk += 6;
		}
	}
	rk[6] = cipherKey[6];
	rk[7] = cipherKey[7];
	if (keyLen == 32) {
		for (;;) {
			temp = rk[7];
			rk[8] = rk[0] ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[9] = rk[1] ^ rk[8];
			rk[10] = rk[2] ^ rk[9];
			rk[11] = rk[3] ^ rk[10];
			if (++i == 7) {
				return 14;
			}
			temp = rk[11];
			rk[12] = rk[4] ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0x000000ff);
			rk[13] = rk[5] ^ rk[12];
			rk[14] = rk[6] ^ rk[13];
			rk[15] = rk[7] ^ rk[14];

			rk += 8;
		}
	}
	return 0;
}



int rijndaelEncrypt(cl_uint4 *pt, cl_uint4 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
{
	boost::chrono::high_resolution_clock::time_point start;

	InitValues *initValues;
	cl_program program;
	cl_kernel krnl;
	cl_int clStatus;

	u32 rk[rkLength];

	start = startStopwatch();
	initValues = openClInit();
	endStopwatch("OpenCl initialization (encryption)", start);


	start = startStopwatch();
	program = getProgram(initValues, encryptKernelSource, encryptKernelBin, options, BUILD_SOURCE);

	if (program == NULL){
		printf("Error while creating program. \n");
		return -1;

	}
	endStopwatch("Program building (encryption)", start);

	start = startStopwatch();
	// Create the OpenCL kernel
	krnl = clCreateKernel(program, "encryptKernel", &clStatus);

	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Kernel creating failed!");
		return clStatus;
	}

	endStopwatch("Kernel creation (encryption) ", start);

	start = startStopwatch();
	unsigned int Nr = rijndaelKeySetupEnc(rk, (u32 *)cipherKey, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	clStatus = rijndaelEncryptOpenCl(pt, ct, rk, blocks, Nr, initValues, krnl);
	

	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Encryption failed!");
		return 1;
	}

	start = startStopwatch();
	clStatus = clReleaseKernel(krnl);
	clStatus = clReleaseProgram(program);
	openClDeInit(initValues);

	endStopwatch("Freeing OpenCL resources (encryption)", start);

	return 0;
}

int rijndaelDecrypt(cl_uint4 *ct, cl_uint4 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
{

	boost::chrono::high_resolution_clock::time_point start;

	InitValues *initValues;
	cl_program program;
	cl_kernel krnl;
	cl_int clStatus;

	u32 rk[rkLength];

	start = startStopwatch();
	initValues = openClInit();
	endStopwatch("OpenCl initialization (decryption)", start);

	
	start = startStopwatch();
	program = getProgram(initValues, encryptKernelSource, encryptKernelBin, options, BUILD_SOURCE);

	if (program == NULL){
		printf("Error while creating program. \n");
		return -1;

	}
	endStopwatch("Program building (decryption)", start);
	
	start = startStopwatch();
	// Create the OpenCL kernel
	krnl = clCreateKernel(program, "encryptKernel", &clStatus);

	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Kernel creating failed!\n ");
		return clStatus;
	}

	endStopwatch("Kernel creation (decryption)", start);
	
	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	start = startStopwatch();
	unsigned int Nr = rijndaelKeySetupEnc(rk, (u32 *)cipherKey, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);

	
	clStatus = rijndaelDecryptOpenCl(ct, pt, rk, blocks, Nr, initValues, krnl);
	

	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Decription failed! \n");
		return 1;
	}

	
	start = startStopwatch();
	clStatus = clReleaseKernel(krnl);
	clStatus = clReleaseProgram(program);
	openClDeInit(initValues);
	endStopwatch("Freeing OpenCL resources (decryption)", start);


	return 0;
}