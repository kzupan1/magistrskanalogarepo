#include "common-types.h"

#define T_TABLE_SIZE 256
#define BIG_T_TABLE_SIZE (5 * T_TABLE_SIZE)

__kernel void encryptKernel(__global uint4 *pt, __global uint4 *ct, __constant u32 *rk, __global u32 * bigTe, const int blocks, const u32 n0, const u32 n1, const unsigned int Nr)
{																																	  
																																	  
	int t = get_local_id(0);
	u64 i = get_global_id(0);

	__local u32 bigTe_sh[BIG_T_TABLE_SIZE];

	if (t < T_TABLE_SIZE){

		bigTe_sh[0 * T_TABLE_SIZE + t] = bigTe[0 * T_TABLE_SIZE + t];
		bigTe_sh[1 * T_TABLE_SIZE + t] = bigTe[1 * T_TABLE_SIZE + t];
		bigTe_sh[2 * T_TABLE_SIZE + t] = bigTe[2 * T_TABLE_SIZE + t];
		bigTe_sh[3 * T_TABLE_SIZE + t] = bigTe[3 * T_TABLE_SIZE + t];
		bigTe_sh[4 * T_TABLE_SIZE + t] = bigTe[4 * T_TABLE_SIZE + t];
		

	}

	barrier(CLK_LOCAL_MEM_FENCE);

																																	  
	if (i < blocks){


		register u32 s0, s1, s2, s3, t0, t1, t2, t3;

		s0 = n0 ^ rk[0];
		s1 = n1 ^ rk[1];
		s2 = ((i & 0xffffffff00000000) >> 32) ^ rk[2];
		s3 = (i & 0xffffffff) ^ rk[3];

		/* round 1: */
		t0 = bigTe_sh[0 * T_TABLE_SIZE + (s0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s3 >> 24)] ^ rk[4];
		t1 = bigTe_sh[0 * T_TABLE_SIZE + (s1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s0 >> 24)] ^ rk[5];
		t2 = bigTe_sh[0 * T_TABLE_SIZE + (s2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s1 >> 24)] ^ rk[6];
		t3 = bigTe_sh[0 * T_TABLE_SIZE + (s3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s2 >> 24)] ^ rk[7];
		/* round 2: */
		s0 = bigTe_sh[0 * T_TABLE_SIZE + (t0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t3 >> 24)] ^ rk[8];
		s1 = bigTe_sh[0 * T_TABLE_SIZE + (t1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t0 >> 24)] ^ rk[9];
		s2 = bigTe_sh[0 * T_TABLE_SIZE + (t2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t1 >> 24)] ^ rk[10];
		s3 = bigTe_sh[0 * T_TABLE_SIZE + (t3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t2 >> 24)] ^ rk[11];
		/* round 3: */
		t0 = bigTe_sh[0 * T_TABLE_SIZE + (s0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s3 >> 24)] ^ rk[12];
		t1 = bigTe_sh[0 * T_TABLE_SIZE + (s1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s0 >> 24)] ^ rk[13];
		t2 = bigTe_sh[0 * T_TABLE_SIZE + (s2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s1 >> 24)] ^ rk[14];
		t3 = bigTe_sh[0 * T_TABLE_SIZE + (s3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s2 >> 24)] ^ rk[15];
		/* round 4: */
		s0 = bigTe_sh[0 * T_TABLE_SIZE + (t0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t3 >> 24)] ^ rk[16];
		s1 = bigTe_sh[0 * T_TABLE_SIZE + (t1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t0 >> 24)] ^ rk[17];
		s2 = bigTe_sh[0 * T_TABLE_SIZE + (t2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t1 >> 24)] ^ rk[18];
		s3 = bigTe_sh[0 * T_TABLE_SIZE + (t3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t2 >> 24)] ^ rk[19];
		/* round 5: */
		t0 = bigTe_sh[0 * T_TABLE_SIZE + (s0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s3 >> 24)] ^ rk[20];
		t1 = bigTe_sh[0 * T_TABLE_SIZE + (s1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s0 >> 24)] ^ rk[21];
		t2 = bigTe_sh[0 * T_TABLE_SIZE + (s2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s1 >> 24)] ^ rk[22];
		t3 = bigTe_sh[0 * T_TABLE_SIZE + (s3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s2 >> 24)] ^ rk[23];
		/* round 6: */
		s0 = bigTe_sh[0 * T_TABLE_SIZE + (t0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t3 >> 24)] ^ rk[24];
		s1 = bigTe_sh[0 * T_TABLE_SIZE + (t1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t0 >> 24)] ^ rk[25];
		s2 = bigTe_sh[0 * T_TABLE_SIZE + (t2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t1 >> 24)] ^ rk[26];
		s3 = bigTe_sh[0 * T_TABLE_SIZE + (t3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t2 >> 24)] ^ rk[27];
		/* round 7: */
		t0 = bigTe_sh[0 * T_TABLE_SIZE + (s0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s3 >> 24)] ^ rk[28];
		t1 = bigTe_sh[0 * T_TABLE_SIZE + (s1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s0 >> 24)] ^ rk[29];
		t2 = bigTe_sh[0 * T_TABLE_SIZE + (s2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s1 >> 24)] ^ rk[30];
		t3 = bigTe_sh[0 * T_TABLE_SIZE + (s3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s2 >> 24)] ^ rk[31];
		/* round 8: */
		s0 = bigTe_sh[0 * T_TABLE_SIZE + (t0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t3 >> 24)] ^ rk[32];
		s1 = bigTe_sh[0 * T_TABLE_SIZE + (t1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t0 >> 24)] ^ rk[33];
		s2 = bigTe_sh[0 * T_TABLE_SIZE + (t2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t1 >> 24)] ^ rk[34];
		s3 = bigTe_sh[0 * T_TABLE_SIZE + (t3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t2 >> 24)] ^ rk[35];
		/* round 9: */
		t0 = bigTe_sh[0 * T_TABLE_SIZE + (s0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s3 >> 24)] ^ rk[36];
		t1 = bigTe_sh[0 * T_TABLE_SIZE + (s1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s0 >> 24)] ^ rk[37];
		t2 = bigTe_sh[0 * T_TABLE_SIZE + (s2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s1 >> 24)] ^ rk[38];
		t3 = bigTe_sh[0 * T_TABLE_SIZE + (s3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s2 >> 24)] ^ rk[39];

		if (Nr > 10) {
			/* round 10: */
			s0 = bigTe_sh[0 * T_TABLE_SIZE + (t0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t3 >> 24)] ^ rk[40];
			s1 = bigTe_sh[0 * T_TABLE_SIZE + (t1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t0 >> 24)] ^ rk[41];
			s2 = bigTe_sh[0 * T_TABLE_SIZE + (t2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t1 >> 24)] ^ rk[42];
			s3 = bigTe_sh[0 * T_TABLE_SIZE + (t3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t2 >> 24)] ^ rk[43];
			/* round 11: */
			t0 = bigTe_sh[0 * T_TABLE_SIZE + (s0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s3 >> 24)] ^ rk[44];
			t1 = bigTe_sh[0 * T_TABLE_SIZE + (s1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s0 >> 24)] ^ rk[45];
			t2 = bigTe_sh[0 * T_TABLE_SIZE + (s2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s1 >> 24)] ^ rk[46];
			t3 = bigTe_sh[0 * T_TABLE_SIZE + (s3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s2 >> 24)] ^ rk[47];
			if (Nr > 12) {
				/* round 12: */
				s0 = bigTe_sh[0 * T_TABLE_SIZE + (t0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t3 >> 24)] ^ rk[48];
				s1 = bigTe_sh[0 * T_TABLE_SIZE + (t1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t0 >> 24)] ^ rk[49];
				s2 = bigTe_sh[0 * T_TABLE_SIZE + (t2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t1 >> 24)] ^ rk[50];
				s3 = bigTe_sh[0 * T_TABLE_SIZE + (t3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((t0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((t1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (t2 >> 24)] ^ rk[51];
				/* round 13: */
				t0 = bigTe_sh[0 * T_TABLE_SIZE + (s0 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s1 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s2 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s3 >> 24)] ^ rk[52];
				t1 = bigTe_sh[0 * T_TABLE_SIZE + (s1 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s2 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s3 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s0 >> 24)] ^ rk[53];
				t2 = bigTe_sh[0 * T_TABLE_SIZE + (s2 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s3 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s0 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s1 >> 24)] ^ rk[54];
				t3 = bigTe_sh[0 * T_TABLE_SIZE + (s3 & 0xff)] ^ bigTe_sh[1 * T_TABLE_SIZE + ((s0 >> 8) & 0xff)] ^ bigTe_sh[2 * T_TABLE_SIZE + ((s1 >> 16) & 0xff)] ^ bigTe_sh[3 * T_TABLE_SIZE + (s2 >> 24)] ^ rk[55];
			}
		}

		u32 x = Nr << 2;

		s0 =
			(bigTe_sh[4 * T_TABLE_SIZE + (t0 & 0xff)] & 0x000000ff) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t1 >> 8) & 0xff)] & 0x0000ff00) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t2 >> 16) & 0xff)] & 0x00ff0000) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t3) >> 24)] & 0xff000000) ^
			rk[x++];

		s1 =
			(bigTe_sh[4 * T_TABLE_SIZE + (t1 & 0xff)] & 0x000000ff) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t2 >> 8) & 0xff)] & 0x0000ff00) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t3 >> 16) & 0xff)] & 0x00ff0000) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t0) >> 24)] & 0xff000000) ^
			rk[x++];

		s2 =
			(bigTe_sh[4 * T_TABLE_SIZE + (t2 & 0xff)] & 0x000000ff) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t3 >> 8) & 0xff)] & 0x0000ff00) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t0 >> 16) & 0xff)] & 0x00ff0000) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t1) >> 24)] & 0xff000000) ^
			rk[x++];

		s3 =
			(bigTe_sh[4 * T_TABLE_SIZE + (t3 & 0xff)] & 0x000000ff) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t0 >> 8) & 0xff)] & 0x0000ff00) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t1 >> 16) & 0xff)] & 0x00ff0000) ^
			(bigTe_sh[4 * T_TABLE_SIZE + ((t2) >> 24)] & 0xff000000) ^
			rk[x];

		ct[i] = (uint4){ pt[i].x ^ s0, pt[i].y ^ s1, pt[i].z ^ s2, pt[i].w ^ s3 };


	}
																																	  
																							  
																																	 																																	  
																																	  
}																																	  

