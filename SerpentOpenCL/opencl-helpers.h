#include <CL/cl.h>

#ifndef __OPEN_CL_HELPERS_H
#define __OPEN_CL_HELPERS_H


typedef struct{
	cl_platform_id *platforms;
	cl_uint num_platforms;

	cl_device_id *device_list;
	cl_uint num_devices;

	cl_context context;

} InitValues;


cl_program getProgram(InitValues *initValues, const char *kernelSource, const char *kernelBin, const char *options, bool buildSource);


InitValues * openClInit();
int openClDeInit(InitValues *initValues);


#endif 