#include "serpent-opencl.h"
#include "serpent-tables.h"

__kernel void decryptKernel(__global uint4 *ct, __global uint4 *pt, __constant u32 *k, const unsigned int blocks)	
{																																		
																																																																
	int i = get_global_id(0);																											
																																		
																																																								
																																		
	if(i < blocks){																								
																																		
																											
																																																																			
		register u32 r0, r1, r2, r3, r4;																								
		
		uint4 block_in = ct[i];
		uint4 block_out;

		r0 = block_in.x;
		r1 = block_in.y;
		r2 = block_in.z;
		r3 = block_in.w;
																																		
		K(r0,r1,r2,r3,32);																												
		SI7(r0,r1,r2,r3,r4);	KL(r1,r3,r0,r4,r2,31);																					
		SI6(r1,r3,r0,r4,r2);	KL(r0,r2,r4,r1,r3,30);																					
		SI5(r0,r2,r4,r1,r3);	KL(r2,r3,r0,r4,r1,29);																					
		SI4(r2,r3,r0,r4,r1);	KL(r2,r0,r1,r4,r3,28);																					
		SI3(r2,r0,r1,r4,r3);	KL(r1,r2,r3,r4,r0,27);																					
		SI2(r1,r2,r3,r4,r0);	KL(r2,r0,r4,r3,r1,26);																					
		SI1(r2,r0,r4,r3,r1);	KL(r1,r0,r4,r3,r2,25);																					
		SI0(r1,r0,r4,r3,r2);	KL(r4,r2,r0,r1,r3,24);																					
		SI7(r4,r2,r0,r1,r3);	KL(r2,r1,r4,r3,r0,23);																					
		SI6(r2,r1,r4,r3,r0);	KL(r4,r0,r3,r2,r1,22);																					
		SI5(r4,r0,r3,r2,r1);	KL(r0,r1,r4,r3,r2,21);																					
		SI4(r0,r1,r4,r3,r2);	KL(r0,r4,r2,r3,r1,20);																					
		SI3(r0,r4,r2,r3,r1);	KL(r2,r0,r1,r3,r4,19);																					
		SI2(r2,r0,r1,r3,r4);	KL(r0,r4,r3,r1,r2,18);																					
		SI1(r0,r4,r3,r1,r2);	KL(r2,r4,r3,r1,r0,17);																					
		SI0(r2,r4,r3,r1,r0);	KL(r3,r0,r4,r2,r1,16);																					
		SI7(r3,r0,r4,r2,r1);	KL(r0,r2,r3,r1,r4,15);																					
		SI6(r0,r2,r3,r1,r4);	KL(r3,r4,r1,r0,r2,14);																					
		SI5(r3,r4,r1,r0,r2);	KL(r4,r2,r3,r1,r0,13);																					
		SI4(r4,r2,r3,r1,r0);	KL(r4,r3,r0,r1,r2,12);																					
		SI3(r4,r3,r0,r1,r2);	KL(r0,r4,r2,r1,r3,11);																					
		SI2(r0,r4,r2,r1,r3);	KL(r4,r3,r1,r2,r0,10);																					
		SI1(r4,r3,r1,r2,r0);	KL(r0,r3,r1,r2,r4,9);																					
		SI0(r0,r3,r1,r2,r4);	KL(r1,r4,r3,r0,r2,8);																					
		SI7(r1,r4,r3,r0,r2);	KL(r4,r0,r1,r2,r3,7);																					
		SI6(r4,r0,r1,r2,r3);	KL(r1,r3,r2,r4,r0,6);																					
		SI5(r1,r3,r2,r4,r0);	KL(r3,r0,r1,r2,r4,5);																					
		SI4(r3,r0,r1,r2,r4);	KL(r3,r1,r4,r2,r0,4);																					
		SI3(r3,r1,r4,r2,r0);	KL(r4,r3,r0,r2,r1,3);																					
		SI2(r4,r3,r0,r2,r1);	KL(r3,r1,r2,r0,r4,2);																					
		SI1(r3,r1,r2,r0,r4);	KL(r4,r1,r2,r0,r3,1);																					
		SI0(r4,r1,r2,r0,r3);	K(r2,r3,r1,r4,0);																						
																																		
		block_out.x = r2;
		block_out.y = r3;
		block_out.z = r1;
		block_out.w = r4;
				
		pt[i] = block_out;
	}																																	
																																																									
																																		
																																		
																																		
}																																		
