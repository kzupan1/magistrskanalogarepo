#include "serpent-opencl.h"
#include "serpent-tables.h"
#include "measure-time.h"
#include "opencl-helpers.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <CL/cl.h>
#include <boost/chrono.hpp>

#define BUILD_SOURCE 0

const char *options = "-I ./serpent-opencl.h -I ./serpent-tables.h ";

const char *encryptKernelSource = "serpentEncryptKernel.cl";
const char *decryptKernelSource = "serpentDecryptKernel.cl";

const char *encryptKernelBin = "serpentEncryptKernel.clbin";
const char *decryptKernelBin = "serpentDecryptKernel.clbin";

cl_int serpentEncryptOpenCl(const u8 pt[], u8 ct[], const u32 k[], const unsigned int plainTextLength, InitValues *initValues, cl_kernel krnl)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	cl_mem devK;

	cl_mem pinnedPt;
	cl_mem pinnedCt;

	u8* hostPinnedPt;
	u8* hostPinnedCt;

    cl_int clStatus;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;
   
	// Create a command queue
	cl_command_queue command_queue = clCreateCommandQueue(initValues->context, initValues->device_list[0], 0, &clStatus);
	start = startStopwatch();

	pinnedPt = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, plainTextLength *sizeof(u8), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedPt failed!");
		return clStatus;
	}

	pinnedCt = clCreateBuffer(initValues->context, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, plainTextLength *sizeof(u8), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedCt failed!");
		return clStatus;
	}

	hostPinnedPt = (u8 *)clEnqueueMapBuffer(command_queue, pinnedPt, CL_TRUE, CL_MAP_WRITE, 0, plainTextLength * sizeof(u8), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}

	memcpy(hostPinnedPt, pt, plainTextLength * sizeof(u8));


	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedPt, hostPinnedPt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);
 
	
	devK = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY, WORDS_PER_KEY_SCHEDULE * sizeof(u32), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clCreateBuffer failed!");
        return clStatus;
    }
	
	endStopwatch("clCreateBuffer (encryption) ", start);

	start = startStopwatch();
	clStatus = clEnqueueWriteBuffer(command_queue, devK, CL_TRUE, 0, WORDS_PER_KEY_SCHEDULE * sizeof(u32), k, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clEnqueueWriteBuffer failed!");
        return clStatus;
    }

	clFlush(command_queue);
	clFinish(command_queue);

	totalTime = endStopwatch("clEnqueueWriteBuffer (encryption)", start);


	// Set the arguments of the kernel
	clStatus = clSetKernelArg(krnl, 0, sizeof(cl_mem), (void *)&pinnedPt);
	clStatus = clSetKernelArg(krnl, 1, sizeof(cl_mem), (void *)&pinnedCt);
	clStatus = clSetKernelArg(krnl, 2, sizeof(cl_mem), (void *)&devK);
	clStatus = clSetKernelArg(krnl, 3, sizeof(unsigned int), (void *)&blocks);



	//!!!must be padded before!!!
	size_t global_size = ((plainTextLength/BYTES_PER_BLOCK - 1)/THREAD_BLOCK_SIZE + 1) * THREAD_BLOCK_SIZE ;
	size_t local_size = THREAD_BLOCK_SIZE;

    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	clStatus = clEnqueueNDRangeKernel(command_queue, krnl, 1, NULL, &global_size, &local_size, 0, NULL, NULL);
	
	// Check for any errors launching the kernel
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "encryption kernel launch failed!");
		printf("%d\n", clStatus);
		return clStatus;
	}

	clFlush(command_queue);
	clFinish(command_queue);

	totalTime += endStopwatch("Encryption kernel", start);

	hostPinnedCt = (u8 *)clEnqueueMapBuffer(command_queue, pinnedCt, CL_TRUE, CL_MAP_READ, 0, plainTextLength * sizeof(u8), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}
	memcpy(ct, hostPinnedCt, plainTextLength * sizeof(u8));
	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedCt, hostPinnedCt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);
       
   	
	// Finally release all OpenCL allocated objePts and host buffers.
	start = startStopwatch();
	clStatus = clReleaseMemObject(devK);


	clStatus = clReleaseCommandQueue(command_queue);

	endStopwatch("clReleaseMemObject (encryption)", start);

	clStatus = clReleaseMemObject(pinnedPt);
	clStatus = clReleaseMemObject(pinnedCt);

	printDuration("Encryption", totalTime);

	return clStatus;
}

cl_int serpentDecryptOpenCl(const u8 ct[], u8 pt[], const u32 k[], const unsigned int plainTextLength, InitValues *initValues, cl_kernel krnl)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	cl_mem devK;

	cl_mem pinnedCt;
	cl_mem pinnedPt;

	u8* hostPinnedPt;
	u8* hostPinnedCt;

    cl_int clStatus;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	// Create a command queue
	cl_command_queue command_queue = clCreateCommandQueue(initValues->context, initValues->device_list[0], 0, &clStatus);

	start = startStopwatch();
	pinnedCt = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, plainTextLength *sizeof(u8), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedPt failed!");
		return clStatus;
	}

	pinnedPt = clCreateBuffer(initValues->context, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, plainTextLength *sizeof(u8), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clCreateBuffer pinnedCt failed!");
		return clStatus;
	}

	hostPinnedCt = (u8 *)clEnqueueMapBuffer(command_queue, pinnedCt, CL_TRUE, CL_MAP_WRITE, 0, plainTextLength * sizeof(u8), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}

	memcpy(hostPinnedCt, ct, plainTextLength * sizeof(u8));


	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedCt, hostPinnedCt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);

	
	devK = clCreateBuffer(initValues->context, CL_MEM_READ_ONLY, WORDS_PER_KEY_SCHEDULE * sizeof(u32), NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clCreateBuffer failed!");
        return clStatus;
    }
	
	
	endStopwatch("clCreateBuffer (decryption) ", start);

	start = startStopwatch();

	clStatus = clEnqueueWriteBuffer(command_queue, devK, CL_TRUE, 0, WORDS_PER_KEY_SCHEDULE * sizeof(u32), k, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "clEnqueueWriteBuffer failed!");
        return clStatus;
    }

	clFlush(command_queue);
	clFinish(command_queue);

	totalTime = endStopwatch("clEnqueueWriteBuffer (decryption) ", start);

	
	// Set the arguments of the kernel
	clStatus = clSetKernelArg(krnl, 0, sizeof(cl_mem), (void *)&pinnedCt);
	clStatus = clSetKernelArg(krnl, 1, sizeof(cl_mem), (void *)&pinnedPt);
	clStatus = clSetKernelArg(krnl, 2, sizeof(cl_mem), (void *)&devK);
	clStatus = clSetKernelArg(krnl, 3, sizeof(unsigned int), (void *)&blocks);



	//!!!must be padded before!!!
	size_t global_size = ((plainTextLength/BYTES_PER_BLOCK - 1)/THREAD_BLOCK_SIZE + 1) * THREAD_BLOCK_SIZE ;
	size_t local_size = THREAD_BLOCK_SIZE; ;

	
    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	clStatus = clEnqueueNDRangeKernel(command_queue, krnl, 1, NULL, &global_size, &local_size, 0, NULL, NULL);
	
	// Check for any errors launching the kernel
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Decryption kernel launch failed!");
		return clStatus;
	}

	clFlush(command_queue);
	clFinish(command_queue);

	totalTime += endStopwatch("Decryption kernel", start);

    
	hostPinnedPt = (u8 *)clEnqueueMapBuffer(command_queue, pinnedPt, CL_TRUE, CL_MAP_READ, 0, plainTextLength * sizeof(u8), 0, NULL, NULL, &clStatus);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueMapBuffer failed!");
		return clStatus;
	}
	memcpy(pt, hostPinnedPt, plainTextLength * sizeof(u8));
	clStatus = clEnqueueUnmapMemObject(command_queue, pinnedPt, hostPinnedPt, 0, NULL, NULL);
	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "clEnqueueUnmapMemObject failed!");
		return clStatus;
	}

	clStatus = clFlush(command_queue);
	clStatus = clFinish(command_queue);
   


	
	// Finally release all OpenCL allocated objects and host buffers.
	start = startStopwatch();
	clStatus = clReleaseMemObject(devK);

	clStatus = clReleaseCommandQueue(command_queue);
	
	endStopwatch("clReleaseMemObject (decryption)", start);
	
	clStatus = clReleaseMemObject(pinnedCt);
	clStatus = clReleaseMemObject(pinnedPt);

	printDuration("Decryption", totalTime);


	return clStatus;
}



int makeKey(const u8 cipherKey[], u32 k[WORDS_PER_KEY_SCHEDULE], const unsigned int keyLen ){

	u8  *k8 = (u8 *)k;
	register u32 r0,r1,r2,r3,r4;
	unsigned int i;

	for (i = 0; i < keyLen; ++i)
		k8[i] = cipherKey[i];
	if (i < BYTES_PER_KEY)
		k8[i++] = 1;
	while (i < BYTES_PER_KEY)
		k8[i++] = 0;

	r0 = k[3];
	r1 = k[4];
	r2 = k[5];
	r3 = k[6];
	r4 = k[7];

	keyiter(k[0],r0,r4,r2,0,0);
	keyiter(k[1],r1,r0,r3,1,1);
	keyiter(k[2],r2,r1,r4,2,2);
	keyiter(k[3],r3,r2,r0,3,3);
	keyiter(k[4],r4,r3,r1,4,4);
	keyiter(k[5],r0,r4,r2,5,5);
	keyiter(k[6],r1,r0,r3,6,6);
	keyiter(k[7],r2,r1,r4,7,7);


	keyiter(k[  0],r3,r2,r0,  8,  8); keyiter(k[  1],r4,r3,r1,  9,  9);
	keyiter(k[  2],r0,r4,r2, 10, 10); keyiter(k[  3],r1,r0,r3, 11, 11);
	keyiter(k[  4],r2,r1,r4, 12, 12); keyiter(k[  5],r3,r2,r0, 13, 13);
	keyiter(k[  6],r4,r3,r1, 14, 14); keyiter(k[  7],r0,r4,r2, 15, 15);
	keyiter(k[  8],r1,r0,r3, 16, 16); keyiter(k[  9],r2,r1,r4, 17, 17);
	keyiter(k[ 10],r3,r2,r0, 18, 18); keyiter(k[ 11],r4,r3,r1, 19, 19);
	keyiter(k[ 12],r0,r4,r2, 20, 20); keyiter(k[ 13],r1,r0,r3, 21, 21);
	keyiter(k[ 14],r2,r1,r4, 22, 22); keyiter(k[ 15],r3,r2,r0, 23, 23);
	keyiter(k[ 16],r4,r3,r1, 24, 24); keyiter(k[ 17],r0,r4,r2, 25, 25);
	keyiter(k[ 18],r1,r0,r3, 26, 26); keyiter(k[ 19],r2,r1,r4, 27, 27);
	keyiter(k[ 20],r3,r2,r0, 28, 28); keyiter(k[ 21],r4,r3,r1, 29, 29);
	keyiter(k[ 22],r0,r4,r2, 30, 30); keyiter(k[ 23],r1,r0,r3, 31, 31);

	k += 50;

	keyiter(k[-26],r2,r1,r4, 32,-18); keyiter(k[-25],r3,r2,r0, 33,-17);
	keyiter(k[-24],r4,r3,r1, 34,-16); keyiter(k[-23],r0,r4,r2, 35,-15);
	keyiter(k[-22],r1,r0,r3, 36,-14); keyiter(k[-21],r2,r1,r4, 37,-13);
	keyiter(k[-20],r3,r2,r0, 38,-12); keyiter(k[-19],r4,r3,r1, 39,-11);
	keyiter(k[-18],r0,r4,r2, 40,-10); keyiter(k[-17],r1,r0,r3, 41, -9);
	keyiter(k[-16],r2,r1,r4, 42, -8); keyiter(k[-15],r3,r2,r0, 43, -7);
	keyiter(k[-14],r4,r3,r1, 44, -6); keyiter(k[-13],r0,r4,r2, 45, -5);
	keyiter(k[-12],r1,r0,r3, 46, -4); keyiter(k[-11],r2,r1,r4, 47, -3);
	keyiter(k[-10],r3,r2,r0, 48, -2); keyiter(k[ -9],r4,r3,r1, 49, -1);
	keyiter(k[ -8],r0,r4,r2, 50,  0); keyiter(k[ -7],r1,r0,r3, 51,  1);
	keyiter(k[ -6],r2,r1,r4, 52,  2); keyiter(k[ -5],r3,r2,r0, 53,  3);
	keyiter(k[ -4],r4,r3,r1, 54,  4); keyiter(k[ -3],r0,r4,r2, 55,  5);
	keyiter(k[ -2],r1,r0,r3, 56,  6); keyiter(k[ -1],r2,r1,r4, 57,  7);
	keyiter(k[  0],r3,r2,r0, 58,  8); keyiter(k[  1],r4,r3,r1, 59,  9);
	keyiter(k[  2],r0,r4,r2, 60, 10); keyiter(k[  3],r1,r0,r3, 61, 11);
	keyiter(k[  4],r2,r1,r4, 62, 12); keyiter(k[  5],r3,r2,r0, 63, 13);
	keyiter(k[  6],r4,r3,r1, 64, 14); keyiter(k[  7],r0,r4,r2, 65, 15);
	keyiter(k[  8],r1,r0,r3, 66, 16); keyiter(k[  9],r2,r1,r4, 67, 17);
	keyiter(k[ 10],r3,r2,r0, 68, 18); keyiter(k[ 11],r4,r3,r1, 69, 19);
	keyiter(k[ 12],r0,r4,r2, 70, 20); keyiter(k[ 13],r1,r0,r3, 71, 21);
	keyiter(k[ 14],r2,r1,r4, 72, 22); keyiter(k[ 15],r3,r2,r0, 73, 23);
	keyiter(k[ 16],r4,r3,r1, 74, 24); keyiter(k[ 17],r0,r4,r2, 75, 25);
	keyiter(k[ 18],r1,r0,r3, 76, 26); keyiter(k[ 19],r2,r1,r4, 77, 27);
	keyiter(k[ 20],r3,r2,r0, 78, 28); keyiter(k[ 21],r4,r3,r1, 79, 29);
	keyiter(k[ 22],r0,r4,r2, 80, 30); keyiter(k[ 23],r1,r0,r3, 81, 31);

	k += 50;

	keyiter(k[-26],r2,r1,r4, 82,-18); keyiter(k[-25],r3,r2,r0, 83,-17);
	keyiter(k[-24],r4,r3,r1, 84,-16); keyiter(k[-23],r0,r4,r2, 85,-15);
	keyiter(k[-22],r1,r0,r3, 86,-14); keyiter(k[-21],r2,r1,r4, 87,-13);
	keyiter(k[-20],r3,r2,r0, 88,-12); keyiter(k[-19],r4,r3,r1, 89,-11);
	keyiter(k[-18],r0,r4,r2, 90,-10); keyiter(k[-17],r1,r0,r3, 91, -9);
	keyiter(k[-16],r2,r1,r4, 92, -8); keyiter(k[-15],r3,r2,r0, 93, -7);
	keyiter(k[-14],r4,r3,r1, 94, -6); keyiter(k[-13],r0,r4,r2, 95, -5);
	keyiter(k[-12],r1,r0,r3, 96, -4); keyiter(k[-11],r2,r1,r4, 97, -3);
	keyiter(k[-10],r3,r2,r0, 98, -2); keyiter(k[ -9],r4,r3,r1, 99, -1);
	keyiter(k[ -8],r0,r4,r2,100,  0); keyiter(k[ -7],r1,r0,r3,101,  1);
	keyiter(k[ -6],r2,r1,r4,102,  2); keyiter(k[ -5],r3,r2,r0,103,  3);
	keyiter(k[ -4],r4,r3,r1,104,  4); keyiter(k[ -3],r0,r4,r2,105,  5);
	keyiter(k[ -2],r1,r0,r3,106,  6); keyiter(k[ -1],r2,r1,r4,107,  7);
	keyiter(k[  0],r3,r2,r0,108,  8); keyiter(k[  1],r4,r3,r1,109,  9);
	keyiter(k[  2],r0,r4,r2,110, 10); keyiter(k[  3],r1,r0,r3,111, 11);
	keyiter(k[  4],r2,r1,r4,112, 12); keyiter(k[  5],r3,r2,r0,113, 13);
	keyiter(k[  6],r4,r3,r1,114, 14); keyiter(k[  7],r0,r4,r2,115, 15);
	keyiter(k[  8],r1,r0,r3,116, 16); keyiter(k[  9],r2,r1,r4,117, 17);
	keyiter(k[ 10],r3,r2,r0,118, 18); keyiter(k[ 11],r4,r3,r1,119, 19);
	keyiter(k[ 12],r0,r4,r2,120, 20); keyiter(k[ 13],r1,r0,r3,121, 21);
	keyiter(k[ 14],r2,r1,r4,122, 22); keyiter(k[ 15],r3,r2,r0,123, 23);
	keyiter(k[ 16],r4,r3,r1,124, 24); keyiter(k[ 17],r0,r4,r2,125, 25);
	keyiter(k[ 18],r1,r0,r3,126, 26); keyiter(k[ 19],r2,r1,r4,127, 27);
	keyiter(k[ 20],r3,r2,r0,128, 28); keyiter(k[ 21],r4,r3,r1,129, 29);
	keyiter(k[ 22],r0,r4,r2,130, 30); keyiter(k[ 23],r1,r0,r3,131, 31);

	/* Apply S-boxes */

	S3(r3,r4,r0,r1,r2); storekeys(r1,r2,r4,r3, 28); loadkeys(r1,r2,r4,r3, 24);
	S4(r1,r2,r4,r3,r0); storekeys(r2,r4,r3,r0, 24); loadkeys(r2,r4,r3,r0, 20);
	S5(r2,r4,r3,r0,r1); storekeys(r1,r2,r4,r0, 20); loadkeys(r1,r2,r4,r0, 16);
	S6(r1,r2,r4,r0,r3); storekeys(r4,r3,r2,r0, 16); loadkeys(r4,r3,r2,r0, 12);
	S7(r4,r3,r2,r0,r1); storekeys(r1,r2,r0,r4, 12); loadkeys(r1,r2,r0,r4,  8);
	S0(r1,r2,r0,r4,r3); storekeys(r0,r2,r4,r1,  8); loadkeys(r0,r2,r4,r1,  4);
	S1(r0,r2,r4,r1,r3); storekeys(r3,r4,r1,r0,  4); loadkeys(r3,r4,r1,r0,  0);
	S2(r3,r4,r1,r0,r2); storekeys(r2,r4,r3,r0,  0); loadkeys(r2,r4,r3,r0, -4);
	S3(r2,r4,r3,r0,r1); storekeys(r0,r1,r4,r2, -4); loadkeys(r0,r1,r4,r2, -8);
	S4(r0,r1,r4,r2,r3); storekeys(r1,r4,r2,r3, -8); loadkeys(r1,r4,r2,r3,-12);
	S5(r1,r4,r2,r3,r0); storekeys(r0,r1,r4,r3,-12); loadkeys(r0,r1,r4,r3,-16);
	S6(r0,r1,r4,r3,r2); storekeys(r4,r2,r1,r3,-16); loadkeys(r4,r2,r1,r3,-20);
	S7(r4,r2,r1,r3,r0); storekeys(r0,r1,r3,r4,-20); loadkeys(r0,r1,r3,r4,-24);
	S0(r0,r1,r3,r4,r2); storekeys(r3,r1,r4,r0,-24); loadkeys(r3,r1,r4,r0,-28);
	k -= 50;
	S1(r3,r1,r4,r0,r2); storekeys(r2,r4,r0,r3, 22); loadkeys(r2,r4,r0,r3, 18);
	S2(r2,r4,r0,r3,r1); storekeys(r1,r4,r2,r3, 18); loadkeys(r1,r4,r2,r3, 14);
	S3(r1,r4,r2,r3,r0); storekeys(r3,r0,r4,r1, 14); loadkeys(r3,r0,r4,r1, 10);
	S4(r3,r0,r4,r1,r2); storekeys(r0,r4,r1,r2, 10); loadkeys(r0,r4,r1,r2,  6);
	S5(r0,r4,r1,r2,r3); storekeys(r3,r0,r4,r2,  6); loadkeys(r3,r0,r4,r2,  2);
	S6(r3,r0,r4,r2,r1); storekeys(r4,r1,r0,r2,  2); loadkeys(r4,r1,r0,r2, -2);
	S7(r4,r1,r0,r2,r3); storekeys(r3,r0,r2,r4, -2); loadkeys(r3,r0,r2,r4, -6);
	S0(r3,r0,r2,r4,r1); storekeys(r2,r0,r4,r3, -6); loadkeys(r2,r0,r4,r3,-10);
	S1(r2,r0,r4,r3,r1); storekeys(r1,r4,r3,r2,-10); loadkeys(r1,r4,r3,r2,-14);
	S2(r1,r4,r3,r2,r0); storekeys(r0,r4,r1,r2,-14); loadkeys(r0,r4,r1,r2,-18);
	S3(r0,r4,r1,r2,r3); storekeys(r2,r3,r4,r0,-18); loadkeys(r2,r3,r4,r0,-22);
	k -= 50;
	S4(r2,r3,r4,r0,r1); storekeys(r3,r4,r0,r1, 28); loadkeys(r3,r4,r0,r1, 24);
	S5(r3,r4,r0,r1,r2); storekeys(r2,r3,r4,r1, 24); loadkeys(r2,r3,r4,r1, 20);
	S6(r2,r3,r4,r1,r0); storekeys(r4,r0,r3,r1, 20); loadkeys(r4,r0,r3,r1, 16);
	S7(r4,r0,r3,r1,r2); storekeys(r2,r3,r1,r4, 16); loadkeys(r2,r3,r1,r4, 12);
	S0(r2,r3,r1,r4,r0); storekeys(r1,r3,r4,r2, 12); loadkeys(r1,r3,r4,r2,  8);
	S1(r1,r3,r4,r2,r0); storekeys(r0,r4,r2,r1,  8); loadkeys(r0,r4,r2,r1,  4);
	S2(r0,r4,r2,r1,r3); storekeys(r3,r4,r0,r1,  4); loadkeys(r3,r4,r0,r1,  0);
	S3(r3,r4,r0,r1,r2); storekeys(r1,r2,r4,r3,  0);

	return 0;


}


int serpentEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;


	InitValues *initValues;
	cl_program program;
	cl_kernel krnl;
	cl_int clStatus;

	u32 k[WORDS_PER_KEY_SCHEDULE];

	start = startStopwatch();
	initValues = openClInit();
	endStopwatch("OpenCl initialization (encryption)", start);


	start = startStopwatch();
	program = getProgram(initValues, encryptKernelSource, encryptKernelBin, options, BUILD_SOURCE);

	if (program == NULL){
		printf("Error while creating program. \n");
		return -1;

	}
	endStopwatch("Program building (encryption)", start);

	start = startStopwatch();
	// Create the OpenCL kernel
	krnl = clCreateKernel(program, "encryptKernel", &clStatus);

	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Kernel creating failed!");
		return clStatus;
	}

	endStopwatch("Kernel creation (encryption) ", start);


	start = startStopwatch();
	makeKey(cipherKey, k, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);
	
	
	clStatus = serpentEncryptOpenCl(pt, ct, k, plainTextLength, initValues, krnl);

	
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "Encryption failed!");
        return 1;
    }

	start = startStopwatch();
	clStatus = clReleaseKernel(krnl);
	clStatus = clReleaseProgram(program);
	openClDeInit(initValues);

	endStopwatch("Freeing OpenCL resources (encryption)", start);


    return 0;
}
int serpentDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){
	
	boost::chrono::high_resolution_clock::time_point start;


	InitValues *initValues;
	cl_program program;
	cl_kernel krnl;
	cl_int clStatus;

	u32 k[WORDS_PER_KEY_SCHEDULE];
	
	start = startStopwatch();
	initValues = openClInit();
	endStopwatch("OpenCl initialization (decryption)", start);


	start = startStopwatch();
	program = getProgram(initValues, decryptKernelSource, decryptKernelBin, options, BUILD_SOURCE);
	endStopwatch("Program building (decryption)", start);

	if (program == NULL){
		printf("Error while creating program. \n");
		return -1;

	}
	

	start = startStopwatch();
	// Create the OpenCL kernel
	krnl = clCreateKernel(program, "decryptKernel", &clStatus);
	endStopwatch("Kernel creation (decryption)", start);

	if (clStatus != CL_SUCCESS) {
		fprintf(stderr, "Kernel creating failed!\n ");
		return clStatus;
	}

	


	start = startStopwatch();
	makeKey(cipherKey, k, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);
	
	
	clStatus = serpentDecryptOpenCl(ct, pt, k, plainTextLength, initValues, krnl);
	
	
	
	if (clStatus != CL_SUCCESS) {
        fprintf(stderr, "Decryption failed!");
        return 1;
    }

	start = startStopwatch();
	clStatus = clReleaseKernel(krnl);
	clStatus = clReleaseProgram(program);
	openClDeInit(initValues);
	endStopwatch("Freeing OpenCL resources (decryption)", start);
	
    return 0;
}