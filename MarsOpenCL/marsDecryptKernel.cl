#include "mars-opencl.h"

#define f_mix(a,b,c,d)                  \
	r = ROR(a, 8);                 \
	b ^= s_sh[a & 0xff];            \
	b += s_sh[(r & 0xff) + 256];    \
	r = ROR(a, 16);                \
	a = ROR(a, 24);               \
	c += s_sh[r & 0xff];            \
	d ^= s_sh[(a & 0xff) + 256]

#define b_mix(a,b,c,d)                  \
	r = ROL(a, 8);                 \
	b ^= s_sh[(a & 0xff) + 256];    \
	c -= s_sh[r & 0xff];            \
	r = ROL(a, 16);                \
	a = ROL(a, 24);               \
	d -= s_sh[(r & 0xff) + 256];    \
	d ^= s_sh[a & 0xff]

#define r_ktr(a,b,c,d,i)    \
	r = a * rk[i + 1];   \
	a = ROR(a, 13);        \
	m = a + rk[i];       \
	l = s_sh[m & 0x1ff];     \
	r = ROL(r, 5);         \
	l ^= r;                 \
	c -= ROL(m, r);        \
	r = ROL(r, 5);         \
	l ^= r;                 \
	d ^= r;                 \
	b -= ROL(l, r)

__kernel void decryptKernel(__global uint4 *ct, __global uint4 *pt, __constant u32 *rk, __global u32 *S, const unsigned int blocks)
{																																		
																																																																
	int t = get_local_id(0);
	int i = get_global_id(0);

	__local u32 s_sh[BIG_S_TABLE_SIZE];



	if (t < S_TABLE_SIZE){

		s_sh[t] = S[t];
		s_sh[t + S_TABLE_SIZE] = S[t + S_TABLE_SIZE];

	}

	barrier(CLK_LOCAL_MEM_FENCE);
																								
																																		
	if(i < blocks){


		register u32 a, b, c, d, l, m, r;

		uint4 block_in = ct[i];
		uint4 block_out;

		d = block_in.x + rk[36];
		c = block_in.y + rk[37];
		b = block_in.z + rk[38];
		a = block_in.w + rk[39];

		f_mix(a, b, c, d); a += d;
		f_mix(b, c, d, a); b += c;
		f_mix(c, d, a, b);
		f_mix(d, a, b, c);
		f_mix(a, b, c, d); a += d;
		f_mix(b, c, d, a); b += c;
		f_mix(c, d, a, b);
		f_mix(d, a, b, c);

		r_ktr(a, b, c, d, 34); r_ktr(b, c, d, a, 32); r_ktr(c, d, a, b, 30); r_ktr(d, a, b, c, 28);
		r_ktr(a, b, c, d, 26); r_ktr(b, c, d, a, 24); r_ktr(c, d, a, b, 22); r_ktr(d, a, b, c, 20);
		r_ktr(a, d, c, b, 18); r_ktr(b, a, d, c, 16); r_ktr(c, b, a, d, 14); r_ktr(d, c, b, a, 12);
		r_ktr(a, d, c, b, 10); r_ktr(b, a, d, c, 8); r_ktr(c, b, a, d, 6); r_ktr(d, c, b, a, 4);

		b_mix(a, b, c, d);
		b_mix(b, c, d, a); c -= b;
		b_mix(c, d, a, b); d -= a;
		b_mix(d, a, b, c);
		b_mix(a, b, c, d);
		b_mix(b, c, d, a); c -= b;
		b_mix(c, d, a, b); d -= a;
		b_mix(d, a, b, c);

		block_out.x = d - rk[0];
		block_out.y = c - rk[1];
		block_out.z = b - rk[2];
		block_out.w = a - rk[3];

		pt[i] = block_out;
	}
																																																									
																																		
																																		
																																		
}																																		
