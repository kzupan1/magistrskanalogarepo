#include "rc6-cpu.h"
#include "measure-time.h"
#include <boost/chrono.hpp>


int makeKey(const u32 cipherKey[], u32 rk[R24], const unsigned int keyLen ){

	u32 l[8], i, j, a, k, b, t;

	rk[0] = P32;

	for(k = 1; k < R24; ++k)
		rk[k] = rk[k-1] + Q32;

	for(k = 0; k < keyLen/BYTES_PER_WORD; ++k)
		l[k] = cipherKey[k];

	a = b = i = j = 0;

	t = (keyLen / BYTES_PER_WORD) - 1;

	for(k = 0; k < 132; ++k)
		{   a = ROL(rk[i] + a + b, 3); b += a;
			b = ROL(l[j] + b, b);
			rk[i] = a; l[j] = b;
			i = (i == 43 ? 0 : i + 1);  // i = (i + 1) % 44;
			j = (j == t ? 0 : j + 1);   // j = (j + 1) % t;
		}


	return 0;
}


#define f_rnd(i,a,b,c,d)                        \
        u = ROL(d * (d + d + 1), 5);           \
        t = ROL(b * (b + b + 1), 5);           \
        a = ROL(a ^ t, u) + rk[i];     \
        c = ROL(c ^ u, t) + rk[i + 1]

#define i_rnd(i,a,b,c,d)                        \
        u = ROL(d * (d + d + 1), 5);           \
        t = ROL(b * (b + b + 1), 5);           \
        c = ROR(c - rk[i + 1], t) ^ u; \
        a = ROR(a - rk[i], u) ^ t


int rc6EncryptCpu(u32 *pt, u32 *ct, const u32 * rk){

	register u32 a, b, c, d, t, u;
	
	a = pt[0];
	b = pt[1] + rk[0];
	c = pt[2];
	d = pt[3] + rk[1];

	f_rnd( 2,a,b,c,d); f_rnd( 4,b,c,d,a);
    f_rnd( 6,c,d,a,b); f_rnd( 8,d,a,b,c);
    f_rnd(10,a,b,c,d); f_rnd(12,b,c,d,a);
    f_rnd(14,c,d,a,b); f_rnd(16,d,a,b,c);
    f_rnd(18,a,b,c,d); f_rnd(20,b,c,d,a);
    f_rnd(22,c,d,a,b); f_rnd(24,d,a,b,c);
    f_rnd(26,a,b,c,d); f_rnd(28,b,c,d,a);
    f_rnd(30,c,d,a,b); f_rnd(32,d,a,b,c);
    f_rnd(34,a,b,c,d); f_rnd(36,b,c,d,a);
    f_rnd(38,c,d,a,b); f_rnd(40,d,a,b,c);

	ct[0] = a + rk[42];
	ct[1] = b;
	ct[2] = c + rk[43];
	ct[3] = d;


	return 0;
}

int rc6DecryptCpu(u32 *ct, u32 *pt, const u32  *rk){

	register u32 a, b, c, d, t, u;

	a = ct[0] - rk[42];
	b = ct[1];
	c = ct[2] - rk[43];
	d = ct[3];

	i_rnd(40,d,a,b,c); i_rnd(38,c,d,a,b);
    i_rnd(36,b,c,d,a); i_rnd(34,a,b,c,d);
    i_rnd(32,d,a,b,c); i_rnd(30,c,d,a,b);
    i_rnd(28,b,c,d,a); i_rnd(26,a,b,c,d);
    i_rnd(24,d,a,b,c); i_rnd(22,c,d,a,b);
    i_rnd(20,b,c,d,a); i_rnd(18,a,b,c,d);
    i_rnd(16,d,a,b,c); i_rnd(14,c,d,a,b);
    i_rnd(12,b,c,d,a); i_rnd(10,a,b,c,d);
    i_rnd( 8,d,a,b,c); i_rnd( 6,c,d,a,b);
    i_rnd( 4,b,c,d,a); i_rnd( 2,a,b,c,d);

	pt[0] = a;
	pt[1] = b - rk[0];
	pt[2] = c;
	pt[3] = d - rk[1];

	return 0;
}



int rc6Encrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	u32 rk[R24];
	
	start = startStopwatch();
	makeKey((u32*)cipherKey, rk, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);

	start = startStopwatch();
	for(int numBlocks = plainTextLength >> WORDS_PER_BLOCK; numBlocks > 0; numBlocks--, pt+=BYTES_PER_BLOCK, ct+=BYTES_PER_BLOCK){
		rc6EncryptCpu((u32 *)pt, (u32 *)ct, rk);
	}
	
	endStopwatch("Encryption", start);
	


    return 0;




}
int rc6Decrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){
	
	boost::chrono::high_resolution_clock::time_point start;
	
	u32 rk[R24];
	
	start = startStopwatch();
	makeKey((u32*)cipherKey, rk, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);
	
	start = startStopwatch();
	for(int numBlocks = plainTextLength >> WORDS_PER_BLOCK; numBlocks > 0; numBlocks--, pt+=BYTES_PER_BLOCK, ct+=BYTES_PER_BLOCK){
		rc6DecryptCpu((u32 *)ct, (u32 *)pt, rk);
	}
	endStopwatch("Decryption", start);
	


    return 0;

}