
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "rijndael-cuda.cuh"
#include "rijndael-tables.cuh"
#include <stdio.h>
#include <stdlib.h>
#include "measure-time.h"
#include <boost/chrono.hpp>

__device__ inline G4 G4_mul(G4 x, G4 y){
	u32 e;
	e = (x.b1 ^  x.b0) & (y.b1 ^ y.b0);
	return{ (x.b0 & y.b0) ^ e, (x.b1 & y.b1) ^ e };
}


__device__ inline G4 G4_scl_N(G4 x){
	return{ x.b1 ^ x.b0, x.b0 };
}

__device__ inline G4 G4_scl_N2(G4 x){
	return{ x.b1, x.b1 ^ x.b0 };
}

__device__ inline G4 G4_sq(G4 x){
	return{ x.b1, x.b0 };
}

__device__ inline G4 G4_xor(G4 x, G4 y){
	return{ x.b0 ^ y.b0, x.b1 ^ y.b1 };
}

__device__ inline G16 G16_mul(G16 x, G16 y){
	G4 e;
	e = G4_scl_N(G4_mul(G4_xor(x.b1, x.b0), G4_xor(y.b1, y.b0)));
	
	return{ G4_xor(G4_mul(x.b0, y.b0), e), G4_xor(G4_mul(x.b1, y.b1), e) };
}

__device__ inline G16 G16_sq_scl(G16 x){
	return{ G4_scl_N2(G4_sq(x.b0)), G4_sq(G4_xor(x.b1, x.b0)) };
}

__device__ inline G16 G16_inv(G16 x){
	G4 e;
	e = G4_sq(G4_xor(G4_scl_N(G4_sq(G4_xor(x.b1, x.b0))), G4_mul(x.b1, x.b0)));
	return{ G4_mul(e, x.b1), G4_mul(e, x.b0) };

}

__device__ inline G16 G16_xor(G16 x, G16 y){
	return{ G4_xor(x.b0, y.b0), G4_xor(x.b1, y.b1) };
}

__device__ inline G256 G256_inv(G256 x){
	G16 e;
	e = G16_inv(G16_xor(G16_sq_scl(G16_xor(x.b1, x.b0)), G16_mul(x.b1, x.b0)));
	return{ G16_mul(e, x.b1), G16_mul(e, x.b0) };
}

__device__ inline G256 G256_xor(G256 x, G256 y){
	return{ G16_xor(x.b0, y.b0), G16_xor(x.b1, y.b1) };
}

__device__ inline G256 G256_newbasis_forward(G256 n){

	return{
			{
				{ 
					n.b1.b1.b0 ^ n.b0.b1.b1 ^ n.b0.b1.b0 ^ n.b0.b0.b1 ^ n.b0.b0.b0,
					n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b0.b0
				}, 
				{
					n.b0.b0.b0,
					n.b1.b1.b1 ^ n.b1.b0.b0 ^ n.b0.b1.b1  ^ n.b0.b0.b1 ^ n.b0.b0.b0 
				} 
			},
			{ 
				{ 
					n.b1.b1.b1 ^ n.b1.b1.b0 ^ n.b1.b0.b1  ^ n.b0.b0.b0,
					n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b0.b1  ^ n.b0.b0.b0
				}, 
				{ 
					n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b1.b0.b0  ^ n.b0.b0.b0,
					n.b1.b1.b1 ^n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b1.b0 ^ n.b0.b0.b1 ^ n.b0.b0.b0 
				} 
			}

		};
}

__device__ inline G256 G256_newbasis_inv_forward(G256 n){
	
	return{
		{
			{
				n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b1.b0.b0 ^ n.b0.b0.b1 ^ n.b0.b0.b0,
				n.b1.b0.b0 ^ n.b0.b1.b1 ^ n.b0.b0.b0
			},
			{
				n.b1.b1.b1 ^ n.b1.b0.b1 ^ n.b0.b1.b0,
				n.b1.b1.b1 ^ n.b1.b1.b0 ^ n.b1.b0.b0
			}
		},
		{
			{
				n.b1.b1.b0 ^ n.b0.b1.b1 ^ n.b0.b0.b1  ^ n.b0.b0.b0,
				n.b1.b1.b0 ^ n.b1.b0.b0
			},
			{
				n.b1.b1.b0 ^ n.b1.b0.b0 ^ n.b0.b0.b1  ^ n.b0.b0.b0,
				n.b1.b1.b1 ^n.b1.b0.b0
			}
		}

	};
}

__device__ inline G256 G256_newbasis_backward(G256 n){
	
	return{
		{
			{
				n.b1.b1.b0 ^ n.b1.b0.b0 ^ n.b0.b0.b1,
				n.b1.b0.b1 ^ n.b1.b0.b0 ^ n.b0.b0.b1
			},
			{
				n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b1.b1  ^ n.b0.b1.b0 ^ n.b0.b0.b0,
				n.b1.b1.b1 ^ n.b1.b1.b0 ^ n.b1.b0.b1  ^ n.b1.b0.b0 ^ n.b0.b1.b1
			}
		},
		{
			{
				n.b1.b1.b1 ^ n.b1.b0.b1 ^ n.b0.b1.b1,
				n.b1.b1.b0 ^ n.b0.b0.b0
			},
			{
				n.b1.b1.b1 ^ n.b0.b1.b1,
				n.b1.b0.b1 ^ n.b0.b1.b1
			}
		}

	};
}

__device__ inline G256 G256_newbasis_inv_backward(G256 n){

	return{
		{
			{
				n.b0.b1.b0,
				n.b1.b0.b1 ^ n.b0.b0.b1
			},
			{
				n.b1.b1.b1 ^ n.b1.b0.b1 ^ n.b1.b0.b0  ^ n.b0.b0.b1,
				n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b1.b0.b0  ^ n.b0.b1.b1 ^ n.b0.b1.b0 ^ n.b0.b0.b1
			}
		},
		{
			{
				n.b1.b1.b0 ^ n.b0.b0.b1,
				n.b1.b1.b1 ^ n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b1.b1 ^ n.b0.b1.b0 ^ n.b0.b0.b0
			},
			{
				n.b1.b1.b1 ^ n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b1.b1 ^ n.b0.b0.b1 ^ n.b0.b0.b0,
				n.b1.b0.b0 ^ n.b0.b0.b1
			}
		}

	};


}

__device__ inline G256 Sbox(G256 n){
	G256 t;
	t = G256_newbasis_forward(n);
	t = G256_inv(t);
	t = G256_newbasis_backward(t);

	t.b1.b1.b0 = ~t.b1.b1.b0;
	t.b1.b0.b1 = ~t.b1.b0.b1;
	t.b0.b0.b1 = ~t.b0.b0.b1;
	t.b0.b0.b0 = ~t.b0.b0.b0;

	return t;
}

__device__ inline G256 iSbox(G256 n){
	G256 t;

	n.b1.b1.b0 = ~n.b1.b1.b0;
	n.b1.b0.b1 = ~n.b1.b0.b1;
	n.b0.b0.b1 = ~n.b0.b0.b1;
	n.b0.b0.b0 = ~n.b0.b0.b0;

	t = G256_newbasis_inv_forward(n);
	t = G256_inv(t);
	t = G256_newbasis_inv_backward(t);

	return t;
}

__device__ inline G256 xtime(G256 n){
	return{
		{
			{
				n.b1.b1.b1,
				n.b0.b0.b0 ^ n.b1.b1.b1
			},
			{
				n.b0.b0.b1,
				n.b0.b1.b0 ^ n.b1.b1.b1
			}
		},
		{
			{
				n.b0.b1.b1 ^ n.b1.b1.b1,
				n.b1.b0.b0
			},
			{
				n.b1.b0.b1,
				n.b1.b1.b0
			}
		}

	};
}

__device__ inline G256 gmul2(G256 n){
	return xtime(n);
}

__device__ inline G256 gmul3(G256 n){
	return G256_xor(xtime(n), n);
}

__device__ inline G256 gmul14(G256 n){
	G256 t1 = xtime(n);
	G256 t2 = xtime(t1);

	return G256_xor(G256_xor(xtime(t2), t2), t1);
}

__device__ inline G256 gmul11(G256 n){
	G256 t1 = xtime(n);
	
	return G256_xor(G256_xor(xtime(xtime(t1)), t1), n);
}

__device__ inline G256 gmul13(G256 n){
	G256 t1 = xtime(xtime(n));

	return G256_xor(G256_xor(xtime(t1), t1), n);
}

__device__ inline G256 gmul9(G256 n){

	return G256_xor(xtime(xtime(xtime(n))), n);
}

__device__ inline G256 keying(G256 n, u8 k){
	return{
		{
			{
				n.b0.b0.b0 ^ (-(k & 0x01)),
				n.b0.b0.b1 ^ (-((k >> 1) & 0x01))
			},
			{
				n.b0.b1.b0 ^ (-((k >> 2) & 0x01)),
				n.b0.b1.b1 ^ (-((k >> 3) & 0x01))
			}
		},
		{
			{
				n.b1.b0.b0 ^ (-((k >> 4) & 0x01)),
				n.b1.b0.b1 ^ (-((k >> 5) & 0x01))
			},
			{
				n.b1.b1.b0 ^ (-((k >> 6) & 0x01)),
				n.b1.b1.b1 ^ (-((k >> 7) & 0x01))
			}
		}

	};






}


__global__ void rijndaelEncryptBitwiseKernel(const u32 *in, u32 *out, u8 *rk, const unsigned int blocks ,const unsigned int Nr)
{


	int tx = threadIdx.x;
	int ty = threadIdx.y;
	int tz = threadIdx.z;


	int bx = blockIdx.x;
	int by = blockIdx.y;
	int block = bx * gridDim.y + by;

	if (block < blocks){
		int tByte = tx * blockDim.y + ty;
		int tBit = tByte * blockDim.z + tz;
		int i = block * BLOCK_SIZE + tBit;


		int r;

		register u32 idx0, idx1, idx2, idx3;

		idx0 = (MOD4(tx + ty) << 2) + MOD4(ty);
		idx1 = (MOD4(tx + ty + 1) << 2) + MOD4(ty + 1);
		idx2 = (MOD4(tx + ty + 2) << 2) + MOD4(ty + 2);
		idx3 = (MOD4(tx + ty + 3) << 2) + MOD4(ty + 3);

		u32 n, s, u, w;

		G256 box1;
		G256 box2;
		G256 box3;

		__shared__ G256_p data_sh[16];
		__shared__ G256_p S_sh[16];
		__shared__ G256_p S2_sh[16];
		__shared__ G256_p S3_sh[16];

		/*First we read the data*/
		n = in[i];

		/*Then we do the keying, we have to take care for rk indexes and bit indexes*/
		//n = keying(n, rk[t]);

		n ^= (-((rk[tByte] >> tz) & 0x01));

		((u32 *)data_sh)[tBit + tByte] = n;

		for (r = 16; r < (Nr << 4); r += 16){
			/*Next we do the S table (or S multiplied with 01, so it is called S1) on the 8 byte data*/


			if (tz == 0){

				box1 = data_sh[tByte].g256;
				box1 = Sbox(box1);
				box2 = gmul2(box1);
				box3 = G256_xor(box1, box2);


				S_sh[tByte].g256 = box1;
				S2_sh[tByte].g256 = box2;
				S3_sh[tByte].g256 = box3;
			}

			__syncthreads();

			/*Now we load additional data needed for calculating xors*/

			/*S2*/
			n = ((u32 *)S2_sh)[idx0 * blockDim.z + tz + idx0];

			/*S3*/
			s = ((u32 *)S3_sh)[idx1 * blockDim.z + tz + idx1];

			/*S*/
			u = ((u32 *)S_sh)[idx2 * blockDim.z + tz + idx2];

			/*S*/
			w = ((u32 *)S_sh)[idx3 * blockDim.z + tz + idx3];

			/*We do the xors*/
			n ^= s ^ u ^ w;

			/*keying*/
			n ^= (-((rk[r + tByte] >> tz) & 0x01));

			((u32 *)data_sh)[tBit + tByte] = n;


			__syncthreads();
		}

		/*apply last round*/


		if (tz == 0){
			box1 = data_sh[tByte].g256;
			box1 = Sbox(box1);

			S_sh[tByte].g256 = box1;

		}


		__syncthreads();


		n = ((u32 *)S_sh)[idx0 * blockDim.z + tz + idx0];
		n ^= (-((rk[r + tByte] >> tz) & 0x01));

		/*Save output*/
		out[i] = n;


	}
	
	
	
	


}

__global__ void rijndaelDecryptBitwiseKernel(const u32 *in, u32 *out, u8 *rk, const unsigned int blocks ,const unsigned int Nr)
{


	int tx = threadIdx.x;
	int ty = threadIdx.y;
	int tz = threadIdx.z;


	int bx = blockIdx.x;
	int by = blockIdx.y;
	int block = bx * gridDim.y + by;

	if (block < blocks){
		int tByte = tx * blockDim.y + ty;
		int tBit = tByte * blockDim.z + tz;
		int i = block * BLOCK_SIZE + tBit;


		int r;

		register u32 idx0, idx1, idx2, idx3;

		idx0 = (MOD4(tx - ty) << 2) + MOD4(ty);
		idx1 = (MOD4(tx - ty - 1) << 2) + MOD4(ty + 1);
		idx2 = (MOD4(tx - ty - 2) << 2) + MOD4(ty + 2);
		idx3 = (MOD4(tx - ty - 3) << 2) + MOD4(ty + 3);

		u32 n, s, u, w;

		G256 box1;
		G256 box2;
		G256 box3;
		G256 box4;
		G256 box8;
		G256 box9;


		__shared__ G256_p data_sh[16];
		__shared__ G256_p S9_sh[16];
		__shared__ G256_p S11_sh[16];
		__shared__ G256_p S13_sh[16];
		__shared__ G256_p S14_sh[16];


		/*First we read the data*/
		n = in[i];

		/*Then we do the keying, we have to take care for rk indexes and bit indexes*/
		//n = keying(n, rk[t]);

		n ^= (-((rk[tByte] >> tz) & 0x01));

		((u32 *)data_sh)[tBit + tByte] = n;

		for (r = 16; r < (Nr << 4); r += 16){
			/*Next we do the S table (or S multiplied with 01, so it is called S1) on the 8 byte data*/


			if (tz == 0){

				box1 = data_sh[tByte].g256;
				box1 = iSbox(box1);
				box2 = xtime(box1);
				box4 = xtime(box2);
				box8 = xtime(box4);
				box9 = G256_xor(box8, box1);

				S9_sh[tByte].g256 = box9;
				S11_sh[tByte].g256 = G256_xor(box9, box2);
				S13_sh[tByte].g256 = G256_xor(box9, box4);
				S14_sh[tByte].g256 = G256_xor(G256_xor(box8, box4), box2);
			}

			__syncthreads();

			/*Now we load additional data needed for calculating xors*/

			/*S2*/
			n = ((u32 *)S14_sh)[idx0 * blockDim.z + tz + idx0];

			/*S3*/
			s = ((u32 *)S11_sh)[idx1 * blockDim.z + tz + idx1];

			/*S*/
			u = ((u32 *)S13_sh)[idx2 * blockDim.z + tz + idx2];

			/*S*/
			w = ((u32 *)S9_sh)[idx3 * blockDim.z + tz + idx3];

			/*We do the xors*/
			n ^= s ^ u ^ w;

			/*keying*/
			n ^= (-((rk[r + tByte] >> tz) & 0x01));

			((u32 *)data_sh)[tBit + tByte] = n;


			__syncthreads();
		}

		/*apply last round*/


		if (tz == 0){
			box1 = data_sh[tByte].g256;
			box1 = iSbox(box1);

			S14_sh[tByte].g256 = box1;

		}


		__syncthreads();


		n = ((u32 *)S14_sh)[idx0 * blockDim.z + tz + idx0];
		n ^= (-((rk[r + tByte] >> tz) & 0x01));

		/*Save output*/
		out[i] = n;

	}
}



__global__ void transposeKernel(const u8 *in, u8 *out, const unsigned int blocks)
{


	int tx = threadIdx.x;
	int ty = threadIdx.y;

	int bx = blockIdx.x;
	int by = blockIdx.y;

	int block = bx * gridDim.y + by;

	if (block < blocks){
		register u8 i0, i1, i2, i3, i4, i5, i6, i7;
		register u8 o0, o1, o2, o3, o4, o5, o6, o7;

		int offset1, offset2;
		offset1 = block * (BLOCK_SIZE * BYTES_PER_WORD) + ty * BLOCK_SIZE + tx;
		offset2 = block * (BLOCK_SIZE * BYTES_PER_WORD) + tx * BITS_PER_WORD + ty;

		i0 = in[offset1 + 0 * BYTES_PER_BLOCK];
		i1 = in[offset1 + 1 * BYTES_PER_BLOCK];
		i2 = in[offset1 + 2 * BYTES_PER_BLOCK];
		i3 = in[offset1 + 3 * BYTES_PER_BLOCK];
		i4 = in[offset1 + 4 * BYTES_PER_BLOCK];
		i5 = in[offset1 + 5 * BYTES_PER_BLOCK];
		i6 = in[offset1 + 6 * BYTES_PER_BLOCK];
		i7 = in[offset1 + 7 * BYTES_PER_BLOCK];

		o0 = ((i0 & 0x01) << 0) | ((i1 & 0x01) << 1) | ((i2 & 0x01) << 2) | ((i3 & 0x01) << 3) | ((i4 & 0x01) << 4) | ((i5 & 0x01) << 5) | ((i6 & 0x01) << 6) | ((i7 & 0x01) << 7);
		o1 = ((i0 & 0x02) >> 1) | ((i1 & 0x02) << 0) | ((i2 & 0x02) << 1) | ((i3 & 0x02) << 2) | ((i4 & 0x02) << 3) | ((i5 & 0x02) << 4) | ((i6 & 0x02) << 5) | ((i7 & 0x02) << 6);
		o2 = ((i0 & 0x04) >> 2) | ((i1 & 0x04) >> 1) | ((i2 & 0x04) << 0) | ((i3 & 0x04) << 1) | ((i4 & 0x04) << 2) | ((i5 & 0x04) << 3) | ((i6 & 0x04) << 4) | ((i7 & 0x04) << 5);
		o3 = ((i0 & 0x08) >> 3) | ((i1 & 0x08) >> 2) | ((i2 & 0x08) >> 1) | ((i3 & 0x08) << 0) | ((i4 & 0x08) << 1) | ((i5 & 0x08) << 2) | ((i6 & 0x08) << 3) | ((i7 & 0x08) << 4);
		o4 = ((i0 & 0x10) >> 4) | ((i1 & 0x10) >> 3) | ((i2 & 0x10) >> 2) | ((i3 & 0x10) >> 1) | ((i4 & 0x10) << 0) | ((i5 & 0x10) << 1) | ((i6 & 0x10) << 2) | ((i7 & 0x10) << 3);
		o5 = ((i0 & 0x20) >> 5) | ((i1 & 0x20) >> 4) | ((i2 & 0x20) >> 3) | ((i3 & 0x20) >> 2) | ((i4 & 0x20) >> 1) | ((i5 & 0x20) << 0) | ((i6 & 0x20) << 1) | ((i7 & 0x20) << 2);
		o6 = ((i0 & 0x40) >> 6) | ((i1 & 0x40) >> 5) | ((i2 & 0x40) >> 4) | ((i3 & 0x40) >> 3) | ((i4 & 0x40) >> 2) | ((i5 & 0x40) >> 1) | ((i6 & 0x40) << 0) | ((i7 & 0x40) << 1);
		o7 = ((i0 & 0x80) >> 7) | ((i1 & 0x80) >> 6) | ((i2 & 0x80) >> 5) | ((i3 & 0x80) >> 4) | ((i4 & 0x80) >> 3) | ((i5 & 0x80) >> 2) | ((i6 & 0x80) >> 1) | ((i7 & 0x80) << 0);

		out[offset2 + 0 * BYTES_PER_WORD] = o0;
		out[offset2 + 1 * BYTES_PER_WORD] = o1;
		out[offset2 + 2 * BYTES_PER_WORD] = o2;
		out[offset2 + 3 * BYTES_PER_WORD] = o3;
		out[offset2 + 4 * BYTES_PER_WORD] = o4;
		out[offset2 + 5 * BYTES_PER_WORD] = o5;
		out[offset2 + 6 * BYTES_PER_WORD] = o6;
		out[offset2 + 7 * BYTES_PER_WORD] = o7;
	}

}

__global__ void transposeInvKernel(const u8 *in, u8 *out, const unsigned int blocks)
{


	int tx = threadIdx.x;
	int ty = threadIdx.y;

	int bx = blockIdx.x;
	int by = blockIdx.y;

	int block = bx * gridDim.y + by;

	if (block < blocks){

		register u8 i0, i1, i2, i3, i4, i5, i6, i7;
		register u8 o0, o1, o2, o3, o4, o5, o6, o7;


		int offset1, offset2;
		offset1 = block * (BLOCK_SIZE * BYTES_PER_WORD) + tx * BITS_PER_WORD + ty;
		offset2 = block * (BLOCK_SIZE * BYTES_PER_WORD) + ty * BLOCK_SIZE + tx;


		i0 = in[offset1 + 0 * BYTES_PER_WORD];
		i1 = in[offset1 + 1 * BYTES_PER_WORD];
		i2 = in[offset1 + 2 * BYTES_PER_WORD];
		i3 = in[offset1 + 3 * BYTES_PER_WORD];
		i4 = in[offset1 + 4 * BYTES_PER_WORD];
		i5 = in[offset1 + 5 * BYTES_PER_WORD];
		i6 = in[offset1 + 6 * BYTES_PER_WORD];
		i7 = in[offset1 + 7 * BYTES_PER_WORD];


		o0 = ((i0 & 0x01) << 0) | ((i1 & 0x01) << 1) | ((i2 & 0x01) << 2) | ((i3 & 0x01) << 3) | ((i4 & 0x01) << 4) | ((i5 & 0x01) << 5) | ((i6 & 0x01) << 6) | ((i7 & 0x01) << 7);
		o1 = ((i0 & 0x02) >> 1) | ((i1 & 0x02) << 0) | ((i2 & 0x02) << 1) | ((i3 & 0x02) << 2) | ((i4 & 0x02) << 3) | ((i5 & 0x02) << 4) | ((i6 & 0x02) << 5) | ((i7 & 0x02) << 6);
		o2 = ((i0 & 0x04) >> 2) | ((i1 & 0x04) >> 1) | ((i2 & 0x04) << 0) | ((i3 & 0x04) << 1) | ((i4 & 0x04) << 2) | ((i5 & 0x04) << 3) | ((i6 & 0x04) << 4) | ((i7 & 0x04) << 5);
		o3 = ((i0 & 0x08) >> 3) | ((i1 & 0x08) >> 2) | ((i2 & 0x08) >> 1) | ((i3 & 0x08) << 0) | ((i4 & 0x08) << 1) | ((i5 & 0x08) << 2) | ((i6 & 0x08) << 3) | ((i7 & 0x08) << 4);
		o4 = ((i0 & 0x10) >> 4) | ((i1 & 0x10) >> 3) | ((i2 & 0x10) >> 2) | ((i3 & 0x10) >> 1) | ((i4 & 0x10) << 0) | ((i5 & 0x10) << 1) | ((i6 & 0x10) << 2) | ((i7 & 0x10) << 3);
		o5 = ((i0 & 0x20) >> 5) | ((i1 & 0x20) >> 4) | ((i2 & 0x20) >> 3) | ((i3 & 0x20) >> 2) | ((i4 & 0x20) >> 1) | ((i5 & 0x20) << 0) | ((i6 & 0x20) << 1) | ((i7 & 0x20) << 2);
		o6 = ((i0 & 0x40) >> 6) | ((i1 & 0x40) >> 5) | ((i2 & 0x40) >> 4) | ((i3 & 0x40) >> 3) | ((i4 & 0x40) >> 2) | ((i5 & 0x40) >> 1) | ((i6 & 0x40) << 0) | ((i7 & 0x40) << 1);
		o7 = ((i0 & 0x80) >> 7) | ((i1 & 0x80) >> 6) | ((i2 & 0x80) >> 5) | ((i3 & 0x80) >> 4) | ((i4 & 0x80) >> 3) | ((i5 & 0x80) >> 2) | ((i6 & 0x80) >> 1) | ((i7 & 0x80) << 0);


		out[offset2 + 0 * BYTES_PER_BLOCK] = o0;
		out[offset2 + 1 * BYTES_PER_BLOCK] = o1;
		out[offset2 + 2 * BYTES_PER_BLOCK] = o2;
		out[offset2 + 3 * BYTES_PER_BLOCK] = o3;
		out[offset2 + 4 * BYTES_PER_BLOCK] = o4;
		out[offset2 + 5 * BYTES_PER_BLOCK] = o5;
		out[offset2 + 6 * BYTES_PER_BLOCK] = o6;
		out[offset2 + 7 * BYTES_PER_BLOCK] = o7;
	}

}


cudaError_t rijndaelEncryptBitwiseCuda(const u8 pt[], u8 ct[], const u32 rk[], const unsigned int plainTextLength, const unsigned int Nr){


	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	u32 words, blocks;
	words = ((plainTextLength - 1) / (BLOCK_SIZE * BYTES_PER_WORD) + 1) * BLOCK_SIZE;
	blocks = (words - 1) / BLOCK_SIZE + 1;


	u32 *devIn;
	u32 *devOut;
	u8  *devRk;


	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		return cudaStatus;
	}

	// cuda malloc
	start = startStopwatch();
	cudaStatus = cudaMalloc((void**)&devIn, words * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMalloc((void**)&devOut, words * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}
	cudaStatus = cudaMalloc((void**)&devRk, rkLength * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	totalTime = endStopwatch("Malloc (bitwise encryption)", start);

	start = startStopwatch();
	cudaStatus = cudaMemcpy(devIn, pt, plainTextLength * sizeof(u8), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMemcpy(devRk, rk, rkLength * sizeof(u32), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}


	totalTime += endStopwatch("Memory copy from host to device (bitwise encryption)", start);

	//!!!must be padded before!!!
	dim3 dimGridTransposing((blocks - 1) / GRID_DIM_Y + 1, GRID_DIM_Y, 1);
	dim3 dimBlockTransposing(BYTES_PER_BLOCK, BYTES_PER_WORD, 1);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	transposeKernel << <dimGridTransposing, dimBlockTransposing >> >((u8 *)devIn, (u8 *)devOut, blocks);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Transposing data kernel (encryption)", start);



	//!!!must be padded before!!!
	dim3 dimGridCrypto((blocks - 1) / GRID_DIM_Y + 1, GRID_DIM_Y, 1);
	dim3 dimBlockCrypto(4, 4, 8);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	rijndaelEncryptBitwiseKernel << <dimGridCrypto, dimBlockCrypto >> >(devOut, devIn, devRk, blocks, Nr);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Encryption kernel", start);


	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	transposeInvKernel << <dimGridTransposing, dimBlockTransposing >> >((u8 *)devIn, (u8 *)devOut, blocks);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Inverse transposing data kernel (encryption)", start);


	// Copy output vector from GPU buffer to host memory.
	start = startStopwatch();
	cudaStatus = cudaMemcpy(ct, devOut, plainTextLength * sizeof(u8), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}
	
	totalTime += endStopwatch("Copy from device to host (bitwise encryption)", start);

	start = startStopwatch();
	cudaFree(devIn);
	cudaFree(devOut);
	cudaFree(devRk);

	totalTime += endStopwatch("Freeing memory (bitwise encryption)", start);

	printDuration("Encryption", totalTime);

	return cudaStatus;
}

cudaError_t rijndaelDecryptBitwiseCuda(const u8 ct[], u8 pt[], const u32 rk[], const unsigned int plainTextLength, const unsigned int Nr){


	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	u32 words, blocks;
	words = ((plainTextLength - 1) / (BLOCK_SIZE * BYTES_PER_WORD) + 1) * BLOCK_SIZE;
	blocks = (words - 1) / BLOCK_SIZE + 1;


	u32 *devIn;
	u32 *devOut;
	u8  *devRk;


	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		return cudaStatus;
	}

	// cuda malloc
	start = startStopwatch();
	cudaStatus = cudaMalloc((void**)&devIn, words * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMalloc((void**)&devOut, words * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}
	cudaStatus = cudaMalloc((void**)&devRk, rkLength * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	totalTime = endStopwatch("Malloc (bitwise encryption)", start);

	start = startStopwatch();
	cudaStatus = cudaMemcpy(devIn, ct, plainTextLength * sizeof(u8), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMemcpy(devRk, rk, rkLength * sizeof(u32), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}


	totalTime += endStopwatch("Memory copy from host to device (bitwise decryption)", start);

	//!!!must be padded before!!!
	dim3 dimGridTransposing((blocks - 1) / GRID_DIM_Y + 1, GRID_DIM_Y, 1);
	dim3 dimBlockTransposing(BYTES_PER_BLOCK, BYTES_PER_WORD, 1);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	transposeKernel << <dimGridTransposing, dimBlockTransposing >> >((u8 *)devIn, (u8 *)devOut, blocks);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Transposing data kernel (decryption)", start);



	//!!!must be padded before!!!
	dim3 dimGridCrypto((blocks - 1) / GRID_DIM_Y + 1, GRID_DIM_Y, 1);
	dim3 dimBlockCrypto(4, 4, 8);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	rijndaelDecryptBitwiseKernel << <dimGridCrypto, dimBlockCrypto >> >(devOut, devIn, devRk, blocks, Nr);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Decryption kernel", start);


	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	transposeInvKernel << <dimGridTransposing, dimBlockTransposing >> >((u8 *)devIn, (u8 *)devOut, blocks);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Inverse transposing data kernel (decryption)", start);


	// Copy output vector from GPU buffer to host memory.
	start = startStopwatch();
	cudaStatus = cudaMemcpy(pt, devOut, plainTextLength * sizeof(u8), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}
	totalTime += endStopwatch("Copy from device to host (bitwise decryption)", start);

	start = startStopwatch();
	cudaFree(devIn);
	cudaFree(devOut);
	cudaFree(devRk);

	totalTime += endStopwatch("Freeing memory (bitwise decryption)", start);

	printDuration("Decryption", totalTime);

	return cudaStatus;
}

int rijndaelKeySetupEnc(u32 rk[], const u32 cipherKey[], const unsigned int keyLen) {
	int i = 0;
	u32 temp;

	rk[0] = cipherKey[0];
	rk[1] = cipherKey[1];
	rk[2] = cipherKey[2];
	rk[3] = cipherKey[3];

	if (keyLen == 16) {
		for (;;) {
			temp = rk[3];
			rk[4] = rk[0] ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];

			rk[5] = rk[1] ^ rk[4];
			rk[6] = rk[2] ^ rk[5];
			rk[7] = rk[3] ^ rk[6];
			if (++i == 10) {
				return 10;
			}
			rk += 4;
		}
	}
	rk[4] = cipherKey[4];
	rk[5] = cipherKey[5];

	if (keyLen == 24) {
		for (;;) {
			temp = rk[5];
			rk[6] = rk[0] ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[7] = rk[1] ^ rk[6];
			rk[8] = rk[2] ^ rk[7];
			rk[9] = rk[3] ^ rk[8];
			if (++i == 8) {
				return 12;
			}
			rk[10] = rk[4] ^ rk[9];
			rk[11] = rk[5] ^ rk[10];
			rk += 6;
		}
	}
	rk[6] = cipherKey[6];
	rk[7] = cipherKey[7];
	if (keyLen == 32) {
		for (;;) {
			temp = rk[7];
			rk[8] = rk[0] ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[9] = rk[1] ^ rk[8];
			rk[10] = rk[2] ^ rk[9];
			rk[11] = rk[3] ^ rk[10];
			if (++i == 7) {
				return 14;
			}
			temp = rk[11];
			rk[12] = rk[4] ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0x000000ff);
			rk[13] = rk[5] ^ rk[12];
			rk[14] = rk[6] ^ rk[13];
			rk[15] = rk[7] ^ rk[14];

			rk += 8;
		}
	}
	return 0;
}



int rijndaelKeySetupDec(u32 rk[], const u32 cipherKey[], const unsigned int keyLen) {
	int Nr, i, j;
	u32 temp;

	/* expand the cipher key: */
	Nr = rijndaelKeySetupEnc(rk, (u32 *)cipherKey, keyLen);
	/* invert the order of the round keys: */
	for (i = 0, j = 4 * Nr; i < j; i += 4, j -= 4) {
		temp = rk[i]; rk[i] = rk[j]; rk[j] = temp;
		temp = rk[i + 1]; rk[i + 1] = rk[j + 1]; rk[j + 1] = temp;
		temp = rk[i + 2]; rk[i + 2] = rk[j + 2]; rk[j + 2] = temp;
		temp = rk[i + 3]; rk[i + 3] = rk[j + 3]; rk[j + 3] = temp;
	}
	/* apply the inverse MixColumn transform to all round keys but the first and the last: */

	for (i = 1; i < Nr; i++) {
		rk += 4;


		rk[0] =
			bigTd[0 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[0]) & 0xff)] & 0xff)] ^
			bigTd[1 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[0] >> 8) & 0xff)] & 0xff)] ^
			bigTd[2 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[0] >> 16) & 0xff)] & 0xff)] ^
			bigTd[3 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + (rk[0] >> 24)] & 0xff)];


		rk[1] =
			bigTd[0 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[1]) & 0xff)] & 0xff)] ^
			bigTd[1 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[1] >> 8) & 0xff)] & 0xff)] ^
			bigTd[2 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[1] >> 16) & 0xff)] & 0xff)] ^
			bigTd[3 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + (rk[1] >> 24)] & 0xff)];


		rk[2] =
			bigTd[0 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[2]) & 0xff)] & 0xff)] ^
			bigTd[1 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[2] >> 8) & 0xff)] & 0xff)] ^
			bigTd[2 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[2] >> 16) & 0xff)] & 0xff)] ^
			bigTd[3 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + (rk[2] >> 24)] & 0xff)];
		
		rk[3] =
			bigTd[0 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[3]) & 0xff)] & 0xff)] ^
			bigTd[1 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[3] >> 8) & 0xff)] & 0xff)] ^
			bigTd[2 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + ((rk[3] >> 16) & 0xff)] & 0xff)] ^
			bigTd[3 * T_TABLE_SIZE + (bigTe[4 * T_TABLE_SIZE + (rk[3] >> 24)] & 0xff)];



	}
	return Nr;
}
int rijndaelEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
{

	boost::chrono::high_resolution_clock::time_point start;

	cudaFree(NULL);
	u32 rk[rkLength];
	
	u8 *pinnedPt;
	u8 *pinnedCt;

	cudaMallocHost(&pinnedPt, plainTextLength);
	cudaMallocHost(&pinnedCt, plainTextLength);

	memcpy(pinnedPt, pt, plainTextLength);

	start = startStopwatch();
	unsigned int Nr = rijndaelKeySetupEnc(rk, (u32 *)cipherKey, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);
	
	
    cudaError_t cudaStatus = rijndaelEncryptBitwiseCuda(pinnedPt, pinnedCt, rk, plainTextLength, Nr);
	
	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Encryption failed!");
        return 1;
    }

	memcpy(ct, pinnedCt, plainTextLength);
	cudaFreeHost(pinnedPt);
	cudaFreeHost(pinnedCt);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }
	
    return 0;
}

int rijndaelDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
{

	boost::chrono::high_resolution_clock::time_point start;

	cudaFree(NULL);
	u32 rk[rkLength];
	
	u8 *pinnedCt;
	u8 *pinnedPt;

	cudaMallocHost(&pinnedCt, plainTextLength);
	cudaMallocHost(&pinnedPt, plainTextLength);

	memcpy(pinnedCt, ct, plainTextLength);

	start = startStopwatch();
	unsigned int Nr = rijndaelKeySetupDec(rk, (u32 *)cipherKey, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);
		
    cudaError_t cudaStatus = rijndaelDecryptBitwiseCuda(pinnedCt, pinnedPt, rk, plainTextLength, Nr);
	
	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Decription failed!");
        return 1;
    }

	memcpy(pt, pinnedPt, plainTextLength);
	cudaFreeHost(pinnedCt);
	cudaFreeHost(pinnedPt);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }
	
    return 0;
}


