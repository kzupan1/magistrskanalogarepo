#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "serpent-cuda.cuh"
#include "serpent-tables.cuh"
#include "measure-time.h"
#include <stdlib.h>
#include <stdio.h>
#include <boost/chrono.hpp>

__constant__ u32 k_constant[WORDS_PER_KEY_SCHEDULE];

__global__ void createCTRKernel(uint4 *out, const int bundles, const u32 n0, const u32 n1){

	u64 i = blockIdx.x * blockDim.x + threadIdx.x;

	if (i < bundles * BITS_PER_WORD){
		out[i] = { n0, n1, ((i & 0xffffffff00000000) >> 32), (i & 0xffffffff) };

	}
}

__global__ void xorCtr(const uint4 *pt, const uint4 *ctr, uint4 *ct, int bundles){

	int i = blockIdx.x * blockDim.x + threadIdx.x;


	if (i < bundles * BITS_PER_WORD){

		ct[i] = { pt[i].x ^ ctr[i].x, pt[i].y ^ ctr[i].y, pt[i].z ^ ctr[i].z, pt[i].w ^ ctr[i].w };

	}




}

__global__ void transposeKernel(u8 *in, u8 *out, const int bundles)
{


	int tx = threadIdx.x;
	int ty = threadIdx.y;

	int by = blockIdx.y;
	int tz = threadIdx.z;

	int bundle = (by * blockDim.z + tz) * gridDim.z + blockIdx.z;

	if (bundle < bundles){
		register u32 i0, i1, i2, i3, i4, i5, i6, i7;
		register u32 o0, o1, o2, o3, o4, o5, o6, o7;

		in += bundle * (BLOCK_SIZE * BYTES_PER_WORD);
		out += bundle * (BLOCK_SIZE * BYTES_PER_WORD);

		int offset1, offset2;
		offset1 = tx * BLOCK_SIZE + ty;
		offset2 = ty * BITS_PER_WORD + tx;

		i0 = in[offset1 + 0 * BYTES_PER_BLOCK];
		i1 = in[offset1 + 1 * BYTES_PER_BLOCK];
		i2 = in[offset1 + 2 * BYTES_PER_BLOCK];
		i3 = in[offset1 + 3 * BYTES_PER_BLOCK];
		i4 = in[offset1 + 4 * BYTES_PER_BLOCK];
		i5 = in[offset1 + 5 * BYTES_PER_BLOCK];
		i6 = in[offset1 + 6 * BYTES_PER_BLOCK];
		i7 = in[offset1 + 7 * BYTES_PER_BLOCK];

		o0 = ((i0 & 0x01) << 0) | ((i1 & 0x01) << 1) | ((i2 & 0x01) << 2) | ((i3 & 0x01) << 3) | ((i4 & 0x01) << 4) | ((i5 & 0x01) << 5) | ((i6 & 0x01) << 6) | ((i7 & 0x01) << 7);
		o1 = ((i0 & 0x02) >> 1) | ((i1 & 0x02) << 0) | ((i2 & 0x02) << 1) | ((i3 & 0x02) << 2) | ((i4 & 0x02) << 3) | ((i5 & 0x02) << 4) | ((i6 & 0x02) << 5) | ((i7 & 0x02) << 6);
		o2 = ((i0 & 0x04) >> 2) | ((i1 & 0x04) >> 1) | ((i2 & 0x04) << 0) | ((i3 & 0x04) << 1) | ((i4 & 0x04) << 2) | ((i5 & 0x04) << 3) | ((i6 & 0x04) << 4) | ((i7 & 0x04) << 5);
		o3 = ((i0 & 0x08) >> 3) | ((i1 & 0x08) >> 2) | ((i2 & 0x08) >> 1) | ((i3 & 0x08) << 0) | ((i4 & 0x08) << 1) | ((i5 & 0x08) << 2) | ((i6 & 0x08) << 3) | ((i7 & 0x08) << 4);
		o4 = ((i0 & 0x10) >> 4) | ((i1 & 0x10) >> 3) | ((i2 & 0x10) >> 2) | ((i3 & 0x10) >> 1) | ((i4 & 0x10) << 0) | ((i5 & 0x10) << 1) | ((i6 & 0x10) << 2) | ((i7 & 0x10) << 3);
		o5 = ((i0 & 0x20) >> 5) | ((i1 & 0x20) >> 4) | ((i2 & 0x20) >> 3) | ((i3 & 0x20) >> 2) | ((i4 & 0x20) >> 1) | ((i5 & 0x20) << 0) | ((i6 & 0x20) << 1) | ((i7 & 0x20) << 2);
		o6 = ((i0 & 0x40) >> 6) | ((i1 & 0x40) >> 5) | ((i2 & 0x40) >> 4) | ((i3 & 0x40) >> 3) | ((i4 & 0x40) >> 2) | ((i5 & 0x40) >> 1) | ((i6 & 0x40) << 0) | ((i7 & 0x40) << 1);
		o7 = ((i0 & 0x80) >> 7) | ((i1 & 0x80) >> 6) | ((i2 & 0x80) >> 5) | ((i3 & 0x80) >> 4) | ((i4 & 0x80) >> 3) | ((i5 & 0x80) >> 2) | ((i6 & 0x80) >> 1) | ((i7 & 0x80) << 0);

		out[offset2 + 0 * BYTES_PER_WORD] = o0;
		out[offset2 + 1 * BYTES_PER_WORD] = o1;
		out[offset2 + 2 * BYTES_PER_WORD] = o2;
		out[offset2 + 3 * BYTES_PER_WORD] = o3;
		out[offset2 + 4 * BYTES_PER_WORD] = o4;
		out[offset2 + 5 * BYTES_PER_WORD] = o5;
		out[offset2 + 6 * BYTES_PER_WORD] = o6;
		out[offset2 + 7 * BYTES_PER_WORD] = o7;
	}

}

__global__ void transposeInvKernel(u8 *in, u8 *out, const int bundles)
{


	int tx = threadIdx.x;
	int ty = threadIdx.y;

	int bx = blockIdx.x;
	int tz = threadIdx.z;

	int bundle = (bx * blockDim.z + tz) * gridDim.z + blockIdx.z;

	if (bundle < bundles){

		register u32 i0, i1, i2, i3, i4, i5, i6, i7;
		register u32 o0, o1, o2, o3, o4, o5, o6, o7;

		in += bundle * (BLOCK_SIZE * BYTES_PER_WORD);
		out += bundle * (BLOCK_SIZE * BYTES_PER_WORD);

		int offset1, offset2;
		offset1 = tx * BITS_PER_WORD + ty;
		offset2 = ty * BLOCK_SIZE + tx;


		i0 = in[offset1 + 0 * BYTES_PER_WORD];
		i1 = in[offset1 + 1 * BYTES_PER_WORD];
		i2 = in[offset1 + 2 * BYTES_PER_WORD];
		i3 = in[offset1 + 3 * BYTES_PER_WORD];
		i4 = in[offset1 + 4 * BYTES_PER_WORD];
		i5 = in[offset1 + 5 * BYTES_PER_WORD];
		i6 = in[offset1 + 6 * BYTES_PER_WORD];
		i7 = in[offset1 + 7 * BYTES_PER_WORD];


		o0 = ((i0 & 0x01) << 0) | ((i1 & 0x01) << 1) | ((i2 & 0x01) << 2) | ((i3 & 0x01) << 3) | ((i4 & 0x01) << 4) | ((i5 & 0x01) << 5) | ((i6 & 0x01) << 6) | ((i7 & 0x01) << 7);
		o1 = ((i0 & 0x02) >> 1) | ((i1 & 0x02) << 0) | ((i2 & 0x02) << 1) | ((i3 & 0x02) << 2) | ((i4 & 0x02) << 3) | ((i5 & 0x02) << 4) | ((i6 & 0x02) << 5) | ((i7 & 0x02) << 6);
		o2 = ((i0 & 0x04) >> 2) | ((i1 & 0x04) >> 1) | ((i2 & 0x04) << 0) | ((i3 & 0x04) << 1) | ((i4 & 0x04) << 2) | ((i5 & 0x04) << 3) | ((i6 & 0x04) << 4) | ((i7 & 0x04) << 5);
		o3 = ((i0 & 0x08) >> 3) | ((i1 & 0x08) >> 2) | ((i2 & 0x08) >> 1) | ((i3 & 0x08) << 0) | ((i4 & 0x08) << 1) | ((i5 & 0x08) << 2) | ((i6 & 0x08) << 3) | ((i7 & 0x08) << 4);
		o4 = ((i0 & 0x10) >> 4) | ((i1 & 0x10) >> 3) | ((i2 & 0x10) >> 2) | ((i3 & 0x10) >> 1) | ((i4 & 0x10) << 0) | ((i5 & 0x10) << 1) | ((i6 & 0x10) << 2) | ((i7 & 0x10) << 3);
		o5 = ((i0 & 0x20) >> 5) | ((i1 & 0x20) >> 4) | ((i2 & 0x20) >> 3) | ((i3 & 0x20) >> 2) | ((i4 & 0x20) >> 1) | ((i5 & 0x20) << 0) | ((i6 & 0x20) << 1) | ((i7 & 0x20) << 2);
		o6 = ((i0 & 0x40) >> 6) | ((i1 & 0x40) >> 5) | ((i2 & 0x40) >> 4) | ((i3 & 0x40) >> 3) | ((i4 & 0x40) >> 2) | ((i5 & 0x40) >> 1) | ((i6 & 0x40) << 0) | ((i7 & 0x40) << 1);
		o7 = ((i0 & 0x80) >> 7) | ((i1 & 0x80) >> 6) | ((i2 & 0x80) >> 5) | ((i3 & 0x80) >> 4) | ((i4 & 0x80) >> 3) | ((i5 & 0x80) >> 2) | ((i6 & 0x80) >> 1) | ((i7 & 0x80) << 0);


		out[offset2 + 0 * BYTES_PER_BLOCK] = o0;
		out[offset2 + 1 * BYTES_PER_BLOCK] = o1;
		out[offset2 + 2 * BYTES_PER_BLOCK] = o2;
		out[offset2 + 3 * BYTES_PER_BLOCK] = o3;
		out[offset2 + 4 * BYTES_PER_BLOCK] = o4;
		out[offset2 + 5 * BYTES_PER_BLOCK] = o5;
		out[offset2 + 6 * BYTES_PER_BLOCK] = o6;
		out[offset2 + 7 * BYTES_PER_BLOCK] = o7;
	}

}


__global__ void serpentEncryptBitwiseKernel(u32 *pt, u32 *ct, const int bundles)
{
	

	int tx = threadIdx.x;
	int ty = threadIdx.y;

	int bundle = blockIdx.x * blockDim.y + threadIdx.y;
	
	
	__shared__ u32 data_sh[8][32];

	if (bundle < bundles){
		int offset = bundle * BLOCK_SIZE;
		pt += offset;
		ct += offset;

		register u32 r0, r1, r2, r3, r4;

		r0 = pt[0 * BITS_PER_WORD + tx];
		r1 = pt[1 * BITS_PER_WORD + tx];
		r2 = pt[2 * BITS_PER_WORD + tx];
		r3 = pt[3 * BITS_PER_WORD + tx];

		K(r0,r1,r2,r3,0);
		S0(r0,r1,r2,r3,r4);	LK(r2,r1,r3,r0,r4,1);
		S1(r2,r1,r3,r0,r4);	LK(r4,r3,r0,r2,r1,2);
		S2(r4,r3,r0,r2,r1);	LK(r1,r3,r4,r2,r0,3);
		S3(r1,r3,r4,r2,r0);	LK(r2,r0,r3,r1,r4,4);
		S4(r2,r0,r3,r1,r4);	LK(r0,r3,r1,r4,r2,5);
		S5(r0,r3,r1,r4,r2);	LK(r2,r0,r3,r4,r1,6);
		S6(r2,r0,r3,r4,r1);	LK(r3,r1,r0,r4,r2,7);
		S7(r3,r1,r0,r4,r2);	LK(r2,r0,r4,r3,r1,8);
		S0(r2,r0,r4,r3,r1);	LK(r4,r0,r3,r2,r1,9);
		S1(r4,r0,r3,r2,r1);	LK(r1,r3,r2,r4,r0,10);
		S2(r1,r3,r2,r4,r0);	LK(r0,r3,r1,r4,r2,11);
		S3(r0,r3,r1,r4,r2);	LK(r4,r2,r3,r0,r1,12);
		S4(r4,r2,r3,r0,r1);	LK(r2,r3,r0,r1,r4,13);
		S5(r2,r3,r0,r1,r4);	LK(r4,r2,r3,r1,r0,14);
		S6(r4,r2,r3,r1,r0);	LK(r3,r0,r2,r1,r4,15);
		S7(r3,r0,r2,r1,r4);	LK(r4,r2,r1,r3,r0,16);
		S0(r4,r2,r1,r3,r0);	LK(r1,r2,r3,r4,r0,17);
		S1(r1,r2,r3,r4,r0);	LK(r0,r3,r4,r1,r2,18);
		S2(r0,r3,r4,r1,r2);	LK(r2,r3,r0,r1,r4,19);
		S3(r2,r3,r0,r1,r4);	LK(r1,r4,r3,r2,r0,20);
		S4(r1,r4,r3,r2,r0);	LK(r4,r3,r2,r0,r1,21);
		S5(r4,r3,r2,r0,r1);	LK(r1,r4,r3,r0,r2,22);
		S6(r1,r4,r3,r0,r2);	LK(r3,r2,r4,r0,r1,23);
		S7(r3,r2,r4,r0,r1);	LK(r1,r4,r0,r3,r2,24);
		S0(r1,r4,r0,r3,r2);	LK(r0,r4,r3,r1,r2,25);
		S1(r0,r4,r3,r1,r2);	LK(r2,r3,r1,r0,r4,26);
		S2(r2,r3,r1,r0,r4);	LK(r4,r3,r2,r0,r1,27);
		S3(r4,r3,r2,r0,r1);	LK(r0,r1,r3,r4,r2,28);
		S4(r0,r1,r3,r4,r2);	LK(r1,r3,r4,r2,r0,29);
		S5(r1,r3,r4,r2,r0);	LK(r0,r1,r3,r2,r4,30);
		S6(r0,r1,r3,r2,r4);	LK(r3,r4,r1,r2,r0,31);
		S7(r3,r4,r1,r2,r0);	 K(r0,r1,r2,r3,32);

		ct[0 * BITS_PER_WORD + tx] = r0;
		ct[1 * BITS_PER_WORD + tx] = r1;
		ct[2 * BITS_PER_WORD + tx] = r2;
		ct[3 * BITS_PER_WORD + tx] = r3;

	}
	

	

}


cudaError_t serpentEncryptBitwiseCuda(const uint4 pt[], uint4 ct[], const u32 k[], const int blocks){


	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	int bundles;
	bundles = (blocks - 1) / BITS_PER_WORD + 1;

	uint4 *devIn;
	uint4 *devOut;

	uint4 *devPt;
	uint4 *devCt;

	srand(0);
	u32 n0 = rand();
	u32 n1 = rand();
	ct[0] = { n0, n1, 0, 0 };

	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		return cudaStatus;
	}

	// cuda malloc
	start = startStopwatch();
	cudaStatus = cudaMalloc((void**)&devIn, bundles * sizeof(u32) * BLOCK_SIZE);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMalloc((void**)&devOut, bundles * sizeof(u32) * BLOCK_SIZE);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}
	
	cudaHostGetDevicePointer((void **)&devPt, (void *)pt, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	cudaHostGetDevicePointer((void **)&devCt, (void *)ct, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}

	endStopwatch("Malloc & GetDevicePointer (bitwise encryption)", start);

	devCt++;

	start = startStopwatch();
	

	cudaStatus = cudaMemcpyToSymbol(k_constant, k, WORDS_PER_KEY_SCHEDULE * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}


	totalTime = endStopwatch("Memory copy from host to device (bitwise encryption)", start);

	dim3 dimGridCreateCtr((bundles - 1) / 8 + 1, 1, 1);
	dim3 dimBlockCreateCtr(THREAD_BLOCK_SIZE, 1, 1);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	createCTRKernel << <dimGridCreateCtr, dimBlockCreateCtr >> >(devIn, bundles, n0, n1);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Creating counter kernel (encryption)", start);

	dim3 dimGridTransposing(1, (bundles - 1) / 8 + 1, 4);
	dim3 dimBlockTransposing(BYTES_PER_WORD, BYTES_PER_BLOCK, 2);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	transposeKernel << <dimGridTransposing, dimBlockTransposing >> >((u8 *)devIn, (u8 *)devOut, bundles);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Transposing data kernel (encryption)", start);


	//!!!must be padded before!!!
	dim3 dimGridCrypto((bundles - 1) / 8 + 1, 1, 1);
	dim3 dimBlockCrypto(CRYPTO_THREAD_BLOCK_SIZE_X, 8, 1);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	serpentEncryptBitwiseKernel << <dimGridCrypto, dimBlockCrypto >> >((u32 *)devOut, (u32 *) devIn, bundles);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Encryption kernel", start);

	dim3 dimGridInverseTransposing((bundles - 1) / 8 + 1, 1, 4);
	dim3 dimBlockInverseTransposing(BYTES_PER_BLOCK, BYTES_PER_WORD, 2);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	transposeInvKernel << <dimGridInverseTransposing, dimBlockInverseTransposing >> >((u8 *)devIn, (u8 *)devOut, bundles);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Inverse transposing data kernel (encryption)", start);

	dim3 dimGridXorCTR((bundles - 1) / 8 + 1, 1, 1);
	dim3 dimBlockXorCTR(THREAD_BLOCK_SIZE, 1, 1);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	xorCtr << <dimGridXorCTR, dimBlockXorCTR >> >(devPt, devOut, devCt, bundles);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Xoring data kernel (encryption)", start);


	start = startStopwatch();
	cudaFree(devIn);
	cudaFree(devOut);

	endStopwatch("Freeing memory (bitwise encryption)", start);

	printDuration("Encryption", totalTime);

	return cudaStatus;
}

cudaError_t serpentDecryptBitwiseCuda(const uint4 ct[], uint4 pt[], const u32 k[], const int blocks){


	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	int bundles;
	bundles = (blocks - 1) / BITS_PER_WORD + 1;


	uint4 *devIn;
	uint4 *devOut;

	uint4 *devCt;
	uint4 *devPt;

	u32 n0 = ct[0].x;
	u32 n1 = ct[0].y;



	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		return cudaStatus;
	}

	// cuda malloc
	start = startStopwatch();
	cudaStatus = cudaMalloc((void**)&devIn, bundles * sizeof(u32) * BLOCK_SIZE);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMalloc((void**)&devOut, bundles * sizeof(u32) * BLOCK_SIZE);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaHostGetDevicePointer((void **)&devCt, (void *)ct, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}

	cudaHostGetDevicePointer((void **)&devPt, (void *)pt, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	

	endStopwatch("Malloc & GetDevicePointer (bitwise decryption)", start);

	devCt++;

	start = startStopwatch();


	cudaStatus = cudaMemcpyToSymbol(k_constant, k, WORDS_PER_KEY_SCHEDULE * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}


	totalTime = endStopwatch("Memory copy from host to device (bitwise decryption)", start);

	dim3 dimGridCreateCtr((bundles - 1) / 8 + 1, 1, 1);
	dim3 dimBlockCreateCtr(THREAD_BLOCK_SIZE, 1, 1);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	createCTRKernel << <dimGridCreateCtr, dimBlockCreateCtr >> >(devIn, bundles, n0, n1);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Creating counter kernel (decryption)", start);

	dim3 dimGridTransposing(1, (bundles - 1) / 8 + 1, 4);
	dim3 dimBlockTransposing(BYTES_PER_WORD, BYTES_PER_BLOCK, 2);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	transposeKernel << <dimGridTransposing, dimBlockTransposing >> >((u8 *)devIn, (u8 *)devOut, bundles);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Transposing data kernel (decryption)", start);


	//!!!must be padded before!!!
	dim3 dimGridCrypto((bundles - 1) / 8 + 1, 1, 1);
	dim3 dimBlockCrypto(CRYPTO_THREAD_BLOCK_SIZE_X, 8, 1);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	serpentEncryptBitwiseKernel << <dimGridCrypto, dimBlockCrypto >> >((u32 *)devOut, (u32 *)devIn, bundles);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Decryption kernel", start);

	dim3 dimGridInverseTransposing((bundles - 1) / 8 + 1, 1, 4);
	dim3 dimBlockInverseTransposing(BYTES_PER_BLOCK, BYTES_PER_WORD, 2);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	transposeInvKernel << <dimGridInverseTransposing, dimBlockInverseTransposing >> >((u8 *)devIn, (u8 *)devOut, bundles);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Inverse transposing data kernel (decryption)", start);

	dim3 dimGridXorCTR((bundles - 1) / 8 + 1, 1, 1);
	dim3 dimBlockXorCTR(THREAD_BLOCK_SIZE, 1, 1);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	xorCtr << <dimGridXorCTR, dimBlockXorCTR >> >(devCt, devOut, devPt, bundles);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Xoring data kernel (decryption)", start);

	start = startStopwatch();
	cudaFree(devIn);
	cudaFree(devOut);
	
	endStopwatch("Freeing memory (bitwise decryption)", start);

	printDuration("Decryption", totalTime);

	return cudaStatus;
}


int makeKey(const u8 cipherKey[], u32 k[WORDS_PER_KEY_SCHEDULE], const unsigned int keyLen ){

	u8  *k8 = (u8 *)k;
	register u32 r0,r1,r2,r3,r4;
	unsigned int i;

	for (i = 0; i < keyLen; ++i)
		k8[i] = cipherKey[i];
	if (i < BYTES_PER_KEY)
		k8[i++] = 1;
	while (i < BYTES_PER_KEY)
		k8[i++] = 0;

	r0 = k[3];
	r1 = k[4];
	r2 = k[5];
	r3 = k[6];
	r4 = k[7];

	keyiter(k[0],r0,r4,r2,0,0);
	keyiter(k[1],r1,r0,r3,1,1);
	keyiter(k[2],r2,r1,r4,2,2);
	keyiter(k[3],r3,r2,r0,3,3);
	keyiter(k[4],r4,r3,r1,4,4);
	keyiter(k[5],r0,r4,r2,5,5);
	keyiter(k[6],r1,r0,r3,6,6);
	keyiter(k[7],r2,r1,r4,7,7);


	keyiter(k[  0],r3,r2,r0,  8,  8); keyiter(k[  1],r4,r3,r1,  9,  9);
	keyiter(k[  2],r0,r4,r2, 10, 10); keyiter(k[  3],r1,r0,r3, 11, 11);
	keyiter(k[  4],r2,r1,r4, 12, 12); keyiter(k[  5],r3,r2,r0, 13, 13);
	keyiter(k[  6],r4,r3,r1, 14, 14); keyiter(k[  7],r0,r4,r2, 15, 15);
	keyiter(k[  8],r1,r0,r3, 16, 16); keyiter(k[  9],r2,r1,r4, 17, 17);
	keyiter(k[ 10],r3,r2,r0, 18, 18); keyiter(k[ 11],r4,r3,r1, 19, 19);
	keyiter(k[ 12],r0,r4,r2, 20, 20); keyiter(k[ 13],r1,r0,r3, 21, 21);
	keyiter(k[ 14],r2,r1,r4, 22, 22); keyiter(k[ 15],r3,r2,r0, 23, 23);
	keyiter(k[ 16],r4,r3,r1, 24, 24); keyiter(k[ 17],r0,r4,r2, 25, 25);
	keyiter(k[ 18],r1,r0,r3, 26, 26); keyiter(k[ 19],r2,r1,r4, 27, 27);
	keyiter(k[ 20],r3,r2,r0, 28, 28); keyiter(k[ 21],r4,r3,r1, 29, 29);
	keyiter(k[ 22],r0,r4,r2, 30, 30); keyiter(k[ 23],r1,r0,r3, 31, 31);

	k += 50;

	keyiter(k[-26],r2,r1,r4, 32,-18); keyiter(k[-25],r3,r2,r0, 33,-17);
	keyiter(k[-24],r4,r3,r1, 34,-16); keyiter(k[-23],r0,r4,r2, 35,-15);
	keyiter(k[-22],r1,r0,r3, 36,-14); keyiter(k[-21],r2,r1,r4, 37,-13);
	keyiter(k[-20],r3,r2,r0, 38,-12); keyiter(k[-19],r4,r3,r1, 39,-11);
	keyiter(k[-18],r0,r4,r2, 40,-10); keyiter(k[-17],r1,r0,r3, 41, -9);
	keyiter(k[-16],r2,r1,r4, 42, -8); keyiter(k[-15],r3,r2,r0, 43, -7);
	keyiter(k[-14],r4,r3,r1, 44, -6); keyiter(k[-13],r0,r4,r2, 45, -5);
	keyiter(k[-12],r1,r0,r3, 46, -4); keyiter(k[-11],r2,r1,r4, 47, -3);
	keyiter(k[-10],r3,r2,r0, 48, -2); keyiter(k[ -9],r4,r3,r1, 49, -1);
	keyiter(k[ -8],r0,r4,r2, 50,  0); keyiter(k[ -7],r1,r0,r3, 51,  1);
	keyiter(k[ -6],r2,r1,r4, 52,  2); keyiter(k[ -5],r3,r2,r0, 53,  3);
	keyiter(k[ -4],r4,r3,r1, 54,  4); keyiter(k[ -3],r0,r4,r2, 55,  5);
	keyiter(k[ -2],r1,r0,r3, 56,  6); keyiter(k[ -1],r2,r1,r4, 57,  7);
	keyiter(k[  0],r3,r2,r0, 58,  8); keyiter(k[  1],r4,r3,r1, 59,  9);
	keyiter(k[  2],r0,r4,r2, 60, 10); keyiter(k[  3],r1,r0,r3, 61, 11);
	keyiter(k[  4],r2,r1,r4, 62, 12); keyiter(k[  5],r3,r2,r0, 63, 13);
	keyiter(k[  6],r4,r3,r1, 64, 14); keyiter(k[  7],r0,r4,r2, 65, 15);
	keyiter(k[  8],r1,r0,r3, 66, 16); keyiter(k[  9],r2,r1,r4, 67, 17);
	keyiter(k[ 10],r3,r2,r0, 68, 18); keyiter(k[ 11],r4,r3,r1, 69, 19);
	keyiter(k[ 12],r0,r4,r2, 70, 20); keyiter(k[ 13],r1,r0,r3, 71, 21);
	keyiter(k[ 14],r2,r1,r4, 72, 22); keyiter(k[ 15],r3,r2,r0, 73, 23);
	keyiter(k[ 16],r4,r3,r1, 74, 24); keyiter(k[ 17],r0,r4,r2, 75, 25);
	keyiter(k[ 18],r1,r0,r3, 76, 26); keyiter(k[ 19],r2,r1,r4, 77, 27);
	keyiter(k[ 20],r3,r2,r0, 78, 28); keyiter(k[ 21],r4,r3,r1, 79, 29);
	keyiter(k[ 22],r0,r4,r2, 80, 30); keyiter(k[ 23],r1,r0,r3, 81, 31);

	k += 50;

	keyiter(k[-26],r2,r1,r4, 82,-18); keyiter(k[-25],r3,r2,r0, 83,-17);
	keyiter(k[-24],r4,r3,r1, 84,-16); keyiter(k[-23],r0,r4,r2, 85,-15);
	keyiter(k[-22],r1,r0,r3, 86,-14); keyiter(k[-21],r2,r1,r4, 87,-13);
	keyiter(k[-20],r3,r2,r0, 88,-12); keyiter(k[-19],r4,r3,r1, 89,-11);
	keyiter(k[-18],r0,r4,r2, 90,-10); keyiter(k[-17],r1,r0,r3, 91, -9);
	keyiter(k[-16],r2,r1,r4, 92, -8); keyiter(k[-15],r3,r2,r0, 93, -7);
	keyiter(k[-14],r4,r3,r1, 94, -6); keyiter(k[-13],r0,r4,r2, 95, -5);
	keyiter(k[-12],r1,r0,r3, 96, -4); keyiter(k[-11],r2,r1,r4, 97, -3);
	keyiter(k[-10],r3,r2,r0, 98, -2); keyiter(k[ -9],r4,r3,r1, 99, -1);
	keyiter(k[ -8],r0,r4,r2,100,  0); keyiter(k[ -7],r1,r0,r3,101,  1);
	keyiter(k[ -6],r2,r1,r4,102,  2); keyiter(k[ -5],r3,r2,r0,103,  3);
	keyiter(k[ -4],r4,r3,r1,104,  4); keyiter(k[ -3],r0,r4,r2,105,  5);
	keyiter(k[ -2],r1,r0,r3,106,  6); keyiter(k[ -1],r2,r1,r4,107,  7);
	keyiter(k[  0],r3,r2,r0,108,  8); keyiter(k[  1],r4,r3,r1,109,  9);
	keyiter(k[  2],r0,r4,r2,110, 10); keyiter(k[  3],r1,r0,r3,111, 11);
	keyiter(k[  4],r2,r1,r4,112, 12); keyiter(k[  5],r3,r2,r0,113, 13);
	keyiter(k[  6],r4,r3,r1,114, 14); keyiter(k[  7],r0,r4,r2,115, 15);
	keyiter(k[  8],r1,r0,r3,116, 16); keyiter(k[  9],r2,r1,r4,117, 17);
	keyiter(k[ 10],r3,r2,r0,118, 18); keyiter(k[ 11],r4,r3,r1,119, 19);
	keyiter(k[ 12],r0,r4,r2,120, 20); keyiter(k[ 13],r1,r0,r3,121, 21);
	keyiter(k[ 14],r2,r1,r4,122, 22); keyiter(k[ 15],r3,r2,r0,123, 23);
	keyiter(k[ 16],r4,r3,r1,124, 24); keyiter(k[ 17],r0,r4,r2,125, 25);
	keyiter(k[ 18],r1,r0,r3,126, 26); keyiter(k[ 19],r2,r1,r4,127, 27);
	keyiter(k[ 20],r3,r2,r0,128, 28); keyiter(k[ 21],r4,r3,r1,129, 29);
	keyiter(k[ 22],r0,r4,r2,130, 30); keyiter(k[ 23],r1,r0,r3,131, 31);

	/* Apply S-boxes */

	S3(r3,r4,r0,r1,r2); storekeys(r1,r2,r4,r3, 28); loadkeys(r1,r2,r4,r3, 24);
	S4(r1,r2,r4,r3,r0); storekeys(r2,r4,r3,r0, 24); loadkeys(r2,r4,r3,r0, 20);
	S5(r2,r4,r3,r0,r1); storekeys(r1,r2,r4,r0, 20); loadkeys(r1,r2,r4,r0, 16);
	S6(r1,r2,r4,r0,r3); storekeys(r4,r3,r2,r0, 16); loadkeys(r4,r3,r2,r0, 12);
	S7(r4,r3,r2,r0,r1); storekeys(r1,r2,r0,r4, 12); loadkeys(r1,r2,r0,r4,  8);
	S0(r1,r2,r0,r4,r3); storekeys(r0,r2,r4,r1,  8); loadkeys(r0,r2,r4,r1,  4);
	S1(r0,r2,r4,r1,r3); storekeys(r3,r4,r1,r0,  4); loadkeys(r3,r4,r1,r0,  0);
	S2(r3,r4,r1,r0,r2); storekeys(r2,r4,r3,r0,  0); loadkeys(r2,r4,r3,r0, -4);
	S3(r2,r4,r3,r0,r1); storekeys(r0,r1,r4,r2, -4); loadkeys(r0,r1,r4,r2, -8);
	S4(r0,r1,r4,r2,r3); storekeys(r1,r4,r2,r3, -8); loadkeys(r1,r4,r2,r3,-12);
	S5(r1,r4,r2,r3,r0); storekeys(r0,r1,r4,r3,-12); loadkeys(r0,r1,r4,r3,-16);
	S6(r0,r1,r4,r3,r2); storekeys(r4,r2,r1,r3,-16); loadkeys(r4,r2,r1,r3,-20);
	S7(r4,r2,r1,r3,r0); storekeys(r0,r1,r3,r4,-20); loadkeys(r0,r1,r3,r4,-24);
	S0(r0,r1,r3,r4,r2); storekeys(r3,r1,r4,r0,-24); loadkeys(r3,r1,r4,r0,-28);
	k -= 50;
	S1(r3,r1,r4,r0,r2); storekeys(r2,r4,r0,r3, 22); loadkeys(r2,r4,r0,r3, 18);
	S2(r2,r4,r0,r3,r1); storekeys(r1,r4,r2,r3, 18); loadkeys(r1,r4,r2,r3, 14);
	S3(r1,r4,r2,r3,r0); storekeys(r3,r0,r4,r1, 14); loadkeys(r3,r0,r4,r1, 10);
	S4(r3,r0,r4,r1,r2); storekeys(r0,r4,r1,r2, 10); loadkeys(r0,r4,r1,r2,  6);
	S5(r0,r4,r1,r2,r3); storekeys(r3,r0,r4,r2,  6); loadkeys(r3,r0,r4,r2,  2);
	S6(r3,r0,r4,r2,r1); storekeys(r4,r1,r0,r2,  2); loadkeys(r4,r1,r0,r2, -2);
	S7(r4,r1,r0,r2,r3); storekeys(r3,r0,r2,r4, -2); loadkeys(r3,r0,r2,r4, -6);
	S0(r3,r0,r2,r4,r1); storekeys(r2,r0,r4,r3, -6); loadkeys(r2,r0,r4,r3,-10);
	S1(r2,r0,r4,r3,r1); storekeys(r1,r4,r3,r2,-10); loadkeys(r1,r4,r3,r2,-14);
	S2(r1,r4,r3,r2,r0); storekeys(r0,r4,r1,r2,-14); loadkeys(r0,r4,r1,r2,-18);
	S3(r0,r4,r1,r2,r3); storekeys(r2,r3,r4,r0,-18); loadkeys(r2,r3,r4,r0,-22);
	k -= 50;
	S4(r2,r3,r4,r0,r1); storekeys(r3,r4,r0,r1, 28); loadkeys(r3,r4,r0,r1, 24);
	S5(r3,r4,r0,r1,r2); storekeys(r2,r3,r4,r1, 24); loadkeys(r2,r3,r4,r1, 20);
	S6(r2,r3,r4,r1,r0); storekeys(r4,r0,r3,r1, 20); loadkeys(r4,r0,r3,r1, 16);
	S7(r4,r0,r3,r1,r2); storekeys(r2,r3,r1,r4, 16); loadkeys(r2,r3,r1,r4, 12);
	S0(r2,r3,r1,r4,r0); storekeys(r1,r3,r4,r2, 12); loadkeys(r1,r3,r4,r2,  8);
	S1(r1,r3,r4,r2,r0); storekeys(r0,r4,r2,r1,  8); loadkeys(r0,r4,r2,r1,  4);
	S2(r0,r4,r2,r1,r3); storekeys(r3,r4,r0,r1,  4); loadkeys(r3,r4,r0,r1,  0);
	S3(r3,r4,r0,r1,r2); storekeys(r1,r2,r4,r3,  0);

	return 0;


}


int serpentEncrypt(uint4 *pt, uint4 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	
	u32 k[WORDS_PER_KEY_SCHEDULE];
	cudaSetDeviceFlags(cudaDeviceMapHost);
	cudaFree(NULL);

	uint4 *pinnedPt;
	uint4 *pinnedCt;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	cudaHostAlloc((void **)&pinnedPt, blocks * sizeof(uint4), cudaHostAllocWriteCombined | cudaHostAllocMapped);
	cudaHostAlloc((void **)&pinnedCt, (blocks + 1) * sizeof(uint4), cudaHostAllocDefault | cudaHostAllocMapped);

	memcpy(pinnedPt, pt, blocks * sizeof(uint4));

	start = startStopwatch();
	makeKey(cipherKey, k, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);

	
    cudaError_t cudaStatus = serpentEncryptBitwiseCuda(pinnedPt, pinnedCt, k, blocks);
	
	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Encryption failed!");
        return 1;
    }

	memcpy(ct, pinnedCt, (blocks + 1) * sizeof(uint4));
	cudaFreeHost(pinnedPt);
	cudaFreeHost(pinnedCt);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }

    return 0;
}
int serpentDecrypt(uint4 *ct, uint4 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	
	u32 k[WORDS_PER_KEY_SCHEDULE];

	cudaSetDeviceFlags(cudaDeviceMapHost);
	cudaFree(NULL);

	uint4 * pinnedCt;
	uint4 *pinnedPt;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	cudaHostAlloc((void **)&pinnedCt, (blocks + 1) * sizeof(uint4), cudaHostAllocWriteCombined | cudaHostAllocMapped);
	cudaHostAlloc((void **)&pinnedPt, blocks * sizeof(uint4), cudaHostAllocDefault | cudaHostAllocMapped);

	memcpy(pinnedCt, ct, (blocks + 1) * sizeof(uint4));

	start = startStopwatch();
	makeKey(cipherKey, k, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);
	
	
    cudaError_t cudaStatus = serpentDecryptBitwiseCuda(pinnedCt, pinnedPt, k, blocks);
	
	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Decryption failed!");
        return 1;
    }

	memcpy(pt, pinnedPt, blocks * sizeof(uint4));
	cudaFreeHost(pinnedCt);
	cudaFreeHost(pinnedPt);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }

    return 0;
}