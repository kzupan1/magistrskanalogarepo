#include "measure-time.h"
#include "rijndael-tables.h"
#include <boost/chrono.hpp>


int rijndaelKeySetupEnc(u32 rk[/*4*(Nr + 1)*/], const u32 cipherKey[], const unsigned int keyLen) {
   	int i = 0;
	u32 temp;

	rk[0] = cipherKey[0];
	rk[1] = cipherKey[1];
	rk[2] = cipherKey[2];
	rk[3] = cipherKey[3];

	if (keyLen == 16) {
		for (;;) {
			temp  = rk[3];
			rk[4] = rk[0] ^
				(Te4[((temp)& 0xff)] & 0xff000000) ^
				(Te4[(temp >> 24)] & 0x00ff0000) ^
				(Te4[((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(Te4[((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[5] = rk[1] ^ rk[4];
			rk[6] = rk[2] ^ rk[5];
			rk[7] = rk[3] ^ rk[6];
			if (++i == 10) {
				return 10;
			}
			rk += 4;
		}
	}
	rk[4] = cipherKey[4];
	rk[5] = cipherKey[5];

	if (keyLen == 24) {
		for (;;) {
			temp = rk[ 5];
			rk[ 6] = rk[ 0] ^
				(Te4[((temp)& 0xff)] & 0xff000000) ^
				(Te4[(temp >> 24)] & 0x00ff0000) ^
				(Te4[((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(Te4[((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[ 7] = rk[ 1] ^ rk[ 6];
			rk[ 8] = rk[ 2] ^ rk[ 7];
			rk[ 9] = rk[ 3] ^ rk[ 8];
			if (++i == 8) {
				return 12;
			}
			rk[10] = rk[ 4] ^ rk[ 9];
			rk[11] = rk[ 5] ^ rk[10];
			rk += 6;
		}
	}
	rk[6] = cipherKey[6];
	rk[7] = cipherKey[7];
	if (keyLen == 32) {
        for (;;) {
        	temp = rk[ 7];
        	rk[ 8] = rk[ 0] ^
				(Te4[((temp)& 0xff)] & 0xff000000) ^
				(Te4[(temp >> 24)] & 0x00ff0000) ^
				(Te4[((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(Te4[((temp >> 8) & 0xff)] & 0x000000ff) ^
        		rcon[i];
        	rk[ 9] = rk[ 1] ^ rk[ 8];
        	rk[10] = rk[ 2] ^ rk[ 9];
        	rk[11] = rk[ 3] ^ rk[10];
			if (++i == 7) {
				return 14;
			}
        	temp = rk[11];
        	rk[12] = rk[ 4] ^
        		(Te4[(temp >> 24)       ] & 0xff000000) ^
        		(Te4[(temp >> 16) & 0xff] & 0x00ff0000) ^
        		(Te4[(temp >>  8) & 0xff] & 0x0000ff00) ^
        		(Te4[(temp      ) & 0xff] & 0x000000ff);
        	rk[13] = rk[ 5] ^ rk[12];
        	rk[14] = rk[ 6] ^ rk[13];
        	rk[15] = rk[ 7] ^ rk[14];

			rk += 8;
        }
	}
	return 0;
}


uint4 rijndaelEncryptCpu(uint4 in, const u32 * rk, const unsigned int Nr){

	register u32 s0, s1, s2, s3, t0, t1, t2, t3;

	

	s0 = in.x ^ rk[0];
	s1 = in.y ^ rk[1];
	s2 = in.z ^ rk[2];
	s3 = in.w ^ rk[3];



	/* round 1: */
	t0 = Te0[s0 & 0xff] ^ Te1[(s1 >> 8) & 0xff] ^ Te2[(s2 >> 16) & 0xff] ^ Te3[s3 >> 24] ^ rk[4];
	t1 = Te0[s1 & 0xff] ^ Te1[(s2 >> 8) & 0xff] ^ Te2[(s3 >> 16) & 0xff] ^ Te3[s0 >> 24] ^ rk[5];
	t2 = Te0[s2 & 0xff] ^ Te1[(s3 >> 8) & 0xff] ^ Te2[(s0 >> 16) & 0xff] ^ Te3[s1 >> 24] ^ rk[6];
	t3 = Te0[s3 & 0xff] ^ Te1[(s0 >> 8) & 0xff] ^ Te2[(s1 >> 16) & 0xff] ^ Te3[s2 >> 24] ^ rk[7];


	/* round 2: */
	s0 = Te0[t0 & 0xff] ^ Te1[(t1 >> 8) & 0xff] ^ Te2[(t2 >> 16) & 0xff] ^ Te3[t3 >> 24] ^ rk[8];
	s1 = Te0[t1 & 0xff] ^ Te1[(t2 >> 8) & 0xff] ^ Te2[(t3 >> 16) & 0xff] ^ Te3[t0 >> 24] ^ rk[9];
	s2 = Te0[t2 & 0xff] ^ Te1[(t3 >> 8) & 0xff] ^ Te2[(t0 >> 16) & 0xff] ^ Te3[t1 >> 24] ^ rk[10];
	s3 = Te0[t3 & 0xff] ^ Te1[(t0 >> 8) & 0xff] ^ Te2[(t1 >> 16) & 0xff] ^ Te3[t2 >> 24] ^ rk[11];

	/* round 3: */
	t0 = Te0[s0 & 0xff] ^ Te1[(s1 >> 8) & 0xff] ^ Te2[(s2 >> 16) & 0xff] ^ Te3[s3 >> 24] ^ rk[12];
	t1 = Te0[s1 & 0xff] ^ Te1[(s2 >> 8) & 0xff] ^ Te2[(s3 >> 16) & 0xff] ^ Te3[s0 >> 24] ^ rk[13];
	t2 = Te0[s2 & 0xff] ^ Te1[(s3 >> 8) & 0xff] ^ Te2[(s0 >> 16) & 0xff] ^ Te3[s1 >> 24] ^ rk[14];
	t3 = Te0[s3 & 0xff] ^ Te1[(s0 >> 8) & 0xff] ^ Te2[(s1 >> 16) & 0xff] ^ Te3[s2 >> 24] ^ rk[15];
	/* round 4: */
	s0 = Te0[t0 & 0xff] ^ Te1[(t1 >> 8) & 0xff] ^ Te2[(t2 >> 16) & 0xff] ^ Te3[t3 >> 24] ^ rk[16];
	s1 = Te0[t1 & 0xff] ^ Te1[(t2 >> 8) & 0xff] ^ Te2[(t3 >> 16) & 0xff] ^ Te3[t0 >> 24] ^ rk[17];
	s2 = Te0[t2 & 0xff] ^ Te1[(t3 >> 8) & 0xff] ^ Te2[(t0 >> 16) & 0xff] ^ Te3[t1 >> 24] ^ rk[18];
	s3 = Te0[t3 & 0xff] ^ Te1[(t0 >> 8) & 0xff] ^ Te2[(t1 >> 16) & 0xff] ^ Te3[t2 >> 24] ^ rk[19];


	/* round 5: */
	t0 = Te0[s0 & 0xff] ^ Te1[(s1 >> 8) & 0xff] ^ Te2[(s2 >> 16) & 0xff] ^ Te3[s3 >> 24] ^ rk[20];
	t1 = Te0[s1 & 0xff] ^ Te1[(s2 >> 8) & 0xff] ^ Te2[(s3 >> 16) & 0xff] ^ Te3[s0 >> 24] ^ rk[21];
	t2 = Te0[s2 & 0xff] ^ Te1[(s3 >> 8) & 0xff] ^ Te2[(s0 >> 16) & 0xff] ^ Te3[s1 >> 24] ^ rk[22];
	t3 = Te0[s3 & 0xff] ^ Te1[(s0 >> 8) & 0xff] ^ Te2[(s1 >> 16) & 0xff] ^ Te3[s2 >> 24] ^ rk[23];
	/* round 6: */
	s0 = Te0[t0 & 0xff] ^ Te1[(t1 >> 8) & 0xff] ^ Te2[(t2 >> 16) & 0xff] ^ Te3[t3 >> 24] ^ rk[24];
	s1 = Te0[t1 & 0xff] ^ Te1[(t2 >> 8) & 0xff] ^ Te2[(t3 >> 16) & 0xff] ^ Te3[t0 >> 24] ^ rk[25];
	s2 = Te0[t2 & 0xff] ^ Te1[(t3 >> 8) & 0xff] ^ Te2[(t0 >> 16) & 0xff] ^ Te3[t1 >> 24] ^ rk[26];
	s3 = Te0[t3 & 0xff] ^ Te1[(t0 >> 8) & 0xff] ^ Te2[(t1 >> 16) & 0xff] ^ Te3[t2 >> 24] ^ rk[27];
	
	/* round 7: */
	t0 = Te0[s0 & 0xff] ^ Te1[(s1 >> 8) & 0xff] ^ Te2[(s2 >> 16) & 0xff] ^ Te3[s3 >> 24] ^ rk[28];
	t1 = Te0[s1 & 0xff] ^ Te1[(s2 >> 8) & 0xff] ^ Te2[(s3 >> 16) & 0xff] ^ Te3[s0 >> 24] ^ rk[29];
	t2 = Te0[s2 & 0xff] ^ Te1[(s3 >> 8) & 0xff] ^ Te2[(s0 >> 16) & 0xff] ^ Te3[s1 >> 24] ^ rk[30];
	t3 = Te0[s3 & 0xff] ^ Te1[(s0 >> 8) & 0xff] ^ Te2[(s1 >> 16) & 0xff] ^ Te3[s2 >> 24] ^ rk[31];
	
	/* round 8: */
	s0 = Te0[t0 & 0xff] ^ Te1[(t1 >> 8) & 0xff] ^ Te2[(t2 >> 16) & 0xff] ^ Te3[t3 >> 24] ^ rk[32];
	s1 = Te0[t1 & 0xff] ^ Te1[(t2 >> 8) & 0xff] ^ Te2[(t3 >> 16) & 0xff] ^ Te3[t0 >> 24] ^ rk[33];
	s2 = Te0[t2 & 0xff] ^ Te1[(t3 >> 8) & 0xff] ^ Te2[(t0 >> 16) & 0xff] ^ Te3[t1 >> 24] ^ rk[34];
	s3 = Te0[t3 & 0xff] ^ Te1[(t0 >> 8) & 0xff] ^ Te2[(t1 >> 16) & 0xff] ^ Te3[t2 >> 24] ^ rk[35];


	/* round 9: */
	t0 = Te0[s0 & 0xff] ^ Te1[(s1 >> 8) & 0xff] ^ Te2[(s2 >> 16) & 0xff] ^ Te3[s3 >> 24] ^ rk[36];
	t1 = Te0[s1 & 0xff] ^ Te1[(s2 >> 8) & 0xff] ^ Te2[(s3 >> 16) & 0xff] ^ Te3[s0 >> 24] ^ rk[37];
	t2 = Te0[s2 & 0xff] ^ Te1[(s3 >> 8) & 0xff] ^ Te2[(s0 >> 16) & 0xff] ^ Te3[s1 >> 24] ^ rk[38];
	t3 = Te0[s3 & 0xff] ^ Te1[(s0 >> 8) & 0xff] ^ Te2[(s1 >> 16) & 0xff] ^ Te3[s2 >> 24] ^ rk[39];

	if (Nr > 10) {
		/* round 10: */
		s0 = Te0[t0 & 0xff] ^ Te1[(t1 >> 8) & 0xff] ^ Te2[(t2 >> 16) & 0xff] ^ Te3[t3 >> 24] ^ rk[40];
		s1 = Te0[t1 & 0xff] ^ Te1[(t2 >> 8) & 0xff] ^ Te2[(t3 >> 16) & 0xff] ^ Te3[t0 >> 24] ^ rk[41];
		s2 = Te0[t2 & 0xff] ^ Te1[(t3 >> 8) & 0xff] ^ Te2[(t0 >> 16) & 0xff] ^ Te3[t1 >> 24] ^ rk[42];
		s3 = Te0[t3 & 0xff] ^ Te1[(t0 >> 8) & 0xff] ^ Te2[(t1 >> 16) & 0xff] ^ Te3[t2 >> 24] ^ rk[43];
		/* round 11: */
		t0 = Te0[s0 & 0xff] ^ Te1[(s1 >> 8) & 0xff] ^ Te2[(s2 >> 16) & 0xff] ^ Te3[s3 >> 24] ^ rk[44];
		t1 = Te0[s1 & 0xff] ^ Te1[(s2 >> 8) & 0xff] ^ Te2[(s3 >> 16) & 0xff] ^ Te3[s0 >> 24] ^ rk[45];
		t2 = Te0[s2 & 0xff] ^ Te1[(s3 >> 8) & 0xff] ^ Te2[(s0 >> 16) & 0xff] ^ Te3[s1 >> 24] ^ rk[46];
		t3 = Te0[s3 & 0xff] ^ Te1[(s0 >> 8) & 0xff] ^ Te2[(s1 >> 16) & 0xff] ^ Te3[s2 >> 24] ^ rk[47];
		if (Nr > 12) {
			/* round 12: */
			s0 = Te0[t0 & 0xff] ^ Te1[(t1 >> 8) & 0xff] ^ Te2[(t2 >> 16) & 0xff] ^ Te3[t3 >> 24] ^ rk[48];
			s1 = Te0[t1 & 0xff] ^ Te1[(t2 >> 8) & 0xff] ^ Te2[(t3 >> 16) & 0xff] ^ Te3[t0 >> 24] ^ rk[49];
			s2 = Te0[t2 & 0xff] ^ Te1[(t3 >> 8) & 0xff] ^ Te2[(t0 >> 16) & 0xff] ^ Te3[t1 >> 24] ^ rk[50];
			s3 = Te0[t3 & 0xff] ^ Te1[(t0 >> 8) & 0xff] ^ Te2[(t1 >> 16) & 0xff] ^ Te3[t2 >> 24] ^ rk[51];
			/* round 13: */
			t0 = Te0[s0 & 0xff] ^ Te1[(s1 >> 8) & 0xff] ^ Te2[(s2 >> 16) & 0xff] ^ Te3[s3 >> 24] ^ rk[52];
			t1 = Te0[s1 & 0xff] ^ Te1[(s2 >> 8) & 0xff] ^ Te2[(s3 >> 16) & 0xff] ^ Te3[s0 >> 24] ^ rk[53];
			t2 = Te0[s2 & 0xff] ^ Te1[(s3 >> 8) & 0xff] ^ Te2[(s0 >> 16) & 0xff] ^ Te3[s1 >> 24] ^ rk[54];
			t3 = Te0[s3 & 0xff] ^ Te1[(s0 >> 8) & 0xff] ^ Te2[(s1 >> 16) & 0xff] ^ Te3[s2 >> 24] ^ rk[55];
		}
	}


	u32 x = Nr << 2;

	s0 =
		(Te4[(t0 & 0xff)] & 0x000000ff) ^
		(Te4[((t1 >> 8) & 0xff)] & 0x0000ff00) ^
		(Te4[((t2 >> 16) & 0xff)] & 0x00ff0000) ^
		(Te4[((t3) >> 24)] & 0xff000000) ^
		rk[x++];

	s1 =
		(Te4[(t1 & 0xff)] & 0x000000ff) ^
		(Te4[((t2 >> 8) & 0xff)] & 0x0000ff00) ^
		(Te4[((t3 >> 16) & 0xff)] & 0x00ff0000) ^
		(Te4[((t0) >> 24)] & 0xff000000) ^
		rk[x++];

	s2 =
		(Te4[(t2 & 0xff)] & 0x000000ff) ^
		(Te4[((t3 >> 8) & 0xff)] & 0x0000ff00) ^
		(Te4[((t0 >> 16) & 0xff)] & 0x00ff0000) ^
		(Te4[((t1) >> 24)] & 0xff000000) ^
		rk[x++];

	s3 =
		(Te4[(t3 & 0xff)] & 0x000000ff) ^
		(Te4[((t0 >> 8) & 0xff)] & 0x0000ff00) ^
		(Te4[((t1 >> 16) & 0xff)] & 0x00ff0000) ^
		(Te4[((t2) >> 24)] & 0xff000000) ^
		rk[x];

	



	return {s0, s1, s2, s3};
}

int rijndaelEncrypt(uint4 *pt, uint4 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	u32 rk[rkLength];
	

	start = startStopwatch();
	unsigned int Nr = rijndaelKeySetupEnc(rk, (u32 *)cipherKey, cipherKeyLength);
	endStopwatch("Encryption key calculation", start );
	
	srand(0);
	u32 n0 = rand();
	u32 n1 = rand();
	u64 ctr = 0;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;
	
	uint4 in;
	uint4 out;

	ct[0] = { n0, n1, 0, 0 };

	start = startStopwatch();

	
	for (int i = 0; i < blocks; i++){
		in = { n0, n1, ((ctr & 0xffffffff00000000) >> 32), (ctr & 0xffffffff) };

		out = rijndaelEncryptCpu(in, rk, Nr);
		
		
		ct[i + 1].x = pt[i].x ^ out.x;
		ct[i + 1].y = pt[i].y ^ out.y;
		ct[i + 1].z = pt[i].z ^ out.z;
		ct[i + 1].w = pt[i].w ^ out.w;
		

		ctr++;
	}
	endStopwatch("Encryption", start);


    return 0;



}

int rijndaelDecrypt(uint4 *ct, uint4 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	u32 rk[rkLength];
	
	start = startStopwatch();
	unsigned int Nr = rijndaelKeySetupEnc(rk, (u32 *)cipherKey, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);

	u32 n0;
	u32 n1;
	u64 ctr = 0;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	uint4 in;
	uint4 out;

	n0 = ct[0].x;
	n1 = ct[0].y;

	start = startStopwatch();
	for (int i = 0; i < blocks; i++){
		in = { n0, n1, ((ctr & 0xffffffff00000000) >> 32), (ctr & 0xffffffff) };

		out = rijndaelEncryptCpu(in, rk, Nr);

		pt[i].x = ct[i + 1].x ^ out.x;
		pt[i].y = ct[i + 1].y ^ out.y;
		pt[i].z = ct[i + 1].z ^ out.z;
		pt[i].w = ct[i + 1].w ^ out.w;


		ctr += 1;
	}
	endStopwatch("Decryption", start);


    return 0;


}