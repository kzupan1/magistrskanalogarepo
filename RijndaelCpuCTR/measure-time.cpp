#include <stdio.h>
#include <boost/chrono.hpp>

using namespace boost::chrono;

high_resolution_clock::time_point startStopwatch()
{
	return high_resolution_clock::now();
}

duration<double, boost::milli> endStopwatch(const char *comment, high_resolution_clock::time_point start)
{

	high_resolution_clock::time_point end = high_resolution_clock::now();

	duration<double, boost::milli> time_span = duration_cast< duration<double, boost::milli> >(end - start);
	
	printf("%s\t%.5lf\tms\n", comment, time_span.count());

	return time_span;
}

void printDuration(const char *comment, duration<double, boost::milli> time_span){

	printf("%s\t%.5lf\tms\n", comment, time_span.count());
}