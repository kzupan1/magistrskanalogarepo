#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdlib.h>
#include <stdio.h>
#include "rc6-cuda.cuh"
#include "measure-time.h"
#include <boost/chrono.hpp>


int makeKey(const u32 cipherKey[], u32 rk[R24], const unsigned int keyLen ){

	u32 l[8], i, j, a, k, b, t;

	rk[0] = P32;

	for(k = 1; k < R24; ++k)
		rk[k] = rk[k-1] + Q32;

	for(k = 0; k < keyLen/BYTES_PER_WORD; ++k)
		l[k] = cipherKey[k];

	a = b = i = j = 0;

	t = (keyLen / BYTES_PER_WORD) - 1;

	for(k = 0; k < 132; ++k)
		{   a = ROL(rk[i] + a + b, 3); b += a;
			b = ROL(l[j] + b, b);
			rk[i] = a; l[j] = b;
			i = (i == 43 ? 0 : i + 1);  // i = (i + 1) % 44;
			j = (j == t ? 0 : j + 1);   // j = (j + 1) % t;
		}


	return 0;
}


#define f_rnd(i,a,b,c,d)                        \
        u = ROL(d * (d + d + 1), 5);           \
        t = ROL(b * (b + b + 1), 5);           \
        a = ROL(a ^ t, u) + rk_constant[i];     \
        c = ROL(c ^ u, t) + rk_constant[i + 1]

#define i_rnd(i,a,b,c,d)                        \
        u = ROL(d * (d + d + 1), 5);           \
        t = ROL(b * (b + b + 1), 5);           \
        c = ROR(c - rk_constant[i + 1], t) ^ u; \
        a = ROR(a - rk_constant[i], u) ^ t


__constant__ u32 rk_constant[R24];

__global__ void encryptKernel(uint4 *pt, uint4 *ct,  const unsigned int blocks){


	int t = threadIdx.x;
	int i = blockIdx.y * gridDim.x * blockDim.x + blockIdx.x * blockDim.x + t;
	
	if(i < blocks){
			

		register u32 a, b, c, d, t, u;
	
		a = pt[i].x;
		b = pt[i].y + rk_constant[0];
		c = pt[i].z;
		d = pt[i].w + rk_constant[1];

		f_rnd( 2,a,b,c,d); f_rnd( 4,b,c,d,a);
		f_rnd( 6,c,d,a,b); f_rnd( 8,d,a,b,c);
		f_rnd(10,a,b,c,d); f_rnd(12,b,c,d,a);
		f_rnd(14,c,d,a,b); f_rnd(16,d,a,b,c);
		f_rnd(18,a,b,c,d); f_rnd(20,b,c,d,a);
		f_rnd(22,c,d,a,b); f_rnd(24,d,a,b,c);
		f_rnd(26,a,b,c,d); f_rnd(28,b,c,d,a);
		f_rnd(30,c,d,a,b); f_rnd(32,d,a,b,c);
		f_rnd(34,a,b,c,d); f_rnd(36,b,c,d,a);
		f_rnd(38,c,d,a,b); f_rnd(40,d,a,b,c);
		
		ct[i] = { a + rk_constant[42], b, c + rk_constant[43], d };

	}

	

}

__global__ void decryptKernel(uint4 *ct, uint4 *pt, const unsigned int blocks){

	
	int t = threadIdx.x;
	int i = blockIdx.y * gridDim.x * blockDim.x + blockIdx.x * blockDim.x + t;
	

	
	if(i < blocks){
		

		register u32 a, b, c, d, t, u;

		a = ct[i].x - rk_constant[42];
		b = ct[i].y;
		c = ct[i].z - rk_constant[43];
		d = ct[i].w;

		i_rnd(40,d,a,b,c); i_rnd(38,c,d,a,b);
		i_rnd(36,b,c,d,a); i_rnd(34,a,b,c,d);
		i_rnd(32,d,a,b,c); i_rnd(30,c,d,a,b);
		i_rnd(28,b,c,d,a); i_rnd(26,a,b,c,d);
		i_rnd(24,d,a,b,c); i_rnd(22,c,d,a,b);
		i_rnd(20,b,c,d,a); i_rnd(18,a,b,c,d);
		i_rnd(16,d,a,b,c); i_rnd(14,c,d,a,b);
		i_rnd(12,b,c,d,a); i_rnd(10,a,b,c,d);
		i_rnd( 8,d,a,b,c); i_rnd( 6,c,d,a,b);
		i_rnd( 4,b,c,d,a); i_rnd( 2,a,b,c,d);

		
		pt[i] = { a, b - rk_constant[0], c, d - rk_constant[1] };

	}

	

}

#define MAX_GRID_SIZE 65535

cudaError_t rc6EncryptCuda(const u8 pt[], u8 ct[], const u32 rk[R24], const unsigned int plainTextLength, const unsigned int threadBlockSize)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	uint4 *devPt;
	uint4 *devCt;

    cudaError_t cudaStatus;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        return cudaStatus;
    }

    // cuda malloc
	start = startStopwatch();
	cudaHostGetDevicePointer((void **)&devPt, (void *)pt, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	cudaHostGetDevicePointer((void **)&devCt, (void *)ct, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	
	endStopwatch("Malloc & GetDevicePointer(encryption)", start);

	start = startStopwatch();

	cudaStatus = cudaMemcpyToSymbol(rk_constant, rk, R24 * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }
	
	totalTime = endStopwatch("Memory copy from host to device (encryption)", start);


	unsigned int threadBlocks = (blocks - 1) / threadBlockSize + 1;
	unsigned int gridDimY = (threadBlocks - 1) / MAX_GRID_SIZE + 1;
	unsigned int gridDimX = (threadBlocks - 1) / gridDimY + 1;

	dim3 dimGrid(gridDimX, gridDimY, 1);
	dim3 dimBlock(threadBlockSize, 1, 1);
	
    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	encryptKernel<<<dimGrid, dimBlock>>>(devPt, devCt, blocks);
	
	
    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        return cudaStatus;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
        return cudaStatus;
    }
	
	totalTime += endStopwatch("Encryption kernel", start);

	printDuration("Encryption", totalTime);

    return cudaStatus;
}

cudaError_t rc6DecryptCuda(const u8 ct[], u8 pt[], const u32 rk[R24], const unsigned int plainTextLength, const unsigned int threadBlockSize)
{

	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	uint4 *devPt;
	uint4 *devCt;

    cudaError_t cudaStatus;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        return cudaStatus;
    }

    // cuda malloc
	start = startStopwatch();
	cudaHostGetDevicePointer((void **)&devCt, (void *)ct, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	cudaHostGetDevicePointer((void **)&devPt, (void *)pt, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	
	endStopwatch("Malloc & GetDevicePointer (decryption)", start);

	start = startStopwatch();
	
	cudaStatus = cudaMemcpyToSymbol(rk_constant, rk, R24 * sizeof(u32));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        return cudaStatus;
    }
	
	totalTime = endStopwatch("Memory copy from host to device (decryption)", start);

	unsigned int threadBlocks = (blocks - 1) / threadBlockSize + 1;
	unsigned int gridDimY = (threadBlocks - 1) / MAX_GRID_SIZE + 1;
	unsigned int gridDimX = (threadBlocks - 1) / gridDimY + 1;

	dim3 dimGrid(gridDimX, gridDimY, 1);
	dim3 dimBlock(threadBlockSize, 1, 1);
	
    // Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	decryptKernel<<<dimGrid, dimBlock>>>(devCt, devPt, blocks);
	
    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        return cudaStatus;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
        return cudaStatus;
    }
	
	totalTime += endStopwatch("Decryption kernel", start);

	printDuration("Decryption", totalTime);

    return cudaStatus;
}


int rc6Encrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength, const unsigned int threadBlockSize){

	boost::chrono::high_resolution_clock::time_point start;

	cudaSetDeviceFlags(cudaDeviceMapHost);
	cudaFree(NULL);
	u32 rk[R24];;
	
	u8 *pinnedPt;
	u8 *pinnedCt;

	cudaHostAlloc((void **)&pinnedPt, plainTextLength, cudaHostAllocWriteCombined | cudaHostAllocMapped);
	cudaHostAlloc((void **)&pinnedCt, plainTextLength, cudaHostAllocDefault | cudaHostAllocMapped);

	memcpy(pinnedPt, pt, plainTextLength);

	start = startStopwatch();
	makeKey((u32*)cipherKey, rk, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);
	
	
	cudaError_t cudaStatus = rc6EncryptCuda(pinnedPt, pinnedCt, rk, plainTextLength, threadBlockSize);
	
	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Encryption failed!");
        return 1;
    }

	memcpy(ct, pinnedCt, plainTextLength);
	cudaFreeHost(pinnedPt);
	cudaFreeHost(pinnedCt);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }

    return 0;




}
int rc6Decrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength, const unsigned int threadBlockSize){
	
	boost::chrono::high_resolution_clock::time_point start;

	cudaSetDeviceFlags(cudaDeviceMapHost);
	cudaFree(NULL);
	u32 rk[R24];;
	
	u8 *pinnedCt;
	u8 *pinnedPt;

	cudaHostAlloc((void **)&pinnedCt, plainTextLength, cudaHostAllocWriteCombined | cudaHostAllocMapped);
	cudaHostAlloc((void **)&pinnedPt, plainTextLength, cudaHostAllocDefault | cudaHostAllocMapped);

	memcpy(pinnedCt, ct, plainTextLength);

	start = startStopwatch();
	makeKey((u32*)cipherKey, rk, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);
	
	
	cudaError_t cudaStatus = rc6DecryptCuda(pinnedCt, pinnedPt, rk, plainTextLength, threadBlockSize);
	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Decryption failed!");
        return 1;
    }

	memcpy(pt, pinnedPt, plainTextLength);
	cudaFreeHost(pinnedCt);
	cudaFreeHost(pinnedPt);
	
    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }

    return 0;

}