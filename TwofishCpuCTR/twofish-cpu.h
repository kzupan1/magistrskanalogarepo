#ifndef __TWOFISH_CUDA_H
#define __TWOFISH_CUDA_H
#include "common-types.h"

#define MAX_ROUNDS 16
#define ROUNDS MAX_ROUNDS

#define INPUT_WHITEN 0
#define OUTPUT_WHITEN (INPUT_WHITEN +  WORDS_PER_BLOCK)
#define ROUND_SUBKEYS  (OUTPUT_WHITEN + WORDS_PER_BLOCK)

#define		TOTAL_SUBKEYS	(ROUND_SUBKEYS + 2*ROUNDS)

#define S_BOX_SIZE 4 * 256

#define SK_STEP 0x02020202
#define SK_BUMP 0x01010101
#define SK_ROTL 9


#define RS_GF_FDBK  0x14D 

#define	RS_rem(x)		\
	{	u8  b  = (u8) (x >> 24);											 \
		u32 g2 = ((b << 1) ^ ((b & 0x80) ? RS_GF_FDBK : 0 )) & 0xFF;		 \
		u32 g3 = ((b >> 1) & 0x7F) ^ ((b & 1) ? RS_GF_FDBK >> 1 : 0 ) ^ g2; \
		x = (x << 8) ^ (g3 << 24) ^ (g2 << 16) ^ (g3 << 8) ^ b;				 \
	}

/*	Macros for extracting bytes from dwords (correct for endianness) */
#define	_b(x,N)	(((u8 *)&x)[((N) & 3)]) /* pick bytes out of a dword */

#define		b0(x)			_b(x,0)		/* extract LSB of DWORD */
#define		b1(x)			_b(x,1)
#define		b2(x)			_b(x,2)
#define		b3(x)			_b(x,3)		/* extract MSB of DWORD */

#define	ROL(x,n) (((x) << ((n) & 0x1F)) | ((x) >> (32-((n) & 0x1F))))
#define	ROR(x,n) (((x) >> ((n) & 0x1F)) | ((x) << (32-((n) & 0x1F))))


int twofishEncrypt(uint4* pt, uint4* ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);
int twofishDecrypt(uint4* ct, uint4* pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);



#endif /* __TWOFISH_CPU_H */