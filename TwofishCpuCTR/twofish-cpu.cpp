#include "twofish-cpu.h"
#include "twofish-tables.h"
#include "measure-time.h"
#include <boost/chrono.hpp>


#define	Fe32(sBox, x,R) (sBox[        2*_b(x, R  )    ] ^ \
				sBox[        2*_b(x, R+1) + 1] ^ \
				sBox[0x200 + 2*_b(x, R+2)    ] ^ \
				sBox[0x200 + 2*_b(x, R+3) + 1])



uint4 twofishEncryptCpu(uint4 in, const u32 * sKey, const u32 * sBox){

	#define encrypt2Rounds()	t0 = Fe32(sBox, x0, 0);\
								t1 = Fe32(sBox, x1, 3);\
								x2 ^= t0 + t1 + sKey[k++];\
								x2  = ROR(x2, 1);		  \
								x3  = ROL(x3, 1);		  \
								x3 ^= t0 + 2*t1 + sKey[k++];\
								t0 = Fe32(sBox, x2, 0);\
								t1 = Fe32(sBox, x3, 3);\
								x0 ^= t0 + t1 + sKey[k++];\
								x0  = ROR(x0, 1) ;		\
								x1  = ROL(x1, 1);			\
								x1 ^= t0 + 2*t1 + sKey[k++];

	#define applyEncryptRounds() encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();



		
	register u32 x0, x1, x2, x3, t0, t1;
	register int k;

	x0 = in.x;
	x1 = in.y;
	x2 = in.z;
	x3 = in.w;

	x0 ^= sKey[INPUT_WHITEN    ];
	x1 ^= sKey[INPUT_WHITEN + 1];
	x2 ^= sKey[INPUT_WHITEN + 2];
	x3 ^= sKey[INPUT_WHITEN + 3];

	k = ROUND_SUBKEYS;
		
	applyEncryptRounds();

	x2 ^= sKey[OUTPUT_WHITEN    ];
	x3 ^= sKey[OUTPUT_WHITEN + 1];
	x0 ^= sKey[OUTPUT_WHITEN + 2];
	x1 ^= sKey[OUTPUT_WHITEN + 3];

	
	return {x2, x3, x0, x1};
}






u32 RS_MDS_Encode(u32 k0, u32 k1)
	{
	int i,j;
	u32 r;

	for (i=r=0;i<2;i++)
		{
		r ^= (i) ? k0 : k1;			/* merge in 32 more key bits */
		for (j=0;j<4;j++)			/* shift one u8 at a time */
			RS_rem(r);
					

		}
	return r;
	}


int makeKey(const u32 cipherKey[], u32 subKeys[TOTAL_SUBKEYS], u32 sBox[S_BOX_SIZE], const unsigned int keyLen ){

	#define	F32(res,x,k32)	\
	{															\
	u32 t=x;													\
	switch (k64Cnt & 3)											\
			{														\
			case 0:  /* same as 4 */								\
						b0(t)   = p8(04)[b0(t)] ^ b0(k32[3]);		\
						b1(t)   = p8(14)[b1(t)] ^ b1(k32[3]);		\
						b2(t)   = p8(24)[b2(t)] ^ b2(k32[3]);		\
						b3(t)   = p8(34)[b3(t)] ^ b3(k32[3]);		\
					 /* fall thru, having pre-processed t */		\
			case 3:		b0(t)   = p8(03)[b0(t)] ^ b0(k32[2]);		\
						b1(t)   = p8(13)[b1(t)] ^ b1(k32[2]);		\
						b2(t)   = p8(23)[b2(t)] ^ b2(k32[2]);		\
						b3(t)   = p8(33)[b3(t)] ^ b3(k32[2]);		\
					 /* fall thru, having pre-processed t */		\
			case 2:	 /* 128-bit keys (optimize for this case) */	\
				res=	MDStab[0][p8(01)[p8(02)[b0(t)] ^ b0(k32[1])] ^ b0(k32[0])] ^	\
						MDStab[1][p8(11)[p8(12)[b1(t)] ^ b1(k32[1])] ^ b1(k32[0])] ^	\
						MDStab[2][p8(21)[p8(22)[b2(t)] ^ b2(k32[1])] ^ b2(k32[0])] ^	\
						MDStab[3][p8(31)[p8(32)[b3(t)] ^ b3(k32[1])] ^ b3(k32[0])] ;	\
			}														\
	}

	u32 k64Cnt = keyLen / 8;
    u32 subkeyCnt = TOTAL_SUBKEYS;
    u32 k32e[4]; // even 32-bit entities
    u32 k32o[4]; // odd 32-bit entities
    u32 sBoxKey[4];

	u32 i, j, offset = 0;
	
	for (i = 0, j = k64Cnt-1; i < 4 && offset < keyLen/BYTES_PER_WORD; i++, j--) {
		 k32e[i] = cipherKey[offset++];
         k32o[i] = cipherKey[offset++];
         sBoxKey[j] = RS_MDS_Encode(k32e[i], k32o[i]); // reverse order
	}


	// compute the round decryption subkeys for PHT. these same subkeys
    // will be used in encryption but will be applied in reverse order.
    u32 q, A, B;

    for (i = q = 0; i < subkeyCnt/2; i++, q += SK_STEP) {
        F32(A, q        , k32e); // A uses even key entities
        F32(B, q+SK_BUMP, k32o); // B uses odd  key entities
        B = B << 8 | ROR(B, 24);
        A += B;
        subKeys[2*i    ] = A;               // combine with a PHT
        A += B;
        subKeys[2*i + 1] = A << SK_ROTL | ROR(A, (32-SK_ROTL));
    }

	// fully expand the table for speed
    u32 k0 = sBoxKey[0];
    u32 k1 = sBoxKey[1];
    u32 k2 = sBoxKey[2];
    u32 k3 = sBoxKey[3];
    u32 b0, b1, b2, b3;
    
    for (i = 0; i < 256; i++) {
        b0 = b1 = b2 = b3 = i;
        switch (k64Cnt & 3) {
        case 0: // same as 4
			b0 = (p8(04)[b0]) ^ b0(k3);
			b1 = (p8(14)[b1]) ^ b1(k3);
			b2 = (p8(24)[b2]) ^ b2(k3);
			b3 = (p8(34)[b3]) ^ b3(k3);
        case 3:
			b0 = (p8(03)[b0]) ^ b0(k2);
			b1 = (p8(13)[b1]) ^ b1(k2);
			b2 = (p8(23)[b2]) ^ b2(k2);
			b3 = (p8(33)[b3]) ^ b3(k2);
        case 2: // 128-bit keys
			sBox[      2*i  ] = MDStab[0][(p8(01)[(p8(02)[b0]) ^ b0(k1)]) ^ b0(k0)];
			sBox[      2*i+1] = MDStab[1][(p8(11)[(p8(12)[b1]) ^ b1(k1)]) ^ b1(k0)];
			sBox[0x200+2*i  ] = MDStab[2][(p8(21)[(p8(22)[b2]) ^ b2(k1)]) ^ b2(k0)];
			sBox[0x200+2*i+1] = MDStab[3][(p8(31)[(p8(32)[b3]) ^ b3(k1)]) ^ b3(k0)];
        }
    }

	

	return 0;
}




int twofishEncrypt(uint4 *pt, uint4 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	u32 subKeys[TOTAL_SUBKEYS];
	u32 sBox[S_BOX_SIZE];
	

	start = startStopwatch();
	makeKey((u32*)cipherKey, subKeys, sBox, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);

	srand(0);
	u32 n0 = rand();
	u32 n1 = rand();
	u64 ctr = 0;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	uint4 in;
	uint4 out;

	ct[0] = { n0, n1, 0, 0 };


	start = startStopwatch();
	for (int i = 0; i < blocks; i++){
		in = { n0, n1, ((ctr & 0xffffffff00000000) >> 32), (ctr & 0xffffffff) };

		out = twofishEncryptCpu(in, subKeys, sBox);


		ct[i + 1].x = pt[i].x ^ out.x;
		ct[i + 1].y = pt[i].y ^ out.y;
		ct[i + 1].z = pt[i].z ^ out.z;
		ct[i + 1].w = pt[i].w ^ out.w;


		ctr++;
	}

	endStopwatch("Encryption", start);



    return 0;




}
int twofishDecrypt(uint4 *ct, uint4 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){
	
	boost::chrono::high_resolution_clock::time_point start;

	u32 subKeys[TOTAL_SUBKEYS];
	u32 sBox[S_BOX_SIZE];
	
	start = startStopwatch();
	makeKey((u32*)cipherKey, subKeys, sBox, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);
	
	u32 n0;
	u32 n1;
	u64 ctr = 0;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	uint4 in;
	uint4 out;

	n0 = ct[0].x;
	n1 = ct[0].y;

	start = startStopwatch();
	for (int i = 0; i < blocks; i++){
		in = { n0, n1, ((ctr & 0xffffffff00000000) >> 32), (ctr & 0xffffffff) };

		out = twofishEncryptCpu(in, subKeys, sBox);

		pt[i].x = ct[i + 1].x ^ out.x;
		pt[i].y = ct[i + 1].y ^ out.y;
		pt[i].z = ct[i + 1].z ^ out.z;
		pt[i].w = ct[i + 1].w ^ out.w;


		ctr += 1;
	}
	endStopwatch("Decryption", start);



    return 0;

}