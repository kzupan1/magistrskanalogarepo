#include "serpent-cpu.h"
#include <stdio.h>
#include <stdlib.h>
#include "helpers.h"


#define PRINT_KEY 0

int main(int argc, char **argv)
{
	int status;
	bool match;
	unsigned int i;
	DataMaterial *keyMaterial;
	DataMaterial *plainTextMaterial;
	DataMaterial *cipherTextMaterial;
	DataMaterial *decipherTextMaterial;


	keyMaterial = (DataMaterial *)malloc(sizeof(DataMaterial));
	plainTextMaterial = (DataMaterial *)malloc(sizeof(DataMaterial));
	cipherTextMaterial = (DataMaterial *)malloc(sizeof(DataMaterial));
	decipherTextMaterial = (DataMaterial *)malloc(sizeof(DataMaterial));

	GetArgs(argc, argv, keyMaterial, plainTextMaterial);

	cipherTextMaterial->bytesLength = plainTextMaterial->bytesLength;
	decipherTextMaterial->bytesLength = plainTextMaterial->bytesLength;

	cipherTextMaterial->bytes = (u8*)malloc(sizeof(u8)*plainTextMaterial->bytesLength);
	decipherTextMaterial->bytes = (u8*)malloc(sizeof(u8)*plainTextMaterial->bytesLength);


	status = serpentEncrypt(plainTextMaterial->bytes, cipherTextMaterial->bytes, keyMaterial->bytes, plainTextMaterial->bytesLength, keyMaterial->bytesLength);

	if (status != 0){
		printf("Encryption unsuccessful \n");
		return -1;
	}

	status = serpentDecrypt(cipherTextMaterial->bytes, decipherTextMaterial->bytes, keyMaterial->bytes, plainTextMaterial->bytesLength, keyMaterial->bytesLength);

	if (status != 0){
		printf("Decryption unsuccessful \n");
		return -1;
	}

	unPadBytes(plainTextMaterial);
	unPadBytes(decipherTextMaterial);


	if (plainTextMaterial->bytesLength != decipherTextMaterial->bytesLength){
		printf("Plain text and deciphered plain text don't match!\n");
		return -1;
	}

	match = true;
	for (i = 0; i < plainTextMaterial->bytesLength; i++){
		if (plainTextMaterial->bytes[i] != decipherTextMaterial->bytes[i]){
			match = false;
			break;
		}
	}

	if (match){
		printf("Plain text and deciphered plain text match:\tyes\n");
	}
	else{
		printf("Plain text and deciphered plain text match:\tno\n");

	}


#if PRINT_KEY
	printf("Key: ");


	for (i = 0; i < keyMaterial->bytesLength; i++){
		printf("%02X", keyMaterial->bytes[i]);
	}

	printf("\n");

	printf("Plaintext: ");

	for (i = 0; i < plainTextMaterial->bytesLength; i++){
		printf("%02X", plainTextMaterial->bytes[i]);
	}
	printf("\n");

	//here we print the result
	printf("Ciphertext: ");

	for (i = 0; i < cipherTextMaterial->bytesLength; i++){
		printf("%02X", cipherTextMaterial->bytes[i]);
	}

	printf("\n");


	printf("Decrypted plaintext: ");

	for (i = 0; i < decipherTextMaterial->bytesLength; i++){
		printf("%02X", decipherTextMaterial->bytes[i]);
	}

	printf("\n");
#endif


	free(keyMaterial->bytes);
	free(keyMaterial);

	free(plainTextMaterial->bytes);
	free(plainTextMaterial);

	free(cipherTextMaterial->bytes);
	free(cipherTextMaterial);

	free(decipherTextMaterial->bytes);
	free(decipherTextMaterial);

	return 0;
}
