#include "serpent-cpu.h"
#include "measure-time.h"
#include <boost/chrono.hpp>
#include "serpent.h"
using CryptoPP::Serpent;
#include <modes.h>
using CryptoPP::ECB_Mode;



int serpentEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	CryptoPP::ECB_Mode< Serpent >::Encryption e;

	start = startStopwatch();
	e.SetKey(cipherKey, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);


	start = startStopwatch();
	e.ProcessData(ct, pt, plainTextLength);
	endStopwatch("Encryption", start);



    return 0;
}
int serpentDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;

	ECB_Mode< Serpent >::Decryption d;

	start = startStopwatch();
	d.SetKey(cipherKey, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);


	start = startStopwatch();
	d.ProcessData(pt, ct, plainTextLength);
	endStopwatch("Decryption", start);



    return 0;
}