#ifndef __SERPENT_CPU_H
#define __SERPENT_CPU_H
#include "common-types.h"


int serpentEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);
int serpentDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);



#endif /* __SERPENT_CPU_H */