
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "rijndael-cuda.cuh"
#include "rijndael-tables.cuh"
#include <stdio.h>
#include <stdlib.h>
#include "measure-time.h"
#include <boost/chrono.hpp>

__device__ inline G4 G4_mul(G4 x, G4 y){
	u32 e;
	e = (x.b1 ^  x.b0) & (y.b1 ^ y.b0);
	return{ (x.b0 & y.b0) ^ e, (x.b1 & y.b1) ^ e };
}


__device__ inline G4 G4_scl_N(G4 x){
	return{ x.b1 ^ x.b0, x.b0 };
}

__device__ inline G4 G4_scl_N2(G4 x){
	return{ x.b1, x.b1 ^ x.b0 };
}

__device__ inline G4 G4_sq(G4 x){
	return{ x.b1, x.b0 };
}

__device__ inline G4 G4_xor(G4 x, G4 y){
	return{ x.b0 ^ y.b0, x.b1 ^ y.b1 };
}

__device__ inline G16 G16_mul(G16 x, G16 y){
	G4 e;
	e = G4_scl_N(G4_mul(G4_xor(x.b1, x.b0), G4_xor(y.b1, y.b0)));
	
	return{ G4_xor(G4_mul(x.b0, y.b0), e), G4_xor(G4_mul(x.b1, y.b1), e) };
}

__device__ inline G16 G16_sq_scl(G16 x){
	return{ G4_scl_N2(G4_sq(x.b0)), G4_sq(G4_xor(x.b1, x.b0)) };
}

__device__ inline G16 G16_inv(G16 x){
	G4 e;
	e = G4_sq(G4_xor(G4_scl_N(G4_sq(G4_xor(x.b1, x.b0))), G4_mul(x.b1, x.b0)));
	return{ G4_mul(e, x.b1), G4_mul(e, x.b0) };

}

__device__ inline G16 G16_xor(G16 x, G16 y){
	return{ G4_xor(x.b0, y.b0), G4_xor(x.b1, y.b1) };
}

__device__ inline G256 G256_inv(G256 x){
	G16 e;
	e = G16_inv(G16_xor(G16_sq_scl(G16_xor(x.b1, x.b0)), G16_mul(x.b1, x.b0)));
	return{ G16_mul(e, x.b1), G16_mul(e, x.b0) };
}

__device__ inline G256 G256_xor(G256 x, G256 y){
	return{ G16_xor(x.b0, y.b0), G16_xor(x.b1, y.b1) };
}

__device__ inline G256 G256_newbasis_forward(G256 n){

	return{
			{
				{ 
					n.b1.b1.b0 ^ n.b0.b1.b1 ^ n.b0.b1.b0 ^ n.b0.b0.b1 ^ n.b0.b0.b0,
					n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b0.b0
				}, 
				{
					n.b0.b0.b0,
					n.b1.b1.b1 ^ n.b1.b0.b0 ^ n.b0.b1.b1  ^ n.b0.b0.b1 ^ n.b0.b0.b0 
				} 
			},
			{ 
				{ 
					n.b1.b1.b1 ^ n.b1.b1.b0 ^ n.b1.b0.b1  ^ n.b0.b0.b0,
					n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b0.b1  ^ n.b0.b0.b0
				}, 
				{ 
					n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b1.b0.b0  ^ n.b0.b0.b0,
					n.b1.b1.b1 ^n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b1.b0 ^ n.b0.b0.b1 ^ n.b0.b0.b0 
				} 
			}

		};
}

__device__ inline G256 G256_newbasis_inv_forward(G256 n){
	
	return{
		{
			{
				n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b1.b0.b0 ^ n.b0.b0.b1 ^ n.b0.b0.b0,
				n.b1.b0.b0 ^ n.b0.b1.b1 ^ n.b0.b0.b0
			},
			{
				n.b1.b1.b1 ^ n.b1.b0.b1 ^ n.b0.b1.b0,
				n.b1.b1.b1 ^ n.b1.b1.b0 ^ n.b1.b0.b0
			}
		},
		{
			{
				n.b1.b1.b0 ^ n.b0.b1.b1 ^ n.b0.b0.b1  ^ n.b0.b0.b0,
				n.b1.b1.b0 ^ n.b1.b0.b0
			},
			{
				n.b1.b1.b0 ^ n.b1.b0.b0 ^ n.b0.b0.b1  ^ n.b0.b0.b0,
				n.b1.b1.b1 ^n.b1.b0.b0
			}
		}

	};
}

__device__ inline G256 G256_newbasis_backward(G256 n){
	
	return{
		{
			{
				n.b1.b1.b0 ^ n.b1.b0.b0 ^ n.b0.b0.b1,
				n.b1.b0.b1 ^ n.b1.b0.b0 ^ n.b0.b0.b1
			},
			{
				n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b1.b1  ^ n.b0.b1.b0 ^ n.b0.b0.b0,
				n.b1.b1.b1 ^ n.b1.b1.b0 ^ n.b1.b0.b1  ^ n.b1.b0.b0 ^ n.b0.b1.b1
			}
		},
		{
			{
				n.b1.b1.b1 ^ n.b1.b0.b1 ^ n.b0.b1.b1,
				n.b1.b1.b0 ^ n.b0.b0.b0
			},
			{
				n.b1.b1.b1 ^ n.b0.b1.b1,
				n.b1.b0.b1 ^ n.b0.b1.b1
			}
		}

	};
}

__device__ inline G256 G256_newbasis_inv_backward(G256 n){

	return{
		{
			{
				n.b0.b1.b0,
				n.b1.b0.b1 ^ n.b0.b0.b1
			},
			{
				n.b1.b1.b1 ^ n.b1.b0.b1 ^ n.b1.b0.b0  ^ n.b0.b0.b1,
				n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b1.b0.b0  ^ n.b0.b1.b1 ^ n.b0.b1.b0 ^ n.b0.b0.b1
			}
		},
		{
			{
				n.b1.b1.b0 ^ n.b0.b0.b1,
				n.b1.b1.b1 ^ n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b1.b1 ^ n.b0.b1.b0 ^ n.b0.b0.b0
			},
			{
				n.b1.b1.b1 ^ n.b1.b1.b0 ^ n.b1.b0.b1 ^ n.b0.b1.b1 ^ n.b0.b0.b1 ^ n.b0.b0.b0,
				n.b1.b0.b0 ^ n.b0.b0.b1
			}
		}

	};


}

__device__ inline G256 Sbox(G256 n){
	G256 t;
	t = G256_newbasis_forward(n);
	t = G256_inv(t);
	t = G256_newbasis_backward(t);

	t.b1.b1.b0 = ~t.b1.b1.b0;
	t.b1.b0.b1 = ~t.b1.b0.b1;
	t.b0.b0.b1 = ~t.b0.b0.b1;
	t.b0.b0.b0 = ~t.b0.b0.b0;

	return t;
}

__device__ inline G256 iSbox(G256 n){
	G256 t;

	n.b1.b1.b0 = ~n.b1.b1.b0;
	n.b1.b0.b1 = ~n.b1.b0.b1;
	n.b0.b0.b1 = ~n.b0.b0.b1;
	n.b0.b0.b0 = ~n.b0.b0.b0;

	t = G256_newbasis_inv_forward(n);
	t = G256_inv(t);
	t = G256_newbasis_inv_backward(t);

	return t;
}

__device__ inline G256 xtime(G256 n){
	return{
		{
			{
				n.b1.b1.b1,
				n.b0.b0.b0 ^ n.b1.b1.b1
			},
			{
				n.b0.b0.b1,
				n.b0.b1.b0 ^ n.b1.b1.b1
			}
		},
		{
			{
				n.b0.b1.b1 ^ n.b1.b1.b1,
				n.b1.b0.b0
			},
			{
				n.b1.b0.b1,
				n.b1.b1.b0
			}
		}

	};
}

__device__ inline G256 gmul2(G256 n){
	return xtime(n);
}

__device__ inline G256 gmul3(G256 n){
	return G256_xor(xtime(n), n);
}

__device__ inline G256 gmul14(G256 n){
	G256 t1 = xtime(n);
	G256 t2 = xtime(t1);

	return G256_xor(G256_xor(xtime(t2), t2), t1);
}

__device__ inline G256 gmul11(G256 n){
	G256 t1 = xtime(n);
	
	return G256_xor(G256_xor(xtime(xtime(t1)), t1), n);
}

__device__ inline G256 gmul13(G256 n){
	G256 t1 = xtime(xtime(n));

	return G256_xor(G256_xor(xtime(t1), t1), n);
}

__device__ inline G256 gmul9(G256 n){

	return G256_xor(xtime(xtime(xtime(n))), n);
}

__device__ inline G256 keying(G256 n, u8 k){
	return{
		{
			{
				n.b0.b0.b0 ^ (-(k & 0x01)),
				n.b0.b0.b1 ^ (-((k >> 1) & 0x01))
			},
			{
				n.b0.b1.b0 ^ (-((k >> 2) & 0x01)),
				n.b0.b1.b1 ^ (-((k >> 3) & 0x01))
			}
		},
		{
			{
				n.b1.b0.b0 ^ (-((k >> 4) & 0x01)),
				n.b1.b0.b1 ^ (-((k >> 5) & 0x01))
			},
			{
				n.b1.b1.b0 ^ (-((k >> 6) & 0x01)),
				n.b1.b1.b1 ^ (-((k >> 7) & 0x01))
			}
		}

	};






}



__global__ void createCTRKernel(uint4 *out, const int bundles, const u32 n0, const u32 n1){

	u64 i = blockIdx.x * blockDim.x + threadIdx.x;

	if (i < bundles * BITS_PER_WORD){
		out[i] = { n0, n1, ((i & 0xffffffff00000000) >> 32), (i & 0xffffffff) };
		
	}
}

__global__ void xorCtr(const uint4 *pt, const uint4 *ctr , uint4 *ct, int bundles){

	int i = blockIdx.x * blockDim.x + threadIdx.x;


	if (i < bundles * BITS_PER_WORD){
	
		ct[i] = { pt[i].x ^ ctr[i].x, pt[i].y ^ ctr[i].y, pt[i].z ^ ctr[i].z, pt[i].w ^ ctr[i].w };
	

	}




}


__global__ void rijndaelEncryptBitwiseKernel(const G256 *in, G256 *out, u8 (*rk)[4][4], const int bundles ,const unsigned int Nr)
{


	int tx = threadIdx.x;
	int ty = threadIdx.y;

	int bx = blockIdx.x;
	int tz = threadIdx.z;

	int bundle = bx * blockDim.z + tz;

	if (bundle < bundles){
		int i = bundle * blockDim.x * blockDim.y + ty * blockDim.y + tx;

		int r;
		register u32 idx0X, idx1X, idx2X, idx3X;
		register u32 idx0Y, idx1Y, idx2Y, idx3Y;

		idx0X = MOD4(tx + 0);
		idx1X = MOD4(tx + 1);
		idx2X = MOD4(tx + 2);
		idx3X = MOD4(tx + 3);

		idx0Y = MOD4(tx + ty + 0);
		idx1Y = MOD4(tx + ty + 1);
		idx2Y = MOD4(tx + ty + 2);
		idx3Y = MOD4(tx + ty + 3);

		

		G256 n, s, u, w;

		__shared__ G256_p S_sh[8][WORDS_PER_BLOCK][BYTES_PER_WORD];

		/*First we read the data*/
		n = in[i];

		/*Then we do the keying, we have to take care for rk indexes and bit indexes*/
		n = keying(n, rk[0][ty][tx]);


		/*
		We have 16 threads so we copy the data first.
		We chosed 16 because every S box needs 8 bit data-> 128/8 = 16
		Other threads can do efficient work instead of wait for first 16 threads to
		calculate S boxes.

		*/
		
		for (r = 1; r < Nr; r ++){
			/*Next we do the S table (or S multiplied with 01, so it is called S1) on the 8 byte data*/


			n = Sbox(n);

			S_sh[tz][ty][tx].g256 = n;


			__syncthreads();

			/*Now we load additional data needed for calculating xors*/

			/*S2*/
			n = gmul2(S_sh[tz][idx0Y][idx0X].g256);

			/*S3*/
			s = gmul3(S_sh[tz][idx1Y][idx1X].g256);

			/*S*/
			u = S_sh[tz][idx2Y][idx2X].g256;

			/*S*/
			w = S_sh[tz][idx3Y][idx3X].g256;

			/*We do the xors*/
			n = G256_xor(G256_xor(G256_xor(w, u), s), n);

			/*keying*/
			n = keying(n, rk[r][ty][tx]);

		}

		/*apply last round*/


		n = Sbox(n);
		S_sh[tz][ty][tx].g256 = n;

		__syncthreads();


		n = S_sh[tz][idx0Y][idx0X].g256;
		n = keying(n, rk[r][ty][tx]);

		/*Save output*/
		out[i] = n;


	}
	
	
	
	


}




__global__ void transposeKernel(u8 *in, u8 *out, const int bundles)
{


	int tx = threadIdx.x;
	int ty = threadIdx.y;
	
	int by = blockIdx.y;
	int tz = threadIdx.z;

	int bundle = (by * blockDim.z + tz) * gridDim.z + blockIdx.z;

	if (bundle < bundles){
		register u32 i0, i1, i2, i3, i4, i5, i6, i7;
		register u32 o0, o1, o2, o3, o4, o5, o6, o7;

		in += bundle * (BLOCK_SIZE * BYTES_PER_WORD);
		out += bundle * (BLOCK_SIZE * BYTES_PER_WORD);

		int offset1, offset2;
		offset1 = tx * BLOCK_SIZE + ty;
		offset2 = ty * BITS_PER_WORD + tx;

		i0 = in[offset1 + 0 * BYTES_PER_BLOCK];
		i1 = in[offset1 + 1 * BYTES_PER_BLOCK];
		i2 = in[offset1 + 2 * BYTES_PER_BLOCK];
		i3 = in[offset1 + 3 * BYTES_PER_BLOCK];
		i4 = in[offset1 + 4 * BYTES_PER_BLOCK];
		i5 = in[offset1 + 5 * BYTES_PER_BLOCK];
		i6 = in[offset1 + 6 * BYTES_PER_BLOCK];
		i7 = in[offset1 + 7 * BYTES_PER_BLOCK];

		o0 = ((i0 & 0x01) << 0) | ((i1 & 0x01) << 1) | ((i2 & 0x01) << 2) | ((i3 & 0x01) << 3) | ((i4 & 0x01) << 4) | ((i5 & 0x01) << 5) | ((i6 & 0x01) << 6) | ((i7 & 0x01) << 7);
		o1 = ((i0 & 0x02) >> 1) | ((i1 & 0x02) << 0) | ((i2 & 0x02) << 1) | ((i3 & 0x02) << 2) | ((i4 & 0x02) << 3) | ((i5 & 0x02) << 4) | ((i6 & 0x02) << 5) | ((i7 & 0x02) << 6);
		o2 = ((i0 & 0x04) >> 2) | ((i1 & 0x04) >> 1) | ((i2 & 0x04) << 0) | ((i3 & 0x04) << 1) | ((i4 & 0x04) << 2) | ((i5 & 0x04) << 3) | ((i6 & 0x04) << 4) | ((i7 & 0x04) << 5);
		o3 = ((i0 & 0x08) >> 3) | ((i1 & 0x08) >> 2) | ((i2 & 0x08) >> 1) | ((i3 & 0x08) << 0) | ((i4 & 0x08) << 1) | ((i5 & 0x08) << 2) | ((i6 & 0x08) << 3) | ((i7 & 0x08) << 4);
		o4 = ((i0 & 0x10) >> 4) | ((i1 & 0x10) >> 3) | ((i2 & 0x10) >> 2) | ((i3 & 0x10) >> 1) | ((i4 & 0x10) << 0) | ((i5 & 0x10) << 1) | ((i6 & 0x10) << 2) | ((i7 & 0x10) << 3);
		o5 = ((i0 & 0x20) >> 5) | ((i1 & 0x20) >> 4) | ((i2 & 0x20) >> 3) | ((i3 & 0x20) >> 2) | ((i4 & 0x20) >> 1) | ((i5 & 0x20) << 0) | ((i6 & 0x20) << 1) | ((i7 & 0x20) << 2);
		o6 = ((i0 & 0x40) >> 6) | ((i1 & 0x40) >> 5) | ((i2 & 0x40) >> 4) | ((i3 & 0x40) >> 3) | ((i4 & 0x40) >> 2) | ((i5 & 0x40) >> 1) | ((i6 & 0x40) << 0) | ((i7 & 0x40) << 1);
		o7 = ((i0 & 0x80) >> 7) | ((i1 & 0x80) >> 6) | ((i2 & 0x80) >> 5) | ((i3 & 0x80) >> 4) | ((i4 & 0x80) >> 3) | ((i5 & 0x80) >> 2) | ((i6 & 0x80) >> 1) | ((i7 & 0x80) << 0);

		out[offset2 + 0 * BYTES_PER_WORD] = o0;
		out[offset2 + 1 * BYTES_PER_WORD] = o1;
		out[offset2 + 2 * BYTES_PER_WORD] = o2;
		out[offset2 + 3 * BYTES_PER_WORD] = o3;
		out[offset2 + 4 * BYTES_PER_WORD] = o4;
		out[offset2 + 5 * BYTES_PER_WORD] = o5;
		out[offset2 + 6 * BYTES_PER_WORD] = o6;
		out[offset2 + 7 * BYTES_PER_WORD] = o7;
	}

}

__global__ void transposeInvKernel(u8 *in, u8 *out, const int bundles)
{


	int tx = threadIdx.x;
	int ty = threadIdx.y;

	int bx = blockIdx.x;
	int tz = threadIdx.z;

	int bundle = (bx * blockDim.z + tz) * gridDim.z + blockIdx.z;

	if (bundle < bundles){

		register u32 i0, i1, i2, i3, i4, i5, i6, i7;
		register u32 o0, o1, o2, o3, o4, o5, o6, o7;

		in += bundle * (BLOCK_SIZE * BYTES_PER_WORD);
		out += bundle * (BLOCK_SIZE * BYTES_PER_WORD);

		int offset1, offset2;
		offset1 = tx * BITS_PER_WORD + ty;
		offset2 = ty * BLOCK_SIZE + tx;


		i0 = in[offset1 + 0 * BYTES_PER_WORD];
		i1 = in[offset1 + 1 * BYTES_PER_WORD];
		i2 = in[offset1 + 2 * BYTES_PER_WORD];
		i3 = in[offset1 + 3 * BYTES_PER_WORD];
		i4 = in[offset1 + 4 * BYTES_PER_WORD];
		i5 = in[offset1 + 5 * BYTES_PER_WORD];
		i6 = in[offset1 + 6 * BYTES_PER_WORD];
		i7 = in[offset1 + 7 * BYTES_PER_WORD];


		o0 = ((i0 & 0x01) << 0) | ((i1 & 0x01) << 1) | ((i2 & 0x01) << 2) | ((i3 & 0x01) << 3) | ((i4 & 0x01) << 4) | ((i5 & 0x01) << 5) | ((i6 & 0x01) << 6) | ((i7 & 0x01) << 7);
		o1 = ((i0 & 0x02) >> 1) | ((i1 & 0x02) << 0) | ((i2 & 0x02) << 1) | ((i3 & 0x02) << 2) | ((i4 & 0x02) << 3) | ((i5 & 0x02) << 4) | ((i6 & 0x02) << 5) | ((i7 & 0x02) << 6);
		o2 = ((i0 & 0x04) >> 2) | ((i1 & 0x04) >> 1) | ((i2 & 0x04) << 0) | ((i3 & 0x04) << 1) | ((i4 & 0x04) << 2) | ((i5 & 0x04) << 3) | ((i6 & 0x04) << 4) | ((i7 & 0x04) << 5);
		o3 = ((i0 & 0x08) >> 3) | ((i1 & 0x08) >> 2) | ((i2 & 0x08) >> 1) | ((i3 & 0x08) << 0) | ((i4 & 0x08) << 1) | ((i5 & 0x08) << 2) | ((i6 & 0x08) << 3) | ((i7 & 0x08) << 4);
		o4 = ((i0 & 0x10) >> 4) | ((i1 & 0x10) >> 3) | ((i2 & 0x10) >> 2) | ((i3 & 0x10) >> 1) | ((i4 & 0x10) << 0) | ((i5 & 0x10) << 1) | ((i6 & 0x10) << 2) | ((i7 & 0x10) << 3);
		o5 = ((i0 & 0x20) >> 5) | ((i1 & 0x20) >> 4) | ((i2 & 0x20) >> 3) | ((i3 & 0x20) >> 2) | ((i4 & 0x20) >> 1) | ((i5 & 0x20) << 0) | ((i6 & 0x20) << 1) | ((i7 & 0x20) << 2);
		o6 = ((i0 & 0x40) >> 6) | ((i1 & 0x40) >> 5) | ((i2 & 0x40) >> 4) | ((i3 & 0x40) >> 3) | ((i4 & 0x40) >> 2) | ((i5 & 0x40) >> 1) | ((i6 & 0x40) << 0) | ((i7 & 0x40) << 1);
		o7 = ((i0 & 0x80) >> 7) | ((i1 & 0x80) >> 6) | ((i2 & 0x80) >> 5) | ((i3 & 0x80) >> 4) | ((i4 & 0x80) >> 3) | ((i5 & 0x80) >> 2) | ((i6 & 0x80) >> 1) | ((i7 & 0x80) << 0);


		out[offset2 + 0 * BYTES_PER_BLOCK] = o0;
		out[offset2 + 1 * BYTES_PER_BLOCK] = o1;
		out[offset2 + 2 * BYTES_PER_BLOCK] = o2;
		out[offset2 + 3 * BYTES_PER_BLOCK] = o3;
		out[offset2 + 4 * BYTES_PER_BLOCK] = o4;
		out[offset2 + 5 * BYTES_PER_BLOCK] = o5;
		out[offset2 + 6 * BYTES_PER_BLOCK] = o6;
		out[offset2 + 7 * BYTES_PER_BLOCK] = o7;
	}

}



cudaError_t rijndaelEncryptBitwiseCuda(const uint4 pt[], uint4 ct[], const u32 rk[], const int blocks, const unsigned int Nr){


	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	int bundles;
	bundles = (blocks - 1)/ BITS_PER_WORD + 1;


	uint4 *devIn;
	uint4 *devOut;

	uint4 *devPt;
	uint4 *devCt;

	srand(0);
	u32 n0 = rand();
	u32 n1 = rand();
	ct[0] = { n0, n1, 0, 0 };


	u8  (*devRk)[4][4];
	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		return cudaStatus;
	}

	// cuda malloc
	start = startStopwatch();
	cudaStatus = cudaMalloc((void**)&devIn, bundles * sizeof(G256) * BYTES_PER_BLOCK);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMalloc((void**)&devOut, bundles * sizeof(G256) * BYTES_PER_BLOCK);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaHostGetDevicePointer((void **)&devPt, (void *)pt, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	cudaHostGetDevicePointer((void **)&devCt, (void *)ct, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMalloc((void**)&devRk, rkLength * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	endStopwatch("Malloc & GetDevicePointer (bitwise encryption)", start);

	devCt++;

	start = startStopwatch();

	cudaStatus = cudaMemcpy(devRk, rk, rkLength * sizeof(u32), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}


	totalTime = endStopwatch("Memory copy from host to device (bitwise encryption)", start);

	dim3 dimGridCreateCtr( (bundles - 1) / 8 + 1, 1, 1);
	dim3 dimBlockCreateCtr(THREAD_BLOCK_SIZE, 1, 1);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	createCTRKernel << <dimGridCreateCtr, dimBlockCreateCtr >> >(devIn, bundles, n0, n1);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Creating counter kernel (encryption)", start);



	dim3 dimGridTransposing(1, (bundles - 1) / 8 + 1, 4);
	dim3 dimBlockTransposing(BYTES_PER_WORD, BYTES_PER_BLOCK, 2);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	transposeKernel << <dimGridTransposing, dimBlockTransposing >> >((u8 *)devIn, (u8 *)devOut, bundles);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Transposing data kernel (encryption)", start);



	//!!!must be padded before!!!
	dim3 dimGridCrypto((bundles - 1)/ 8 + 1, 1, 1);
	dim3 dimBlockCrypto(WORDS_PER_BLOCK, BYTES_PER_WORD, 8);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	rijndaelEncryptBitwiseKernel << <dimGridCrypto, dimBlockCrypto >> >((G256 *)devOut, (G256 *)devIn, devRk, bundles ,Nr);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Encryption kernel", start);

	dim3 dimGridInverseTransposing((bundles - 1) / 8 + 1, 1, 4);
	dim3 dimBlockInverseTransposing(BYTES_PER_BLOCK, BYTES_PER_WORD, 2);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	transposeInvKernel << <dimGridInverseTransposing, dimBlockInverseTransposing >> >((u8 *)devIn, (u8 *)devOut, bundles);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Inverse transposing data kernel (encryption)", start);

	dim3 dimGridXorCTR((bundles - 1) / 8 + 1, 1, 1);
	dim3 dimBlockXorCTR(THREAD_BLOCK_SIZE, 1, 1);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	xorCtr << <dimGridXorCTR, dimBlockXorCTR >> >(devPt, devOut, devCt, bundles);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Xoring data kernel (encryption)", start);

	start = startStopwatch();
	cudaFree(devIn);
	cudaFree(devOut);
	cudaFree(devRk);

	endStopwatch("Freeing memory (bitwise encryption)", start);

	printDuration("Encryption", totalTime);

	return cudaStatus;
}

cudaError_t rijndaelDecryptBitwiseCuda(const uint4 ct[], uint4 pt[], const u32 rk[], const int blocks, const unsigned int Nr){


	boost::chrono::high_resolution_clock::time_point start;
	duration<double, boost::milli> totalTime;

	int bundles;
	bundles = (blocks - 1) / BITS_PER_WORD + 1;


	uint4 *devIn;
	uint4 *devOut;
	u8  (*devRk)[4][4];

	uint4 *devCt;
	uint4 *devPt;

	u32 n0 = ct[0].x;
	u32 n1 = ct[0].y;

	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		return cudaStatus;
	}

	// cuda malloc
	start = startStopwatch();
	cudaStatus = cudaMalloc((void**)&devIn, bundles * sizeof(G256) * BYTES_PER_BLOCK);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaStatus = cudaMalloc((void**)&devOut, bundles * sizeof(G256) * BYTES_PER_BLOCK);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}
	cudaStatus = cudaMalloc((void**)&devRk, rkLength * sizeof(u32));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		return cudaStatus;
	}

	cudaHostGetDevicePointer((void **)&devCt, (void *)ct, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}
	cudaHostGetDevicePointer((void **)&devPt, (void *)pt, 0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaHostGetDevicePointer failed!");
		return cudaStatus;
	}

	endStopwatch("Malloc & GetDevicePointer (bitwise decryption)", start);

	devCt++;

	start = startStopwatch();

	cudaStatus = cudaMemcpy(devRk, rk, rkLength * sizeof(u32), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		return cudaStatus;
	}


	totalTime = endStopwatch("Memory copy from host to device (bitwise decryption)", start);


	dim3 dimGridCreateCtr((bundles - 1) / 8 + 1, 1, 1);
	dim3 dimBlockCreateCtr(THREAD_BLOCK_SIZE, 1, 1);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	createCTRKernel << <dimGridCreateCtr, dimBlockCreateCtr >> >(devIn, bundles, n0, n1);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Creating counter kernel (decryption)", start);



	dim3 dimGridTransposing(1, (bundles - 1) / 8 + 1, 4);
	dim3 dimBlockTransposing(BYTES_PER_WORD, BYTES_PER_BLOCK, 2);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	transposeKernel << <dimGridTransposing, dimBlockTransposing >> >((u8 *)devIn, (u8 *)devOut, bundles);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Transposing data kernel (decryption)", start);



	//!!!must be padded before!!!
	dim3 dimGridCrypto((bundles - 1) / 8 + 1, 1, 1);
	dim3 dimBlockCrypto(WORDS_PER_BLOCK, BYTES_PER_WORD, 8);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	rijndaelEncryptBitwiseKernel << <dimGridCrypto, dimBlockCrypto >> >((G256 *)devOut, (G256 *)devIn, devRk, bundles, Nr);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Decryption kernel", start);

	dim3 dimGridInverseTransposing((bundles - 1) / 8 + 1, 1, 4);
	dim3 dimBlockInverseTransposing(BYTES_PER_BLOCK, BYTES_PER_WORD, 2);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	transposeInvKernel << <dimGridInverseTransposing, dimBlockInverseTransposing >> >((u8 *)devIn, (u8 *)devOut, bundles);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Inverse transposing data kernel (decryption)", start);


	dim3 dimGridXorCTR((bundles - 1) / 8 + 1, 1, 1);
	dim3 dimBlockXorCTR(THREAD_BLOCK_SIZE, 1, 1);

	// Launch a kernel on the GPU with one thread for each element.
	start = startStopwatch();
	xorCtr << <dimGridXorCTR, dimBlockXorCTR >> >(devCt, devOut, devPt, bundles);
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		return cudaStatus;
	}

	totalTime += endStopwatch("Xoring data kernel (decryption)", start);

	start = startStopwatch();
	cudaFree(devIn);
	cudaFree(devOut);
	cudaFree(devRk);

	totalTime += endStopwatch("Freeing memory (bitwise decryption)", start);

	printDuration("Decryption", totalTime);

	return cudaStatus;
}

int rijndaelKeySetupEnc(u32 rk[], const u32 cipherKey[], const unsigned int keyLen) {
	int i = 0;
	u32 temp;

	rk[0] = cipherKey[0];
	rk[1] = cipherKey[1];
	rk[2] = cipherKey[2];
	rk[3] = cipherKey[3];

	if (keyLen == 16) {
		for (;;) {
			temp = rk[3];
			rk[4] = rk[0] ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];

			rk[5] = rk[1] ^ rk[4];
			rk[6] = rk[2] ^ rk[5];
			rk[7] = rk[3] ^ rk[6];
			if (++i == 10) {
				return 10;
			}
			rk += 4;
		}
	}
	rk[4] = cipherKey[4];
	rk[5] = cipherKey[5];

	if (keyLen == 24) {
		for (;;) {
			temp = rk[5];
			rk[6] = rk[0] ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[7] = rk[1] ^ rk[6];
			rk[8] = rk[2] ^ rk[7];
			rk[9] = rk[3] ^ rk[8];
			if (++i == 8) {
				return 12;
			}
			rk[10] = rk[4] ^ rk[9];
			rk[11] = rk[5] ^ rk[10];
			rk += 6;
		}
	}
	rk[6] = cipherKey[6];
	rk[7] = cipherKey[7];
	if (keyLen == 32) {
		for (;;) {
			temp = rk[7];
			rk[8] = rk[0] ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x000000ff) ^
				rcon[i];
			rk[9] = rk[1] ^ rk[8];
			rk[10] = rk[2] ^ rk[9];
			rk[11] = rk[3] ^ rk[10];
			if (++i == 7) {
				return 14;
			}
			temp = rk[11];
			rk[12] = rk[4] ^
				(bigTe[4 * T_TABLE_SIZE + (temp >> 24)] & 0xff000000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 16) & 0xff)] & 0x00ff0000) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp >> 8) & 0xff)] & 0x0000ff00) ^
				(bigTe[4 * T_TABLE_SIZE + ((temp)& 0xff)] & 0x000000ff);
			rk[13] = rk[5] ^ rk[12];
			rk[14] = rk[6] ^ rk[13];
			rk[15] = rk[7] ^ rk[14];

			rk += 8;
		}
	}
	return 0;
}



int rijndaelEncrypt(uint4 *pt, uint4 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
{

	boost::chrono::high_resolution_clock::time_point start;

	
	u32 rk[rkLength];
	cudaSetDeviceFlags(cudaDeviceMapHost);
	cudaFree(NULL);

	uint4 *pinnedPt;
	uint4 *pinnedCt;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	cudaHostAlloc((void **)&pinnedPt, blocks * sizeof(uint4), cudaHostAllocWriteCombined | cudaHostAllocMapped);
	cudaHostAlloc((void **)&pinnedCt, (blocks + 1) * sizeof(uint4), cudaHostAllocDefault | cudaHostAllocMapped);

	memcpy(pinnedPt, pt, blocks * sizeof(uint4));

	start = startStopwatch();
	unsigned int Nr = rijndaelKeySetupEnc(rk, (u32 *)cipherKey, cipherKeyLength);
	endStopwatch("Encryption key calculation", start);
	
	
    cudaError_t cudaStatus = rijndaelEncryptBitwiseCuda(pinnedPt, pinnedCt, rk, blocks, Nr);
	
	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Encryption failed!");
        return 1;
    }

	memcpy(ct, pinnedCt, (blocks + 1) * sizeof(uint4));
	cudaFreeHost(pinnedPt);
	cudaFreeHost(pinnedCt);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }
	
    return 0;
}

int rijndaelDecrypt(uint4 *ct, uint4 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength)
{

	boost::chrono::high_resolution_clock::time_point start;

	
	u32 rk[rkLength];

	cudaSetDeviceFlags(cudaDeviceMapHost);
	cudaFree(NULL);
	
	uint4 * pinnedCt;
	uint4 *pinnedPt;

	int blocks = (plainTextLength - 1) / BYTES_PER_BLOCK + 1;

	cudaHostAlloc((void **)&pinnedCt, (blocks + 1) * sizeof(uint4), cudaHostAllocWriteCombined | cudaHostAllocMapped);
	cudaHostAlloc((void **)&pinnedPt, blocks * sizeof(uint4), cudaHostAllocDefault | cudaHostAllocMapped);

	memcpy(pinnedCt, ct, (blocks + 1) * sizeof(uint4));

	start = startStopwatch();
	unsigned int Nr = rijndaelKeySetupEnc(rk, (u32 *)cipherKey, cipherKeyLength);
	endStopwatch("Decryption key calculation", start);
		
    cudaError_t cudaStatus = rijndaelDecryptBitwiseCuda(pinnedCt, pinnedPt, rk, blocks, Nr);
	
	
	if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Decription failed!");
        return 1;
    }

	memcpy(pt, pinnedPt, blocks * sizeof(uint4));
	cudaFreeHost(pinnedCt);
	cudaFreeHost(pinnedPt);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }
	
    return 0;
}


