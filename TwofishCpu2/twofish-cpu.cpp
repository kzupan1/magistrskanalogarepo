#include "twofish-cpu.h"
#include "twofish-tables.h"
#include "measure-time.h"
#include <boost/chrono.hpp>


#define Q0_T0 0x4ace95b023f6d718
#define Q0_T1 0xd9076a4f53218bce
#define Q0_T2 0x17423f8c09d6e5ab
#define Q0_T3 0xac5803b9e6214f7d

#define Q1_T0 0x5ca04913e67fdb82
#define Q1_T1 0x809f5ad673c4b2e1
#define Q1_T2 0xf3b28de0a96157c4
#define Q1_T3 0xa802f746ed3c159b

#define T(M, N, n) (Q##M_T##N >> (n << 2)) & 0xf

inline u32 q0_t0(u32 n){
	return  (Q0_T0 >> (n << 2)) & 0xf;
}

inline u32 q0_t1(u32 n){
	return  (Q0_T1 >> (n << 2)) & 0xf;
}

inline u32 q0_t2(u32 n){
	return  (Q0_T2 >> (n << 2)) & 0xf;
}

inline u32 q0_t3(u32 n){
	return  (Q0_T3 >> (n << 2)) & 0xf;
}

inline u32 q1_t0(u32 n){
	return  (Q1_T0 >> (n << 2)) & 0xf;
}

inline u32 q1_t1(u32 n){
	return  (Q1_T1 >> (n << 2)) & 0xf;
}

inline u32 q1_t2(u32 n){
	return  (Q1_T2 >> (n << 2)) & 0xf;
}

inline u32 q1_t3(u32 n){
	return  (Q1_T3 >> (n << 2)) & 0xf;
}

inline u32 q0(u32 x){
	u32 a0, a1;
	u32 b0, b1;

	a0 = x >> 4;
	b0 = x & 0xf;
	
	a1 = a0 ^ b0;
	b1 = (a0 ^ ROR4(b0, 1) ^ (a0 << 3)) & 0xf;
	
	a0 = q0_t0(a1);
	b0 = q0_t1(b1);

	a1 = a0 ^ b0;
	b1 = (a0 ^ ROR4(b0, 1) ^ (a0 << 3)) & 0xf;
	
	a0 = q0_t2(a1);
	b0 = q0_t3(b1);
	
	return (b0 << 4) | a0;


}

inline u32 q1(u32 x){
	u32 a0, a1;
	u32 b0, b1;

	a0 = x >> 4;
	b0 = x & 0xf;

	a1 = a0 ^ b0;
	b1 = (a0 ^ ROR4(b0, 1) ^ (a0 << 3)) & 0xf;

	a0 = q1_t0(a1);
	b0 = q1_t1(b1);

	a1 = a0 ^ b0;
	b1 = (a0 ^ ROR4(b0, 1) ^ (a0 << 3)) & 0xf;

	a0 = q1_t2(a1);
	b0 = q1_t3(b1);

	return (b0 << 4) | a0;


}

#define	Fe32(sBox, x,R) (sBox[        2*_b(x, R  )    ] ^ \
				sBox[        2*_b(x, R+1) + 1] ^ \
				sBox[0x200 + 2*_b(x, R+2)    ] ^ \
				sBox[0x200 + 2*_b(x, R+3) + 1])

inline u32 f32(u32 x, const u8 *k8, u32 k64Cnt)
{
	u32 t0 =  x        & 0xff;
	u32 t1 = (x >>  8) & 0xff;
	u32 t2 = (x >> 16) & 0xff;
	u32 t3 = (x >> 24) & 0xff;

	switch (k64Cnt & 3)
	{
	case 0:		/* 256 bits of key */
		t0 = q1(t0) ^ k8[12];
		t1 = q0(t1) ^ k8[13];
		t2 = q0(t2) ^ k8[14];
		t3 = q1(t3) ^ k8[15];
		/* fall thru, having pre-processed b[0]..b[3] with k32[3] */
	case 3:		/* 192 bits of key */
		t0 = q1(t0) ^ k8[8];
		t1 = q1(t1) ^ k8[9];
		t2 = q0(t2) ^ k8[10];
		t3 = q0(t3) ^ k8[11];
		/* fall thru, having pre-processed b[0]..b[3] with k32[2] */
	case 2:		/* 128 bits of key */
		t0 = q1(q0(q0(t0) ^ k8[4]) ^ k8[0]);
		t1 = q0(q0(q1(t1) ^ k8[5]) ^ k8[1]);
		t2 = q1(q1(q0(t2) ^ k8[6]) ^ k8[2]);
		t3 = q0(q1(q1(t3) ^ k8[7]) ^ k8[3]);
	}

	/* Now perform the MDS matrix multiply inline. */
	return	((M00(t0) ^ M01(t1) ^ M02(t2) ^ M03(t3))) ^
			((M10(t0) ^ M11(t1) ^ M12(t2) ^ M13(t3)) << 8) ^
			((M20(t0) ^ M21(t1) ^ M22(t2) ^ M23(t3)) << 16) ^
			((M30(t0) ^ M31(t1) ^ M32(t2) ^ M33(t3)) << 24);
}

int twofishEncryptCpu(u32 *pt, u32 *ct, const u32 * sKey, const u8 * sBoxKey, const u32 k64Cnt){

	#define encrypt2Rounds()	t0 = f32(x0, sBoxKey, k64Cnt);\
								t1 = f32(ROL(x1, 8), sBoxKey, k64Cnt); \
								x2 ^= t0 + t1 + sKey[k++];\
								x2  = ROR(x2, 1);		  \
								x3  = ROL(x3, 1);		  \
								x3 ^= t0 + 2*t1 + sKey[k++];\
								t0 = f32(x2, sBoxKey, k64Cnt);\
								t1 = f32(ROL(x3, 8), sBoxKey, k64Cnt); \
								x0 ^= t0 + t1 + sKey[k++];\
								x0  = ROR(x0, 1) ;		\
								x1  = ROL(x1, 1);			\
								x1 ^= t0 + 2*t1 + sKey[k++];

	#define applyEncryptRounds() encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();\
								 encrypt2Rounds();



		
	register u32 x0, x1, x2, x3, t0, t1;
	register int k;

	x0 = pt[0];
	x1 = pt[1];
	x2 = pt[2];
	x3 = pt[3];

	x0 ^= sKey[INPUT_WHITEN    ];
	x1 ^= sKey[INPUT_WHITEN + 1];
	x2 ^= sKey[INPUT_WHITEN + 2];
	x3 ^= sKey[INPUT_WHITEN + 3];

	k = ROUND_SUBKEYS;
		
	applyEncryptRounds();

	x2 ^= sKey[OUTPUT_WHITEN    ];
	x3 ^= sKey[OUTPUT_WHITEN + 1];
	x0 ^= sKey[OUTPUT_WHITEN + 2];
	x1 ^= sKey[OUTPUT_WHITEN + 3];

	ct[0] = x2;
	ct[1] = x3;
	ct[2] = x0;
	ct[3] = x1;
	


	return 0;
}

int twofishDecryptCpu(u32 *ct, u32 *pt, const u32  *sKey, const u8 *sBoxKey, const u32 k64Cnt){

	#define decrypt2Rounds()	t0 = f32(x2, sBoxKey, k64Cnt);\
								t1 = f32(ROL(x3, 8), sBoxKey, k64Cnt);\
								x1 ^= t0 + 2*t1 + sKey[k--];\
								x1  = ROR(x1, 1);\
								x0  = ROL(x0, 1);\
								x0 ^= t0 + t1 + sKey[k--];\
								t0 = f32(x0, sBoxKey, k64Cnt);\
								t1 = f32(ROL(x1, 8), sBoxKey, k64Cnt);\
								x3 ^= t0 + 2*t1 + sKey[k--];\
								x3  = ROR(x3, 1);\
								x2  = ROL(x2, 1);\
								x2 ^= t0 + t1 + sKey[k--];
	
	#define applyDecryptRounds() decrypt2Rounds();\
								 decrypt2Rounds();\
								 decrypt2Rounds();\
								 decrypt2Rounds();\
								 decrypt2Rounds();\
								 decrypt2Rounds();\
								 decrypt2Rounds();\
								 decrypt2Rounds();


		
	register u32 x0, x1, x2, x3, t0, t1;
	register int k;

	x2 = ct[0];
	x3 = ct[1];
	x0 = ct[2];
	x1 = ct[3];

	x2 ^= sKey[OUTPUT_WHITEN    ];
	x3 ^= sKey[OUTPUT_WHITEN + 1];
	x0 ^= sKey[OUTPUT_WHITEN + 2];
	x1 ^= sKey[OUTPUT_WHITEN + 3];

	k = ROUND_SUBKEYS + 2*ROUNDS - 1;
		
	applyDecryptRounds();

	x0 ^= sKey[INPUT_WHITEN    ];
	x1 ^= sKey[INPUT_WHITEN + 1];
	x2 ^= sKey[INPUT_WHITEN + 2];
	x3 ^= sKey[INPUT_WHITEN + 3];
     
	pt[0] = x0;
	pt[1] = x1;
	pt[2] = x2;
	pt[3] = x3;

	return 0;
}




u32 RS_MDS_Encode(u32 k0, u32 k1)
	{
	int i,j;
	u32 r;

	for (i=r=0;i<2;i++)
		{
		r ^= (i) ? k0 : k1;			/* merge in 32 more key bits */
		for (j=0;j<4;j++)			/* shift one u8 at a time */
			RS_rem(r);
					

		}
	return r;
	}





int makeKey(const u32 cipherKey[], u32 subKeys[TOTAL_SUBKEYS], u32 sBoxKey[4], const u32 k64Cnt ){

	#define	F32(res,x,k32)	\
	{															\
	u32 t=x;													\
	switch (k64Cnt & 3)											\
			{														\
			case 0:  /* same as 4 */								\
						b0(t)   = p8(04)[b0(t)] ^ b0(k32[3]);		\
						b1(t)   = p8(14)[b1(t)] ^ b1(k32[3]);		\
						b2(t)   = p8(24)[b2(t)] ^ b2(k32[3]);		\
						b3(t)   = p8(34)[b3(t)] ^ b3(k32[3]);		\
					 /* fall thru, having pre-processed t */		\
			case 3:		b0(t)   = p8(03)[b0(t)] ^ b0(k32[2]);		\
						b1(t)   = p8(13)[b1(t)] ^ b1(k32[2]);		\
						b2(t)   = p8(23)[b2(t)] ^ b2(k32[2]);		\
						b3(t)   = p8(33)[b3(t)] ^ b3(k32[2]);		\
					 /* fall thru, having pre-processed t */		\
			case 2:	 /* 128-bit keys (optimize for this case) */	\
				res=	MDStab[0][p8(01)[p8(02)[b0(t)] ^ b0(k32[1])] ^ b0(k32[0])] ^	\
						MDStab[1][p8(11)[p8(12)[b1(t)] ^ b1(k32[1])] ^ b1(k32[0])] ^	\
						MDStab[2][p8(21)[p8(22)[b2(t)] ^ b2(k32[1])] ^ b2(k32[0])] ^	\
						MDStab[3][p8(31)[p8(32)[b3(t)] ^ b3(k32[1])] ^ b3(k32[0])] ;	\
			}														\
	}

    u32 subkeyCnt = TOTAL_SUBKEYS;
    u32 k32e[4]; // even 32-bit entities
    u32 k32o[4]; // odd 32-bit entities
    //u32 sBoxKey[4];

	u32 i, j, offset = 0;
	
	for (i = 0, j = k64Cnt-1; i < 4 && offset < k64Cnt * 2; i++, j--) {
		 k32e[i] = cipherKey[offset++];
         k32o[i] = cipherKey[offset++];
         sBoxKey[j] = RS_MDS_Encode(k32e[i], k32o[i]); // reverse order
	}


	// compute the round decryption subkeys for PHT. these same subkeys
    // will be used in encryption but will be applied in reverse order.
    u32 q, A, B;

    for (i = q = 0; i < subkeyCnt/2; i++, q += SK_STEP) {
        F32(A, q        , k32e); // A uses even key entities
        F32(B, q+SK_BUMP, k32o); // B uses odd  key entities
        B = B << 8 | ROR(B, 24);
        A += B;
        subKeys[2*i    ] = A;               // combine with a PHT
        A += B;
        subKeys[2*i + 1] = A << SK_ROTL | ROR(A, (32-SK_ROTL));
    }

	// fully expand the table for speed
	/*
	u32 k0 = sBoxKey[0];
    u32 k1 = sBoxKey[1];
    u32 k2 = sBoxKey[2];
    u32 k3 = sBoxKey[3];
    u32 b0, b1, b2, b3;
    
    for (i = 0; i < 256; i++) {
        b0 = b1 = b2 = b3 = i;
        switch (k64Cnt & 3) {
        case 0: // same as 4
			b0 = (p8(04)[b0]) ^ b0(k3);
			b1 = (p8(14)[b1]) ^ b1(k3);
			b2 = (p8(24)[b2]) ^ b2(k3);
			b3 = (p8(34)[b3]) ^ b3(k3);
        case 3:
			b0 = (p8(03)[b0]) ^ b0(k2);
			b1 = (p8(13)[b1]) ^ b1(k2);
			b2 = (p8(23)[b2]) ^ b2(k2);
			b3 = (p8(33)[b3]) ^ b3(k2);
        case 2: // 128-bit keys
			sBox[      2*i  ] = MDStab[0][(p8(01)[(p8(02)[b0]) ^ b0(k1)]) ^ b0(k0)];
			sBox[      2*i+1] = MDStab[1][(p8(11)[(p8(12)[b1]) ^ b1(k1)]) ^ b1(k0)];
			sBox[0x200+2*i  ] = MDStab[2][(p8(21)[(p8(22)[b2]) ^ b2(k1)]) ^ b2(k0)];
			sBox[0x200+2*i+1] = MDStab[3][(p8(31)[(p8(32)[b3]) ^ b3(k1)]) ^ b3(k0)];
        }
    }

	*/

	return 0;
}




int twofishEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){

	boost::chrono::high_resolution_clock::time_point start;


	u32 subKeys[TOTAL_SUBKEYS];
	u32 sBoxKey[4];
	u32 k64Cnt = cipherKeyLength / 8;


	start = startStopwatch();
	makeKey((u32*)cipherKey, subKeys, sBoxKey, k64Cnt);
	endStopwatch("Encryption key calculation", start);




	start = startStopwatch();
	for(int numBlocks = plainTextLength >> WORDS_PER_BLOCK; numBlocks > 0; numBlocks--, pt+=BYTES_PER_BLOCK, ct+=BYTES_PER_BLOCK){
		twofishEncryptCpu((u32 *)pt, (u32 *)ct, subKeys, (u8 *)sBoxKey, k64Cnt);
	}
	endStopwatch("Encryption", start);



    return 0;




}
int twofishDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength){
	
	boost::chrono::high_resolution_clock::time_point start;

	u32 subKeys[TOTAL_SUBKEYS];
	u32 sBoxKey[4];
	u32 k64Cnt = cipherKeyLength / 8;


	start = startStopwatch();
	makeKey((u32*)cipherKey, subKeys, sBoxKey, k64Cnt);
	endStopwatch("Decryption key calculation", start);
	
	start = startStopwatch();
	for(int numBlocks = plainTextLength >> WORDS_PER_BLOCK; numBlocks > 0; numBlocks--, pt+=BYTES_PER_BLOCK, ct+=BYTES_PER_BLOCK){
		twofishDecryptCpu((u32 *)ct, (u32 *)pt, subKeys, (u8 *)sBoxKey, k64Cnt);
	}
	endStopwatch("Decryption", start);



    return 0;

}