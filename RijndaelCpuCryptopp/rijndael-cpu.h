#ifndef __RIJNDAEL_CPU_H
#define __RIJNDAEL_CPU_H

#include "common-types.h"


int rijndaelEncrypt(u8 *pt, u8 *ct, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);
int rijndaelDecrypt(u8 *ct, u8 *pt, const u8 cipherKey[], const unsigned int plainTextLength, const unsigned int cipherKeyLength);

#endif /* __RIJNDAEL_CPU_H */